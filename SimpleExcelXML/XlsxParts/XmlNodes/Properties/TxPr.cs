﻿using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Charts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// Настройки какого-либо текстового объекта, связанные с цветом
	/// </summary>
	internal class TxPr
	{

		public BodyPr body { get; set; }
		public LstStyle style { get; set; }
		public DefRPr def { get; set; }
		string prefix;

		public static TxPr DefaultTxPr { get { return new TxPr(); } }

		public TxPr(string pref = "c", BodyPr body = null, LstStyle style = null, DefRPr def = null)
		{
			this.body = body ?? BodyPr.Default;
			this.style = style ?? LstStyle.Default;
			this.def = def ?? DefRPr.DefaultRpr;
			this.prefix = pref;
		}

		public TxPr(XElement xTxPr)
		{
			TryLoad(xTxPr);
		}

		public void TryLoad(XElement root)
		{
			var a = root.GetNamespaceOfPrefix("a");

			var xBody = root.GetBody();
			if (xBody.HasInfo())
				body = new BodyPr(xBody);

			var xStyle = root.GetStyle();
			if (xStyle.HasInfo())
				style = new LstStyle(xStyle);

			var p = root.GetP();
			if (!p.HasInfo())
				return;

			var xpPr = p.FirstPPR().Element(a + "defRPr");
			if (xpPr.HasInfo())
				def = new DefRPr("pPr", xpPr);
		}

		public void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("txPr", prefix);

			body.Save(doc);
			style.Save(doc);

			doc.OpenPrefElement("p", "a");
			{
				doc.OpenPrefElement("pPr", "a");
				def.Save(doc);
				doc.CloseElement();
			}
			doc.CloseElement()
				.CloseElement();
		}

	}
}
