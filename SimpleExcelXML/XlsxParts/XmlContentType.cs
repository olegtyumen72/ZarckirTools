﻿using SimpleExcelXML.Interfaces;
using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts
{
	/// <summary>
	/// Файл связки
	/// </summary>
	class XmlContentType : IXmlFile
	{

		public enum Parts { Workbook, Worksheets, Theme, Styles, SharedStr, Core, App, Drawing, Chart }
		enum Def { rels, xml }

		public string RelativePath { get; } = "";
		public string File { get; } = "[Content_Types].xml";
		public string AbsolutePath { get; set; }

		Dictionary<string, Parts> config;
		Dictionary<string, Def> defConf = new Dictionary<string, Def>()
		{
			{"application/vnd.openxmlformats-package.relationships+xml", Def.rels},
			{"application/xml", Def.xml }
		};
		List<string> xmlNodes = new List<string>()
		{
			 "Types",
			 "Default",
			 "Override"
		};
		List<string> xmlAtrs = new List<string>()
		{
			"xmlns",
			"Extension",
			"ContentType",
			"PartName"
		};
		Dictionary<Parts, string> partTypes = new Dictionary<Parts, string>()
		{
			{ Parts.Workbook, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml" },
			{ Parts.Worksheets, "application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml" },
			{ Parts.Theme, "application/vnd.openxmlformats-officedocument.theme+xml" },
			{ Parts.Styles, "application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml" },
			{ Parts.SharedStr, "application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml" },
			{ Parts.Core, "application/vnd.openxmlformats-package.core-properties+xml" },
			{ Parts.App, "application/vnd.openxmlformats-officedocument.extended-properties+xml" },
			{ Parts.Drawing, "application/vnd.openxmlformats-officedocument.drawing+xml" },
			{Parts.Chart, "application/vnd.openxmlformats-officedocument.drawingml.chart+xml" }
		};
		Dictionary<Def, string> defTypes = new Dictionary<Def, string>()
		{
			{ Def.rels, "application/vnd.openxmlformats-package.relationships+xml" },
			{ Def.xml, "application/xml" },
		};



		public XmlContentType(Dictionary<string, Parts> partsPath, string path)
		{
			config = partsPath;
			AbsolutePath = path;

		}

		public void Add(Parts part, string path)
		{
			if (config.ContainsKey(path))
				config[path] = part;
			else
				config.Add(path, part);
		}

		public void Save()
		{
			var dir = Path.Combine(AbsolutePath, RelativePath);
			dir.CheckDir();

			var file = Path.Combine(dir, File);
			//file.CheckTxtFile();

			using (var doc = new XmlWriterWrapper(file))
			{
				CreateDoc(doc);
				doc.Save();
			}
		}

		private void CreateDoc(XmlWriterWrapper doc)
		{
			XmlTools.DefaultDeclaration(doc);
			doc.OpenElement("Types", null, XmlTools.xa["contTs"]);

			foreach (var el in defConf)
			{
				XmlTools.NewDefault(doc, el.Value.ToString(), el.Key);
			}

			foreach (var el in config)
			{
				var key = el.Value.ToString();
				var part = el.Key.Replace("\\", "/");
				var contTp = partTypes[el.Value];

				if (part[0] != '/')
					part = part.Insert(0, "/");

				XmlTools.NewOverride(doc, part, contTp);
			}

			doc.CloseElement();
		}

		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}

		public string RelativeToFile()
		{
			return Path.Combine(RelativePath, File);
		}
	}
}
