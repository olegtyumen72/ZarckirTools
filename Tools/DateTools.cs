﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools
{
	public static class DateTools
	{
		public static int MonthDifference(this DateTime lValue, DateTime rValue)
		{
			return Math.Abs((lValue.Month - rValue.Month) + 12 * (lValue.Year - rValue.Year));
		}

	}
}
