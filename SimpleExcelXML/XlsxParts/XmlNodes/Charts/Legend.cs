﻿using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;
using SimpleExcelXML.XlsxParts.XmlNodes.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Charts
{
	internal class Legend: XmlPropBase
	{

		LegendPos legendPos;
		public Layout layout { get; set; }
		string overlay;
		public SpPr spPr { get; set; }
		public TxPr txPr { get; set; }
		string prefix;

		public Legend(string prefix = "c", string overlay = "0", LegendPos pos = LegendPos.b, Layout lay = null, SpPr spProp = null, TxPr txtProp = null)
		{
			this.prefix = prefix;
			legendPos = pos;
			layout = lay ?? Layout.DefaultLayout;
			spPr = spProp ?? SpPr.DefaultSpPr;
			txPr = txtProp ?? TxPr.DefaultTxPr;
			this.overlay = overlay;
		}

		public Legend(XElement xLegend) : base(xLegend)
		{
		}

		public override void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("legend", prefix)
				.OpenPrefElement(nameof(legendPos)).AddAtribute("val", legendPos.ToString()).CloseElement();

			layout.Save(doc);

			doc.OpenPrefElement(nameof(overlay), prefix).AddAtribute("val", overlay).CloseElement();

			spPr.Save(doc);

			txPr.Save(doc);

			doc.CloseElement();
		}

		public override void TryLoad(XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");

			var pos = root.Element(c + "legendPos");			
			var atrs = XDocumentTools.ParseAtrs(pos);
			legendPos = ParseLegendPos(ReadAtrValue(atrs, nameof(legendPos)));

			var xLay = root.Element(c + "layout");
			if (xLay.HasInfo())
				layout = new Layout(xLay);

			overlay = root.Element(c + "overlay")?.Attribute("val")?.Value;

			var xSpPr = root.GetSpPr();
			if (xSpPr.HasInfo())
				spPr = new SpPr(xSpPr);

			var xTxPr = root.GetTxPr();
			if (xTxPr.HasInfo())
				txPr = new TxPr(xTxPr);		
		}

		private LegendPos ParseLegendPos(string v)
		{
			var res = LegendPos.b;
			if (Enum.TryParse(v, out res))
				return res;

			return LegendPos.b;
		}
	}
}
