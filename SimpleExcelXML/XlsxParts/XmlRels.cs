﻿using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Interfaces;
using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts
{
	internal class Relatives : Dictionary<XmlRels.RelType, List<RelsData>>
	{

	}

	internal class RelsData
	{
		public string RelId;
		public string RelPath;
	}

	/// <summary>
	/// Класс хранит относительные пути до файлов и их тип
	/// </summary>
	internal class XmlRels : IXmlFile
	{
		/// <summary>
		/// Все используемые виды файлов
		/// </summary>
		public enum RelType { Miss, App, Core, Workbook, Styles, Theme, Worksheet, Chartsheet, SharedStrings, Chart, Drawing, ChartColorStyle, ChartStyle }
		/// <summary>
		/// Маркер на то, что главный это релс или нет
		/// </summary>
		public bool IsGlobal = false;

		/// <summary>
		/// Тип + xml_namespace
		/// </summary>
		private Dictionary<RelType, string> typeToRef = new Dictionary<RelType, string>()
		{
			{RelType.App, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties" },
			{RelType.Core, "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties" },
			{RelType.Workbook, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" },
			{RelType.Styles, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" },
			{RelType.Theme, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme" },
			{RelType.Worksheet, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet" },
			{RelType.SharedStrings, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings" },
			{RelType.Chart, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart" },
			{RelType.Drawing, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing" },
			{RelType.Chartsheet, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chartsheet" }
		};
		/// <summary>
		/// Example: { rId:3@target:docProps/app.xml , Type.App }
		/// </summary>

		///<summary>
		/// Файл - тип
		/// </summary>
		public Dictionary<IXmlVisibleFile, RelType> PathToType { get; private set; } = new Dictionary<IXmlVisibleFile, RelType>();
		/// <summary>
		/// Данные о файле
		/// </summary>
		public Relatives TypeToPath { get; private set; } = new Relatives();
		//ReportBook sheets;

		public string RelativePath { get; }
		public string File { get; } = ".rels";
		public string AbsolutePath { get; set; }

		/// <summary>
		/// Счётчик для rId - они должны быть уникальны внутри одного rels
		/// </summary>
		private int lastRId { get; set; } = 1;

		public XmlRels(string path, Dictionary<IXmlVisibleFile, RelType> pathType = null)
		{
			PathToType = pathType ?? new Dictionary<IXmlVisibleFile, RelType>();
			AbsolutePath = path;

			SetRidS(this.PathToType.Keys);
		}

		public XmlRels(Dictionary<IXmlVisibleFile, RelType> pathType, string path, bool isGlobal) : this(path, pathType)
		{
			RelativePath = isGlobal ? "_rels" : "xl\\_rels";
			File = isGlobal ? File : "workbook.xml.rels";
			IsGlobal = isGlobal;
		}

		public XmlRels(string path, string file, string relativ, Dictionary<IXmlVisibleFile, RelType> pathType = null) : this(path, pathType)
		{
			File = file;
			RelativePath = relativ;
		}

		public void TryLoad()
		{
			var file = AbsoluteToFile();
			var dir = Directory.GetDirectoryRoot(file);

			if (!System.IO.File.Exists(file))
			{
				return;
			}

			var doc = XDocument.Load(file);
			var root = doc.Root;
			var ns = root.GetDefaultNamespace();

			var relats = root.Elements(ns + "Relationship");

			foreach (var rel in relats)
			{

				var atr = XDocumentTools.ParseAtrs(rel);
				if (atr.Count <= 0)
					continue;

				var rId = atr["Id"];
				var type = ParseType(atr["Type"].Split('/').Last());
				var relPath = atr["Target"];

				if (!TypeToPath.ContainsKey(type))
					TypeToPath.Add(type, new List<RelsData>());

				TypeToPath[type].Add(new RelsData() { RelId = rId, RelPath = relPath.Replace("../", "") });
			}
		}

		/// <summary>
		/// Получение из xml файла непосредственного типа
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		private RelType ParseType(string type)
		{
			var rType = type.First().ToString().ToUpperInvariant() + type.Substring(1);
			RelType res = RelType.Miss;
			if (!Enum.TryParse(rType, out res))
			{
				if (type.Contains("drawi"))
				{
					res = RelType.Drawing;
				}
				else if (type.Contains("chartColorStyle"))
				{
					res = RelType.ChartColorStyle;
				}
				else if (type.Contains("chartStyle"))
				{
					res = RelType.ChartStyle;
				}
				else if (type.Contains("sheet") && type.Contains("char"))
				{
					res = RelType.Chartsheet;
				}
				else if (type.Contains("char"))
				{
					res = RelType.Chart;
				}
			}

			return res;
		}

		/// <summary>
		/// Присвоение файлу-ребёнку rId 
		/// </summary>
		/// <param name="files"></param>
		private void SetRidS(IEnumerable<IXmlVisibleFile> files)
		{
			foreach (var el in files)
			{
				SetRId(el);
			}
		}

		private void SetRId(IXmlVisibleFile file)
		{
			file.Id = $"rId{lastRId++}";
		}

		/// <summary>
		/// Добавление файла в рельсы
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="type"></param>
		public void Add(IXmlVisibleFile obj, RelType type)
		{
			SetRId(obj);
			PathToType.Add(obj, type);
		}

		public void Add(IXmlVisibleFile obj, RelType type, ReportSheet rs)
		{
			rs.rId = $"rId{lastRId}";
			Add(obj, type);
		}

		public void Save()
		{
			var dir = Path.Combine(AbsolutePath, RelativePath);
			dir.CheckDir();

			var file = Path.Combine(dir, File);
			//file.CheckTxtFile();


			using (var doc = new XmlWriterWrapper(file))
			{
				CreateDoc(doc);
				doc.Save();
			}
		}

		private void CreateDoc(XmlWriterWrapper doc)
		{
			XmlTools.DefaultDeclaration(doc);
			doc.OpenElement("Relationships", null, XmlTools.xa["relatsProp"]);

			foreach (var el in PathToType)
			{
				var type = el.Value;
				var path = el.Key.RelativeToFile().Replace("\\", "/");
				if (path[0] == '/')
					path = path.Replace("/", "");
				if (!IsGlobal && path.Contains("xl"))
					path = path.Replace("xl/", "");

				var typeVal = typeToRef[type];

				XmlTools.NewRelation(doc, $"{el.Key.Id}", path, typeVal);
			}

			doc.CloseElement();
		}

		/// <summary>
		/// Получение файла из рельсы 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="rId"></param>
		/// <returns></returns>
		internal T GetFile<T>(string rId)
		{
			foreach (var el in PathToType)
			{
				if (el.Key.Id == rId)
					return (T)el.Key;
			}

			return default(T);
		}

		internal List<T> GetFiles<T>(RelType type)
		{
			var res = new List<T>();

			foreach (var file in PathToType)
			{
				if (file.Value == type)
					res.Add((T)file.Key);
			}

			return res;
		}

		internal T GetFile<T>(RelType type)
		{
			if (!PathToType.ContainsValue(type))
				return default(T);

			foreach (var el in PathToType)
			{
				if (el.Value == type)
					return (T)el.Key;
			}

			return default(T);
		}


		internal string GetId(IXmlVisibleFile obj)
		{
			foreach (var el in PathToType)
			{
				if (el.Key == obj)
					return el.Key.Id;
			}

			throw new Exception("Miss element from relative");
		}

		private Dictionary<string, string> ParseConf(string key)
		{
			var props = key.Split('@');

			var res = new Dictionary<string, string>();
			foreach (var prop in props)
			{
				var tmp = prop.Split(':');
				res.Add(tmp[0], tmp[1]);
			}

			return res;
		}

		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}

		public string RelativeToFile()
		{
			return Path.Combine(RelativePath, File);
		}
	}
}
