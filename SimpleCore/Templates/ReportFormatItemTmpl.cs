﻿using SimpleCore.Objs;
using SimpleCore.Parts;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Templates
{
	public class ReportFormatItemTmpl<T, V> : IReportFormatItem<T, V>, ISimpleClone<ReportFormatItemTmpl<T, V>>
		where T : ICellRange
		where V : IConditionalFormat
	{
		public ReportFormatItemTmpl()
		{
			Range = (T)Activator.CreateInstance(typeof(T), new object[] { -1, -1, -1, -1 });
			ConditionalFormats = new List<V>();
		}

		public ReportFormatItemTmpl(T range, IEnumerable<V> formats = null) : this()
		{
			Range = range;
			if (formats != null)
				ConditionalFormats.AddRange(formats);
		}

		public ReportFormatItemTmpl(IReportFormatItem<ICellRange, IConditionalFormat> src)
		{
			var range = src.Range;
			var conditionals = src.ConditionalFormats;

			Range = (T)Activator.CreateInstance(typeof(T), new object[] { range.cell1_Col, range.cell1_Row, range.cell2_Col, range.cell2_Row });
			ConditionalFormats = new List<V>();
			if (conditionals != null)
				ConditionalFormats.AddRange(conditionals.Select((v) => (V)Activator.CreateInstance(typeof(V), new object[] { v })));

			Col = src.Col;
			Row = src.Row;
			NFormat = src.NFormat;
			FontFormat = src.FontFormat == null ? null : src.FontFormat.Clone();
			BordersAll = src.BordersAll;
			HorizontalAligmentCenter = src.HorizontalAligmentCenter;
			VerticalAligmentCenter = src.VerticalAligmentCenter;
			//AutoFit = src.AutoFit;
			HideColumns = src.HideColumns;
			HideRows = src.HideRows;
			//Range = src.Range;
			ColumnWidth = src.ColumnWidth;
			RowOrientation = src.RowOrientation;
			ColumnsOrientation = src.ColumnsOrientation;
			Wrap = src.Wrap;
			MergeArea = src.MergeArea;
			MergeColumns = src.MergeColumns;
			AllRows = src.AllRows;
			AllCols = src.AllCols;
			Background = src.Background;
			//ConditionalFormats = src.ConditionalFormats.Select((v) => v).ToList();
			RowHeight = src.RowHeight;
			IsSingle = src.IsSingle;
			//AutoFitCols = src.AutoFitCols;
			//MergeRows = src.MergeRows;

		}

		public int StyleId { get; set; } = -1;

		/// <summary>
		/// Столбец форматирования. Начиная с 1.
		/// </summary>
		public int Col { get; set; } = -1;

		/// <summary>
		/// Строка форматирования. . Начиная с 1.
		/// </summary>
		public int Row { get; set; } = -1;

		/// <summary>
		/// Формат числа.
		/// </summary>
		public string NFormat { get; set; } = "";

		/// <summary>
		/// Параметры шрифта.
		/// </summary>
		public SimpleFontFamily FontFormat { get; set; } = null;

		public bool BordersAll { get; set; } = false;

		public bool HorizontalAligmentCenter { get; set; } = false;

		public bool VerticalAligmentCenter { get; set; } = false;

		//public bool AutoFit {get;set;} = false;

		public bool HideColumns { get; set; } = false;

		public bool HideRows { get; set; } = false;

		public bool IsSingle { get; set; } = false;

		public T Range { get; set; } = default(T);

		public double ColumnWidth { get; set; } = -1;

		public double RowHeight { get; set; } = -1;

		public bool MergeArea { get; set; } = false;

		//public bool MergeRows {get;set;} = false;

		public bool MergeColumns { get; set; } = false;

		public int ColumnsOrientation { get; set; } = 0;

		public int RowOrientation { get; set; } = 0;

		public bool Wrap { get; set; } = false;

		public List<V> ConditionalFormats { get; set; } = new List<V>();

		/// <summary>
		/// Используется в связки с Row и Col для взятия всех ячеек начиная с 
		/// Cell[Col,Row] и до конца всех колонок
		/// </summary>
		public bool AllCols { get; set; } = false;

		/// <summary>
		/// Используется в связки с Row и Col для взятия всех ячеек начиная с 
		/// Cell[Col,Row] и до конца всех строчек
		/// </summary>
		public bool AllRows { get; set; } = false;

		/// <summary>
		/// Задаёт цвет ячеек в заданном range
		/// </summary>
		public Color Background { get; set; } = Color.Empty;

		///// <summary>
		///// Авто ширина колонок
		///// </summary>
		//public bool AutoFitCols;

		public virtual ReportFormatItemTmpl<T, V> Clone()
		{
			return new ReportFormatItemTmpl<T, V>(Range, ConditionalFormats)
			{
				Col = this.Col,
				Row = this.Row,
				NFormat = this.NFormat,
				FontFormat = this.FontFormat == null ? null : this.FontFormat.Clone(),
				BordersAll = this.BordersAll,
				HorizontalAligmentCenter = this.HorizontalAligmentCenter,
				VerticalAligmentCenter = this.VerticalAligmentCenter,
				//AutoFit = this.AutoFit,
				HideColumns = this.HideColumns,
				HideRows = this.HideRows,
				//Range = this.Range,
				ColumnWidth = this.ColumnWidth,
				RowOrientation = this.RowOrientation,
				ColumnsOrientation = this.ColumnsOrientation,
				Wrap = this.Wrap,
				MergeArea = this.MergeArea,
				MergeColumns = this.MergeColumns,
				AllRows = this.AllRows,
				AllCols = this.AllCols,
				Background = this.Background,
				//ConditionalFormats = this.ConditionalFormats.Select((v) => v).ToList(),
				RowHeight = this.RowHeight,
				IsSingle = this.IsSingle
				//AutoFitCols = this.AutoFitCols,
				//MergeRows = this.MergeRows,
			};
		}

		public virtual IReportFormatItem<ICellRange, IConditionalFormat> Convert()
		{
			return new ReportFormatItemObj(Range, this.ConditionalFormats.Select((v) => (IConditionalFormat)v))
			{
				Col = this.Col,
				Row = this.Row,
				NFormat = this.NFormat,
				FontFormat = this.FontFormat == null ? null : this.FontFormat.Clone(),
				BordersAll = this.BordersAll,
				HorizontalAligmentCenter = this.HorizontalAligmentCenter,
				VerticalAligmentCenter = this.VerticalAligmentCenter,
				//AutoFit = this.AutoFit,
				HideColumns = this.HideColumns,
				HideRows = this.HideRows,
				//Range = this.Range,
				ColumnWidth = this.ColumnWidth,
				RowOrientation = this.RowOrientation,
				ColumnsOrientation = this.ColumnsOrientation,
				Wrap = this.Wrap,
				MergeArea = this.MergeArea,
				MergeColumns = this.MergeColumns,
				AllRows = this.AllRows,
				AllCols = this.AllCols,
				Background = this.Background,
				//ConditionalFormats = ,
				RowHeight = this.RowHeight,
				IsSingle = this.IsSingle
				//AutoFitCols = this.AutoFitCols,
				//MergeRows = this.MergeRows,
			};
		}
	}
}

