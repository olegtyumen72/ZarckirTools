﻿using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Interfaces;
using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts
{

	/// <summary>
	/// Если что то не понятно - смотреть XmlWrokSHeet - там описано всё
	/// </summary>
	class XmlWorkBook : IXmlVisibleFile
	{		


		ReportBook rb;
		public string RelativePath { get; } = "xl\\";
		public string File { get; } = "workbook.xml";
		public string AbsolutePath { get; set; }
		public string Id { get; set ; } //= "rId1";

		public XmlWorkBook(ReportBook rbCur, string path)//, int id)
		{
			rb = rbCur;
			AbsolutePath = path;
			//Id = $"rId{id}";
		}

		public ReportBook TryLoad()
		{
			var dir = MainDir();
			var file = AbsoluteToFile();

			if (!System.IO.File.Exists(file))
				return null;

			var doc = XDocument.Load(file);
			var root = doc.Root;
			var ns = root.GetDefaultNamespace();
			var r = root.GetNamespaceOfPrefix("r");

			foreach(var sheet in root.Element(ns + "sheets").Elements(ns + "sheet"))
			{
				var name = sheet.Attribute("name").Value;
				var sheetId = sheet.Attribute("sheetId").Value.ToInt();
				var rid = sheet.Attribute(r + "id").Value;

				var rs = new ReportSheet(name)
				{
					Id = sheetId,
					rId = rid

				};

				rb.Add(rs);
			}

			return rb;

		}

		string MainDir()
		{
			return Path.Combine(AbsolutePath, RelativePath);
		}

		public void Save()
		{
			var dir = Path.Combine(AbsolutePath, RelativePath);
			dir.CheckDir();
			var file = Path.Combine(dir, File);
			//file.CheckTxtFile();

			using (var doc = new XmlWriterWrapper(file))
			{
				CreateDoc(doc);
				doc.Save();
			}
		}

		void CreateDoc(XmlWriterWrapper doc)
		{
			XmlTools.DefaultDeclaration(doc);

			doc.OpenElement("workbook", null, XmlTools.xa["spredMain"])
				.AddAtribute("r", XmlTools.xa["offRelat"], "xmlns")
				.AddAtribute("mc", XmlTools.xa["compab"], "xmlns")
				.AddAtribute("x15", XmlTools.xa["spredMain1"], "xmlns")
				.AddAtribute("Ignorable", "x15", "mc", XmlTools.xa["compab"])

				.OpenElement("fileVersion")
				.AddAtribute(XmlTools.xa["appN"], "xl")
				.AddAtribute(XmlTools.xa["lastEdi"], "0")
				.AddAtribute(XmlTools.xa["loweEdi"], "0")
				.CloseElement()

				.OpenElement("bookViews")

				.OpenElement("workbookView")
				.AddAtribute(XmlTools.xa["xwb"], "0")
				.AddAtribute(XmlTools.xa["ywb"], "0")
				.CloseElement()

				.CloseElement()

				.OpenElement("sheets");

			foreach (var sheet in rb)
			{
				doc.OpenElement("sheet")
					.AddAtribute(XmlTools.xa["n"], sheet.Name)
					.AddAtribute(XmlTools.xa["wsId"], sheet.Id.ToString())
					.AddAtribute("id", $"{sheet.rId.ToString()}", "r", XmlTools.xa["offRelat"])
					.CloseElement();				
			}

			doc.CloseElement();
		}

		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}

		public string RelativeToFile()
		{
			return Path.Combine(RelativePath, File);
		}
	}
}
