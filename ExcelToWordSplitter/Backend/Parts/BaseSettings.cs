﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Document.NET;

namespace ExcelToWordSplitter.Backend.Tools
{
	public class BaseSettings
	{	
		public bool BreakAcrossPhage { get; set; } = false;
		public Alignment Aligmen { get; set; } = Alignment.center;
		public VerticalAlignment VertAligment { get; set; } = VerticalAlignment.Center;
		public SimpleAutoFit Fit { get; set; } = SimpleAutoFit.Phage;
		public TableDesign Design { get; set; } = TableDesign.TableGrid;
		public int ColWidth { get; set; } = -1;
		public int RoundNumbers { get; set; } = -1;

	}
}
