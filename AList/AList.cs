﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Tools;
using Tools.Interface;

namespace AssociativeList
{

	public class SimpleHList : Dictionary<string, Dictionary<string, object>>
	{
	}


	public class HList
	{
		private List<Type> Lvls;
		private Dictionary<object, object> root = new Dictionary<object, object>();
		private Dictionary<object, object> current;

		public HList()
		{
			Lvls = null;
			current = null;
		}

		public HList(IEnumerable<Type> cols)
		{
			Lvls = cols.ToList();
		}

		public HList(IEnumerable<Type> cols, Dictionary<object, object> tree) : this(cols)
		{
			Lvls = cols.ToList();
			root = tree;
			current = root;
		}

		#region throw errors
		private void ThrowEmptyTree()
		{
			throw new Exception("Tree is empty");
		}

		private void ThrowMissKey(object key)
		{
			throw new Exception($"Miss key at root[{key}]");
		}

		private void ThrowMissMathTypes(int lvl = -1)
		{
			if (lvl == -1)
			{
				throw new Exception($"Miss match type");
			}
			else
			{
				throw new Exception($"Miss match types at lvl {lvl}");
			}
		}

		private void ThrowWrongLvl()
		{
			throw new Exception("Try get value from transport level");
		}
		#endregion

		// oleg -> 23 -> person = 5
		//public void Add(params object[] src)
		//{
		//	Add(src);
		//}

		public void Add(IEnumerable<object> objs)
		{
			if (Lvls == null || Lvls.Count <= 0)
			{
				Lvls = new List<Type>();
				foreach (var obj in objs)
				{
					Lvls.Add(obj.GetType());
				}
				Lvls[Lvls.Count - 1] = typeof(string);
			}


			var data = objs.ToArray();
			if (data.Length < Lvls.Count)
			{
				throw new Exception("Not enough parameters for Add");
			}

			current = root;
			for (int i = 0; i < data.Length - 1; i++)
			{
				var curData = CheckLvl(data, i);

				if (i == data.Length - 2)
				{
					var nextData = CheckLvl(data, i + 1);
					AddOrUpdate(curData, nextData);
				}
				else
				{
					AddOrUpdate(curData);
				}
			}
		}

		public void Remove(IEnumerable<object> objs)
		{
			current = root;
			for (int i = 0; i < objs.Count() - 2; i++)
			{
				current = (Dictionary<object, object>)current[objs.ElementAt(i)];
			}
			current.Remove(objs.ElementAt(objs.Count() - 2));
		}

		private void AddOrUpdate(object curData, object nextData = null)
		{
			if (nextData == null)
			{
				if (!current.ContainsKey(curData))
				{
					current.Add(curData, new Dictionary<object, object>());
				}

				current = (Dictionary<object, object>)current[curData];
			}
			else
			{
				if (!current.ContainsKey(curData))
				{
					current.Add(curData, nextData);
				}
				else
				{
					current[curData] = nextData;
				}
			}
		}

		public HList this[object key]
		{
			get
			{
				if (key.GetType().Name != Lvls.First().Name)
				{
					ThrowMissMathTypes();
				}

				if (!root.ContainsKey(key))
				{
					ThrowMissKey(key);
				}

				if (root == null)
				{
					ThrowEmptyTree();
				}

				if (!(root is Dictionary<object, object>))
				{
					return null;
				}

				var newObj = new HList(Lvls.GetRange(1, Lvls.Count - 1), (Dictionary<object, object>)root[key]);
				return newObj;
			}
			set
			{
				if (root.ContainsKey(key))
				{
					root[key] = value;
				}
				else
				{
					root.Add(key, value);
				}
			}
		}

		public T GetValue<T>(object key)
		{
			if (root.First().Value is Dictionary<object, object>)
			{
				ThrowWrongLvl();
			}

			var elem = GetFromRoot(key);
			if (!(elem is T))
			{
				ThrowMissMathTypes();
			}

			return (T)elem;
		}

		private object GetFromRoot(object key)
		{
			if (root.ContainsKey(key))
			{
				return root[key];
			}
			else
			{
				foreach (var el in root)
				{
					if (key.ToString() == el.Key.ToString())
					{
						return el.Value;
					}
				}
			}

			return null;
		}

		private bool CheckCurrent(object val)
		{
			if (current.Count <= 0)
			{
				return false;
			}
			else if (current.First().Key.GetType().Name == val.GetType().Name)
			{
				return current.ContainsKey(val);
			}
			else
			{
				foreach (var el in current)
				{
					if (val.ToString() == el.Key.ToString())
					{
						return true;
					}
				}
				return false;
			}
		}

		public bool ContainsPath(IEnumerable<object> keys)
		{
			current = root;
			var array = keys.ToList();
			for (int i = 0; i < array.Count; i++)
			{
				var key = array[i];
				if (!CheckCurrent(key))
				{
					return false;
				}
				//if (!current.ContainsKey(key)) return false;
				var isLast = IsLastLvl(key);
				if (isLast && i != array.Count - 1)
				{
					return false;
				}

				if (!isLast)
				{
					current = (Dictionary<object, object>)current[key];
				}
			}
			return true;
		}
		public bool ContainsKey(object key)
		{
			return current.ContainsKey(key);
		}

		private bool IsLastLvl(object key)
		{
			if (current.ContainsKey(key))
			{
				return !(current[key] is Dictionary<object, object>);
			}
			else
			{
				return !(current.Any((v) => v.Value is Dictionary<object, object>));
			}
		}

		private object CheckLvl(object[] data, int i)
		{
			var curData = data[i];
			var curType = Lvls[i];
			if (curData == null)
			{
				curData = AList.DefaultByColType(curType);
			}

			if (curData.GetType().Name != curType.Name)
			{
				curData = TryChangeType(curData, curType, i);
			}

			return curData;
		}

		private object TryChangeType(object val, Type t, int i)
		{
			try
			{
				return Convert.ChangeType(val, t);
			}
			catch
			{
				throw new Exception($"Miss match types at lvl {i + 1}");
			}
		}

		public int GetDeep()
		{
			return Lvls.Count;
		}
	}

	public static class AListTools
	{
		public static T ValueAt<T>(this IList list, int indx)
		{
			return (T)Convert.ChangeType(list[indx], typeof(T));
		}

		public static T ValueOrDefault<T>(this AList src, int indx, string col)
		{

			if (src != null && src.CountRows > indx)
			{
				return (T)src[col][indx];
			}
			else
			{
				return AList.DefaultByColType<T>();
			}
		}
	}

	public class ARow : Dictionary<string, object>
	{

	}

	public class AList<T> : Dictionary<string, List<T>>
	{

	}
	// v["well"][0]
	//AList v;
	//["Well"] => [...]
	//["Name"] => [...]
	//for (int i=0;i < v.CountRow; i++){
	//	foreach(var col in v){
	//		
	//}
	// }
	public class AList : Dictionary<string, IList>, ILoadable
	{
		// [Well] => ["w1" , "w2"] 
		// aList["Well"] 
		// aList.RowCount 
		// aList["Well"][0]


		public int CountRows
		{
			get { return this.First().Value.Count; }
		}

		public AList()
		{

		}

		public AList(IDataReader dr)
		{
			Load(dr);
		}

		public AList(string[] cols, Type type)
		{

			foreach (var col in cols)
			{
				Add(col, SystemTools.GenerateDynamicTypeList(type) as IList);
			}
		}

		public AList(string[] cols, Type[] types)
		{
			if (cols.Length != types.Length)
			{
				throw new Exception("Count cols and types not eqal");
			}

			for (int i = 0; i < cols.Length; i++)
			{
				Add(cols[i], SystemTools.GenerateDynamicTypeList(types[i]) as IList);
			}
		}

		public AList(AList src)
		{
			foreach (var el in src)
			{
				Add(el.Key, SystemTools.CloneDinamic<IList>(el.Value));
			}
		}

		public void SafeAdd<T>(string col, T data)
		{
			if (!ContainsKey(col))
			{
				Add(col, SystemTools.GenerateDynamicTypeList(data.GetType()) as IList);
			}
			if (this[col] == null)
			{
				this[col] = SystemTools.GenerateDynamicTypeList(data.GetType()) as IList;
			}
			this[col].Add(data);
		}

		public void Fill<T>(T data)
		{
			foreach (var el in this)
			{
				AddOrChange(data, el.Key, 0);
			}
		}

		public void AddOrChange<T>(T data, string col, int indx)
		{
			if (this[col].Count > indx)
			{
				this[col].Insert(indx, data);
			}
			else
			{
				this[col].Add(data);
			}
		}

		public int KeyIndex(string key)
		{
			var keys = Keys.ToList();
			for (int i = 0; i < Keys.Count; i++)
			{
				if (key == keys[i])
				{
					return i;
				}
			}
			return -1;
		}

		public void Add(ARow aRow)
		{
			foreach (var el in aRow)
			{
				this[el.Key].Add(el.Value);
			}
		}

		//public void Add(string key, object val)
		//{
		//	this[key].Add(val);
		//}

		internal int ValueIndex(string v)
		{
			for (int i = 0; i < Count; i++)
			{
				if (this.ElementAt(i).Value.Contains(v))
				{
					return i;
				}
			}
			return -1;
		}

		private IList InitList(Type t)
		{
			var qwe = SystemTools.GenerateDynamicTypeList(t);
			return qwe;
		}

		public void Load(IDataReader dr)
		{
			//bool reader = false;
			for (int i = 0; i < dr.FieldCount; i++)
			{
				Type t = dr.GetFieldType(i);
				IList list = null;
				//if (t.Name.ToLower() == "object")
				//{
				//	if (!dr.Read()) throw new Exception("Alert!");
				//	var val = dr.GetValue(i);
				//	DbTools.ParseSqliteUnknow(val);
				//	reader = true;
				//}
				//else
				{
					list = SystemTools.GenerateDynamicTypeList(t);
					Add(dr.GetName(i), list as IList);
				}

			}



			while (dr.Read())
			{

				for (int i = 0; i < Keys.Count; i++)
				{
					if (dr.IsDBNull(i))
					{
						//continue;
						switch (dr.GetFieldType(i).Name.ToLower())
						{

							case "int":
							case "int32":
							case "double":
							case "float":
								{
									this[Keys.ElementAt(i)].Add(null);
									break;
								}
							case "string":
								{
									this[Keys.ElementAt(i)].Add("");
									break;
								}
							case "object":
							case "datetime":
								{
									this[Keys.ElementAt(i)].Add(null);
									break;
								}
							default:
								{
									this[Keys.ElementAt(i)].Add(-1);
									break;
								}
						}

					}
					else
					{
						switch (dr.GetFieldType(i).Name.ToLower())
						{
							//case "string":
							//	{
							//		var str = dr.GetString(i);
							//		var datetime = dr.GetString(i).TryParse<DateTime>(DateTime.TryParse);
							//		if(datetime == default(DateTime))
							//		{
							//			this[Keys.ElementAt(i)].Add(str);
							//		}
							//		else
							//		{
							//			this[Keys.ElementAt(i)].Add(datetime);
							//		}
							//		break;
							//	}
							//case "datetime":
							//	{
							//		this[Keys.ElementAt(i)].Add(DateTime.Parse(dr.GetString(i)));
							//		break;
							//	}
							default:
								{
									this[Keys.ElementAt(i)].Add(Convert.ChangeType(dr.GetValue(i), dr.GetFieldType(i)));
									break;
								}
						}
					}
				}
			}
			dr.Close();
		}

		public AList DbSelect<T>(string col, Func<T, bool> act)
		{
			var list = this[col];
			var select = Init(this);

			for (int i = 0; i < list.Count; i++)
			{
				var res = act((T)list[i]);
				if (res)
				{
					select.Add(this, i);
				}
			}
			return select;
		}

		public AList DbSelectParallel<T>(string col, Func<T, bool> act)
		{
			var list = this[col];
			var select = Init(this);

			Parallel.For(0, list.Count, (v) =>
			{
				var res = act((T)list[v]);
				if (res)
				{
					lock (select)
					{
						select.Add(this, v);
					}
				}
			});

			return select;
		}

		public AList DbSelect(Func<AList, bool> func)
		{
			var select = Init(this);

			//parallel?
			//Parallel.For(0, CountRows, (i) =>
			//{
			//	AList row = null;
			//	lock (this)
			//	{
			//		row = GetRow(i);
			//	}

			//	var res = func(row);
			//	if (res)
			//	{
			//		lock (this)
			//		{
			//			select.Add(this, i);
			//		}
			//	}
			//});

			//one thread
			for (int i = 0; i < this.CountRows; i++)
			{
				var row = GetRow(i);
				var res = func(row);
				if (res)
				{
					select.Add(this, i);
				}
			}

			return select;
		}

		private AList GetRow(int i)
		{
			var res = new AList(this)
			{
				{ this, i }
			};
			return res;
		}

		public AList RemoveEmpty()
		{
			for (int i = 0; i < CountRows; i++)
			{
				if (HasEmpty(this, i))
				{
					RemoveRow(this, i);
					i--;
				}
			}

			return this;
		}

		public void RemoveRow(AList src, int row)
		{
			foreach (var el in src)
			{
				el.Value.RemoveAt(row);
			}
		}

		public bool HasEmpty(AList src, int row)
		{
			foreach (var el in src)
			{
				if (el.Value[row] == null || IsEmpty(el.Value[row].GetType(), el.Value[row]))
				{
					return true;
				}
			}

			return false;
		}

		private bool IsEmpty(Type t, object value)
		{
			switch (t.Name.ToLower())
			{
				default:
					{
						return value == null;
					}
				case "int":
				case "int32":
				case "double":
				case "float":
					{
						return value == null;
					}
				case "string":
					{
						return ((string)value).Length <= 0;
					}

			}
		}

		public bool ContainsRow(AList aList, int row)
		{
			for (int i = 0; i < this.First().Value.Count; i++)
			{
				var goodRow = true;
				foreach (var el in aList)
				{
					if (this[el.Key][i].ToString() != aList[el.Key][row].ToString())
					{
						goodRow = false;
						break;
					}
				}

				if (goodRow)
				{
					return true;
				}
			}

			return false;
		}

		public AList Distinct()
		{
			var res = new AList(this);
			for (int i = 0; i < this.First().Value.Count; i++)
			{
				if (!res.ContainsRow(this, i))
				{
					res.Add(this, i);
				}
			}

			return res;
		}

		public void Add(AList alist)
		{
			for (int row = 0; row < alist.CountRows; row++)
			{
				foreach (var el in alist)
				{
					this[el.Key].Add(el.Value[row]);
				}
			}
		}

		public void Add(AList aList, int row)
		{
			foreach (var el in aList)
			{
				this[el.Key].Add(el.Value[row]);
			}
		}

		public int FindxIndKey(string key)
		{
			var keys = Keys.ToList();
			for (int i = 0; i < Keys.Count; i++)
			{
				if (key == keys[i])
				{
					return i;
				}
			}
			return -1;
		}

		public ARow RowAt(int i)
		{
			var res = new ARow();
			foreach (var el in this)
			{
				res.Add(el.Key, el.Value[i]);
			}

			return res;
		}

		public bool DbContains<T>(T value, params string[] cols)
			where T : IComparable
		{
			for (int i = 0; i < CountRows; i++)
			{
				foreach (var el in cols)
				{
					if (value.CompareTo(this[el][i]) == 0)
					{
						return true;
					}
				}
			}

			return false;
		}

		#region Interfaces

		public void LoadFrom(IDataReader dr)
		{
			Load(dr);
		}

		#endregion

		#region static

		public static AList Create()
		{
			return new AList();
		}

		private static AList Init(AList aList)
		{
			var res = new AList();
			foreach (var el in aList)
			{
				res.Add(el.Key, SystemTools.CloneDinamic<IList>(el.Value));
			}
			return res;
		}

		public static T DefaultByColType<T>()
		{
			switch (typeof(T).Name.ToLower())
			{
				case "int":
				case "int32":
				case "int16":
				case "float":
				case "double":
					{
						return (T)Convert.ChangeType(-1, typeof(T));
					}
				default:
					{
						return default;
					}
			}
		}

		public static object DefaultByColType(Type t)
		{
			switch (t.Name.ToLower())
			{
				case "int":
				case "int32":
				case "int16":
				case "float":
				case "double":
					{
						return Convert.ChangeType(-1, t);
					}
				default:
					{
						return null;
					}
			}
		}

		#endregion
	}
}