﻿using SimpleCore.Parts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCsv.Parts
{
	[Serializable]
	public class ReportLine : List<object>, IReportLine, ISimpleConvert<IReportLine>
	{

		/// <summary>
		/// Пустой конструктор.
		/// </summary>
		public ReportLine()
		{
		}

		/// <summary>
		/// Конструктор по коллекции.
		/// </summary>
		/// <param name="items">Элементы коллекции.</param>
		public ReportLine(IEnumerable<object> items)
		{
			AddRange(items);
		}

		public IReportLine Convert()
		{
			return this;
		}

		/// <summary>
		/// Создание строки с одной ячейкой.
		/// </summary>
		/// <param name="value">Значение ячейки.</param>
		/// <returns>Строку таблицы с одной ячейкой.</returns>
		public static implicit operator ReportLine(string value)
		{
			return new ReportLine() { value };
		}

	}
}
