﻿using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Parts
{
	public interface IReportFormatItem<T,V> : ISimpleConvert<IReportFormatItem<ICellRange, IConditionalFormat>>
		where T: ICellRange
		where V: IConditionalFormat
	{
		/// <summary>
		/// Столбец форматирования. Начиная с 1.
		/// </summary>
		int Col { get; set; }

		/// <summary>
		/// Строка форматирования. . Начиная с 1.
		/// </summary>
		int Row { get; set; }

		/// <summary>
		/// Формат числа.
		/// </summary>
		string NFormat { get; set; }

		/// <summary>
		/// Параметры шрифта.
		/// </summary>
		SimpleFontFamily FontFormat { get; set; }

		bool BordersAll { get; set; }

		bool IsSingle { get; set; }

		bool HorizontalAligmentCenter { get; set; }

		bool VerticalAligmentCenter { get; set; }

		// bool AutoFit = false;

		bool HideColumns { get; set; }

		bool HideRows { get; set; }

		T Range { get; set; }

		double ColumnWidth { get; set; }

		double RowHeight { get; set; }

		bool MergeArea { get; set; }

		// bool MergeRows = false;

		bool MergeColumns { get; set; }

		int ColumnsOrientation { get; set; }

		int RowOrientation { get; set; }

		bool Wrap { get; set; }

		List<V> ConditionalFormats { get; set; }

		/// <summary>
		/// Используется в связки с Row и Col для взятия всех ячеек начиная с 
		/// Cell[Col,Row] и до конца всех колонок
		/// </summary>
		bool AllCols { get; set; }

		/// <summary>
		/// Используется в связки с Row и Col для взятия всех ячеек начиная с 
		/// Cell[Col,Row] и до конца всех строчек
		/// </summary>
		bool AllRows { get; set; }

		/// <summary>
		/// Задаёт цвет ячеек в заданном range
		/// </summary>
		Color Background { get; set; }

		///// <summary>
		///// Авто ширина колонок
		///// </summary>
		// bool AutoFitCols;


	}
}
