﻿using SimpleCore.Objs;
using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Templates
{
	public class ReportSheetTmpl<T, CellRange, ConditionalFormat, FormatItem, Chart, ChartItem, Translater> : List<T>, ISimpleConvert<IReportSheet>, ICoreReportSheet
		where T : ISimpleConvert<IReportLine>
		where ChartItem : IReportChartItem
		where Chart : IReportChart<ChartItem>, ISimpleConvert<IReportChart<IReportChartItem>>
		where CellRange : ICellRange
		where ConditionalFormat : IConditionalFormat
		where FormatItem : IReportFormatItem<CellRange, ConditionalFormat>
		where Translater : ITranslater
	{
		public string Name { get; set; }
		public object Tag { get; set; }
		/// <summary>
		/// Фон ячеек
		/// </summary>
		public Color CellsBg { get; set; } = Color.Empty;
		/// <summary>
		/// Цвет шрифта ячеек
		/// </summary>
		public Color CellsFont { get; set; } = Color.Empty;
		/// <summary>
		/// Авто-выбор ширины колонки для всех столбцов
		/// </summary>
		public bool AutoFit { get; set; } = false;
		public List<FormatItem> LocalFormats { get; set; }
		public List<CellRange> LocalCellsTr { get; set; }
		public List<Chart> LocalGrapth { get; set; }
		public Translater LocalTranslater { get; set; }

		List<IReportFormatItem<ICellRange, IConditionalFormat>> ICoreReportSheet.Formats
		{
			get
			{
				return LocalFormats.Select((v) => v.Convert()).ToList();
			}
			set
			{
				throw new NotImplementedException();
			}
		}
		List<ICellRange> ICoreReportSheet.cells_tr
		{
			get
			{
				return LocalCellsTr.Select((v) => (ICellRange)v).ToList();
			}
			set
			{
				LocalCellsTr = value.ConvertAll((v) => (CellRange)Activator.CreateInstance(typeof(CellRange), new object[] { v.cell1_Col, v.cell1_Row, v.cell2_Col, v.cell2_Row }));
			}

		}
		List<IReportChart<IReportChartItem>> ICoreReportSheet.Graphs
		{
			get
			{
				return LocalGrapth.ConvertAll((v) => v.Convert());
			}
			set
			{
				throw new NotImplementedException();
			}
		}
		ITranslater ICoreReportSheet.Translater { get { return (ITranslater)LocalTranslater; } set { throw new NotImplementedException(); } }

		public IReportSheet Convert()
		{
			return new ReportSheetObj(
				Name,
				CellsBg,
				CellsFont,
				Tag,
				ConvertAll((v) => v.Convert()),
				LocalFormats?.Select((v) => v.Convert()).ToList(),
				LocalCellsTr.ConvertAll((v) => v.Clone()),
				LocalTranslater,
				AutoFit
				)
			{
				Graphs = LocalGrapth.ConvertAll((v) => v.Convert())
			};
		}

		public ReportSheetTmpl(string name, string tag = null)
		{
			Name = name;
			Tag = tag;
			LocalCellsTr = new List<CellRange>();
			LocalFormats = new List<FormatItem>();
			LocalGrapth = new List<Chart>();
		}

		public ReportSheetTmpl(IReportSheet rs)
		{
			Name = rs.Name;
			Tag = rs.Tag;
			LocalTranslater = (Translater)rs.Translater;
			LocalCellsTr = rs.cells_tr.ConvertAll((v) => (CellRange)Activator.CreateInstance(typeof(CellRange), new object[] { v }));
			LocalFormats = rs.Formats.Select((v) => (FormatItem)Activator.CreateInstance(typeof(FormatItem), new object[] { v })).ToList();
			LocalGrapth = rs.Graphs.Select((v) => (Chart)Activator.CreateInstance(typeof(Chart), new object[] { v })).ToList();
			CellsBg = rs.CellsBg;
			CellsFont = rs.CellsFont;
			AutoFit = rs.AutoFit;

			foreach (var el in rs)
			{
				Add((T)Activator.CreateInstance(typeof(T), el));
				//Add(new ReportLineTmpl(el));
			}
		}
	}
}
