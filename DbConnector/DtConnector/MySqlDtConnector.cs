﻿using DbConnector.Core;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbConnector.DtConnector
{
	public class MySqlDtConnector : IDbDtConnector
	{

		private readonly MySqlConnection _con;
		private readonly Action<string> _logMsg;
		private readonly Action<DataTable> _logData;

		public MySqlDtConnector(string connstring, Action<string> logMsg = null, Action<DataTable> logData = null)
		{
			_con = new MySqlConnection(connstring);
			_logMsg = logMsg;
			_logData = logData;
			_con.Open();
		}

		public SqlVers SqlVersion
		{
			get
			{
				return SqlVers.MySql;
			}
		}

		public bool CngConDb(string name)
		{
			throw new NotImplementedException();
		}

		public bool CreateDb(string name)
		{
			throw new NotImplementedException();
		}

		public bool CreateTable(string table, List<string> col, List<string> etc)
		{
			throw new NotImplementedException();
		}

		public bool DeleteQuery(string table, string where)
		{
			throw new NotImplementedException();
		}

		public bool DeleteTable(string table)
		{
			throw new NotImplementedException();
		}

		public void Dispose()
		{
			throw new NotImplementedException();
		}

		public bool DropDb(string name)
		{
			throw new NotImplementedException();
		}

		public DataTable GetDictionary(string table)
		{
			throw new NotImplementedException();
		}

		public V GetKeyFromDb<V>(string tbl, string col, string matchCol, string matchVal)
		{
			throw new NotImplementedException();
		}

		public V GetKeyFromDbW<V>(string tbl, string col, string where)
		{
			throw new NotImplementedException();
		}

		public V GetKeyFromDic<V>(string table, string col, string targer)
		{
			throw new NotImplementedException();
		}

		public V GetLast<V>(string table, string col)
		{
			throw new NotImplementedException();
		}

		public bool InsertQuery(string table, string col, string values)
		{
			throw new NotImplementedException();
		}

		public bool IsEmpty(DataTable data, string col = "")
		{
			throw new NotImplementedException();
		}

		public DataTable Query(string query)
		{
			throw new NotImplementedException();
		}

		public ISqlPartData<DataTable> SelectPartial(string table, string col, int offset, int count, string colForOrder = "", string where = "", bool isLazy = true)
		{
			throw new NotImplementedException();
		}

		public DataTable SelectQuery(string table, string col, string where = "", string etc = "")
		{
			throw new NotImplementedException();
		}

		public bool TruncateTable(string table)
		{
			throw new NotImplementedException();
		}

		public bool UpdateWithDuplicate(string table, string col, string value)
		{
			throw new NotImplementedException();
		}
	}
}
