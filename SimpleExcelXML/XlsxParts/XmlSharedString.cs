﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.Interfaces;
using SimpleExcelXML.Tools;

namespace SimpleExcelXML.XlsxParts
{
	/// <summary>
	/// Коллекция всех текстовых данных в экселе по принципу: Id - Text
	/// </summary>
	internal class XmlSharedString : IXmlVisibleFile
	{
		private static Regex isSpecialChar = new Regex(@"[\W]");
		/// <summary>
		/// Глобальный счётчик для ID текстовых данных
		/// </summary>
		private int lastId = 0;
		private Dictionary<string, int> values = new Dictionary<string, int>();

		public string Id { get; set; }
		public string RelativePath { get; } = "xl\\";
		public string File { get; } = "sharedStrings.xml";
		public string AbsolutePath { get; set; }

		public XmlSharedString(string path)
		{
			AbsolutePath = path;
			//Id = $"rId{id}";
		}

		internal void TryLoad()
		{
			var file = AbsoluteToFile();
			//var dir = Directory.GetDirectoryRoot(file);

			if (!System.IO.File.Exists(file))
			{
				return;
			}

			var doc = XDocument.Load(file);
			var root = doc.Root;
			var ns = root.GetDefaultNamespace();

			var elems = root.Elements(ns + "si")?.ToList();
			if (elems == null || elems.Count <= 0)
				return;

			var vals = new List<string>();
			foreach(var elem in elems)
			{
				var child = elem.Elements().First();
				if(child.Name.LocalName == "t")
				{
					vals.Add(child.Value);
				}
				else if(child.Name.LocalName == "r")
				{
					vals.Add(child.LazyElement("t").Value);
				}
			}

			vals.ForEach((v) => AddUnique(v, false));
		}

		/// <summary>
		/// Делает проверку на то, является ли обьект текстовыми данными
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public bool CanAdd(object obj)
		{
			var tmp = -1d;
			var strObj = obj.ToString();
			if (
				!String.IsNullOrEmpty(obj.ToString().Trim()) &&
				!double.TryParse(strObj, NumberStyles.Any, CultureInfo.InvariantCulture, //CultureInfo.GetCultureInfo("en-US"), 
				out tmp)
				//(obj.GetType().Name.ToLower() == "string" &&
				//strObj.ToUpper().Any((v) => XmlTools.alphabet.Contains(v))) ||
				//!double.TryParse(strObj, out tmp) ||
				//(isSpecialChar.IsMatch(strObj) || strObj.Contains('_'))
				)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Добавляет уникальный текстовые данные
		/// </summary>
		/// <param name="val"></param>
		/// <param name="isChecked"></param>
		/// <returns></returns>
		public int AddUnique(object val, bool isChecked = false)
		{
			if (isChecked || CanAdd(val))
			{
				var valStr = val.ToString();
				if (!values.ContainsKey(valStr))
				{
					values.Add(valStr, lastId++);
				}
			}
			//else
			//{
			//	throw new Exception("Miss type for value in sharedString");
			//}

			return GetUnique(val);
		}

		/// <summary>
		/// По данным получает id обьекта, либо -1
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		private int GetUnique(object val)
		{
			var valStr = val.ToString();
			if (values.ContainsKey(valStr))
			{
				return values[valStr];
			}
			else
			{
				return -1;
			}
		}

		/// <summary>
		/// По ID получает данные
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		internal object GetValue(int index)
		{
			if (values.ContainsValue(index))
			{
				return values.FirstOrDefault((v) => v.Value == index).Key;
			}
			else
			{
				return null;
			}
		}

		public void Save()
		{
			var dir = Path.Combine(AbsolutePath, RelativePath);
			dir.CheckDir();

			var file = Path.Combine(dir, File);
			//file.CheckTxtFile();

			using (var doc = new XmlWriterWrapper(file))
			{
				CreateDoc(doc);
				doc.Save();
			}
		}

		private void CreateDoc(XmlWriterWrapper doc)
		{
			XmlTools.DefaultDeclaration(doc);
			doc.OpenElement("sst", null, XmlTools.xa["spredMain"]);

			foreach (var val in values)
			{
				doc.OpenElement("si").OpenElement("t", val.Key).CloseElement().CloseElement();
			}

			doc.CloseElement();
		}

		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}		

		public string RelativeToFile()
		{
			return Path.Combine(RelativePath, File);
		}
	}
}
