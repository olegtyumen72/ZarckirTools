﻿using ExcelToWordSplitter.Backend.Parts.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Document.NET;

namespace ExcelToWordSplitter.Backend.Tools
{
	/// <summary>
	/// Описывает область (StartCol;StartRow) : (EndCol;EndRow)
	/// НУмерация колонок и строк с 1
	/// </summary>
	public struct TableRange
	{
		public int StartCol { get; set; }
		public int StartRow { get; set; }
		public int EndCol { get; set; }
		public int EndRow { get; set; }
		/// <summary>
		/// Описание линий таблицы для каждой области
		/// Описывается отдельно левая, правая, верхняя и тд линии
		/// </summary>
		public Dictionary<TableCellBorderType, Border> Grid { get; set; }

		public TableRange(int startCol, int startRow, int endCol, int endRow, Dictionary<TableCellBorderType, Border> grid = null)
		{
			StartCol = startCol;
			StartRow = startRow;
			EndCol = endCol;
			EndRow = endRow;
			Grid = grid;
		}

		public bool isEmpty()
		{
			return StartCol < 0 || StartRow < 0 || EndCol < 0 || EndRow < 0;
		}

		public TableRange Clone()
		{
			return (TableRange)MemberwiseClone();
		}

		public int RowCount()
		{
			return 1 + EndRow - StartRow;
		}

		public int ColCount()
		{
			return 1 + EndCol - StartCol;
		}

		public int CellCount()
		{
			return RowCount() * ColCount();
		}

		public override string ToString()
		{
			return $"({StartCol}:{StartRow};{EndCol}:{EndRow})";
		}
	}
}
