﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Tools
{
	internal static class SimpleCoreBoost
	{

		public static Color Copy(this Color value)
		{
			if (value.IsEmpty)
				return Color.Empty;
			if (value.IsKnownColor)
				return Color.FromKnownColor(value.ToKnownColor());
			if (value.IsNamedColor)
				return Color.FromName(value.Name);
			else
				return Color.FromArgb(value.A, value.R, value.G, value.B);
		}

		public static List<T> Copy<T>(this List<T> src)
		{
			var res = new List<T>();

			foreach (var el in src)
				res.Add(el);

			return res;
		}

		public static Rectangle Copy(this Rectangle src)
		{
			return new Rectangle(src.X, src.Y, src.Width, src.Height);
		}

		public static Bitmap Copy(this Bitmap src)
		{
			return (Bitmap)src?.Clone();
		}
	}
}
