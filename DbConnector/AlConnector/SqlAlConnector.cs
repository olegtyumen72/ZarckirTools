﻿using AssociativeList;
using DbConnector.Core;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace DbConnector.AlConnector
{
	public class SqlAlConnector : IDbAlConnector
	{
		private readonly SqlConnection _con;
		private readonly Action<string> _logMsg;
		private readonly Action<AList> _logData;

		public SqlVers SqlVersion
		{
			get
			{
				return SqlVers.MsSql;
			}
		}

		public SqlAlConnector(string connstring, Action<string> logMsg = null, Action<AList> logData = null)
		{
			_con = new SqlConnection(connstring);
			_logMsg = logMsg;
			_logData = logData;
			_con.Open();
		}

		public SqlAlConnector(string serverUrl, string port, string db, string login, string pas = "", bool isWindowAuth = false, Action<string> logMsg = null, Action<AList> logData = null)
		{
			//"Data Source=MSI-GL62M;Initial Catalog=PD_TEST2;Integrated Security=True"
			var con = $"Data source = {serverUrl}";
			con += port.Length > 0 ? $",{port};" : ";";
			con += $"initial catalog={db};Integrated security={isWindowAuth};";
			if (!isWindowAuth)
			{
				con += $"user id={login};";
				con += pas.Length > 0 ? $"password = {pas};" : "";
			}
			con += "Connection Timeout=30;";

			_con = new SqlConnection(con);
			_logMsg = logMsg;
			_logData = logData;
			_con.Open();
		}


		public void Dispose()
		{
			_con.Close();
		}

		public AList GetDictionary(string table)
		{
			var query = $"Select * from {table}";
			return Query(query);
		}

		public V GetLast<V>(string table, string col)
		{
			col = DbTools.AddMsSqlBrackets(col);
			var res = SelectQuery(table, col, null, $"Order by {col} DESC LIMIT 1");
			return res.First().Value.Cast<V>().FirstOrDefault();
		}

		public bool InsertQuery(string table, string col, string values)
		{
			var query = $"insert into {table}({col}) VALUES {values}";
			return GetRes(Query(query));
		}

		private bool GetRes(AList associativeList)
		{
			return associativeList.FirstOrDefault().Value.Cast<bool>().FirstOrDefault();
		}

		public AList Query(string query)
		{
			_logMsg?.Invoke(query);
			using (var cmd = _con.CreateCommand())
			{
				cmd.CommandTimeout = 360;
				cmd.CommandText = query;
				try
				{
					using (var reader = cmd.ExecuteReader())
					{
						//if (reader.HasRows)
						//{
						var table = new AList();
						table.Load(reader);
						if (table.Count <= 0)
						{
							var res = GetBoolRes(true);
							_logData?.Invoke(res);
							return res;

						}
						else
						{
							_logData?.Invoke(table);
							return table;
						}
						//}
						//else
						//{
						//	return GetBoolRes(true);
						//}
					}
				}
				catch
				{
					var res = GetBoolRes(false);
					_logData?.Invoke(res);

					return res;
				}
			}
		}

		public AList SelectQuery(string table, string col, string where = "", string etc = "")
		{
			var query = $"Select {col} from {table}";
			if (where?.Length > 0)
			{
				query += $" WHERE {where}";
			}
			if (etc?.Length > 0)
			{
				query += $" {etc}";
			}

			return Query(query);
		}

		public bool UpdateWithDuplicate(string table, string col, string value)
		{


			var cols = col.Split(',');
			var colVal = new string[cols.Length];
			for (int i = 0; i < cols.Length; i++)
			{
				colVal[i] = $"{cols[i]}=VALUES({cols[i]})";
			}

			var query = $"insert into {table}({col}) VALUES {value} ON DUPLICATE KEY UPDATE {String.Join(",", colVal)}";

			return GetRes(Query(query));
		}

		public V GetKeyFromDb<V>(string tbl, string col, string matchCol, string matchVal)
		{
			return GetKeyFromDbW<V>(tbl, col, matchCol + " = '" + matchVal + "'");
		}

		public V GetKeyFromDic<V>(string table, string col, string target)
		{
			var tmp = DbTools.RemoveMsSqlBracets(col);
			col = DbTools.AddMsSqlBrackets(col);
			var data = SelectQuery(table, col, "[Name_eng] = '" + target + "'");
			data = IsEmpty(data, tmp) ? SelectQuery(table, col, "[Name_rus] = '" + target + "'") : data;

			if (IsEmpty(data, tmp))
			{
				return AList.DefaultByColType<V>();
			}
			else
			{
				return (V)Convert.ChangeType(data[tmp][0], typeof(V));
			}
		}

		/// <summary>
		/// Получить первое значение по колонке с условием where
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="table"></param>
		/// <param name="col"></param>
		/// <param name="where"></param>
		/// <returns></returns>
		public V GetKeyFromDbW<V>(string table, string col, string where)
		{
			col = DbTools.AddMsSqlBrackets(col);
			var data = SelectQuery(table, col, where);
			col = DbTools.RemoveMsSqlBracets(col);

			if (IsEmpty(data, col))
			{
				return AList.DefaultByColType<V>();
			}
			else
			{
				return (V)Convert.ChangeType(data[col][0], typeof(V));
			}
		}

		public bool DeleteQuery(string table, string where)
		{
			string query = $"DELETE FROM {table} WHERE {where}";
			return GetRes(Query(query));
		}

		private AList GetBoolRes(bool v)
		{
			var tmp = new AList
			{
				{ "0", new List<bool>() { v } }
			};
			return tmp;
		}

		//bool IsEmpty(AList data, string col)
		//{
		//	if (data.ContainsKey("0"))
		//	{
		//		return true;
		//	}
		//	return data[col] == null || data[col].Count <= 0;
		//}

		public bool IsEmpty(AList data, string col = "")
		{
			if (data == null) return true;
			col = col.Length <= 0 ? data.First().Key : col;
			return
				data == null ||
				data.Values.First() == null ||
				data.Values.First().Count <= 0 ||
				(data.Values.First().Count == 1 && data.Values.First()[0].GetType().Name.ToLower() == "boolean" && data.Keys.First() == "0");
		}

		public bool CreateTable(string table, List<string> col, List<string> etc)
		{
			var query = $"CREATE TABLE {table}(";

			query += string.Join(",", col);
			query += string.Join("", etc);

			return GetRes(Query(query));
		}

		public bool DeleteTable(string table)
		{
			var query = $"DROP TABLE {table}";

			return GetRes(Query(query));
		}

		public bool TruncateTable(string table)
		{
			var query = $"TRUNCATE TABLE {table}";

			return GetRes(Query(query));
		}

		public ISqlPartData<AList> SelectPartial(string table, string col, int offset, int count, string colForOrder = "", string where = "", bool isLazy = true)
		{
			throw new NotImplementedException();
		}

		public bool CreateDb(string name)
		{
			throw new NotImplementedException();
		}

		public bool DropDb(string name)
		{
			throw new NotImplementedException();
		}

		public bool CngConDb(string name)
		{
			throw new NotImplementedException();
		}
	}
}
