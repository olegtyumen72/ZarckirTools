﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleCore.Parts;
using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Tools;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Sheets
{
	internal class Col
	{
		public static int MinWidth = 16;
		public static int MaxCol = 16384;

		public double Width { get; set; }
		public int Max { get; set; }
		public int Min { get; set; }
		public int StyleId { get; set; }
		public bool IsAddable { get { return Max != -1 && Min != -1; } }

		public Col(int style, int max, int min, double width):this(style)
		{
			Width = width;
			Max = max;
			Min = min;
		}

		public Col(int style)
		{
			Width = MinWidth;
			Max = -1;
			Min = -1;
			StyleId = style;
		}

		public Col(ReportFormatItem p, int style) : this(style)
		{
			if (p.AllCols && p.Col > 0)
			{
				Min = p.Col;
				Max = MaxCol;
			}
			else if (p.Range != null && p.Range.cell1_Col > 0 && p.Range.cell2_Col > 0)
			{
				Min = p.Range.cell1_Col;
				Max = p.Range.cell2_Col;
			}
			else
			{
				return;
			}

			if (p.ColumnWidth > 0)
			{
				Width = p.ColumnWidth;
			}
			else if (p.HideColumns)
			{
				Width = 4;
			}
			else
			{
				Width = 9;
			}
		}


		public void Save(XmlWriterWrapper doc)
		{
			if (Max < 0 || Min < 0)
			{
				throw new Exception("Miss formated col");
			}

			if (Width == -1)
			{
				return;
			}

			doc.OpenElement("col");
			{
				doc.AddAtribute("min", Min);
				doc.AddAtribute("max", Max);
				doc.AddAtribute("style", StyleId);
				doc.AddAtribute("width", Width.ToString().Replace(',','.'));
				doc.AddAtribute("customWidth", "1");
			}
			doc.CloseElement();
		}

		public Col Clone()
		{
			return new Col(StyleId)
			{
				Width = this.Width,
				Max = this.Max,
				Min = this.Min
			};
		}
	}
}
