﻿using SimpleCore.Parts;
using SimpleCore.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.ExcelParts
{
	public class ReportSheet : ReportSheetTmpl<ReportLine, CellRangeTmpl, ConditionalFormatTmpl, ReportFormatItem, ReportChart, ReportChartItem, Translater>
	{
		internal string rId = null;
		internal int Id = -1;				

		public ReportSheet(IReportSheet rs) : base(rs)
		{
		}

		public ReportSheet(string name, string tag = null) : base(name, tag)
		{
		}
	}
}
