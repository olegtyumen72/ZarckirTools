﻿using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Tools
{
	public static class InterfacesTools
	{

		public static int FindIndex(this IReportBook rb, Func<IReportSheet, bool> selector)
		{
			for (int i = 0; i < rb.Count; i++)
			{
				if (selector(rb[i])) return i;
			}

			return -1;
		}


	}
}
