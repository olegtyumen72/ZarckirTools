﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTools.Views
{
	/// <summary>
	/// Interaction logic for NamedCombobox.xaml
	/// </summary>
	public partial class NamedCombobox : UserControl //, INotifyPropertyChanged
	{
		public static DependencyProperty TitleDP = DependencyProperty.Register(nameof(Title), typeof(string), typeof(NamedCombobox), new FrameworkPropertyMetadata("Внимание!", new PropertyChangedCallback(OnPropCange)));
		public static DependencyProperty SelectedDP = DependencyProperty.Register(nameof(SelectedItem), typeof(string), typeof(NamedCombobox), new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnPropCange)));
		public static DependencyProperty ItemsDP = DependencyProperty.Register(nameof(Items), typeof(ObservableCollection<string>), typeof(NamedCombobox), new FrameworkPropertyMetadata(new ObservableCollection<string>(), new PropertyChangedCallback(OnPropCange)));

		//public event PropertyChangedEventHandler PropertyChanged;

		public string Title
		{
			get
			{
				return (string)GetValue(TitleDP);
			}
			set
			{
				SetValue(TitleDP, value);
				//PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Title)));
			}
		}

		public string SelectedItem
		{
			get
			{
				return (string)GetValue(SelectedDP);
			}
			set
			{
				SetValue(SelectedDP, value);
				//PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedItem)));
			}
		}

		public ObservableCollection<string> Items
		{
			get
			{
				return (ObservableCollection<string>)GetValue(ItemsDP);
			}
			set
			{
				SetValue(ItemsDP, value);
				//PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Items)));
			}

		}

		public NamedCombobox()
		{
			InitializeComponent();
		}

		static void OnPropCange(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var src = (NamedCombobox)sender;
			switch (e.Property.Name)
			{
				case nameof(SelectedItem):
					{
						src.SelectedItem = (string)e.NewValue;
						break;
					}
				case nameof(Title):
					{
						src.Title = (string)e.NewValue;
						break;
					}
				case nameof(Items):
					{
						src.Items = (ObservableCollection<string>)e.NewValue;
						break;
					}
				default:
					{
						throw new Exception("Unecaption behaviour");
					}
			}
		}
	}
}
