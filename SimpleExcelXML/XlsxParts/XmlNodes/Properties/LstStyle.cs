﻿using System;
using System.Xml.Linq;
using SimpleExcelXML.Tools;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// Функционал класса не известен, однако нужен для корректной работы
	/// </summary>
	public class LstStyle
	{

		public static LstStyle Default { get { return new LstStyle(); } }

		public LstStyle()
		{

		}

		public LstStyle(XElement xStyle)
		{
			TryLoad(xStyle);
		}	
		
		public void TryLoad(XElement root)
		{

		}

		internal void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("lstStyle", "a").CloseElement();
		}
	}
}