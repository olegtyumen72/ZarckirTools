﻿using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Styles
{
	internal class BorderNode
	{
		public int DiagonalDown { get; set; }
		public int DiagonalUp { get; set; }

		public BorderElement Left { get; set; }
		public BorderElement Right { get; set; }
		public BorderElement Top { get; set; }
		public BorderElement Bottom { get; set; }
		public BorderElement Diagonal { get; set; }
		public bool IsAll
		{
			get
			{
				return !Left.Clr.IsEmpty &&
					!Right.Clr.IsEmpty &&
					!Top.Clr.IsEmpty &&
					!Bottom.Clr.IsEmpty;
			}
		}
		public bool IsEmpty
		{
			get
			{
				return IsEmptyElem(Left) &&
					IsEmptyElem(Right) &&
					IsEmptyElem(Top) &&
					IsEmptyElem(Bottom) &&
					IsEmptyElem(Diagonal);
			}
		}

		private bool IsEmptyElem(BorderElement elem)
		{
			return elem?.IsEmpty ?? true;
		}

		public bool isDefult { get; set; }
		public int Id { get; set; }

		public void Save(XmlWriterWrapper doc)
		{
			doc.OpenElement("border");

			if (DiagonalDown >= 0)
				doc.AddAtribute("diagonalDown", DiagonalDown);
			if (DiagonalUp >= 0)
				doc.AddAtribute("diagonalUp", DiagonalUp);

			Left?.Save(doc);
			Right?.Save(doc);
			Top?.Save(doc);
			Bottom?.Save(doc);
			Diagonal?.Save(doc);

			doc.CloseElement();
		}

		public BorderNode(ReportFormatItem p, int id) : this(id)
		{
			if (p.BordersAll)
			{
				Left = BorderElement.Default(BorderElement.Elem.left);
				Right = BorderElement.Default(BorderElement.Elem.right);
				Top = BorderElement.Default(BorderElement.Elem.top);
				Bottom = BorderElement.Default(BorderElement.Elem.bottom);
			}
		}

		public BorderNode(int id)
		{
			Id = id;
			Left = new BorderElement(BorderElement.Elem.left);
			Right = new BorderElement(BorderElement.Elem.right);
			Top = new BorderElement(BorderElement.Elem.top);
			Bottom = new BorderElement(BorderElement.Elem.bottom);
			Diagonal = new BorderElement(BorderElement.Elem.diagonal);
		}

		public bool IsSrc(ReportFormatItem p)
		{
			if (p.BordersAll && !IsAll)
			{
				return false;
			}
			else if (!p.BordersAll && IsAll)
			{
				return false;
			}

			return true;
		}

		internal BorderNode Update(BorderNode border)
		{
			if (Id == border.Id)
			{
				return border;
			}

			if (isDefult)
				return border;

			Left.Update(border.Left);
			Right.Update(border.Right);
			Top.Update(border.Top);
			Bottom.Update(border.Bottom);
			Diagonal.Update(border.Diagonal);

			DiagonalDown = border.DiagonalDown;
			DiagonalUp = border.DiagonalUp;

			return this;
		}
	}
}
