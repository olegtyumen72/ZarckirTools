﻿using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Styles
{
	internal class NumFmtNode
	{
		public int NumFmtId { get; set; }
		public string FormatCode { get; set; }
		public bool IsEmpty
		{
			get
			{
				return NumFmtId < 0 && string.IsNullOrEmpty(FormatCode);
			}
		}


		public NumFmtNode(int numFmtId)
		{
			NumFmtId = numFmtId;
		}

		public NumFmtNode(string frmtCd, int numFmtId) : this(numFmtId)
		{
			FormatCode = frmtCd;
		}

		public NumFmtNode Clone()
		{
			var res = new NumFmtNode(NumFmtId)
			{
				FormatCode = this.FormatCode
			};

			return res;
		}

		public void Load(XElement element, Dictionary<string, string> atrs = null)
		{
			if (atrs == null)
				atrs = XDocumentTools.ParseAtrs(element);

			if (atrs.ContainsKey("formatCode"))
				FormatCode = atrs["formatCode"];
			if (atrs.ContainsKey("numFmtId"))
				NumFmtId = atrs["numFmtId"].ToInt();
		}

		public void Save(XmlWriterWrapper doc)
		{
			doc.OpenElement("numFmt").AddAtribute("formatCode", FormatCode).AddAtribute("numFmtId", NumFmtId).CloseElement();
		}

		internal NumFmtNode Update(NumFmtNode numFmt)
		{
			return numFmt.Clone();
		}

		public static NumFmtNode Load(XElement elem)
		{
			var atrs = XDocumentTools.ParseAtrs(elem);

			if (atrs.Count < 2)
			{
				return null;
			}

			var res = new NumFmtNode(atrs["numFmtId"].ToInt());
			res.Load(elem, atrs);

			return res;
		}

		internal bool isSrc(ReportFormatItem p, ReportSheet rs)
		{
			return p.NumFmtId == NumFmtId;
		}

		internal static NumFmtNode Load(ReportFormatItem p)
		{
			return new NumFmtNode(p.NumFmtId);
		}
	}
}
