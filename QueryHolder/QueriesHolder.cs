﻿using AssociativeList;
using DbConnector.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace SqlHelpers
{
	public enum QueryAc
	{
		Ins = 1,
		Upd = 2,
		Del = 0
	}	

	public class QueriesHolder
	{

		private Dictionary<string, Query> _queries;

		public QueriesHolder()
		{
			_queries = new Dictionary<string, Query>();
		}

		public Query this[string key]
		{
			get { return _queries[key]; }
		}

		public QueriesHolder Add(string queryName)
		{
			_queries.Add(queryName, new Query());
			return this;
		}

		public QueriesHolder AddInfo(string tbl, QueryAc action, string actionCols, string queryName = "")
		{
			Query q = FindQuery(queryName);

			//if (q == null) throw new Exception("Miss query holder name");

			if (q == null && queryName.Length > 0)
			{
				Add(queryName);
				q = _queries[queryName];
				//q = new query();
				//_queries.add(queryname, q);
			}

			FullUpdateQuery(q, tbl, action, actionCols);
			return this;
		}

		public QueriesHolder AddCols(string queryName, QueryAc action, string cols)
		{
			this[queryName].AddCols(action, cols);
			return this;
		}

		public QueriesHolder AddVals(string queryName, QueryAc action, string[] vals)
		{
			this[queryName].AddVals(action, vals);
			return this;
		}

		public QueriesHolder AddVal(string queryName, QueryAc action, string val = "")
		{
			this[queryName].AddVal(action, val);
			return this;
		}

		private QueriesHolder FullUpdateQuery(Query query, string tbl, QueryAc action, string cols)
		{
			query.UpdateTable(tbl);
			query.AddCols(action, cols);
			return this;
		}

		public QueriesHolder InitDel(string queryName = "")
		{
			var q = FindQuery(queryName);
			q.AddVal(QueryAc.Del);
			return this;
		}

		private Query FindQuery(string queryName)
		{
			if (queryName.Length <= 0)
			{
				return _queries.Last().Value;
			}
			else
			{
				return _queries.FirstOrDefault((v) => v.Key == queryName).Value;
			}
		}

		void InitQuery(Query q, QueryAc act, Action<string, string, string> acting)
		{
			if (q.Has(act))
			{
				foreach (string onAct in q.GetVals(act))
				{
					acting(q.Table, q.GetCols(act), onAct);
				}
			}
		}

		public void SendDb<T>(IDbConnector<T> db)
		{
			foreach (var pair in _queries)
			{
				InitQuery(pair.Value, QueryAc.Del, (table, cols, where) => db.DeleteQuery(table, where));
				InitQuery(pair.Value, QueryAc.Ins, (table, cols, vals) => db.InsertQuery(table, cols, vals));
				InitQuery(pair.Value, QueryAc.Upd, (table, cols, vals) => db.UpdateWithDuplicate(table, cols, vals));
			}
		}

		public QueriesHolder CheckAndAdd(string queryName, QueryAc action, string value)
		{
			if (value.Length <= 0) return this;
			value = value.Last() == ',' ? value.DelLastChar() : value;
			return AddVal(queryName, action, value);
		}
	}
}
