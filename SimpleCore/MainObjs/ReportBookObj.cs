﻿using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Objs
{
	public class ReportBookObj : List<IReportSheet>, IReportBook
	{
		public IReportSheet this[string name]
		{
			get
			{
				return this.First((v) => v.Name.ToLower().Contains(name));
			}
		}

		public string Path { get; set; }

		public ReportBookObj(string path, IEnumerable<IReportSheet> col = null)
		{
			if (col != null)
				AddRange(col);
			Path = path;
		}
	}
}
