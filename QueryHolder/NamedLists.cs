﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlHelpers
{

	public class NamedLists : NamedLists<object>
	{
		public NamedLists(string str, List<List<object>> vals) : base(str, vals)
		{

		}
	}

	public class NamedLists<T>
	{
		public string Query { get; set; }
		public List<List<T>> Vals { get; set; }

		public NamedLists(string str, List<List<T>> vals)
		{
			Query = str;
			Vals = vals;
		}
	}
}
