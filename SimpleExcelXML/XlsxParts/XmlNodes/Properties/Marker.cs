﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Charts;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// Маркер графика
	/// </summary>
	internal class Marker: XmlPropBase
	{
		public MarkerSymbol Symbol { get; set; } = MarkerSymbol.none;
		private int size = 13;
		public SpPr MarkerProp { get; set; } = null;
		public bool isDraw = false;

		public static Marker DefaultMarker
		{
			get
			{
				return new Marker(MarkerSymbol.none, -1)
				{
					MarkerProp = SpPr.DefaultSpPr					
				};
			}
		}

		public Marker(MarkerSymbol symbl = MarkerSymbol.none, int size = 13)
		{
			Symbol = symbl;
			this.size = size;
		}

		public Marker(XElement xMarker) : base(xMarker)
		{
		}

		public override void Save(XmlWriterWrapper doc)
		{
			if (!isDraw) return;			

			doc.OpenPrefElement("marker", "c")
				.OpenPrefElement(nameof(Symbol).ToLower(), "c").AddAtribute("val", Symbol.ToString()).CloseElement();


			if (size > 10)
			{
				doc.OpenPrefElement(nameof(size), "c").AddAtribute("val", size.ToString()).CloseElement();
			}

			if (MarkerProp != null && Symbol != MarkerSymbol.none)
			{
				MarkerProp?.Save(doc);
			}

			doc.CloseElement();
		}

		public override void TryLoad(XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");
			Symbol = ParseSymbol(root.Element(c + nameof(Symbol).ToLower())?.Attribute("val")?.Value);
			size = root.Element(c + nameof(size))?.Attribute("val")?.Value?.ToInt() ?? -1;

			var xSpPr = root.GetSpPr();
			if (xSpPr.HasInfo())
				MarkerProp = new SpPr(xSpPr);
		}

		private MarkerSymbol ParseSymbol(string value)
		{
			var res = MarkerSymbol.none;
			if (String.IsNullOrEmpty(value))
				return res;

			if (Enum.TryParse(value, out res))
				return res;

			return MarkerSymbol.none;
		}
	}
}
