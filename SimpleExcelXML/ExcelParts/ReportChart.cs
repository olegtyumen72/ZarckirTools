﻿using SimpleCore.Parts;
using SimpleCore.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.ExcelParts
{
	public class ReportChart : ReportChartTmpl<ReportChartItem>
	{

		public int Id = -1;
		public int rId = -1;

		public ReportChart() : base()
		{


		}

		public ReportChart(IReportChart<IReportChartItem> v) : base(v)
		{

		}
	}
}
