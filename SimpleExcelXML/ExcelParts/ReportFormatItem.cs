﻿using SimpleCore.Parts;
using SimpleCore.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.ExcelParts
{
	public class ReportFormatItem : ReportFormatItemTmpl<CellRangeTmpl, ConditionalFormatTmpl>
	{
		public int NumFmtId { get; set; } = 0;

		public ReportFormatItem()
		{
		}

		public ReportFormatItem(IReportFormatItem<ICellRange, IConditionalFormat> src) : base(src)
		{
		}

		public ReportFormatItem(CellRangeTmpl range, IEnumerable<ConditionalFormatTmpl> formats = null) : base(range, formats)
		{
		}

		public string ToFontConf()
		{
			return $"{FontFormat.FontName}:{FontFormat.Size}:{FontFormat.Italic}:{FontFormat.Bold}:{FontFormat.Underline}";
		}

		public string ToBorderConf()
		{
			if (BordersAll)
				return "All";
			else
				return "None";
		}

		public override ReportFormatItemTmpl<CellRangeTmpl, ConditionalFormatTmpl> Clone()
		{
			var res = new ReportFormatItem(Range, ConditionalFormats)
			{
				Col = this.Col,
				Row = this.Row,
				NFormat = this.NFormat,
				FontFormat = this.FontFormat == null ? null : this.FontFormat.Clone(),
				BordersAll = this.BordersAll,
				HorizontalAligmentCenter = this.HorizontalAligmentCenter,
				VerticalAligmentCenter = this.VerticalAligmentCenter,
				//AutoFit = this.AutoFit,
				HideColumns = this.HideColumns,
				HideRows = this.HideRows,
				//Range = this.Range,
				ColumnWidth = this.ColumnWidth,
				RowOrientation = this.RowOrientation,
				ColumnsOrientation = this.ColumnsOrientation,
				Wrap = this.Wrap,
				MergeArea = this.MergeArea,
				MergeColumns = this.MergeColumns,
				AllRows = this.AllRows,
				AllCols = this.AllCols,
				Background = this.Background,
				//ConditionalFormats = this.ConditionalFormats.Select((v) => v).ToList(),
				RowHeight = this.RowHeight,
				IsSingle = this.IsSingle,
				//AutoFitCols = this.AutoFitCols,
				//MergeRows = this.MergeRows,
				NumFmtId = this.NumFmtId
			};

			return res;
		}
	}
}
