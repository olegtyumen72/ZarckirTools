﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.Tools;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// настройки линии
	/// </summary>
	public class Ln
	{
		private string prefix;
		public int w { get; set; }

		private string cap;
		private string cmpd;
		private string algn;
		public SolidFill fill { get; private set; }
		private bool round;

		public static Ln DefaultLn { get { return new Ln("a", 12700, null, null, null, SolidFill.DefultSolid); } }


		public Ln(string pref = "a", int w = 12700, string cap = "flat", string cmpd = "sng", string algn = "ctr", SolidFill fill = null, bool round = false)
		{
			prefix = pref;
			this.w = w;
			this.cap = cap;
			this.cmpd = cmpd;
			this.algn = algn;
			this.fill = fill;
			this.round = round;
		}

		public Ln(XElement xLn)
		{
			TryLoad(xLn);
		}

		public void TryLoad(XElement root)
		{
			var atrs = XDocumentTools.ParseAtrs(root);

			if (atrs.ContainsKey(nameof(w)))
				w = atrs[nameof(w)].ToInt();

			if (atrs.ContainsKey(nameof(cap)))
				cap = atrs[nameof(cap)];

			if (atrs.ContainsKey(nameof(cmpd)))
				cmpd = atrs[nameof(cmpd)];

			if (atrs.ContainsKey(nameof(algn)))
				algn = atrs[nameof(algn)];

			if (atrs.ContainsKey(nameof(round)))
				round = atrs[nameof(round)].ToBool();

			var xFill = root.GetSolidFill();
			if (xFill == null || xFill.IsFullEmpty())
				fill = SolidFill.DefultSolid;
			else
				fill = new SolidFill(xFill);
		}

		public void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("ln", prefix);

			if (w != -1)
				doc.AddAtribute(nameof(w), w.ToString());
			if (!String.IsNullOrEmpty(cap))
				doc.AddAtribute(nameof(cap), cap);
			if (!string.IsNullOrEmpty(cmpd))
				doc.AddAtribute(nameof(cmpd), cmpd);
			if (!String.IsNullOrEmpty(algn))
				doc.AddAtribute(nameof(algn), algn);

			fill.Save(doc);

			if (round)
				doc.OpenElement(nameof(round)).CloseElement();

			doc.CloseElement();
		}

	}
}
