﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;
using SimpleExcelXML.XlsxParts.XmlNodes.Properties;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Charts
{

	internal class Ser : XmlPropBase
	{
		public int idx { get; set; }
		public int order { get; set; }
		public TypedRange title { get; set; } = null;

		private int invertIfNegative;
		private string prefix;
		public int smooth { get; set; } = 1;
		public List<TypedRange> ranges { get; private set; } = new List<TypedRange>();
		public Marker Marker { get; set; }
		public SpPr LineProperty { get; set; }
		public string Sheet { get; set; }

		private Func<Color> GetColor;
		private Func<MarkerSymbol> GetMarker;

		private Ser()
		{
		}

		public Ser(string title, string prefix = "c", int invertIfNegat = -1, string sheet = "", int smooth = 0) : this()
		{
			//this.idx = idx;
			//this.order = order;
			this.title = new TypedRange(TypedRange.Type.tx, title, true, sheet);
			this.invertIfNegative = invertIfNegat;
			this.prefix = prefix;
			this.smooth = smooth;
		}

		public Ser(ReportChartItem item, string sheetName, XmlChart src) : this(item.Title)
		{
			GetColor = () => src.curColor;
			GetMarker = () => src.curMarker;
			Sheet = sheetName;
			AddRange(item);
		}

		public Ser(XElement xSer) : base(xSer)
		{
		}

		public void AddRange(TypedRange range)
		{
			ranges.Add(range);
		}

		public void AddRange(ReportChartItem chart)
		{
			var mainColor = chart.Color.IsEmpty ? GetColor() : chart.Color;
			//chart.Color = chart.Color.IsEmpty ? GetColor() : chart.Color;
			if (chart.ChartType == SimpleCore.Parts.ChartTypeEnum.Points)
			{
				LineProperty = SpPr.DefaultSpPr;
				Marker = new Marker(GetMarker(), chart.LineWidth < 2 ? 4 : chart.LineWidth)
				{
					MarkerProp = new SpPr("c", new SolidFill(mainColor), null)
				};
			}
			else
			{
				LineProperty = new SpPr(
						"c",
						new SolidFill(mainColor),
						new Ln("a", 12700, "flat", "sng", "ctr", new SolidFill(mainColor), false)
						);
				Marker = Marker.DefaultMarker;
				//Marker.MarkerProp.Ln = null;
			}



			if (chart.ChartType == SimpleCore.Parts.ChartTypeEnum.XYGraphic || chart.ChartType == SimpleCore.Parts.ChartTypeEnum.Points)
			{
				if (String.IsNullOrEmpty(chart.RangeY))
					throw new Exception("For point and XYGrapth need both x and y values");

				ranges.Add(new TypedRange(TypedRange.Type.yVal, chart.RangeY, false, Sheet));
				ranges.Add(new TypedRange(TypedRange.Type.xVal, chart.RangeX, false, Sheet));

				Marker.isDraw = true;

				if (chart.ChartType == SimpleCore.Parts.ChartTypeEnum.Points)
					Marker.Symbol = GetMarker();

			}
			else if (chart.ChartType == SimpleCore.Parts.ChartTypeEnum.Graphic)
			{
				ranges.Add(new TypedRange(TypedRange.Type.yVal, chart.RangeY, false, Sheet));

				//if (!String.IsNullOrEmpty(chart.RangeY))
				//	ranges.Add(new TypedRange(TypedRange.Type.yVal, chart.RangeY));
			}
			else if (chart.ChartType == SimpleCore.Parts.ChartTypeEnum.Histogram)
			{
				ranges.Add(new TypedRange(TypedRange.Type.val, chart.RangeY, false, Sheet));

				if (!String.IsNullOrEmpty(chart.RangeX))
					ranges.Add(new TypedRange(TypedRange.Type.cat, chart.RangeX, false, Sheet));
			}
			else
			{
				throw new Exception("Miss chart type");
			}
		}

		public override void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("ser", prefix)

				.OpenPrefElement(nameof(idx)).AddAtribute("val", idx.ToString()).CloseElement()
				.OpenPrefElement(nameof(order)).AddAtribute("val", order.ToString()).CloseElement();


			title?.Save(doc);

			LineProperty?.Save(doc);
			Marker?.Save(doc);

			if (invertIfNegative != -1)
				doc.OpenPrefElement(nameof(invertIfNegative)).AddAtribute("val", invertIfNegative.ToString()).CloseElement();

			foreach (var range in ranges)
			{
				range.Save(doc);
			}

			if (smooth != -1)
				doc.OpenPrefElement(nameof(smooth), "c").AddAtribute("val", smooth.ToString()).CloseElement();

			doc.CloseElement();
		}

		public override void TryLoad(XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");

			idx = root.Element(c + nameof(idx))?.Attribute("val")?.Value.ToInt() ?? -1;
			order = root.Element(c + nameof(order))?.Attribute("val")?.Value.ToInt() ?? -1;

			ranges = new List<TypedRange>();
			var xRanges = root.Elements().Where((v) => TypedRange.IsTypedRange(v.Name.LocalName));
			foreach (var xRange in xRanges)
			{
				if (TypedRange.IsTitle(xRange.Name.LocalName))
				{
					title = new TypedRange(xRange);					
				}
				else
				{
					ranges.Add(new TypedRange(xRange));
				}
			}

			var xSpPr = root.GetSpPr();
			if (xSpPr.HasInfo())
				LineProperty = new SpPr(xSpPr);

			var xMarker = root.GetMarker();
			if (xMarker.HasInfo())
				Marker = new Marker(xMarker);


			invertIfNegative = root.Element(c + nameof(invertIfNegative))?.Attribute("val")?.Value?.ToInt() ?? -1;
			smooth = root.Element(c + nameof(smooth))?.Attribute("val")?.Value?.ToInt() ?? -1;
		}
	}
}
