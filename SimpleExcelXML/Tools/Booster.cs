﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.Tools
{
	/// <summary>
	/// Упроститель жизни, всякие переводы из одного в другое
	/// </summary>
	public static class Booster
	{
		static readonly ColorConverter colorConv = new ColorConverter();

		public static string ToARGB(this Color src)
		{
			return $"{src.A.ToString("X2")}{src.R.ToString("X2")}{src.G.ToString("X2")}{src.B.ToString("X2")}";
		}

		public static string ToRGB(this Color src)
		{
			return $"{src.R.ToString("X2")}{src.G.ToString("X2")}{src.B.ToString("X2")}";
		}

		internal static int ToInt(this string src)
		{
			int res = default(int);
			int.TryParse(src, NumberStyles.Any, CultureInfo.InvariantCulture, out res);
			return res;
		}

		internal static int ToInt(this string src, int defVal)
		{
			int res = defVal;
			int.TryParse(src, NumberStyles.Any, CultureInfo.InvariantCulture, out res);
			return res;
		}

		internal static double ToDbl(this string src)
		{
			var res = double.NaN;
			if (double.TryParse(src, NumberStyles.Any, CultureInfo.InvariantCulture, out res))
			{
				return res;
			}
			else
			{
				return double.NaN;
			}
		}

		internal static Color ToColor(this string hex)
		{
			var res = Color.Orange;
			try
			{
				if (colorConv.IsValid(hex))
				{
					res = (Color)colorConv.ConvertFromString(hex);
				}
				else if (colorConv.IsValid("#" + hex))
				{
					res = (Color)colorConv.ConvertFromString("#" + hex);
				}
				else
				{
					res = System.Drawing.ColorTranslator.FromHtml("#" + hex);
				}
			}
			catch(Exception e)
			{

			}

			return res;
		}		

		internal static bool ToBool(this string src)
		{
			return Convert.ToBoolean(src);
		}

		internal static bool StrToBool(this string src)
		{
			return src != "0" && !string.IsNullOrEmpty(src);
		}

		internal static int BoolToInt(this bool src)
		{
			return src ? 1 : 0;
		}

		internal static bool IsSame(this SimpleFonts.SimpleFontFamily src, SimpleFonts.SimpleFontFamily dest)
		{
			return src.Bold == dest.Bold && src.Italic == dest.Italic && src.Underline == dest.Underline && src.Size == dest.Size && src.Name == dest.Name;
		}
	}
}
