﻿using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// Настрйоки какого-либо объекта связанные с отображением линии
	/// </summary>
	public class SpPr
	{

		public SolidFill mainFill { get; private set; }
		public Ln Ln { get; private set; }
		string prefix;

		public static SpPr DefaultSpPr { get { return new SpPr(); } }

		public SpPr(string prefix = "c", SolidFill main = null, Ln ln = null)
		{
			this.prefix = prefix;
			mainFill = main ?? SolidFill.DefultSolid;
			this.Ln = ln ?? Ln.DefaultLn;
		}

		public SpPr(XElement xSpPr)
		{
			TryLoad(xSpPr);
		}

		public void TryLoad(XElement root)
		{
			var a = root.GetNamespaceOfPrefix("a");
			var fill = root.Element(a + "solidFill");
			if (fill != null)
				mainFill = new SolidFill(fill);

			var xLn = root.Element(a + "ln");
			if (xLn != null)
				this.Ln = new Ln(xLn);
		}

		public void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("spPr", prefix);

			mainFill.Save(doc);
			Ln.Save(doc);
			
			doc/*.OpenPrefElement("effectLst", "a").CloseElement()*/
				.CloseElement();
		}
	}
}
