﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTools.Views
{
	/// <summary>
	/// Interaction logic for ManySelector.xaml
	/// </summary>
	public partial class ManySelector : UserControl
	{

		public static DependencyProperty ItemsDP = DependencyProperty.Register(nameof(Items), typeof(ObservableCollection<object>), typeof(ManySelector), new FrameworkPropertyMetadata(new ObservableCollection<object>(), new PropertyChangedCallback(OnPropCange)));

		public static DependencyProperty SelectedItemsDP = DependencyProperty.Register(nameof(SelectedItems), typeof(ObservableCollection<object>), typeof(ManySelector), new FrameworkPropertyMetadata(new ObservableCollection<object>(), new PropertyChangedCallback(OnPropCange)));

		public static DependencyProperty SelectedItemInSelectionDP = DependencyProperty.Register(nameof(SelectedItemInSelection), typeof(object), typeof(ManySelector), new FrameworkPropertyMetadata(new object(), new PropertyChangedCallback(OnPropCange)));

		public static DependencyProperty SelectedItemDP = DependencyProperty.Register(nameof(SelectedItem), typeof(object), typeof(ManySelector), new FrameworkPropertyMetadata(new object(), new PropertyChangedCallback(OnPropCange)));

		public static DependencyProperty SelectedTitleDP = DependencyProperty.Register(nameof(SelectedTitle), typeof(string), typeof(ManySelector), new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnPropCange)));

		public static DependencyProperty SelectedTitleInSelectionDP = DependencyProperty.Register(nameof(SelectedTitleInSelection), typeof(string), typeof(ManySelector), new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnPropCange)));

		public static DependencyProperty ItemsDataTemplateDP = DependencyProperty.Register(nameof(ItemsDataTemplate), typeof(DataTemplate), typeof(ManySelector), new FrameworkPropertyMetadata(new DataTemplate(), new PropertyChangedCallback(OnPropCange)));

		public static DependencyProperty SelectionDataTemplateDP = DependencyProperty.Register(nameof(SelectionDataTemplate), typeof(DataTemplate), typeof(ManySelector), new FrameworkPropertyMetadata(new DataTemplate(), new PropertyChangedCallback(OnPropCange)));

		public static RoutedEvent SelectionChangeRE = EventManager.RegisterRoutedEvent(nameof(SelectionChange), RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<object>), typeof(ManySelector));
		public static RoutedEvent SelectionChangeInSelectionRE = EventManager.RegisterRoutedEvent(nameof(SelectionChangeInSelection), RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<object>), typeof(ManySelector));
		public static RoutedEvent ItemAddInSelectionRE = EventManager.RegisterRoutedEvent(nameof(ItemAddInSelection), RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<object>), typeof(ManySelector));
		public static RoutedEvent ItemRemovedFromSelectionRE = EventManager.RegisterRoutedEvent(nameof(ItemRemovedFromSelection), RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<object>), typeof(ManySelector));



		public event RoutedPropertyChangedEventHandler<object> SelectionChange
		{
			add => AddHandler(SelectionChangeRE, value);
			remove => RemoveHandler(SelectionChangeRE, value);
		}

		public event RoutedPropertyChangedEventHandler<object> ItemAddInSelection
		{
			add => AddHandler(ItemAddInSelectionRE, value);
			remove => RemoveHandler(ItemAddInSelectionRE, value);
		}

		public event RoutedPropertyChangedEventHandler<object> ItemRemovedFromSelection
		{
			add => AddHandler(ItemRemovedFromSelectionRE, value);
			remove => RemoveHandler(ItemRemovedFromSelectionRE, value);
		}

		public event RoutedPropertyChangedEventHandler<object> SelectionChangeInSelection
		{
			add => AddHandler(SelectionChangeInSelectionRE, value);
			remove => RemoveHandler(SelectionChangeInSelectionRE, value);
		}

		public ObservableCollection<object> Items
		{
			get => (ObservableCollection<object>)GetValue(ItemsDP);
			set => SetValue(ItemsDP, value);
		}

		public ObservableCollection<object> SelectedItems
		{
			get => (ObservableCollection<object>)GetValue(SelectedItemsDP);
			set
			{

				SetValue(SelectedItemsDP, value);
			}
		}

		public object SelectedItem
		{
			get => GetValue(SelectedItemDP);
			set
			{
				var arg = new RoutedPropertyChangedEventArgs<object>(SelectedItem, value, SelectionChangeRE);
				SetValue(SelectedItemDP, value);
				RaiseEvent(arg);
			}
		}

		public object SelectedItemInSelection
		{
			get => GetValue(SelectedItemInSelectionDP);
			set
			{
				var arg = new RoutedPropertyChangedEventArgs<object>(SelectedItemInSelection, value, SelectionChangeInSelectionRE);
				SetValue(SelectedItemInSelectionDP, value);
				RaiseEvent(arg);
			}
		}

		public string SelectedTitle
		{
			get => (string)GetValue(SelectedTitleDP);
			set => SetValue(SelectedTitleDP, value);
		}

		public string SelectedTitleInSelection
		{
			get => (string)GetValue(SelectedTitleInSelectionDP);
			set => SetValue(SelectedTitleInSelectionDP, value);
		}

		public DataTemplate ItemsDataTemplate
		{
			get => (DataTemplate)GetValue(ItemsDataTemplateDP);
			set => SetValue(ItemsDataTemplateDP, value);
		}

		public DataTemplate SelectionDataTemplate
		{
			get => (DataTemplate)GetValue(SelectionDataTemplateDP);
			set => SetValue(SelectionDataTemplateDP, value);
		}

		public ManySelector()
		{
			InitializeComponent();
		}

		private static void OnPropCange(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var src = (ManySelector)sender;
			switch (e.Property.Name)
			{
				case nameof(SelectedItem):
					{
						src.SelectedItem = e.NewValue;
						break;
					}
				case nameof(SelectedTitle):
					{
						src.SelectedTitle = (string)e.NewValue;
						break;
					}
				case nameof(SelectedItemInSelection):
					{
						src.SelectedItemInSelection = e.NewValue;
						break;
					}
				case nameof(SelectedTitleInSelection):
					{
						src.SelectedTitleInSelection = (string)e.NewValue;
						break;
					}
				case nameof(Items):
					{
						src.Items = (ObservableCollection<object>)e.NewValue;
						break;
					}
				case nameof(SelectedItems):
					{
						src.SelectedItems = (ObservableCollection<object>)e.NewValue;
						break;
					}
				case nameof(SelectionDataTemplate):
					{
						src.SelectionDataTemplate = (DataTemplate)e.NewValue;
						break;
					}
				case nameof(ItemsDataTemplate):
					{
						src.ItemsDataTemplate = (DataTemplate)e.NewValue;
						break;
					}
				default:
					{
						throw new Exception("Unecaption behaviour");
					}
			}
		}

		private void Add_BtnClck(object sender, RoutedEventArgs e)
		{
			if (SelectedItem != null)
			{				
				SelectedItems.Add(SelectedItem);
				SelectedItemInSelection = SelectedItem;

				Items.Remove(SelectedItem);
				SelectedItem = null;

				var args = new RoutedPropertyChangedEventArgs<object>(null, SelectedItemInSelection, ItemAddInSelectionRE);
				RaiseEvent(args);
			}
		}

		private void Rmv_BtnClck(object sender, RoutedEventArgs e)
		{
			if (SelectedItemInSelection != null)
			{
				Items.Add(SelectedItemInSelection);
				SelectedItem = SelectedItemInSelection;

				SelectedItems.Remove(SelectedItemInSelection);				
				SelectedItemInSelection = null;

				var args = new RoutedPropertyChangedEventArgs<object>(null, SelectedItem, ItemRemovedFromSelectionRE);
				RaiseEvent(args);
			}
		}

		private void Crt_BtnClck(object sender, RoutedEventArgs e)
		{
			var msg = new WritableMessageBox();
			msg.Message = "Введите имя нового элемента";
			if (msg.ShowDialog() == true)
			{
				if (ItemsList.IsFocused)
				{
					Items.Add(msg.WriteText);
				}

				if (SelectionList.IsFocused)
				{
					Items.Add(msg.WriteText);
				}
			}
		}

		private void Erase_BtnClck(object sender, RoutedEventArgs e)
		{

		}

		void MoveFromSrcToDest(ObservableCollection<object> src, ObservableCollection<object> des)
		{ 
			foreach(var el in src)
			{
				des.Add(el);
			}

			src.Clear();
		}

		private void AddAll_BtnClck(object sender, RoutedEventArgs e)
		{
			MoveFromSrcToDest(Items, SelectedItems);
		}

		private void RmvAll_BtnClck(object sender, RoutedEventArgs e)
		{
			MoveFromSrcToDest(SelectedItems, Items);
		}
	}
}
