﻿using System;
using System.Drawing;

namespace SimpleFonts
{
	[Serializable]
	public class SimpleFontFamily
	{
		public static readonly SimpleFontFamily Default = new SimpleFontFamily(Fonts.Times_New_Roman, 14);

		public static SimpleFontFamily Parse(string name, int size)
		{
			var clearName = name.Replace(" ", "_");
			var res = new SimpleFontFamily(Fonts.Times_New_Roman, size <= 0 ? 12 : size);
			res.IsEmpty = true;

			if (size < 0 || String.IsNullOrEmpty(name))
				return res;

			try
			{
				res.FontName = (Fonts)Enum.Parse(typeof(Fonts), clearName);
				res.IsEmpty = false;
			}
			catch (Exception)
			{
				clearName = clearName.Replace("_", "");
				if (!String.IsNullOrEmpty(clearName))
				{
					foreach (var font in Enum.GetNames(typeof(Fonts)))
					{
						if (font.Replace("_", "").Contains(clearName))
						{
							res.FontName = (Fonts)Enum.Parse(typeof(Fonts), font);
							res.IsEmpty = false;
							break;
						}
					}
				}
			}

			return res;
		}

		public string Name
		{
			get
			{
				return ToString();
			}
		}

		public SimpleFontFamily()
		{
			//IsEmpty = true;
		}

		public SimpleFontFamily(Fonts f, int size)
		{
			FontName = f;
			Size = size;
		}

		public SimpleFontFamily Clone()
		{
			return (SimpleFontFamily)MemberwiseClone();
		}


		public SimpleFontFamily Clone1()
		{
			return (SimpleFontFamily)MemberwiseClone();
		}

		public bool IsEqual(SimpleFontFamily other)
		{
			return
				FontName == other.FontName &&
				Italic == other.Italic &&
				Bold == other.Bold &&
				Underline == other.Underline &&
				Size == other.Size;

		}

		public Fonts FontName = Fonts.Arial;
		public bool Italic = false;
		public bool Bold = false;
		public bool Underline = false;
		public int Size = 11;
		public bool IsEmpty { get; private set; } = false;

		/// <summary>
		/// Возвращает название шрифта
		/// </summary>
		/// <returns>Название шрифта</returns>
		/// <example>Font = Fonts.Arial_Black; Результат: Arial Black</example>
		public override string ToString()
		{
			return FontName.ToString().Replace("_", " ");
		}


	}

	public static class SimpleFontLinq
	{

		public static Font ToFont(this SimpleFontFamily src, GraphicsUnit gu)
		{
			var res = src.Bold ? FontStyle.Bold : FontStyle.Regular;
			res |= src.Italic ? FontStyle.Italic : FontStyle.Regular;
			res |= src.Underline ? FontStyle.Underline : FontStyle.Regular;
			return new Font(src.ToFontFamily(), src.Size, res, gu);
		}

		public static Font ToFontPixel(this SimpleFontFamily src)
		{
			var res = src.Bold ? FontStyle.Bold : FontStyle.Regular;
			res |= src.Italic ? FontStyle.Italic : FontStyle.Regular;
			res |= src.Underline ? FontStyle.Underline : FontStyle.Regular;
			return new Font(src.ToFontFamily(), src.Size, res, GraphicsUnit.Pixel);
		}

		public static FontFamily ToFontFamily(this SimpleFontFamily src)
		{
			return new FontFamily(src.Name.Replace("_", " "));
		}

		public static FontFamily ToFontFamily(this Fonts src)
		{
			return new FontFamily(src.ToString().Replace("_", " "));
		}

		public static Font ToFont(this Fonts src, float size, GraphicsUnit gu)
		{
			return new Font(src.ToFontFamilyName(), size, FontStyle.Regular, gu);
		}

		public static Font ToFontPixel(this Fonts src, float size)
		{
			return new Font(src.ToFontFamilyName(), size, FontStyle.Regular, GraphicsUnit.Pixel);
		}

		public static Font ToFontDefPixel(this Fonts src)
		{
			return new Font(src.ToFontFamilyName(), 12, FontStyle.Regular, GraphicsUnit.Pixel);
		}

		public static string ToFontFamilyName(this Fonts src)
		{
			return src.ToString().Replace("_", " ");
		}

	}

	[Serializable]
	public enum Fonts
	{
		Agency_FB,
		Aharoni,
		Aldhabi,
		Algerian,
		Andalus,
		Angsana_New,
		AngsanaUPC,
		Aparajita,
		Arabic_Typesetting,
		Arial,
		Arial_Black,
		Arial_Narrow,
		Arial_Rounded_MT_Bold,
		Arial_Unicode_MS,
		Baskerville_Old_Face,
		Batang,
		BatangChe,
		Bauhaus_93,
		Bell_MT,
		Berlin_Sans_FB,
		Berlin_Sans_FB_Demi,
		Bernard_MT_Condensed,
		Blackadder_ITC,
		Bodoni_MT,
		Bodoni_MT_Black,
		Bodoni_MT_Condensed,
		Bodoni_MT_Poster_Compressed,
		Book_Antiqua,
		Bookman_Old_Style,
		Bookshelf_Symbol_7,
		Bradley_Hand_ITC,
		Britannic_Bold,
		Broadway,
		Browallia_New,
		BrowalliaUPC,
		Brush_Script_MT,
		Buxton_Sketch,
		Caladea,
		Calibri,
		Calibri_Light,
		Californian_FB,
		Calisto_MT,
		Cambria,
		Cambria_Math,
		Candara,
		Carlito,
		Castellar,
		Centaur,
		Century,
		Century_Gothic,
		Century_Schoolbook,
		Chiller,
		Colonna_MT,
		Comic_Sans_MS,
		Consolas,
		Constantia,
		Cooper_Black,
		Copperplate_Gothic_Bold,
		Copperplate_Gothic_Light,
		Corbel,
		Cordia_New,
		CordiaUPC,
		Courier_New,
		Curlz_MT,
		DaunPenh,
		David,
		DejaVu_Sans,
		DejaVu_Sans_Condensed,
		DejaVu_Sans_Light,
		DejaVu_Sans_Mono,
		DejaVu_Serif,
		DejaVu_Serif_Condensed,
		DengXian,
		DilleniaUPC,
		DokChampa,
		Dotum,
		DotumChe,
		Ebrima,
		Edwardian_Script_ITC,
		Elephant,
		Engravers_MT,
		Eras_Bold_ITC,
		Eras_Demi_ITC,
		Eras_Light_ITC,
		Eras_Medium_ITC,
		Estrangelo_Edessa,
		EucrosiaUPC,
		Euphemia,
		FangSong,
		Felix_Titling,
		Footlight_MT_Light,
		Forte,
		Franklin_Gothic_Book,
		Franklin_Gothic_Demi,
		Franklin_Gothic_Demi_Cond,
		Franklin_Gothic_Heavy,
		Franklin_Gothic_Medium,
		Franklin_Gothic_Medium_Cond,
		FrankRuehl,
		FreesiaUPC,
		Freestyle_Script,
		French_Script_MT,
		Gabriola,
		Gadugi,
		Garamond,
		Gautami,
		Gentium_Basic,
		Gentium_Book_Basic,
		Georgia,
		Gigi,
		Gill_Sans_MT,
		Gill_Sans_MT_Condensed,
		Gill_Sans_MT_Ext_Condensed_Bold,
		Gill_Sans_Ultra_Bold,
		Gill_Sans_Ultra_Bold_Condensed,
		Gisha,
		Gloucester_MT_Extra_Condensed,
		Goudy_Old_Style,
		Goudy_Stout,
		Gulim,
		GulimChe,
		Gungsuh,
		GungsuhChe,
		Haettenschweiler,
		Harlow_Solid_Italic,
		Harrington,
		High_Tower_Text,
		Impact,
		Imprint_MT_Shadow,
		Informal_Roman,
		IrisUPC,
		Iskoola_Pota,
		JasmineUPC,
		Javanese_Text,
		Jokerman,
		Juice_ITC,
		KaiTi,
		Kalinga,
		Kartika,
		Khmer_UI,
		KodchiangUPC,
		Kokila,
		Kristen_ITC,
		Kunstler_Script,
		Lao_UI,
		Latha,
		Lato,
		Lato_Light,
		Lato_Semibold,
		Leelawadee,
		Leelawadee_UI,
		Leelawadee_UI_Semilight,
		Levenim_MT,
		Liberation_Mono,
		Liberation_Sans,
		Liberation_Sans_Narrow,
		Liberation_Serif,
		LilyUPC,
		Linux_Biolinum_G,
		Linux_Libertine_Display_G,
		Linux_Libertine_G,
		Lucida_Bright,
		Lucida_Calligraphy,
		Lucida_Console,
		Lucida_Fax,
		Lucida_Handwriting,
		Lucida_Sans,
		Lucida_Sans_Typewriter,
		Lucida_Sans_Unicode,
		Magneto,
		Maiandra_GD,
		Malgun_Gothic,
		Mangal,
		Marlett,
		Matura_MT_Script_Capitals,
		Meiryo,
		Meiryo_UI,
		Microsoft_Himalaya,
		Microsoft_JhengHei,
		Microsoft_JhengHei_Light,
		Microsoft_JhengHei_UI,
		Microsoft_JhengHei_UI_Light,
		Microsoft_MHei,
		Microsoft_NeoGothic,
		Microsoft_New_Tai_Lue,
		Microsoft_PhagsPa,
		Microsoft_Sans_Serif,
		Microsoft_Tai_Le,
		Microsoft_Uighur,
		Microsoft_YaHei,
		Microsoft_YaHei_Light,
		Microsoft_YaHei_UI,
		Microsoft_YaHei_UI_Light,
		Microsoft_Yi_Baiti,
		MingLiU,
		MingLiU_HKSCS,
		Miriam,
		Miriam_Fixed,
		Mistral,
		Mongolian_Baiti,
		Monotype_Corsiva,
		MoolBoran,
		MS_Gothic,
		MS_Mincho,
		MS_Outlook,
		MS_PGothic,
		MS_PMincho,
		MS_Reference_Sans_Serif,
		MS_Reference_Specialty,
		MS_UI_Gothic,
		MT_Extra,
		MV_Boli,
		Myanmar_Text,
		Narkisim,
		Niagara_Engraved,
		Niagara_Solid,
		Nirmala_UI,
		Nirmala_UI_Semilight,
		NSimSun,
		Nyala,
		OCR_A_Extended,
		Old_English_Text_MT,
		Onyx,
		Open_Sans,
		OpenSymbol,
		Palace_Script_MT,
		Palatino_Linotype,
		Papyrus,
		Parchment,
		Perpetua,
		Perpetua_Titling_MT,
		Plantagenet_Cherokee,
		Playbill,
		PMingLiU,
		Poor_Richard,
		Pristina,
		PT_Serif,
		Raavi,
		Rage_Italic,
		Ravie,
		Rockwell,
		Rockwell_Condensed,
		Rockwell_Extra_Bold,
		Rod,
		Sakkal_Majalla,
		Script_MT_Bold,
		Segoe_Marker,
		Segoe_Print,
		Segoe_Script,
		Segoe_UI,
		Segoe_UI_Black,
		Segoe_UI_Emoji,
		Segoe_UI_Light,
		Segoe_UI_Semibold,
		Segoe_UI_Semilight,
		Segoe_UI_Symbol,
		Segoe_WP,
		Segoe_WP_Black,
		Segoe_WP_Light,
		Segoe_WP_Semibold,
		Segoe_WP_SemiLight,
		Shonar_Bangla,
		Showcard_Gothic,
		Shruti,
		SimHei,
		Simplified_Arabic,
		Simplified_Arabic_Fixed,
		SimSun,
		Sitka_Banner,
		Sitka_Display,
		Sitka_Heading,
		Sitka_Small,
		Sitka_Subheading,
		Sitka_Text,
		SketchFlow_Print,
		Snap_ITC,
		Source_Code_Pro,
		Source_Sans_Pro,
		Source_Sans_Pro_Black,
		Source_Sans_Pro_ExtraLight,
		Source_Sans_Pro_Light,
		Source_Sans_Pro_Semibold,
		Stencil,
		Sylfaen,
		Symbol,
		Tahoma,
		TeamViewer12,
		Tempus_Sans_ITC,
		Times_New_Roman,
		Traditional_Arabic,
		Trebuchet_MS,
		Tunga,
		Tw_Cen_MT,
		Tw_Cen_MT_Condensed,
		Tw_Cen_MT_Condensed_Extra_Bold,
		Urdu_Typesetting,
		Utsaah,
		Vani,
		Verdana,
		Vijaya,
		Viner_Hand_ITC,
		Vivaldi,
		Vladimir_Script,
		Vrinda,
		Webdings,
		Wide_Latin,
		Wingdings,
		Wingdings_2,
		Wingdings_3,
		Yu_Gothic,
		Yu_Gothic_Light,
		Yu_Mincho,
		Yu_Mincho_Demibold,
		Yu_Mincho_Light
	}
}

