﻿using SimpleCore.Objs;
using SimpleCore.Parts;
using SimpleCore.Tools;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Templates
{
	public class ReportChartTmpl<T> : IReportChart<T>, ISimpleConvert<IReportChart<IReportChartItem>>, ISimpleClone<ReportChartTmpl<T>>
		where T : IReportChartItem, ISimpleClone<T>, ISimpleConvert<IReportChartItem>
	{
		public ReportChartTmpl()
		{

		}

		public ReportChartTmpl(IReportChart<IReportChartItem> v)
		{

			TitleSettings = v.TitleSettings;
			Title = v.Title;
			TitleXSettings = v.TitleXSettings;
			TitleX = v.TitleX;
			TitleYSettings = v.TitleYSettings;
			TitleY = v.TitleY;
			AxesXTitles = v.AxesXTitles;
			Items = v.Items.Select((vv) => (T)Activator.CreateInstance(typeof(T), new object[] { vv })).ToList();
			GraphRect = v.GraphRect;
			MinX = v.MinX;
			MinY = v.MinY;
			MaxX = v.MaxX;
			MaxY = v.MaxY;
			IsAxisYRevers = v.IsAxisYRevers;
			YAxisHasMinorGridLines = v.YAxisHasMinorGridLines;
			XAxisHasMinorGridLines = v.XAxisHasMinorGridLines;
			LegendSettings = v.LegendSettings;
			ShowLegend = v.ShowLegend;
			YMajorGridLinesColor = v.YMajorGridLinesColor;
			XMajorGridLinesColor = v.XMajorGridLinesColor;
			YAxisLinesColor = v.YAxisLinesColor;
			XAxisLinesColor = v.XAxisLinesColor;
			YLabelsSettings = v.YLabelsSettings;
			XLabelsSettings = v.XLabelsSettings;
			HasXMajorTickMark = v.HasXMajorTickMark;
			HasYMajorTickMark = v.HasYMajorTickMark;
			XMajorUnits = v.XMajorUnits;
			YMajorUnits = v.YMajorUnits;
			XNumberFormat = v.XNumberFormat;
			YNumberFormat = v.YNumberFormat;
			Pic = v.Pic;
		}

		public SimpleFontFamily TitleSettings { get; set; } = new SimpleFontFamily();

		/// <summary>
		/// Заголовок графика.
		/// </summary>
		public string Title { get; set; } = "";

		public SimpleFontFamily TitleXSettings { get; set; } = new SimpleFontFamily();

		/// <summary>
		/// Подпись оси X
		/// </summary>
		public string TitleX { get; set; } = "";

		public SimpleFontFamily TitleYSettings { get; set; } = new SimpleFontFamily();

		/// <summary>
		/// Подпись оси Y
		/// </summary>
		public string TitleY { get; set; } = "";

		/// <summary>
		/// Подпись маркеров по оси X. Если их на 1 больше чем строк данных, то надписи вставляются по краям диапазонов.
		/// </summary>
		public List<string> AxesXTitles { get; set; } = new List<string>();

		/// <summary>
		/// Элементы данного графика.
		/// </summary>
		public List<T> Items { get; set; } = new List<T>();

		/// <summary>
		/// Куда выводить график.
		/// </summary>
		public Rectangle GraphRect { get; set; } = new Rectangle(0, 0, 100, 100);

		/// <summary>
		/// Минимальное значение по оси X.
		/// </summary>
		public double MinX { get; set; } = double.NaN;

		/// <summary>
		/// Максимальные значение по оси X.
		/// </summary>
		public double MaxX { get; set; } = double.NaN;

		/// <summary>
		/// Максимальные значение по оси Y.
		/// </summary>
		public double MinY { get; set; } = double.NaN;

		/// <summary>
		/// Максимальные значение по оси Y.
		/// </summary>
		public double MaxY { get; set; } = double.NaN;

		public bool IsAxisYRevers { get; set; } = false;

		public bool YAxisHasMinorGridLines { get; set; } = false;

		public bool XAxisHasMinorGridLines { get; set; } = false;

		public SimpleFontFamily LegendSettings { get; set; } = new SimpleFontFamily();

		public bool ShowLegend { get; set; } = true;

		public Bitmap Pic { get; set; } = null;

		public Color YMajorGridLinesColor { get; set; } = Color.Empty;
		public Color YMinorGridLinesColor { get; set; } = Color.Empty;

		public Color XMajorGridLinesColor { get; set; } = Color.Empty;
		public Color XMinorGridLinesColor { get; set; } = Color.Empty;

		public Color YAxisLinesColor { get; set; } = Color.Empty;
		public Color XAxisLinesColor { get; set; } = Color.Empty;

		public SimpleFontFamily YLabelsSettings { get; set; } = new SimpleFontFamily();
		public SimpleFontFamily XLabelsSettings { get; set; } = new SimpleFontFamily();

		public bool HasXMajorTickMark { get; set; } = true;
		public bool HasYMajorTickMark { get; set; } = true;

		public double XMajorUnits { get; set; } = -1;
		public double YMajorUnits { get; set; } = -1;

		/// <summary>
		/// Не работает: #,##; #.##;
		/// Работает: #0.00 ; #0,00
		/// </summary>
		public string XNumberFormat { get; set; } = "";
		/// <summary>
		/// Не работает: #,##; #.##;
		/// Работает: #0.00 ; #0,00
		/// </summary>
		public string YNumberFormat { get; set; } = "";

		public ReportChartTmpl<T> Clone()
		{
			var rc = (ReportChartTmpl<T>)this.MemberwiseClone();
			rc.TitleSettings = TitleSettings.Clone();
			rc.TitleXSettings = TitleXSettings.Clone();
			rc.TitleYSettings = TitleYSettings.Clone();
			rc.AxesXTitles = new List<string>(AxesXTitles);

			rc.Items = new List<T>();
			foreach (var itm in Items)
			{
				rc.Items.Add(itm.Clone());
			}

			rc.GraphRect = new Rectangle(GraphRect.Location, GraphRect.Size);
			rc.LegendSettings = LegendSettings.Clone();
			rc.YLabelsSettings = YLabelsSettings.Clone();
			rc.XLabelsSettings = XLabelsSettings.Clone();
			if (Pic != null)
				rc.Pic = new Bitmap(Pic);

			rc.YMajorGridLinesColor = YMajorGridLinesColor.Copy();
			rc.YMinorGridLinesColor = YMinorGridLinesColor.Copy();

			rc.XMajorGridLinesColor = XMajorGridLinesColor.Copy();
			rc.XMinorGridLinesColor = XMinorGridLinesColor.Copy();

			rc.YAxisLinesColor = YAxisLinesColor.Copy();
			rc.XAxisLinesColor = XAxisLinesColor.Copy();
			return rc;
		}

		public IReportChart<IReportChartItem> Convert()
		{
			return new ReportChartObj()
			{
				AxesXTitles = this.AxesXTitles.Copy(),
				GraphRect = this.GraphRect.Copy(),
				HasXMajorTickMark = this.HasXMajorTickMark,
				HasYMajorTickMark = this.HasYMajorTickMark,
				IsAxisYRevers = this.IsAxisYRevers,
				Items = this.Items.ConvertAll((v) => v.Convert()),
				LegendSettings = this.LegendSettings.Clone(),
				MaxX = this.MaxX,
				MaxY = this.MaxY,
				MinX = this.MinX,
				MinY = this.MinY,
				Pic = this.Pic.Copy(),
				ShowLegend = this.ShowLegend,
				Title = this.Title,
				TitleSettings = this.TitleSettings.Clone(),
				TitleX = this.TitleX,
				TitleXSettings = this.TitleXSettings.Clone(),
				TitleY = this.TitleY,
				TitleYSettings = this.TitleYSettings.Clone(),
				XAxisHasMinorGridLines = this.XAxisHasMinorGridLines,
				XAxisLinesColor = this.XAxisLinesColor.Copy(),
				XLabelsSettings = this.XLabelsSettings.Clone(),
				XMajorGridLinesColor = this.XMajorGridLinesColor.Copy(),
				XMajorUnits = this.XMajorUnits,
				XMinorGridLinesColor = this.XMinorGridLinesColor.Copy(),
				XNumberFormat = this.XNumberFormat,
				YAxisHasMinorGridLines = this.YAxisHasMinorGridLines,
				YAxisLinesColor = this.YAxisLinesColor.Copy(),
				YLabelsSettings = this.YLabelsSettings.Clone(),
				YMajorGridLinesColor = this.YMajorGridLinesColor.Copy(),
				YMajorUnits = this.YMajorUnits,
				YMinorGridLinesColor = this.YMinorGridLinesColor.Copy(),
				YNumberFormat = this.YNumberFormat
			};
		}
	}
}
