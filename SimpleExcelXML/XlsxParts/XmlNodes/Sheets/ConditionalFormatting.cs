﻿using SimpleCore.Templates;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Sheets
{
	internal class ConditionalFormatting
	{

		public CellRangeTmpl Range { get; set; }
		public string Type { get; set; }
		public string ValueType { get; set; }
		public int DxId { get; set; }
		public int Priority { get; set; }
		public string Operator { get; set; }		
		public int Rank { get; set; }
		public int Precent { get; set; }
		
		public List<ISaveable> Data { get; set; }

		public ConditionalFormatting()
		{

		}


	}
}
