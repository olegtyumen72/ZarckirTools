﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SimpleExcelXML.Tools
{
	/// <summary>
	/// Основной класс для работы с xml файлами, для сохранения
	/// </summary>
	public class XmlWriterWrapper : IDisposable
	{

		//bool isAtrOpen = false;
		
		bool hasDeclaration = false;
		/// <summary>
		/// Открыт ли последний созданный элемент и не закрыт
		/// </summary>
		bool isLastOpen = false;
		/// <summary>
		/// Сколько элементов не закрыто
		/// </summary>
		int forClose = 0;
		/// <summary>
		/// Сам класс, для которого писала оболочка
		/// </summary>
		XmlWriter writer;
		/// <summary>
		/// Путь куда сохранять
		/// </summary>
		string path;
		/// <summary>
		/// Неймспейсы + префиксы
		/// </summary>
		Dictionary<string, string> nss = new Dictionary<string, string>();
		/// <summary>
		/// Последний использованный неймспейс + префикс его
		/// </summary>
		KeyValuePair<string, string> lastNs = new KeyValuePair<string, string>();
		/// <summary>
		/// Последний атрибут пример: Val, X, Y
		/// </summary>
		string lastAtr = "";

		public XmlWriterWrapper(string filepath)
		{
			Encoding utf8noBOM = new UTF8Encoding(false);
			path = filepath;

			var set = new XmlWriterSettings()
			{
				Indent = true,
				Encoding = utf8noBOM
			};
			writer = XmlWriter.Create(filepath, set);
		}

		void InitNewElem()
		{
			forClose++;
			isLastOpen = true;
		}

		bool IsWritable(string val)
		{
			return !String.IsNullOrEmpty(val);
		}

		/// <summary>
		/// Добавить неймспейс
		/// </summary>
		/// <param name="name"></param>
		/// <param name="info"></param>
		/// <returns></returns>
		public XmlWriterWrapper AddDeclaration(string name, string info)
		{
			if (hasDeclaration) throw new Exception("Declaration was alredy added");
			

			writer.WriteProcessingInstruction(name, info);
			hasDeclaration = true;

			return this;
		}

		/// <summary>
		/// Добавить элементу последний использованный атрибут
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public XmlWriterWrapper AddLastAtr(string value)
		{
			return AddAtribute(lastAtr, value);
		}

		public XmlWriterWrapper AddLastAtr(object value)
		{
			return AddAtribute(lastAtr, value);
		}

		public XmlWriterWrapper AddXmlnsAtr(string name, string value)
		{
			return AddAtribute(name, value, "xmlns");
		}

		public XmlWriterWrapper AddXmlnsAtr(string name, object value)
		{
			return AddAtribute(name, value, "xmlns");
		}

		public XmlWriterWrapper AddAtribute(string name = null, object value = null, string prefix = null, string uri = null)
		{
			return AddAtribute(name, value.ToString(), prefix, uri);
		}

		public XmlWriterWrapper AddAtribute(string name = null, string value = null, string prefix = null, string uri = null)
		{
			//CheckAtr();

			if (!isLastOpen)
			{
				throw new Exception("Miss node element");
			}

			lastAtr = name;

			if (IsWritable(prefix))
				writer.WriteAttributeString(prefix, name, uri, value);
			else if (IsWritable(uri))
				writer.WriteAttributeString(name, uri, value);
			else if (IsWritable(value) && IsWritable(name))
				writer.WriteAttributeString(name, value);
			else
				throw new Exception("miss args for atribute");

			//isAtrOpen = true;
			if (uri == null)
				AddNamespace(prefix, value, name);
			else
				AddNamespace(prefix, uri, name);

			return this;
		}

		void AddNamespace(string prefix = null, string uri = null, string name = null)
		{
			if (IsWritable(prefix))
			{

				if (uri != null && !nss.ContainsKey(uri))
				{

					if (prefix.ToLower() == "xmlns" && IsWritable(uri))
					{
						nss.Add(uri, name);
					}
					else
					{
						nss.Add(uri, prefix);
					}
					lastNs = nss.Last();
				}
			}
		}

		public XmlWriterWrapper OpenRefElement(string name, string ns = "", string value = "")
		{
			if (!nss.ContainsKey(ns)) throw new Exception("Miss namespace for element");

			if (lastNs.Key != ns)
			{
				lastNs = nss.FirstOrDefault((v) => v.Key == ns);
			}

			var prefix = lastNs.Value;
			var uri = ns;

			return OpenElement(name, value, ns, prefix);
		}		

		public XmlWriterWrapper OpenPrefElement(string name, string prefix = "", string value = "")
		{
			if (!String.IsNullOrEmpty(prefix) && lastNs.Value != prefix)
				lastNs = nss.First((v) => v.Value == prefix);

			var elem = lastNs;
			var ns = elem.Key;

			if (!nss.ContainsKey(ns)) throw new Exception("Miss namespace for element");

			return OpenElement(name, value, ns, prefix);
		}

		public XmlWriterWrapper OpenPrefElement(string name, string prefix, object value)
		{
			return OpenPrefElement(name, prefix, value.ToString());
		}

		public XmlWriterWrapper UpdateValue(string value)
		{
			if (!isLastOpen) throw new Exception("Before insert node value, create node");

			if (IsWritable(value))
				writer.WriteString(value);

			return this;
		}

		public XmlWriterWrapper OpenElement(string name, string value = "", string ns = "", string prefix = "")
		{
			InitNewElem();

			var goodNs = IsWritable(ns);

			AddNamespace(prefix, ns, name);

			if (IsWritable(prefix) && goodNs)
				writer.WriteStartElement(prefix, name, ns);
			else if (goodNs)
				writer.WriteStartElement(name, ns);
			else
				writer.WriteStartElement(name);

			if (IsWritable(value))
				writer.WriteString(value);

			return this;
		}

		public XmlWriterWrapper UpdateElement(string value)
		{
			if (!isLastOpen) throw new Exception("Last element is close. Cant update");

			writer.WriteString(value);

			return this;
		}

		public XmlWriterWrapper CloseElement()
		{
			forClose--;
			isLastOpen = false;
			writer.WriteEndElement();

			return this;
		}

		public XmlWriterWrapper Save()
		{

			for (int i = 0; i < forClose; i++)
				writer.WriteEndElement();

			writer.Flush();
			writer.Close();

			return this;
		}


		public void Dispose()
		{
			writer.Dispose();
		}
	}
}
