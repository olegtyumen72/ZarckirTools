﻿using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;
using SimpleExcelXML.XlsxParts.XmlNodes.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Charts
{
	internal class XYChart: XmlPropBase, IXmlChart
	{
		public static string Word { get; } = "scatterChart";

		public bool varyColors { get; set; } = false;
		public List<Ser> Data { get; set; } = new List<Ser>();
		public DLbls dLbls { get; set; } = DLbls.Default;
		public List<TypedAx> Axes { get; set; } = new List<TypedAx>();
		public string Prefix { get; set; } = "c";
		public string KeyWord { get { return Word; } }

		public ScatterStyle scatterStyle = ScatterStyle.smoothMarker;							
		
		///// <summary>
		///// Настройки линии, если нужно прозрачную - заполнить дефолтно
		///// </summary>
		//public SpPr ChartProp { get; set; } = SpPr.DefaultSpPr;
		//public Marker Marker { get; set; } = null;

		public XYChart(string pref = "c", bool varyColors = false, ScatterStyle style = ScatterStyle.smoothMarker)
		{
			Prefix = pref;
			this.varyColors = varyColors;
			scatterStyle = style;
		}

		public XYChart(XElement xScatter) : base(xScatter)
		{
		}

		internal void AddAxe(TypedAx main, TypedAx connecte)
		{
			main.crossAx = connecte.axId;
			connecte.crossAx = connecte.axId;

			Axes.Add(main);
			Axes.Add(connecte);
		}

		public void AddData(Ser data)
		{
			Data.Add(data);
		}

		public override void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("scatterChart", Prefix);

			doc.OpenPrefElement(nameof(scatterStyle), "c").AddAtribute("val", scatterStyle.ToString()).CloseElement();
			doc.OpenPrefElement(nameof(varyColors), "c").AddLastAtr((varyColors ? 1 : 0).ToString()).CloseElement();

			foreach (var el in Data)
			{
				el.Save(doc);
			}

			dLbls?.Save(doc);

			foreach (var axe in Axes)
			{
				doc.OpenPrefElement("axId", "c").AddAtribute("val", axe.axId.ToString()).CloseElement();
			}

			doc.CloseElement();
		}

		public override void TryLoad(XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");
			scatterStyle = ParseStyle(root.Element(c + nameof(scatterStyle))?.Attribute("val")?.Value);
			varyColors = ParseBool(root.Element(c + nameof(varyColors))?.Attribute("val")?.Value);

			var sers = root.Elements().Where((v) => v.Name.LocalName.Contains("ser"));
			foreach (var ser in sers)
				Data.Add(new Ser(ser));

			var dlb = root.Element(c + "dLbls");
			if (dlb.HasInfo())
				dLbls = new DLbls(dlb);

			var axIds = root.Elements(c + "axId");
			foreach (var el in axIds)
			{
				Axes.Add(new TypedAx() { axId = el.Attribute("val").Value.ToInt() });
			}
		}

		private ScatterStyle ParseStyle(string value)
		{
			var res = ScatterStyle.smoothMarker;
			if (String.IsNullOrEmpty(value))
				return res;

			if (Enum.TryParse(value, out res))
				return res;

			return ScatterStyle.smoothMarker;
		}

		private bool ParseBool(string str)
		{
			if (String.IsNullOrEmpty(str))
				return false;

			var i = str.ToInt();
			if (i == 1) return true;
			else return false;
		}

		internal static bool IsXYChart(string name)
		{
			var res = false;
			var lower = name.Trim().ToLower();

			if (lower.Contains("linechart") || lower.Contains("scatterchart"))
				res = true;

			return res;
			
		}
	}
}
