﻿using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Styles
{
	internal class BorderElement
	{
		public static BorderElement Default(Elem name)
		{
			return new BorderElement(name)
			{
				Style = StyleElem._thin,
				Clr = new ExcelColor()
				{
					Index = 64
				}
			};
		}

		public enum StyleElem { _thin, _medium, _double, _none }
		public enum Elem { left, right, top, bottom, diagonal }

		public Elem Name { get; set; }
		//default index 64
		public ExcelColor Clr { get; set; }
		public StyleElem Style { get; set; }
		public bool IsEmpty
		{
			get
			{
				return Style == StyleElem._none && Clr.IsEmpty;
			}
		}

		public BorderElement(Elem type)
		{
			Name = type;
			Style = StyleElem._none;
			Clr = ExcelColor.ColorElem();
			//Clr.Index = 64;
		}

		public BorderElement(Elem type, int index) : this(type)
		{
			Clr.Index = index;
		}

		public void Update(BorderElement be)
		{
			if (Name != be.Name)
				return;

			Clr = be.Clr.Clone();
			Style = be.Style;
		}

		public void Load(XElement element)
		{
			var atrs = XDocumentTools.ParseAtrs(element);
			Name = ParseName(element.Name.LocalName);
			if (atrs.ContainsKey("style"))
			{
				Style = ParseStyle(atrs["style"]);
			}
			if (element.HasElements)
			{
				Clr.Load(element.Elements().First());
			}
		}

		private Elem ParseName(string elemName)
		{
			var res = Elem.diagonal;
			if (Enum.TryParse(elemName, out res))
			{
				return res;
			}

			var tEnum = typeof(Elem);
			foreach (var name in Enum.GetNames(tEnum))
			{
				if (elemName.Contains(name))
					return (Elem)Enum.Parse(tEnum, name);
			}

			throw new Exception("Miss enum");
		}

		private StyleElem ParseStyle(string styleStr)
		{
			var res = StyleElem._double;
			var tmp = "_" + styleStr;
			if (Enum.TryParse(tmp, out res))
			{
				return res;
			}

			var tEnum = typeof(StyleElem);
			foreach (var name in Enum.GetNames(tEnum))
			{
				if (tmp.Contains(name))
					return (StyleElem)Enum.Parse(tEnum, name);
			}

			throw new Exception("Miss enum");
		}

		internal void Save(XmlWriterWrapper doc)
		{
			doc.OpenElement(Name.ToString());
			if (Style != StyleElem._none)
				doc.AddAtribute("style", Style.ToString().Replace("_", ""));
			Clr?.Save(doc);
			doc.CloseElement();
		}


	}
}
