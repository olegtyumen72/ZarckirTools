﻿using SimpleExcelXML.XlsxParts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.Interfaces
{
	/// <summary>
	/// Старая версия общего интерфейса для всех xml файлов. Следует переработать
	/// </summary>
	interface IXmlFile
	{
		string RelativePath { get; }
		string File { get; }
		string AbsolutePath { get; set; }
		string AbsoluteToFile();
		string RelativeToFile();
		void Save();

	}

	interface IXmlVisibleFile : IXmlFile
	{
		string Id { get; set; }
	}

	interface IXmlVisibleSheet: IXmlVisibleFile
	{
		void AddGrapth(XmlDrawing draw);
	}
}
