﻿using AssociativeList;
using DbConnector.Core;
using DbConnector.Tools;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace DbConnector.AlConnector
{
	public class MySqlAlConnector : IDbAlConnector
	{
		private readonly MySqlConnection _con;
		private readonly Action<string> _logMsg;
		private readonly Action<AList> _logData;

		public SqlVers SqlVersion
		{
			get
			{
				return SqlVers.MySql;
			}
		}

		public MySqlAlConnector(string connstring, Action<string> logMsg = null, Action<AList> logData = null)
		{
			_con = new MySqlConnection(connstring);
			_logMsg = logMsg;
			_logData = logData;
			_con.Open();
		}


		public void Dispose()
		{
			_con?.Close();
			_con?.Dispose();
		}

		public AList GetDictionary(string table)
		{
			var query = $"Select * from {table}";
			return Query(query);
		}

		public V GetLast<V>(string table, string col)
		{
			col = DbTools.AddMySqlBracets(col);
			var res = SelectQuery(table, col, null, $"Order by {col} DESC LIMIT 1");
			if (res.FirstOrDefault().Value.Count <= 0 || res.FirstOrDefault().Value[0] is bool)
				switch (typeof(V).Name.ToLower())
				{
					case "int":
					case "int32":
					case "int64":
					case "int16":
					case "double":
					case "float":
						{
							return (V)Convert.ChangeType(1, typeof(V));
						}
					default:
						{
							return default(V);
						}
				}
			return res.First().Value.Cast<V>().FirstOrDefault();
		}

		public bool InsertQuery(string table, string col, string values)
		{
			var query = $"insert into {table}({col}) VALUES {values}";
			return GetRes(Query(query));
		}

		private bool GetRes(AList associativeList)
		{
			return associativeList.FirstOrDefault().Value.Cast<bool>().FirstOrDefault();
		}

		public AList Query(string query)
		{
			_logMsg?.Invoke(query);
			using (var cmd = _con.CreateCommand())
			{
				cmd.CommandText = query;
				try
				{
					var reader = cmd.ExecuteReader();
					//if (reader.HasRows)
					//{
					var table = new AList();
					table.Load(reader);
					if (table.Count <= 0)
					{
						var res = GetBoolRes(true);
						_logData?.Invoke(res);
						return res;

					}
					else
					{
						_logData?.Invoke(table);
						return table;
					}
					//}
					//else
					//{
					//	return GetBoolRes(true);
					//}
				}
				catch (Exception e)
				{
					var res = GetBoolRes(false);
					_logData?.Invoke(res);
					return res;
				}
			}
		}

		public AList SelectQuery(string table, string col, string where = "", string etc = "")
		{
			var query = $"Select {col} from {table}";
			if (where?.Length > 0)
			{
				query += $" WHERE {where}";
			}
			if (etc?.Length > 0)
			{
				query += $" {etc}";
			}

			return Query(query);
		}

		public bool UpdateWithDuplicate(string table, string col, string value)
		{
			var cols = col.Split(',');
			var colVal = new string[cols.Length];
			for (int i = 0; i < cols.Length; i++)
			{
				colVal[i] = $"{cols[i]}=VALUES({cols[i]})";
			}

			var query = $"insert into {table}({col}) VALUES {value} ON DUPLICATE KEY UPDATE {String.Join(",", colVal)}";

			return GetRes(Query(query));
		}

		public V GetKeyFromDb<V>(string tbl, string col, string matchCol, string matchVal)
		{
			return GetKeyFromDbW<V>(tbl, col, matchCol + " = \"" + matchVal + "\"");
		}

		public V GetKeyFromDic<V>(string table, string col, string target)
		{
			var tmp = DbTools.RemoveMySqlBracets(col);
			col = DbTools.AddMySqlBracets(col);
			var data = SelectQuery(table, col, "`Name_eng` = \"" + target + "\"");
			data = IsEmpty(data, tmp) ? SelectQuery(table, col, "`Name_rus` = \"" + target + "\"") : data;

			if (IsEmpty(data, tmp))
			{
				return default(V);
			}
			else
			{
				return (V)Convert.ChangeType(data[tmp][0], typeof(V));
			}
		}

		public V GetKeyFromDbW<V>(string table, string col, string where)
		{
			col = DbTools.AddMySqlBracets(col);
			var data = SelectQuery(table, col, where);
			col = DbTools.RemoveMySqlBracets(col);

			if (IsEmpty(data, col))
			{
				switch (typeof(V).Name.ToLower())
				{
					default:
						{
							return default(V);
						}
					case "int":
					case "int32":
					case "int64":
					case "double":
					case "float":
						{
							return (V)Convert.ChangeType(-1, typeof(V));
						}
				}
			}
			else
			{
				return (V)Convert.ChangeType(data[col][0], typeof(V));
			}
		}

		public bool DeleteQuery(string table, string where)
		{
			string query = $"DELETE FROM {table} WHERE {where}";
			return GetRes(Query(query));
		}

		private AList GetBoolRes(bool v)
		{
			var tmp = new AList
			{
				{ "0", new List<bool>() { v } }
			};
			return tmp;
		}

		//bool IsEmpty(AList data, string col)
		//{
		//	if (data.ContainsKey("0"))
		//	{
		//		return true;
		//	}
		//	return data[col] == null || data[col].Count <= 0;
		//}

		public bool IsEmpty(AList data, string col = "")
		{
			if (data == null) return true;
			col = col.Length <= 0 ? data.First().Key : col;
			return
				data == null ||
				data.Values.First() == null ||
				data.Values.First().Count <= 0 ||
				(data.Values.First().Count == 1 && data.Values.First()[0].GetType().Name.ToLower() == "boolean" && data.Keys.First() == "0");
		}


		public bool DeleteTable(string table)
		{
			throw new NotImplementedException();
		}

		public bool TruncateTable(string table)
		{
			throw new NotImplementedException();
		}

		public bool CreateTable(string table, List<string> col, List<string> etc)
		{
			throw new NotImplementedException();
		}

		public ISqlPartData<AList> SelectPartial(string table, string col, int offset, int count, string orderedCols = "", string where = "", bool isLazy = true)
		{
			orderedCols = orderedCols.Length <= 0 ? col : orderedCols;
			return new PartialSqlData<AList>(count, offset,
			(cnt, off) =>
			{
				var order = $"LIMIT {off},{cnt}"; //$"Order by {orderedCols} {off},{cnt}";
				var res = SelectQuery(table, col, where, order);
				return res;
			}
			, (v) => IsEmpty(v), isLazy);
		}

		public bool CreateDb(string name)
		{
			return GetRes(Query($"create database `{name}`"));
		}

		public bool DropDb(string name)
		{
			return GetRes(Query($"drop database '{name}'"));
		}

		public bool CngConDb(string name)
		{
			try
			{
				_con.ChangeDatabase(name);
				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}
