﻿using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Templates
{
	public class CellRangeTmpl : ICellRange, ISimpleConvert<ICellRange>
	{
		public int cell1_Col { get; set; } = -1;
		public int cell1_Row { get; set; } = -1;
		public int cell2_Col { get; set; } = -1;
		public int cell2_Row { get; set; } = -1;

		/// <summary>
		/// если хотя бы один из параметров -1, то считаем пустым
		/// </summary>
		/// <returns></returns>
		public bool isEmpty()
		{
			if (cell1_Col <= -1 || cell2_Col <= -1 || cell1_Row <= -1 || cell2_Row <= -1) return true;
			else return false;
		}

		public ICellRange Clone()
		{
			var res = (ICellRange)MemberwiseClone();
			return res;
		}

		/// <summary>
		/// Рендж зависит от колонок,
		/// т.е. представляет из себя строчку
		/// </summary>
		public bool IsColumn
		{
			get { return cell1_Row == cell2_Row; }
		}

		/// <summary>
		/// Рендж зависит от строчек,
		/// т.е. представлет из себя колонку
		/// </summary>
		public bool IsRow
		{
			get { return cell1_Col == cell2_Col; }
		}

		/// <summary>
		/// Рендж зависит от колонок и строчек,
		/// т.е. представляет из себя подобие прямоугольника
		/// </summary>
		public bool IsRect
		{
			get { return cell1_Col != cell2_Col && cell1_Row != cell2_Row; }
		}

		public bool HasEnd
		{
			get
			{
				return cell1_Col >= 0 && cell2_Col >= 0 && cell1_Row >= 0 && cell2_Row >= 0;
			}
		}

		public CellRangeTmpl(int Cell1_Col, int Cell1_Row, int Cell2_Col, int Cell2_Row)
		{
			cell1_Col = Cell1_Col;
			cell1_Row = Cell1_Row;
			cell2_Col = Cell2_Col;
			cell2_Row = Cell2_Row;
		}

		public CellRangeTmpl(ICellRange range)
		{
			cell1_Col = range.cell1_Col;
			cell1_Row = range.cell1_Row;
			cell2_Col = range.cell2_Col;
			cell2_Row = range.cell2_Row;
		}

		public int RowCount()
		{
			return cell2_Row - cell1_Row;
		}

		public int ColumnCount()
		{
			return cell2_Col - cell1_Col;
		}

		public bool Contains(ICellRange other)
		{
			return other.cell1_Col >= cell1_Col && other.cell2_Col <= cell2_Col && other.cell1_Row >= cell1_Row && other.cell2_Row <= cell2_Row;
		}

		public bool ContainRow(int row)
		{
			return cell1_Row <= row && cell2_Row >= row;
		}

		public bool ContainColumn(int col)
		{
			return cell1_Col <= col && cell2_Col >= col;
		}

		public ICellRange Convert()
		{
			return this;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is ICellRange)) return false;

			var tmp = (ICellRange)obj;

			return tmp.cell1_Col == cell1_Col && tmp.cell1_Row == cell1_Row && tmp.cell2_Col == cell2_Col && tmp.cell2_Row == cell2_Row;
		}

		public bool NearRow(int row)
		{
			return cell1_Row - 1 == row || cell1_Row == row || cell2_Row + 1 == row || cell2_Row == row;
		}

		public bool NearColumn(int col)
		{
			return cell1_Col - 1 == col || cell1_Col == col || cell2_Col + 1 == col || cell2_Col == col;
		}

		public bool NearCol1(int col)
		{
			return cell1_Col - 1 == col || cell1_Col == col;
		}

		public bool NearCol2(int col)
		{
			return cell2_Col + 1 == col || cell2_Col == col;
		}

		public int NearColumnCell(int col)
		{
			if (cell1_Col - 1 == col || cell1_Col == col)
				return cell1_Col;
			else if (cell2_Col + 1 == col || cell2_Col == col)
				return cell2_Col;

			return int.MinValue;
		}

		public int NearColumnCell(CellRangeTmpl range)
		{
			if (range.NearColumn(cell1_Col))
			{
				return cell1_Col;
			}
			if (range.NearColumn(cell2_Col))
			{
				return cell2_Col;
			}

			return int.MinValue;
		}

		public int NearRowCell(CellRangeTmpl range)
		{
			if (range.NearRow(cell1_Row))
			{
				return cell1_Row;
			}
			if (range.NearRow(cell2_Row))
			{
				return cell2_Row;
			}

			return int.MinValue;
		}

		public int NearRowCell(int row)
		{
			if (cell1_Row - 1 == row || cell1_Row == row)
				return cell1_Row;
			else if (cell2_Row + 1 == row || cell2_Row == row)
				return cell2_Row;

			return int.MinValue;
		}

		public void ExpandColumn(int minCol, int maxCol)
		{
			if (cell1_Col >= minCol)
				cell1_Col = minCol;
			if (cell2_Col <= maxCol)
				cell2_Col = maxCol;
			//if(keyCol == cell1_Col)
			//{
			//	cell1_Col = minCol;
			//}
			//else if(keyCol == cell2_Col)
			//{
			//	cell2_Col = maxCol;
			//}

			//throw new Exception("Wrong range for expand");
		}

		public void ExpandColumn(int col)
		{
			if (cell1_Col >= col)
				cell1_Col = col;
			else if (cell2_Col <= col)
				cell2_Col = col;

			//if (keyCol == cell1_Col)
			//{
			//	cell1_Col = col;
			//}
			//else if (keyCol == cell2_Col)
			//{
			//	cell2_Col = col;
			//}

			//throw new Exception("Wrong range for expand");
		}

		public void ExpandRow(int minRow, int maxRow)
		{

			if (cell1_Row >= minRow)
				cell1_Row = minRow;
			if (cell2_Row <= maxRow)
				cell2_Row = maxRow;

			//if (keyRow == cell1_Row)
			//{


			//}
			//else if (keyRow == cell2_Row)
			//{
			//	cell2_Row = maxRow;
			//}

			//throw new Exception("Wrong range for expand");
		}

		public override string ToString()
		{
			return $"LeftTop:{{{cell1_Col};{cell1_Row}}} BottomRight{{{cell2_Col}:{cell2_Row}}}";
		}
	}
}
