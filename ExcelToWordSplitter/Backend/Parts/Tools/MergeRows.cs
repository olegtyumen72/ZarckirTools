﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToWordSplitter.Backend.Parts.Tools
{
	/// <summary>
	/// Сливает строки index1, index2 по всем Cols
	/// </summary>
	public class MergeRows : ColBase
	{
		public List<int> Cols = new List<int>();
		public int Index2 = -1;
	}
}
