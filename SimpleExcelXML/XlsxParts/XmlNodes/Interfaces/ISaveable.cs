﻿using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Interfaces
{
	public interface ISaveable
	{
		/// <summary>
		/// Класс который можно сохранить с помощью враппер-xml
		/// </summary>
		/// <param name="doc"></param>
		void Save(XmlWriterWrapper doc);


	}
}
