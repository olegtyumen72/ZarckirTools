﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Parts
{
	public interface ISimpleConvert<T>
	{

		T Convert();


	}
}
