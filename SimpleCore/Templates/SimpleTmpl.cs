﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleCore.Parts;

namespace SimpleCore.Templates
{
	public abstract class SimpleTmpl<E, F, J> : ISimpleCore<E, F, J>
		where E : ISimpleConvert<IReportBook>
		where F : ISimpleConvert<IReportSheet>
		where J : ISimpleConvert<IReportLine>
	{
		public SimpleConverter<IReportBook, E> RbConv { get; }
		public SimpleConverter<IReportSheet, F> RsConv { get; }
		public SimpleConverter<IReportLine, J> RlConv { get; }

		//public SimpleConverter<IReportBook, ReportBook> RbConv { get; }
		//public SimpleConverter<IReportSheet, ReportSheet> RsConv { get; }
		//public SimpleConverter<IReportLine, ReportLine> RlConv { get; }

		public event Action<int> UpdateMaxState;
		public event Action<int> UpdateCurState;
		public event Action<double> UpdateProcState;
		public event Action WorkEnd;

		public SimpleTmpl()
		{
			//RsConv = new SimpleConverter<IReportSheet, ReportSheetTmpl<V>>(
			//	(v) => (ReportSheetTmpl<V>)Activator.CreateInstance(typeof(ReportSheetTmpl<V>), v),
			//	(v) => v.Convert()
			//	);
			//RlConv = new SimpleConverter<IReportLine, ReportLineTmpl>((v) => new ReportLineTmpl(v), (v) => v);
			//RbConv = new SimpleConverter<IReportBook, ReportBookTmpl<T>>(
			//	(v) => (ReportBookTmpl<T>)Activator.CreateInstance(typeof(ReportBookTmpl<T>), v),
			//	(v) => v.Convert()
			//	);

			RlConv = new SimpleConverter<IReportLine, J>(
				(v) => (J)Activator.CreateInstance(typeof(J), v),
				(v) => v.Convert()
			);

			RsConv = new SimpleConverter<IReportSheet, F>(
				(v) => (F)Activator.CreateInstance(typeof(F), v),
				(v) => v.Convert()
			);

			RbConv = new SimpleConverter<IReportBook, E>(
				(v) => (E)Activator.CreateInstance(typeof(E), v),
				(v) => v.Convert()
			);

		}

		public abstract void Dispose();

		public abstract IReportBook Load(string path);

		public abstract void Save(object obj);

		public abstract void Save(object obj, string path);

		public abstract void SaveAs(object obj, string path = "");

		public abstract Task SaveAsync(object obj);

		public abstract Task SaveAsync(object obj, string path);

		public abstract IReportSheet UpdateReportSheetWithTranslater(IReportSheet rs, ITranslater tr = null);
	}
}
