﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;
using SimpleExcelXML.XlsxParts.XmlNodes.Properties;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Charts
{
	internal class TypedAx : XmlPropBase
	{
		public enum TypeAx { catAx, valAx }

		public TypeAx type { get; set; } = TypeAx.catAx;
		public int axId { get; set; } = -1;
		public Scaling scaling { get; set; } = Scaling.Default;
		public bool delete { get; set; } = false;
		public AxePos axPos { get; set; } = AxePos.b;
		public SpPr majorGridlines { get; set; } = null;
		public SpPr minorGridlines { get; set; } = null;
		public Title Title { get; set; } = null;
		/// <summary>
		/// Формат отображение чисел
		/// </summary>
		public NumFmt numFmt { get; set; } = NumFmt.Default;
		public TickMark majorTickMark { get; set; } = TickMark._none;
		public TickMark minorTickMark { get; set; } = TickMark._none;
		public TickPos tickLblPos { get; set; } = TickPos.nextTo;
		public SpPr spPr { get; set; } = null;

		public TxPr txPr { get; set; } = new TxPr("c",
			new BodyPr("a", "ellipsis", "horz", "square", "ctr", 1, 1),
			LstStyle.Default,
			new DefRPr("defRPr", "a", 900, 0, 0, Underline.none, "noStrike", 0, -1, new SolidFill("a", 65000, 35000, "tx1")));
		public int crossAx { get; set; } = -1;
		public string crosses { get; set; } = "autoZero";
		public string crossBetween { get; set; } = "between";
		public bool auto { get; set; } = true;
		public string lblAlgn { get; set; } = "ctr";
		public int lblOffset { get; set; } = 100;
		public string noMultiLvlLbl { get; set; } = "0";
		public string prefix { get; set; } = "c";
		public double majorUnit { get; set; } = -1;
		public double minorUnit { get; set; } = -1;
		public object Tag { get; set; }

		public TypedAx() : base()
		{
			//spPr.Ln.fill.schemeColor = Color.LightGray;
		}

		public TypedAx(XElement root) : base(root)
		{
		}

		public override void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement(type.ToString(), prefix)
				.OpenPrefElement(nameof(axId), "c").AddAtribute("val", axId.ToString()).CloseElement();

			scaling?.Save(doc);

			doc.OpenPrefElement(nameof(delete)).AddLastAtr((delete ? 1 : 0).ToString()).CloseElement()
			.OpenPrefElement(nameof(axPos)).AddLastAtr(axPos.ToString()).CloseElement();

			if (type == TypeAx.valAx)
			{
				if (majorGridlines != null)
				{
					doc.OpenPrefElement(nameof(majorGridlines), "c");
					majorGridlines?.Save(doc);
					doc.CloseElement();
				}

				if (minorGridlines != null)
				{
					doc.OpenPrefElement(nameof(minorGridlines), "c");
					minorGridlines?.Save(doc);
					doc.CloseElement();
				}

				numFmt?.Save(doc);
				//doc.OpenPrefElement(nameof(numFmt), "c").AddAtribute("formatCode", numFmt).CloseElement();
			}

			Title?.Save(doc);

			doc.OpenPrefElement(nameof(majorTickMark)).AddLastAtr(majorTickMark.ToString().Remove(0, 1)).CloseElement()
				.OpenPrefElement(nameof(minorTickMark)).AddLastAtr(minorTickMark.ToString().Remove(0, 1)).CloseElement()
				.OpenPrefElement(nameof(tickLblPos)).AddLastAtr(tickLblPos.ToString()).CloseElement();

			spPr?.Save(doc);

			txPr?.Save(doc);

			doc.OpenPrefElement(nameof(crossAx), "c").AddAtribute("val", crossAx.ToString()).CloseElement()
				.OpenPrefElement(nameof(crosses)).AddLastAtr(crosses).CloseElement();

			if (type == TypeAx.catAx)
			{
				doc.OpenPrefElement(nameof(auto)).AddLastAtr((auto ? 1 : 0).ToString()).CloseElement()
					.OpenPrefElement(nameof(lblAlgn)).AddLastAtr(lblAlgn).CloseElement()
					.OpenPrefElement(nameof(lblOffset)).AddLastAtr(lblOffset.ToString()).CloseElement()
					.OpenPrefElement(nameof(noMultiLvlLbl)).AddLastAtr(noMultiLvlLbl).CloseElement();
			}
			else
			{
				doc.OpenPrefElement(nameof(crossBetween), "c").AddAtribute("val", crossBetween).CloseElement();
			}

			if (majorUnit > 0)
			{
				doc.OpenPrefElement(nameof(majorUnit), "c").AddAtribute("val", majorUnit).CloseElement();
			}

			if (minorUnit > 0)
			{
				doc.OpenPrefElement(nameof(minorUnit), "c").AddAtribute("val", minorUnit).CloseElement();
			}

			doc.CloseElement();
		}

		public override void TryLoad(XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");

			axId = root.Element(c + "axId").Attribute("val").Value.ToInt();
			delete = ParseToBool(root.Element(c + nameof(delete))?.Attribute("val")?.Value);
			axPos = ParseAxPos(root.Element(c + nameof(axPos))?.Attribute("val")?.Value);
			majorTickMark = ParseTickMark(root.Element(c + nameof(majorTickMark))?.Attribute("val")?.Value);
			minorTickMark = ParseTickMark(root.Element(c + nameof(minorTickMark))?.Attribute("val")?.Value);
			tickLblPos = ParseTickLib(root.Element(c + nameof(tickLblPos))?.Attribute("val")?.Value);
			crossAx = root.Element(c + nameof(crossAx))?.Attribute("val")?.Value?.ToInt() ?? -1;
			crosses = root.Element(c + nameof(crosses))?.Attribute("val")?.Value ?? "autoZero";
			majorUnit = root.Element(c + nameof(majorUnit))?.Attribute("val")?.Value?.ToInt() ?? -1;
			minorUnit = root.Element(c + nameof(minorUnit))?.Attribute("val")?.Value?.ToInt() ?? -1;

			type = ParseAxType(root.Name.LocalName);

			var xScal = root.Element(c + "scaling");
			if (xScal.HasInfo())
				scaling = new Scaling(xScal);

			if (type == TypeAx.valAx)
			{
				var xMajor = root.Element(c + nameof(majorGridlines))?.GetSpPr();
				if (xMajor.HasInfo())
					majorGridlines = new SpPr(xMajor);

				var xMinor = root.Element(c + nameof(minorGridlines))?.GetSpPr();
				if (xMinor.HasInfo())
					minorGridlines = new SpPr(xMinor);

				var xNumFmt = root.Element(c + nameof(numFmt));
				if (xNumFmt.HasInfo())
					numFmt = new NumFmt(xNumFmt);
			}

			var xTitle = root.GetTitle();
			if (xTitle.HasInfo())
				Title = new Title(xTitle);

			var xSpPr = root.GetSpPr();
			if (xSpPr.HasInfo())
				spPr = new SpPr(xSpPr);

			var xTxPr = root.GetTxPr();
			if (xTxPr.HasInfo())
				txPr = new TxPr(xTxPr);

			if (type == TypeAx.catAx)
			{
				auto = ParseToBool(root.Element(c + nameof(auto))?.Attribute("val")?.Value);
				lblAlgn = root.Element(c + nameof(lblAlgn))?.Attribute("val")?.Value;
				lblOffset = root.Element(c + nameof(lblOffset))?.Attribute("val")?.Value?.ToInt() ?? -1;
				noMultiLvlLbl = root.Element(c + nameof(noMultiLvlLbl))?.Attribute("val")?.Value;
			}
			else
			{
				crossBetween = root.Element(c + nameof(crossBetween))?.Attribute("val")?.Value;
			}
		}

		private TickPos ParseTickLib(string value)
		{
			var res = TickPos.none;
			if (String.IsNullOrEmpty(value))
				return res;

			if (Enum.TryParse(value, out res))
				return res;

			return TickPos.none;
		}

		private TickMark ParseTickMark(string value)
		{
			var res = TickMark._none;
			if (String.IsNullOrEmpty(value))
				return res;

			if (Enum.TryParse("_" + value, out res))
				return res;

			return TickMark._none;
		}

		private TypeAx ParseAxType(string axeType)
		{
			var res = TypeAx.valAx;
			if (Enum.TryParse(axeType, out res))
				return res;

			return TypeAx.valAx;
		}

		private AxePos ParseAxPos(string value)
		{
			var res = AxePos.b;

			if (String.IsNullOrEmpty(value))
				return res;

			if (Enum.TryParse(value, out res))
				return res;

			return AxePos.b;
		}

		private bool ParseToBool(string str)
		{
			if (String.IsNullOrEmpty(str))
				return false;

			var i = str.ToInt();
			if (i == 1) return true;
			else return false;
		}
	}
}
