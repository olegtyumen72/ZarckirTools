﻿using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Interfaces;
using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts
{
	/// <summary>
	/// Функциональный файл - прост нужен
	/// </summary>
	internal class XmlApp : IXmlVisibleFile
	{
		string aplication = "Microsoft Excel";
		int secure = 1;
		string pass = "";
		bool scale = false;
		string comp;
		bool isCheckLinks = false;
		bool isSharDoc = false;
		bool isHyperChange = false;
		string verison = "16.0300";
		List<string> lists = new List<string>();

		//Dictionary<string, string> xa = new Dictionary<string, string>()
		//{
		//	{"extProp", "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" },
		//	{"vTypes",  "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes" },
		//};
		//Dictionary<string, string> xp = new Dictionary<string, string>()
		//{
		//	{"vec", "vt:vector" },
		//	{ "lpstr", "vt:lpst" },
		//	{"i4", "vt:i4" },
		//};

		public string RelativePath { get; } = "docProps\\";
		public string AbsolutePath { get; set; }
		public string File { get; } = "app.xml";
		public string Id { get; set; } //= "rId3";

		public XmlApp(ReportBook rb, string absPath)//, int id)
		{
			AbsolutePath = absPath;
			aplication = rb.App;
			secure = rb.Pass?.Length > 0 ? 1 : 0;
			pass = rb.Pass;
			comp = rb.Company;
			lists = rb.Select((v) => v.Name).ToList();
			if (rb.Version != verison) throw new Exception("Rb and excel miss match verions");
			//Id = $"rId{id}";

		}


		public void Save()
		{
			var mainDir = Path.Combine(AbsolutePath, RelativePath);
			mainDir.CheckDir();

			var mainFile = Path.Combine(mainDir, File);
			//mainFile.CheckTxtFile();

			using (var doc = new XmlWriterWrapper(mainFile))
			{
				CreateDoc(doc);
				doc.Save();
			}
		}

		private void CreateDoc(XmlWriterWrapper doc)
		{
			XmlTools.DefaultDeclaration(doc);
			doc.OpenElement("Properties",null, XmlTools.xa["extProp"]).AddAtribute("vt", XmlTools.xa["vTypes"], "xmlns")
				.OpenElement("Application", aplication).CloseElement()
				.OpenElement("DocSecurity", secure.ToString()).CloseElement()
				.OpenElement("ScaleCrop", this.scale.ToString().ToLower()).CloseElement()
				.OpenElement("TitlesOfParts");
			XmlTools.NewVector(doc, XmlTools.xa["vTypes"], new Dictionary<string, string>()
			{
				{ XmlTools.xa["s"], lists.Count.ToString() },
				{ XmlTools.xa["BT"], "lpstr" }
			});

			foreach (var sheet in lists)
			{
				XmlTools.NewLpstr(doc, XmlTools.xa["vTypes"], sheet);
			}

			doc.CloseElement().CloseElement();

			doc.OpenElement("Company", comp).CloseElement()
				.OpenElement("LinksUpToDate", isCheckLinks.ToString().ToLower()).CloseElement()
				.OpenElement("SharedDoc", isSharDoc.ToString().ToLower()).CloseElement()
				.OpenElement("HyperlinksChanged", isHyperChange.ToString().ToLower()).CloseElement()
				.OpenElement("AppVersion", verison).CloseElement();
		}

		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}

		public string RelativeToFile()
		{
			return Path.Combine(RelativePath, File);
		}
	}
}
