﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Document.NET;

namespace ExcelToWordSplitter.Backend.Parts.Tools
{
	/// <summary>
	/// ФОрматирует что-то как-то старый код
	/// </summary>
	public class FormatColumn : ColBase
	{
		public int ColWidth = -1;
		public VerticalAlignment VertAligment = VerticalAlignment.Bottom;
	}
}
