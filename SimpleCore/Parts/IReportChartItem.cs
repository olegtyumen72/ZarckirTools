﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Parts
{
	public enum ChartTypeEnum
	{
		/// <summary>
		/// Гистограмма.
		/// </summary>
		Histogram,

		/// <summary>
		/// График (Y-plot) соединенный точками.
		/// </summary>            
		Graphic,

		/// <summary>
		/// График (X,Y-plot) соединенный точками.
		/// </summary>    
		XYGraphic,

		/// <summary>
		/// Набор точек.
		/// </summary>
		Points
	}

	public interface IReportChartItem
	{

		/// <summary>
		/// Тип элемента графика.
		/// </summary>
		ChartTypeEnum ChartType { get; set; }

		/// <summary>
		/// Заголовок графика.
		/// </summary>
		 string Title { get; set; }

		/// <summary>
		/// Массив данных графика по оси Х. $A1:A5 - ссылка на страницу, 
		/// {1;2;3} - просто числа которые будут занесены прямо в график.
		/// При этом не обходимо IsInLineValueX {get;set;} true
		/// </summary>
		 string RangeX { get; set; }

		/// <summary>
		/// Если true, вставляет конкретные значения, а не ссылки на ячейки
		/// </summary>
		 bool IsInLineValueX { get; set; }

		/// <summary>
		/// Массив данных графика по оси Y. $A1:A5 - ссылка на страницу, 
		/// {10;20;20} - просто числа которые будут занесены прямо в график.
		/// При этом не обходимо IsInLineValueY {get;set;} true
		/// </summary>
		 string RangeY { get; set; }

		/// <summary>
		/// Если true, вставляет конкретные значения, а не ссылки на ячейки
		/// </summary>
		 bool IsInLineValueY { get; set; }

		 Color Color { get; set; }

		 int LineWidth { get; set; }


	}
}
