﻿using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Templates
{
	public class ReportChartItemTmpl : IReportChartItem, ISimpleConvert<IReportChartItem>, ISimpleClone<ReportChartItemTmpl>
	{
		public ReportChartItemTmpl(IReportChartItem v)
		{
			ChartType = v.ChartType;
			Title = v.Title;
			RangeX = v.RangeX;
			RangeY = v.RangeY;
			IsInLineValueX = v.IsInLineValueX;
			IsInLineValueY = v.IsInLineValueY;
			Color = v.Color;
			LineWidth = v.LineWidth;
			
		}

		public ReportChartItemTmpl()
		{

		}

		/// <summary>
		/// Тип элемента графика.
		/// </summary>
		public ChartTypeEnum ChartType { get; set; } = ChartTypeEnum.Histogram;

		/// <summary>
		/// Заголовок графика.
		/// </summary>
		public string Title { get; set; } = "";

		/// <summary>
		/// Массив данных графика по оси Х. $A1:A5 - ссылка на страницу, 
		/// {1;2;3} - просто числа которые будут занесены прямо в график.
		/// При этом не обходимо IsInLineValueX {get;set;} =  true
		/// </summary>
		public string RangeX { get; set; } = null;

		/// <summary>
		/// Если true, вставляет конкретные значения, а не ссылки на ячейки
		/// </summary>
		public bool IsInLineValueX { get; set; } = false;

		/// <summary>
		/// Массив данных графика по оси Y. $A1:A5 - ссылка на страницу, 
		/// {10;20;20} - просто числа которые будут занесены прямо в график.
		/// При этом не обходимо IsInLineValueY {get;set;} =  true
		/// </summary>
		public string RangeY { get; set; } = null;

		/// <summary>
		/// Если true, вставляет конкретные значения, а не ссылки на ячейки
		/// </summary>
		public bool IsInLineValueY { get; set; } = false;

		public Color Color { get; set; } = Color.Empty;

		public int LineWidth { get; set; } = -1;

		public virtual ReportChartItemTmpl Clone()
		{
			return new ReportChartItemTmpl()
			{
				ChartType = this.ChartType,
				Color = this.Color,
				IsInLineValueX = this.IsInLineValueX,
				IsInLineValueY = this.IsInLineValueY,
				LineWidth = this.LineWidth,
				RangeX = this.RangeX,
				RangeY = this.RangeY,
				Title = this.Title
			};
		}

		public IReportChartItem Convert()
		{
			return (IReportChartItem)this;
		}
	}
}
