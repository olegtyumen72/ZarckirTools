﻿using NPOI.XWPF.UserModel;
using NpoiWrapper.Interfaces;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpoiWrapper.Tools
{
	public static class ParagraphTools
	{
		public static Paragraph ToHeadMainFormat(this Paragraph p, SimpleFontFamily font, Color c)
		{
			p.StyleName = "Heading1";
			p.Color = c;
			p.Font = font;
			p.Aligment = Aligment.CENTER;
			return p;
		}

		public static Paragraph ToHeadSecondFormat(this Paragraph p, SimpleFontFamily font, Color c)
		{
			p.StyleName = "Heading2";
			p.Color = c;
			p.Font = font;
			p.Aligment = Aligment.LEFT;
			return p;
		}

		public static Paragraph ToHeadSecondCenterFormat(this Paragraph p, SimpleFontFamily font, Color c)
		{
			p.StyleName = "Heading2";
			p.Color = c;
			p.Font = font;
			p.Aligment = Aligment.CENTER;
			return p;
		}

		public static Paragraph ToDefaultTextCentre(this Paragraph p, SimpleFontFamily font, Color c)
		{
			p.Font = font;
			p.Color = c;
			p.Aligment = Aligment.CENTER;
			return p;
		}

		public static Paragraph ToDefaultText(this Paragraph p, SimpleFontFamily font, Color c)
		{
			p.Font = font;
			p.Color = c;
			p.Aligment = Aligment.BOTH;
			return p;
		}

		public static Paragraph ToDefaultTextRight(this Paragraph p, SimpleFontFamily font, Color c)
		{
			p.Font = font;
			p.Color = c;
			p.Aligment = Aligment.RIGHT;
			return p;
		}

		public static Paragraph ToCaptionLeft(this Paragraph p, SimpleFontFamily font, Color c)
		{
			p.Font = font;
			p.Color = c;
			p.Aligment = Aligment.LEFT;
			return p;
		}

		public static Paragraph ToCaptionCentre(this Paragraph p, SimpleFontFamily font, Color c)
		{
			p.Font = font;
			p.Color = c;
			p.Aligment = Aligment.CENTER;
			return p;
		}

		public static Paragraph ToCaptionRight(this Paragraph p, SimpleFontFamily font, Color c)
		{
			p.Font = font;
			p.Color = c;
			p.Aligment = Aligment.RIGHT;
			return p;
		}

		public static Paragraph ToDefaultTableText(this Paragraph p, SimpleFontFamily font, Color c)
		{
			p.Font = font;
			p.Color = c;
			p.Aligment = Aligment.BOTH;
			return p;
		}
	}

	public enum Aligment
	{
		LEFT = 1,
		CENTER = 2,
		RIGHT = 3,
		BOTH = 4,
		MEDIUM_KASHIDA = 5,
		DISTRIBUTE = 6,
		NUM_TAB = 7,
		HIGH_KASHIDA = 8,
		LOW_KASHIDA = 9,
		THAI_DISTRIBUTE = 10
	}

	public class Paragraph : IRunableCollection
	{
		public SimpleFontFamily Font;
		public string StyleName;
		public Aligment Aligment;
		public Color Color;
		public bool IsNewLine;
		public bool IsNewPhage;
		private ObservableCollection<IDataSource> _data;
		private XWPFParagraph src;


		public Paragraph(SimpleFontFamily font, string sName = "", Aligment alig = Aligment.LEFT, bool newLineForAll = false, bool newPhageForAll = false)
		{
			Font = font;
			StyleName = sName;
			Aligment = alig;
			Color = Color.Black;
			IsNewLine = newLineForAll;
			IsNewPhage = newPhageForAll;
			_data = new ObservableCollection<IDataSource>();
		}

		public void Save(NpoiWord doc)
		{
			src = doc._doc.CreateParagraph();
			src.Alignment = ConvertEnum(Aligment);
			if (!string.IsNullOrEmpty(StyleName))
				src.Style = StyleName;

			foreach (var el in _data)
			{
				if (el is WordText)
					AddText(el as WordText);

				if (el is Pictures)
					AddPictures(el as Pictures);
			}

		}

		internal XWPFRun Update(XWPFParagraph forUpd)
		{
			src = forUpd;
			forUpd.Alignment = ConvertEnum(Aligment);
			if (!string.IsNullOrEmpty(StyleName))
				forUpd.Style = StyleName;
			var r = InitDefRun();

			return r;
		}

		public void AddPictures(IEnumerable<ImageSrc> pics)
		{
			var pic = new Pictures()
			{
				Pics = new List<ImageSrc>(pics)
			};
			_data.Add(pic);
			//AddData(pics);
		}

		public void AddSentences(IEnumerable<string> text)
		{
			var txt = new WordText()
			{
				Sentences = new List<string>(text)
			};
			_data.Add(txt);
		}

		public Paragraph CloneNoText()
		{
			var res = this.MemberwiseClone() as Paragraph;
			res._data = new ObservableCollection<IDataSource>();
			res.src = null;

			return res;
		}

		private XWPFRun InitDefRun()
		{
			var r = src.CreateRun();
			r.SetColor(RGB(Color));
			r.FontFamily = Font.Name;			
			r.SetUnderline(Font.Underline ? UnderlinePatterns.Dash : UnderlinePatterns.None);
			r.FontSize = Font.Size;
			r.IsBold = Font.Bold;
			r.IsItalic = Font.Italic;

			return r;
		}

		private void AddPictures(Pictures pictures)
		{
			var r = InitDefRun();
			//	src.CreateRun();
			//r.SetColor(RGB(Color));
			//r.SetFontFamily(Font.Name, FontCharRange.Ascii);
			//r.SetUnderline(Font.Underline ? UnderlinePatterns.Dash : UnderlinePatterns.None);
			//r.FontSize = Font.Size;
			//r.IsBold = Font.Bold;
			//r.IsItalic = Font.Italic;

			foreach (var pic in pictures.Pics)
			{
				var text = "";
				r.AddPicture(pic.MemStream, (int)PictureType.JPEG, pic.Id, 7525 * pic.Pic.Width, 7525 * pic.Pic.Height);
				if (pic.IsNumeric)
					text += $"{pic.Num}. ";
				text += pic.Text;
				r.AppendText(text);
			}
		}

		private void AddText(WordText text)
		{
			var r = InitDefRun();
			//	src.CreateRun();
			//r.SetColor(RGB(Color));
			//r.SetFontFamily(Font.Name, FontCharRange.Ascii);
			//r.SetUnderline(Font.Underline ? UnderlinePatterns.Dash : UnderlinePatterns.None);
			//r.FontSize = Font.Size;
			//r.IsBold = Font.Bold;
			//r.IsItalic = Font.Italic;

			foreach (var sent in text.Sentences)
			{
				r.AppendText(sent);
				if (IsNewLine)
					r.AddCarriageReturn();
				if (IsNewPhage)
					r.AddBreak(BreakType.PAGE);
			}
		}

		//private void AddData(IEnumerable<IDataSource> elems)
		//{
		//	foreach (var el in elems)
		//	{
		//		_data.Add(el);
		//	}
		//}

		private ParagraphAlignment ConvertEnum(Aligment aligment)
		{
			return ((ParagraphAlignment)(int)aligment);
		}

		private string RGB(Color src)
		{
			return $"{src.R.ToString("X2")}{src.G.ToString("X2")}{src.B.ToString("X2")}";
		}
	}
}
