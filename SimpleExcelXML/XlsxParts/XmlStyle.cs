﻿using SimpleCore.Templates;
using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Interfaces;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Styles;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts
{
	/// <summary>
	/// Файл описывает стили ячеек
	/// </summary>
	internal class XmlStyle : IXmlVisibleFile
	{
		/// <summary>
		/// Стандартый вид ячейки - применяется для всех, у которых нет не стандартного стиля
		/// </summary>
		private static ReportFormatItem DefaultFormatItem = new ReportFormatItem() { NumFmtId = 0, Row = 1, Col = 1, AllCols = true, BordersAll = false, Background = Color.Empty, FontFormat = new SimpleFontFamily() { FontName = Fonts.Calibri, Size = 11 } };

		public string RelativePath { get; } = "xl\\";
		public string File { get; } = "styles.xml";
		public string AbsolutePath { get; set; }
		public string Id { get; set; }
		/// <summary>
		/// Набор экселей и их стилей в сыром виде
		/// </summary>
		private Dictionary<ReportSheet, List<ReportFormatItem>> FormatItems { get; } = new Dictionary<ReportSheet, List<ReportFormatItem>>();

		/// <summary>
		/// Дистинкт набоор настройки бордеров для ячейки
		/// </summary>
		private List<BorderNode> StyleBord = new List<BorderNode>();
		/// <summary>
		/// Дистинкт набоор настройки шрифтов для ячейки
		/// </summary>
		//private Dictionary<SimpleFontFamily, int> StyleF = new Dictionary<SimpleFontFamily, int>();
		private List<FontNode> StyleFont = new List<FontNode>();
		/// <summary>
		/// Дистинкт набоор закраски ячейки
		/// </summary>
		private List<FillNode> StyleFill = new List<FillNode>();
		private List<NumFmtNode> StyleNums = new List<NumFmtNode>();
		/// <summary>
		/// Дистинкт набоор сочетаний из: шрифта, закраски, стиля бордеров
		/// </summary>
		private List<XfsNode> DistinctXfs = new List<XfsNode>();
		private List<XfsNode> DistinctStyleXfs = new List<XfsNode>();
		private List<Dxf> DistinctDxf = new List<Dxf>();
		/// <summary>
		/// Сочетание полного стиля для [ReportSheetName][CellRange] = стиль
		/// </summary>
		private Dictionary<string, Dictionary<CellRangeTmpl, XfsNode>> Xfss = new Dictionary<string, Dictionary<CellRangeTmpl, XfsNode>>();
		/// <summary>
		/// Глобальный айдишник шрифтов
		/// </summary>
		private int fontsId = 0;
		/// <summary>
		/// Глобальный айдишник закраски
		/// </summary>
		private int fillsId = 0;
		/// <summary>
		/// Глобальный айдишник бордеров
		/// </summary>
		private int borderId = 0;
		/// <summary>
		/// Глобальный айдишник настройки стиля ячейки
		/// </summary>
		private int cellXfsId = 0;

		private FontNode curFont;
		private BorderNode curBoder;
		private FillNode curFill;
		private XfsNode curXfs;
		private NumFmtNode curNumFmt;


		public XmlStyle(string path, bool isLoad = false)
		{
			AbsolutePath = path;
			//Id = $"rId{id}";
			//this.FormatItems = FormatItems;
			if (!isLoad)
			{
				//#region Default parameters
				StyleFill.Add(new FillNode(fillsId++)
				{
					patternType = "none",
					isDefault = true
				});
				StyleFill.Add(new FillNode(fillsId++)
				{
					patternType = "gray125",
					isDefault = true
				});
				curFill = StyleFill.Last();

				//StyleFont.Add(new FontNode(fontsId++)
				//{
				//	Font = DefaultFormatItem.FontFormat
				//});

				//StyleBord.Add(new BorderNode(borderId++));

				//DistinctXfs.Add(new XfsNode(StyleFont.Last(), StyleBord.Last(), StyleFill.Last(), null, DefaultFormatItem, cellXfsId++));
				//#endregion


				InitFont(DefaultFormatItem, null);
				InitBorder(DefaultFormatItem);
				InitXf(DefaultFormatItem, null);

				curFont.IsDefault = true;
				curBoder.isDefult = true;
				curXfs.isDefault = true;

				curBoder = null;
				curFill = null;
				curFont = null;
				curXfs = null;
			}
		}

		public void Save()
		{
			var dir = Path.Combine(AbsolutePath, RelativePath);
			dir.CheckDir();

			var file = Path.Combine(dir, File);
			//file.CheckTxtFile();

			using (var doc = new XmlWriterWrapper(file))
			{
				CreateDoc(doc);
				doc.Save();
			}
		}

		private void CreateDoc(XmlWriterWrapper doc)
		{

			XmlTools.DefaultDeclaration(doc);
			doc.OpenElement("styleSheet", null, XmlTools.xa["spredMain"])
				.AddAtribute("mc", XmlTools.xa["compab"], "xmlns")
				.AddAtribute("Ignorable", "x14ac x16r2", "mc")
				.AddAtribute("x14ac", XmlTools.xa["x14ac"], "xmlns")
				.AddAtribute("x16r2", XmlTools.xa["x16r2"], "xmlns");

			//StyleF.Add(DefaultFormatItem.FontFormat, fontsId++);			

			doc.OpenElement("fonts");
			foreach (var font in StyleFont)
			{
				font.Save(doc);
			}
			doc.CloseElement();

			doc.OpenElement("fills");
			foreach (var fill in StyleFill)
			{
				fill.Save(doc);
			}
			doc.CloseElement();

			doc.OpenElement("borders");
			foreach (var border in StyleBord)
			{
				border.Save(doc);
			}
			doc.CloseElement();

			doc.OpenElement("cellXfs");
			foreach (var xfs in DistinctXfs)
			{
				xfs.Save(doc);
			}
			doc.CloseElement();

			if (DistinctDxf.Count > 0)
			{
				doc.OpenElement("dxfs");
				foreach (var dxf in DistinctDxf)
				{
					dxf.Save(doc);
				}
				doc.CloseElement();
			}

			doc.OpenElement("tableStyles")
				.AddAtribute("count", 0)
				.AddAtribute("defaultTableStyle", "TableStyleMedium2")
				.AddAtribute("defaultPivotStyle", "PivotStyleLight16")
			.CloseElement();

			doc.CloseElement();
		}

		public void TryLoad()
		{

			var file = AbsoluteToFile();
			var dir = Directory.GetDirectoryRoot(file);

			if (!System.IO.File.Exists(file))
			{
				return;
			}

			var doc = XDocument.Load(file);

			var root = doc.Root;
			var ns = root.GetDefaultNamespace();
			var numFmts = root?.Elements(ns + "numFmts")?.Elements(ns + "numFmt");
			var fonts = root?.Elements(ns + "fonts")?.Elements(ns + "font");
			var fills = root?.Element(ns + "fills")?.Elements(ns + "fill");
			var borders = root?.Element(ns + "borders")?.Elements(ns + "border");
			var cellStyleXfs = root?.Element(ns + "cellStyleXfs")?.Elements(ns + "xf");
			var cellXfs = root?.Element(ns + "cellXfs")?.Elements(ns + "xf");
			var dxfs = root?.Element(ns + "dxfs")?.Elements(ns + "dxf");
			//var tablStyles = root.Elements("tableStyles");

			if (numFmts != null)
				LoadNumFmts(numFmts);
			if (fonts != null)
				LoadFonts(fonts);
			if (fills != null)
				LoadFills(fills);
			if (borders != null)
				LoadBorders(borders);
			if (dxfs != null)
				LoadDxfs(dxfs);
			if (cellStyleXfs != null)
				LoadCellXfs(cellStyleXfs, DistinctStyleXfs);
			if (cellXfs != null)
				LoadCellXfs(cellXfs, DistinctXfs);
			if (cellStyleXfs != null)
				MergeStyleAndXfs();
		}



		/// <summary>
		/// Создаёт стиль для rs на основе Xfs
		/// </summary>
		/// <param name="rs"></param>
		/// <param name="style"></param>
		public void FillFormatItem(ReportSheet rs, ShortFormatItem style)
		{
			var xf = DistinctXfs.ElementAt(style.StyleId);

			if (xf.IsFont)
				style.FontFormat = xf.Font.Font;
			if (xf.IsBorder)
				style.BordersAll = true;
			if (xf.IsFill)
				style.Background = xf.Fill.Fg.Clr;

			style.ColumnsOrientation = xf.Rotation;
			style.Wrap = xf.IsWrap;
			style.HorizontalAligmentCenter = xf.Horizont == XmlNodes.Charts.Aligment.center;
			style.VerticalAligmentCenter = xf.Vertical == XmlNodes.Charts.Aligment.center;
			style.NumFmtId = xf.NumFmt?.NumFmtId ?? 0;
		}

		private void LoadCellXfs(IEnumerable<XElement> cellXfs, List<XfsNode> collection)
		{
			foreach (var xf in cellXfs)
			{
				if (!xf.HasAttributes)
					continue;


				XfsNode res = LoadXfs(xf);
				collection.Add(res);
				curXfs = res;
			}
		}
		private XfsNode LoadXfs(XElement xfElem)
		{
			var atrs = XDocumentTools.ParseAtrs(xfElem);

			int fontId = 0, border = 0, fill = 0, txtRotato = 0, numFmtId = 0, xfId = -1;
			bool isWrap = false, isprotect = false, isfont = false, isborder = false, isfill = false, isnumFmt = false, isAligm = false;
			string hor = "", vert = "";

			if (atrs.ContainsKey("fontId"))
				fontId = atrs["fontId"].ToInt();
			if (atrs.ContainsKey("fillId"))
				fill = atrs["fillId"].ToInt();
			if (atrs.ContainsKey("borderId"))
				border = atrs["borderId"].ToInt();
			if (atrs.ContainsKey("numFmtId"))
				numFmtId = atrs["numFmtId"].ToInt();
			if (atrs.ContainsKey("xfId"))
				xfId = atrs["xfId"].ToInt();
			if (atrs.ContainsKey("applyProtection"))
				isprotect = atrs["applyProtection"].StrToBool();
			if (atrs.ContainsKey("applyAlignment"))
				isAligm = atrs["applyAlignment"].StrToBool();
			if (atrs.ContainsKey("applyBorder"))
				isborder = atrs["applyBorder"].StrToBool();
			if (atrs.ContainsKey("applyNumberFormat"))
				isnumFmt = atrs["applyNumberFormat"].StrToBool();
			if (atrs.ContainsKey("applyFont"))
				isfont = atrs["applyFont"].StrToBool();
			if (atrs.ContainsKey("applyFill"))
				isfill = atrs["applyFill"].StrToBool();
			if (atrs.ContainsKey("alignment@horizontal"))
				hor = atrs["alignment@horizontal"];
			if (atrs.ContainsKey("alignment@vertical"))
				vert = atrs["alignment@vertical"];
			if (atrs.ContainsKey("alignment@textRotation"))
				txtRotato = atrs["alignment@textRotation"].ToInt();
			if (atrs.ContainsKey("alignment@wrapText"))
				isWrap = Convert.ToBoolean(atrs["alignment@wrapText"].ToInt());


			var res = new XfsNode(StyleFont.ElementAt(fontId), StyleBord.ElementAt(border), StyleFill.ElementAt(fill), StyleNums.FirstOrDefault((v) => v.NumFmtId == numFmtId), hor, vert, txtRotato, isWrap, cellXfsId++)
			{
				IsAligment = isAligm,
				IsBorder = isborder,
				IsFill = isfill,
				IsFont = isfont,
				IsNumFmt = isnumFmt,
				IsWrap = isWrap,
				IsProtection = isprotect,
			};

			if (xfId >= 0)
			{
				if (DistinctStyleXfs.Count > xfId)
					res.Xfs = DistinctStyleXfs.ElementAt(xfId);
				else
					res.Xfs = new XfsNode(xfId, true);
			}

			return res;
		}
		private void MergeStyleAndXfs()
		{
			foreach (var xfsCell in DistinctXfs)
			{
				if (xfsCell.Xfs == null)
					continue;

				XfsNode style = null;
				if (xfsCell.Xfs != null && xfsCell.Xfs.IsEmpty)
				{
					if (xfsCell.Xfs.Id >= 0 && xfsCell.Xfs.Id < DistinctStyleXfs.Count)
					{
						style = DistinctStyleXfs.ElementAt(xfsCell.Xfs.Id);
					}
					else
						continue;
				}
				else
				{
					style = xfsCell.Xfs;
				}

				xfsCell.MergeStyle(style);
			}
		}

		private void LoadBorders(IEnumerable<XElement> borders)
		{
			foreach (var border in borders)
			{
				if (!border.HasElements)
					continue;


				BorderNode res = LoadBorder(border);
				StyleBord.Add(res);
				curBoder = res;
			}
		}
		private BorderNode LoadBorder(XElement bordElem)
		{
			var atrs = XDocumentTools.ParseAtrs(bordElem);
			BorderNode res = new BorderNode(borderId++);
			if (atrs.Count > 0)
			{
				if (atrs.ContainsKey("diagonalDown"))
				{
					res.DiagonalDown = atrs["diagonalDown"].ToInt();
				}
				if (atrs.ContainsKey("diagonalUp"))
				{
					res.DiagonalUp = atrs["diagonalUp"].ToInt();
				}


				foreach (var elem in bordElem.Elements())
				{
					switch (elem.Name.LocalName)
					{
						case "left":
							{
								res.Left = new BorderElement(BorderElement.Elem.left);
								res.Left.Load(elem);
								break;
							}
						case "right":
							{
								res.Right = new BorderElement(BorderElement.Elem.right);
								res.Right.Load(elem);
								break;
							}
						case "top":
							{
								res.Top = new BorderElement(BorderElement.Elem.top);
								res.Top.Load(elem);
								break;
							}
						case "bottom":
							{
								res.Bottom = new BorderElement(BorderElement.Elem.bottom);
								res.Bottom.Load(elem);
								break;
							}
						case "diagonal":
							{
								res.Diagonal = new BorderElement(BorderElement.Elem.diagonal);
								res.Diagonal.Load(elem);
								break;
							}
					}
				}
			}
			else
			{
				res.isDefult = true;
			}

			return res;
		}

		private void LoadFills(IEnumerable<XElement> fills)
		{
			var colorConv = new ColorConverter();

			foreach (var fill in fills)
			{
				if (!fill.HasElements)
					continue;

				FillNode res = LoadFill(fill);
				StyleFill.Add(res);
				curFill = res;
			}
		}
		private FillNode LoadFill(XElement fillElem)
		{
			var fgClr = Color.Empty;
			var bgClr = Color.Empty;
			string pattType = "";
			var atrs = XDocumentTools.ParseAtrs(fillElem);

			if (atrs.ContainsKey("patternFill@fgColor@rgb"))
				fgClr = atrs["patternFill@fgColor@rgb"].ToColor();
			if (atrs.ContainsKey("patternFill@bgColor@rgb"))
				bgClr = atrs["patternFill@bgColor@rgb"].ToColor();
			if (atrs.ContainsKey("patternFill@patternType"))
				pattType = atrs["patternFill@patternType"];

			return new FillNode(fillsId++, pattType, fgClr, bgClr);
		}

		private void LoadFonts(IEnumerable<XElement> fonts)
		{
			foreach (var font in fonts)
			{
				if (!font.HasElements)
					continue;

				FontNode res = LoadFont(font);
				StyleFont.Add(res);
				curFont = res;
			}
		}
		private FontNode LoadFont(XElement fontElem)
		{
			var fName = "";
			var color = Color.Empty;
			var size = -1;
			var clr_cnv = new ColorConverter();
			var bold = false;
			var italic = false;
			var under = false;
			var theme = "";
			var tint = "";
			XAttribute curAtr = null;

			foreach (var el in fontElem.Elements())
			{
				if (!el.HasAttributes)
				{
					switch (el.Name.LocalName.ToLower())
					{
						case "b":
							{
								bold = true;
								break;
							}
						case "i":
							{
								italic = true;
								break;
							}
						case "u":
							{
								under = true;
								break;
							}
						default:
							{
								break;
							}
					};
				}
				else
				{
					switch (el.Name.LocalName.Trim().ToLower())
					{
						case "sz":
							{
								curAtr = el.Attribute("val");
								if (curAtr != null)
									size = el.Attribute("val").Value.ToInt();

								break;
							}
						case "color":
							{
								if (el.HasAtr("theme"))
								{
									theme = el.Attribute("theme").Value;
									if (el.HasAtr("tint"))
										tint = el.Attribute("tint").Value;
								}
								else
								{
									curAtr = el.Attribute("rgb");
									if (curAtr != null)
									{
										var strColor = curAtr.Value;
										color = strColor.ToColor();
									}
								}
								break;
							}
						case "name":
							{
								curAtr = el.Attribute("val");
								if (curAtr != null)
									fName = curAtr.Value;

								break;
							}
					}
				}
			}

			var rEl = SimpleFontFamily.Parse(fName, size);
			rEl.Bold = bold;
			rEl.Italic = italic;
			rEl.Underline = under;

			return new FontNode(fontsId++, rEl, color, theme, tint);
		}

		private void LoadNumFmts(IEnumerable<XElement> numFmts)
		{
			foreach (var numFmt in numFmts)
			{
				if (!numFmt.HasAttributes)
					continue;

				curNumFmt = NumFmtNode.Load(numFmt);
				StyleNums.Add(curNumFmt);
			}
		}
		private void LoadDxfs(IEnumerable<XElement> dxfs)
		{
			foreach (var dxf in dxfs)
			{
				var ns = dxf.GetDefaultNamespace();
				if (!dxf.HasElements)
					continue;

				var fontElem = dxf.Element(ns + "font");
				FontNode font = null;
				if (fontElem != null)
					font = LoadFont(fontElem);

				var fillElem = dxf.Element(ns + "fill");
				FillNode fill = null;
				if (fillElem != null)
					fill = LoadFill(fillElem);

				var borderElem = dxf.Element(ns + "border");
				BorderNode border = null;
				if (borderElem != null)
					border = LoadBorder(borderElem);


				var numFmtElem = dxf.Element(ns + "numFmt");
				NumFmtNode numFmt = null;
				if (numFmt != null)
					numFmt = NumFmtNode.Load(numFmtElem);

				var res = new Dxf()
				{
					Border = border,
					Fill = fill,
					Font = font,
					NumFmt = numFmt
				};
				DistinctDxf.Add(res);
			}
		}



		/// <summary>
		/// Правильно стилизированный рендж из формат итема
		/// </summary>
		/// <param name="p"></param>
		/// <returns></returns>
		private CellRangeTmpl RangeFromFormat(ReportFormatItem p)
		{
			CellRangeTmpl range = null;
			if (p.Row == -1 && p.Col == -1 && p.Range != null)
			{
				curXfs.isGlobal = false;
				range = p.Range;
				//Xf.Add(p.Range, elem);
			}
			else if (p.Row != -1 && p.Col != -1)
			{
				range = new CellRangeTmpl(p.Col, p.Row, int.MaxValue, int.MaxValue);
			}
			else if (p.Row != -1)
			{
				range = new CellRangeTmpl(-1, p.Row, -1, int.MaxValue);
			}
			else if (p.Col != -1)
			{
				range = new CellRangeTmpl(p.Col, -1, int.MaxValue, -1);
			}
			else
			{
				throw new Exception("cant get range for style");
			}

			return range;
		}

		/// <summary>
		/// Поиск пересечение ренджей по формат итемам
		/// </summary>
		/// <param name="src"></param>
		/// <param name="dest"></param>
		/// <returns></returns>
		private CellRangeTmpl FindMinCross(CellRangeTmpl src, CellRangeTmpl dest)
		{

			var isCross =
				RangeTools.CrossesPoints(src.cell1_Col, src.cell2_Col, dest.cell1_Col) &&
				RangeTools.CrossesPoints(src.cell1_Row, src.cell2_Row, dest.cell1_Row);
			isCross = isCross ||
				RangeTools.CrossesPoints(src.cell1_Col, src.cell2_Col, dest.cell2_Col) &&
				RangeTools.CrossesPoints(src.cell1_Row, src.cell2_Row, dest.cell2_Row);
			isCross = isCross ||
				RangeTools.CrossesPoints(dest.cell1_Col, dest.cell2_Col, src.cell1_Col) &&
				RangeTools.CrossesPoints(dest.cell1_Row, dest.cell2_Row, src.cell1_Row);
			isCross = isCross ||
				RangeTools.CrossesPoints(dest.cell1_Col, dest.cell2_Col, src.cell2_Col) &&
				RangeTools.CrossesPoints(dest.cell1_Row, dest.cell2_Row, src.cell2_Row);


			if (!isCross)
			{
				return null;
			}

			if (src.IsContains(dest))
			{
				return dest;
			}

			if (dest.IsContains(src))
			{
				return src;
			}

			var cross = new CellRangeTmpl(-1, -1, -1, -1);

			if (src.cell1_Col == dest.cell2_Col && src.cell2_Col > dest.cell1_Col)
			{
				cross.cell1_Col = src.cell1_Col;
				cross.cell2_Col = src.cell1_Col;
			}
			else if (src.cell2_Col == dest.cell1_Col && src.cell1_Col > dest.cell2_Col)
			{
				cross.cell1_Col = src.cell2_Col;
				cross.cell2_Col = src.cell2_Col;
			}

			if (src.cell1_Row == dest.cell2_Row && src.cell2_Row > dest.cell1_Row)
			{
				cross.cell1_Row = src.cell1_Row;
				cross.cell2_Row = src.cell1_Row;
			}
			else if (src.cell2_Row == dest.cell1_Row && src.cell1_Row > dest.cell2_Row)
			{
				cross.cell1_Row = src.cell2_Row;
				cross.cell2_Row = src.cell2_Row;
			}

			if (!cross.isEmpty())
			{
				return cross;
			}

			if (src.cell1_Col < dest.cell1_Col)
			{
				cross.cell1_Col = dest.cell1_Col;
			}
			else
			{
				cross.cell1_Col = src.cell1_Col;
			}

			if (src.cell2_Col < dest.cell2_Col)
			{
				cross.cell2_Col = src.cell2_Col;
			}
			else
			{
				cross.cell2_Col = dest.cell2_Col;
			}

			if (src.cell1_Row < dest.cell1_Row)
			{
				cross.cell1_Row = dest.cell1_Row;
			}
			else
			{
				cross.cell1_Row = src.cell1_Row;
			}

			if (src.cell2_Row < dest.cell2_Row)
			{
				cross.cell2_Row = src.cell2_Row;
			}
			else
			{
				cross.cell2_Row = dest.cell2_Row;
			}

			if (cross.isEmpty())
			{
				throw new Exception("Can't find right cross");
			}

			return cross;
		}



		/// <summary>
		/// Стандартный тип для ячеек, без специального типа
		/// </summary>
		/// <param name="sheet"></param>
		/// <returns></returns>
		public int DefultStyle(string sheet)
		{
			if (Xfss.ContainsKey(sheet))
			{
				return Xfss[sheet].First().Value.Id;
			}

			return Xfss["all"].First().Value.Id;
		}
		public SimpleFontFamily DefaultFont(string sheet)
		{
			if (Xfss.ContainsKey(sheet))
			{
				return StyleFont.First((v) => v.IsDefault).Font;
			}

			return DefaultFormatItem.FontFormat;
		}


		/// <summary>
		/// Поиск нужного стиля для ячейки относительно определнного репортшита
		/// </summary>
		/// <param name="col"></param>
		/// <param name="row"></param>
		/// <param name="sheet"></param>
		/// <returns></returns>
		public int StyleForIndex(int col, int row, string sheet)
		{
			col += 1; row += 1;
			CellRangeTmpl minimal = null;
			if (!Xfss.ContainsKey(sheet))
			{
				return -1;
			}

			foreach (var el in Xfss[sheet])
			{
				XfsNode selected = null;
				var range = el.Key;

				if (range.cell1_Col == -1 && range.cell1_Row != -1 && range.cell2_Col == -1)
				{
					if (row >= range.cell1_Row)
					{
						selected = el.Value;
					}
				}
				else if (range.cell1_Row == -1 && range.cell2_Row == -1 && range.cell1_Col != -1)
				{
					if (col >= range.cell1_Col)
					{
						selected = el.Value;
					}
				}
				else if (range.cell1_Col != -1 && range.cell1_Row != -1 && range.cell2_Col == int.MaxValue && range.cell2_Row == int.MaxValue)
				{
					if (range.cell1_Col <= col && range.cell1_Row <= row)
					{
						selected = el.Value;
					}
				}
				else
				{
					if (
						(range.cell1_Col <= col && range.cell2_Col >= col) &&
						(range.cell1_Row <= row && range.cell2_Row >= row)
					)
					{
						selected = el.Value;
					}
				}

				if (selected != null)
				{
					if (minimal == null)
					{
						minimal = range;
					}
					else if (!range.isEmpty() && minimal.isEmpty())
					{
						minimal = range;
					}
					else if (!range.isEmpty() && !minimal.isEmpty())
					{
						minimal = range.ColumnCount() < minimal.ColumnCount() ? range : minimal;
					}
				}
			}

			if (minimal == null)
			{
				return -1;
			}
			else
			{
				return Xfss[sheet][minimal].Id;
			}
		}
		public int StyleForIndex(ReportFormatItem p, string sheet)
		{
			if (!Xfss.ContainsKey(sheet))
			{
				return -1;
			}

			var range = RangeFromFormat(p);
			foreach (var el in Xfss[sheet])
			{
				if (el.Key.Equals(range))
				{
					return el.Value.Id;
				}
			}

			return -1;
		}


		/// <summary>
		/// Получить все стили из репортбука и всех его листов
		/// </summary>
		/// <param name="rb"></param>
		public void AddRb(ReportBook rb)
		{
			foreach (var rs in rb)
			{

				var def = new ReportFormatItem(DefaultFormatItem.Clone().Convert());

				if (!rs.CellsBg.IsEmpty)
					def.Background = rs.CellsBg;

				InitBorder(def);
				InitFills(def, rs);

				InitFont(def, rs);
				if (!rs.CellsFont.IsEmpty)
					curFont.ExcelColor.Clr = rs.CellsFont;

				InitXf(def, rs);
				curXfs.isDefault = true;
				EmptyState();

				AddRange(rs, rs.LocalFormats);
			}
		}
		/// <summary>
		/// Получить стили из листов
		/// </summary>
		/// <param name="rs"></param>
		/// <param name="formats"></param>
		public void AddRange(ReportSheet rs, List<ReportFormatItem> formats)
		{
			foreach (var item in formats)
			{
				InitFont(item, rs);
				InitBorder(item);
				InitFills(item, rs);
				InitNumFmt(item, rs);
				InitXf(item, rs);

				item.StyleId = curXfs.Id;
				EmptyState();
			}
		}


		private void InitBorder(ReportFormatItem p)
		{

			if (p == null)
			{
				p = DefaultFormatItem;
			}

			foreach (var el in StyleBord)
			{
				if (el.IsSrc(p))
				{
					curBoder = el;
					return;
				}
			}

			StyleBord.Add(new BorderNode(p, borderId++));
			curBoder = StyleBord.Last();
		}

		private void InitFills(ReportFormatItem p, ReportSheet rs)
		{
			if (p == null)
			{
				p = DefaultFormatItem;
			}

			foreach (var el in StyleFill)
			{
				if (el.isSrc(p, rs))
				{
					curFill = el;
					return;
				}
			}

			StyleFill.Add(new FillNode(p, fillsId++, rs));
			curFill = StyleFill.Last();
		}

		private void InitFont(ReportFormatItem p, ReportSheet rs)
		{

			if (p == null)
			{
				p = DefaultFormatItem;
			}

			if (p.FontFormat == null)
			{
				curFont = StyleFont.First((v) => v.IsDefault);
				return;
			}

			for (int i = 0; i < StyleFont.Count; i++)
			{
				var el = StyleFont.ElementAt(i);
				if (el.Font.IsSame(p.FontFormat))
				{
					curFont = el;
					return;
				}
			}

			var res = new FontNode(fontsId++, p.FontFormat);
			StyleFont.Add(res);
			curFont = res;
		}
		/// <summary>
		/// Получение Xf на основе FormatItem
		/// </summary>
		/// <param name="p"></param>
		/// <param name="sheet"></param>
		private void InitXf(ReportFormatItem p, ReportSheet sheet)
		{
			if (p == null)
			{
				p = DefaultFormatItem;
			}

			foreach (var el in Xfss)
			{
				for (int i = 1; i < el.Value.Count; i++)
				{
					var dic = el.Value.ElementAt(i);
					if (dic.Value.IsSrc(p, curFont, sheet))
					{
						curXfs = dic.Value;
						break;
					}

					if (curXfs != null)
					{
						break;
					}
				}
			}

			bool isNew = false;
			if (curXfs == null)
			{
				curXfs = new XfsNode(curFont, curBoder, curFill, curNumFmt, p, cellXfsId++);
				DistinctXfs.Add(curXfs);
				isNew = true;
			}

			var range = RangeFromFormat(p);

			if (!p.IsSingle)
			{
				if (sheet != null && Xfss.ContainsKey(sheet.Name))
				{
					for (int i = 1; i < Xfss[sheet.Name].Count; i++)
					{
						var xfs = Xfss[sheet.Name].ElementAt(i);
						var cross = FindMinCross(xfs.Key, range);

						if (cross != null && cross != xfs.Key && cross != range)
						{
							var newXfs = xfs.Value.UpdateNew(curXfs, cellXfsId++);

							//if (!Xfss.ContainsKey(sheet))
							//	Xfss.Add(sheet, new Dictionary<CellRangeTmpl, XfsNode>() { [cross] = newXfs });
							//else
							Xfss[sheet.Name].Add(cross, newXfs);
						}
						else if (cross == xfs.Key)
						{
							xfs.Value.Update(curXfs);
						}
						else if (cross == range && curXfs != xfs.Value)
						{
							curXfs.Update(xfs.Value);
						}
					}
				}
			}

			if (sheet == null)
			{
				if (!Xfss.ContainsKey("all"))
				{
					Xfss.Add("all", new Dictionary<CellRangeTmpl, XfsNode>() { [range] = curXfs });
				}
				else
				{
					Xfss["all"][range] = curXfs;
				}
			}
			else if (!Xfss.ContainsKey(sheet.Name))
			{
				Xfss.Add(sheet.Name, new Dictionary<CellRangeTmpl, XfsNode>() { [range] = curXfs });
			}
			else if (isNew || !Xfss[sheet.Name].ContainsKey(range) || !Xfss[sheet.Name].ContainsValue(curXfs))
			{
				Xfss[sheet.Name].Add(range, curXfs);
			}
		}

		private void InitNumFmt(ReportFormatItem p, ReportSheet rs)
		{
			if (p == null)
			{
				p = DefaultFormatItem;
			}

			foreach (var el in StyleNums)
			{
				if (el.isSrc(p, rs))
				{
					curNumFmt = el;
					return;
				}
			}

			curNumFmt = NumFmtNode.Load(p);
			StyleNums.Add(curNumFmt);
		}



		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}

		public string RelativeToFile()
		{
			return Path.Combine(RelativePath, File);
		}

		private void EmptyState()
		{
			curBoder = null;
			curFill = null;
			curFont = null;
			curNumFmt = null;
			curXfs = null;
		}
	}
}
