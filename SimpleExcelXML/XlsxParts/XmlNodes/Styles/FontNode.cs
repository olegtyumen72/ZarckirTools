﻿using SimpleExcelXML.Tools;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Styles
{
	internal class FontNode
	{
		public int Id { get; set; }
		public SimpleFontFamily Font { get; set; }
		public ExcelColor ExcelColor { get; set; }		
		public bool IsDefault { get; set; }

		public FontNode(int id)
		{
			Id = id;
			ExcelColor = new ExcelColor();			
			Font = new SimpleFontFamily(Fonts.Calibri, 13);
			IsDefault = false;
		}

		public FontNode(int id, SimpleFontFamily f) : this(id)
		{
			Font = f;
		}

		public FontNode(int id, SimpleFontFamily f, Color color, string theme, string tint) : this(id, f)
		{
			if (ExcelColor.IsEmpty && theme.Length <= 0)
			{
				ExcelColor.Clr = Color.Black;
			}
			else
			{
				ExcelColor.Clr = color;
				ExcelColor.Theme = theme;
				ExcelColor.Tint = tint;
			}
		}

		internal FontNode Update(FontNode font)
		{
			if (Id == font.Id)
				return font;			

			if (IsDefault)
				return font;
			
			if (Font.IsSame(font.Font) && ExcelColor.Clr == font.ExcelColor.Clr)
			{
				return font;
			}

			ExcelColor.Update(font.ExcelColor);
			Font.Bold = font.Font.Bold || Font.Bold;
			Font.Italic = font.Font.Italic || Font.Italic;
			Font.Underline = font.Font.Underline || Font.Underline;
			Font.Size = font.Font.Size;
			Font.FontName = font.Font.FontName;

			return this;
		}

		internal void Save(XmlWriterWrapper doc)
		{
			doc.OpenElement("font");
			//var font = key

			if (Font == null)
			{
				doc.OpenElement("sz").AddAtribute("val", "13").CloseElement()
						.OpenElement("color").AddAtribute("rgb", Color.Black.ToARGB()).CloseElement()
						.OpenElement("name").AddAtribute("val", Fonts.Calibri.ToString()).CloseElement();

				//ConfToId.Add("defaultFont", fontsId++);
			}			
			else if(Font.IsEmpty == false)
			{
				if (Font.Bold)
				{
					doc.OpenElement("b").CloseElement();
				}

				if (Font.Italic)
				{
					doc.OpenElement("i").CloseElement();
				}

				if (Font.Underline)
				{
					doc.OpenElement("u").CloseElement();
				}

				if (Font.Size > 0)
				{
					doc.OpenElement("sz").AddAtribute("val", Font.Size.ToString()).CloseElement();
				}

				ExcelColor.Save(doc);
				
				doc.OpenElement("name").AddAtribute("val", Font.FontName.ToString().Replace("_", " ")).CloseElement();
			}

			doc.CloseElement();
		}
	}
}
