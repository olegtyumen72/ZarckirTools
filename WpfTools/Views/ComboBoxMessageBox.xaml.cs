﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfTools.Views
{
	/// <summary>
	/// Interaction logic for ComboBoxMessageBox.xaml
	/// </summary>
	public partial class ComboBoxMessageBox : Window, INotifyPropertyChanged
	{

		public static DependencyProperty MessageDP = DependencyProperty.Register(nameof(Title), typeof(string), typeof(ComboBoxMessageBox), new FrameworkPropertyMetadata("Внимание!", new PropertyChangedCallback(OnPropCange)));
		public static DependencyProperty SelectedDP = DependencyProperty.Register(nameof(SelectedItem), typeof(string), typeof(ComboBoxMessageBox), new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnPropCange)));
		public static DependencyProperty ItemsDP = DependencyProperty.Register(nameof(Items), typeof(ObservableCollection<string>), typeof(ComboBoxMessageBox), new FrameworkPropertyMetadata(new ObservableCollection<string>(), new PropertyChangedCallback(OnPropCange)));

		public event PropertyChangedEventHandler PropertyChanged;

		public string Title
		{
			get
			{
				return (string)GetValue(MessageDP);
			}
			set
			{
				SetValue(MessageDP, value);
				//PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MessageDP)));
			}
		}

		public string SelectedItem
		{
			get
			{
				return (string)GetValue(SelectedDP);
			}
			set
			{
				SetValue(SelectedDP, value);
			}
		}

		public ObservableCollection<string> Items
		{
			get
			{
				return (ObservableCollection<string>)GetValue(ItemsDP);
			}
			set
			{
				SetValue(ItemsDP, value);
			}

		}


		public ComboBoxMessageBox()
		{
			InitializeComponent();
		}

		static void OnPropCange(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var src = (ComboBoxMessageBox)sender;
			switch (e.Property.Name)
			{
				case nameof(SelectedItem):
					{
						src.SelectedItem = (string)e.NewValue;
						break;
					}
				case nameof(Title):
					{
						src.Title = (string)e.NewValue;
						break;
					}
				case nameof(Items):
					{
						src.Items = (ObservableCollection<string>)e.NewValue;
						break;
					}
				default:
					{
						throw new Exception("Unecaption behaviour");
					}
			}
		}

		private void Ok_BtnClick(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void Cncl_BtnClick(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
		}
	}
}
