﻿using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SimpleCore.Templates
{
	public class TranslaterTmpl : ITranslater
	{
		/// <summary>
		/// Текущий язык (локаль) ru/en/fr/kz.
		/// </summary>
		public string Lang { get; set; } = "en";

		Dictionary<string, string> m_translations { get; set; } = new Dictionary<string, string>();

		/// <summary>
		/// Загрузить язык из папки плагина
		/// </summary>
		/// <param name="filename">Путь к файлу переводов.</param>
		/// <param name="lang">Код языка.</param>
		public bool LoadLanguage(string file, string lang)
		{
			Lang = lang;

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(file);

			XmlNodeList items = doc.GetElementsByTagName("items");
			if (items.Count == 0)
			{
				return false;
			}

			m_translations = new Dictionary<string, string>();
			foreach (XmlElement item in items[0].ChildNodes)
			{
				// Ищем в элементе en узел - исходный текст.
				var en_nodes = item.GetElementsByTagName("name");
				if (en_nodes.Count == 0)
				{
					// !!! Скажем что узел пуст.
				}
				else
				{
					// Ищем нужный нам перевод.
					var lang_nodes = item.GetElementsByTagName(Lang);
					if (lang_nodes.Count == 0)
					{
						// !!! Скажем что нет перевода для узла.
					}
					// Все хорошо. Грузим перевод.
					else
					{
						string key = en_nodes[0].InnerText.Replace("\\r\\n", "\r\n");
						key = key.Replace("\\r", "\r");
						key = key.Replace("\\n", "\n");
						key = key.Replace("\\t", "\t");

						string text = lang_nodes[0].InnerText.Replace("\\r\\n", "\r\n");
						text = text.Replace("\\r", "\r");
						text = text.Replace("\\n", "\n");
						text = text.Replace("\\t", "\t");

						m_translations[key] = text;
					}
				}
			}
			return true;

		}

		/// <summary>
		/// Перевести текст на текущий язык.
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public string Translate(string text)
		{
			// Так как английский язык базовый, вернем текст без перевода.
			// Проверим есть ли перевод и если есть вернем его.
			if (m_translations.ContainsKey(text))
			{
				return m_translations[text];
			}

			// Ругаемся на то что перевода нет, и вернем исходный текст.
			else
			{
				return text;
			}
		}

		/// <summary>
		/// Загрузить язык из папки плагина
		/// </summary>
		/// <param name="file">путь к файлу</param>
		/// <param name="lang">язык, который надо загрузить</param>
		/// <param name="is_key">true - искать атрибут isKey, использовать это значений в качестве ключа</param>
		/// <returns></returns>
		public bool LoadLanguage(string file, string lang, bool is_key)
		{
			Lang = lang;

			XmlDocument doc = new XmlDocument();
			doc.Load(file);

			XmlNodeList items = doc.GetElementsByTagName("items");
			if (items.Count == 0)
			{
				return false;
			}

			m_translations = new Dictionary<string, string>();
			foreach (XmlElement item in items[0].ChildNodes)
			{
				XmlNodeList nodes = item.GetElementsByTagName("word");

				string key = null;
				string value = null;

				//если задан использовать ключ
				if (is_key)
				{

					foreach (XmlNode node in nodes)
					{
						foreach (XmlAttribute att in node.Attributes)
						{
							if (att.Name == "isKey" && att.Value == "true")
							{
								key = node.ChildNodes.Count > 0 ? node.ChildNodes[0].InnerText : null;
							}
						}
					}
				}

				//теперь пробегаем по атрибутам  и ищем атрибут-язык
				foreach (XmlNode node in nodes)
				{

					foreach (XmlAttribute att in node.Attributes)
					{
						if (att.Name == "language")
						{
							//если мы не нашли ключ или он не задан is_key = false
							//в качестве ключа будет использоваться значение Lang языка 
							if (key == null && att.Value == Lang)
							{
								key = node.ChildNodes.Count > 0 ? node.ChildNodes[0].InnerText : null;
							}
							//ищем значение языка-перевода
							else if (att.Value == lang)
							{
								value = node.ChildNodes.Count > 0 ? node.ChildNodes[0].InnerText : null;
							}
						}
					}

				}

				//добавляем в словарь если (key, value) заданы
				if (key != null)
				{
					key = key.Replace("\\r\\n", "\r\n");
					key = key.Replace("\\r", "\r");
					key = key.Replace("\\n", "\n");
					key = key.Replace("\\t", "\t");

					if (value != null)
					{
						value = value.Replace("\\r\\n", "\r\n");
						value = value.Replace("\\r", "\r");
						value = value.Replace("\\n", "\n");
						value = value.Replace("\\t", "\t");
						if (m_translations.ContainsKey(key))
							m_translations[key] = value;
						else
							m_translations.Add(key, value);
					}
				}

			}
			return true;

		}

		/// <summary>
		/// перевод текста на загруженный язык по ключу
		/// перед переводом программа собирает аргументы в {} и заменяет их на индекс аргумента
		/// после перевода текста, аргументы восстанавливаются через string.Format
		/// пример:       text = "report sheet = {Name1} for project = {Name2}"
		/// приводится к формату "report sheet = {0} for project = {1}"
		/// </summary>
		/// <param name="text">текст для перевода с аргументами</param>
		/// <returns>переведенный текст</returns>
		public string TranslateWithUpdatingText(string text)
		{
			int number = 0;
			List<string> res = new List<string>();
			int i = 0;
			//получаем все слова между знаками {}
			//если их нет, то программа пропускает этот шаг
			while (i >= 0 && i <= text.Length)
			{
				i = text.IndexOf('{', i);
				if (i < 0)
					break;
				int j = text.IndexOf('}', i + 1);
				if (j < 0)
					break;

				//получаем текст между ними - это будет наш аргумент
				string new_text = text.Substring(i + 1, j - i - 1);
				i++;
				//заменяем аргумент на индекс в массиве
				text = text.Replace("{" + new_text + "}", "{" + number++ + "}");
				res.Add(new_text);
			}

			//пытаемся перевести, иначе возвращаем текст
			string text_tr = text;
			if (m_translations.ContainsKey(text))
			{
				text_tr = m_translations[text];
			}

			//восстанавливаем аргументы при необходимости
			if (res.Count > 0)
			{
				text_tr = string.Format(text_tr, res.ToArray());
			}

			return text_tr;
		}
	}
}
