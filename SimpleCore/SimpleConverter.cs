﻿using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore
{
	public class SimpleConverter<T,V>		
	{
		Func<T, V> _convBack;
		Func<V, T> _conv;

		public SimpleConverter(Func<T, V> convBack,	Func<V, T> conv)
		{
			_conv = conv;
			_convBack = convBack;
		}

		public T Conv(V src)
		{
			return _conv.Invoke(src);
		}

		public V ConvBack(T src)
		{
			return _convBack.Invoke(src);
		}


	}
}
