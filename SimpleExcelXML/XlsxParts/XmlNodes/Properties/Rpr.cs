﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.XlsxParts.XmlNodes.Charts;
using SimpleFonts;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// Сын DefRPr - тот же функционал, однако в другом блоке (блок R)
	/// </summary>
	internal class Rpr : DefRPr
	{
		public Rpr(Rpr other) : base("rPr", other.prefix, other.sz, other.b, other.i, other.u, null, -1, -1, null, other.Font)
		{
		}

		public Rpr(int sz = 0, int b = 0, int i = 0, Underline u = Underline.none, Fonts f = Fonts.Arial) : base("rPr", "a", sz, b, i, u, null, -1, -1, null, f)
		{
		}

		public Rpr(DefRPr other) : base("rPr", "a", other.sz, other.b, other.i, other.u, null, -1, -1, null, other.Font)
		{
		}

		public Rpr(string name, XElement xpPr) : base(name, xpPr)
		{
			//this.name = "rPr";
		}

		public Rpr(string name, XElement xpPr, DefRPr src) : base(name, xpPr)
		{
			//this.name = "rPr";
		}
	}
}
