﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToWordSplitter.Backend.Parts.Tools
{
	/// <summary>
	/// Сливает колонки index1, index2 по всем Row
	/// </summary>
	public class MergeColumns : ColBase
	{
		public List<int> Rows = new List<int>();
		public int Index2 = -1;
	}
}
