﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleCore.Parts;
using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Interfaces;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Charts;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;
using SimpleExcelXML.XlsxParts.XmlNodes.Properties;
using SimpleFonts;

namespace SimpleExcelXML.XlsxParts
{
	/// <summary>
	/// Файл описывыющий сам график в экселе
	/// </summary>
	internal class XmlChart : IXmlVisibleFile
	{

		private IEnumerable<Color> GetColor()
		{

			var colors = new List<Color>()
			{
				Color.FromArgb(103,162,219),
				Color.FromArgb(243,134,67),
				Color.FromArgb(169,169,169),
				Color.FromArgb(211,158,0),
				Color.FromArgb(55,93,161),
				Color.FromArgb(91,142,57),
				Color.FromArgb(255,197,197),
				Color.FromArgb(0,176,240),
				Color.FromArgb(255,255,128),
				Color.FromArgb(255,128,128),
				//Color.FromArgb(0,176,240),
			};

			while (true)
			{
				foreach (var el in colors)
				{
					yield return el;
				}
			}			
		}
		private IEnumerator<Color> colors;
		public Color curColor
		{
			get
			{
				colors.MoveNext();
				var val = colors.Current;
				return val;
			}
		}

		private IEnumerator<MarkerSymbol> GetMarker()
		{
			var names = Enum.GetNames(typeof(MarkerSymbol));

			while (true)
			{
				foreach (var name in names)
				{
					if (name == "none")
						continue;

					yield return (MarkerSymbol)Enum.Parse(typeof(MarkerSymbol), name);
				}
			}
		}
		private IEnumerator<MarkerSymbol> markers { get; set; }
		public MarkerSymbol curMarker
		{
			get
			{
				var val = markers.Current;
				if (val == MarkerSymbol.none)
				{
					markers.MoveNext();
					val = markers.Current;
				}
				markers.MoveNext();
				return val;
			}
		}


		private string id;
		public string Id
		{
			get
			{
				return id;
			}
			set
			{
				id = value;
				//File = $"chart{id.Remove(0,3)}.xml";
			}
		}
		public string RelativePath { get; } = "xl\\charts";
		public string File { get; private set; }
		public string AbsolutePath { get; set; }
		public string SheetName { get; set; }

		internal ReportChart grapth { get; private set; }
		private XmlRels rels;


		private XmlChart()
		{
			markers = GetMarker();
			colors = GetColor().GetEnumerator();
		}

		public XmlChart(string path, ReportChart areaGrapth, string sheet, int fileId) : this()//, int rId, int fileId)
		{
			AbsolutePath = path;
			//Id = $"rId{rId}";
			File = $"chart{fileId}.xml";
			this.grapth = areaGrapth;
			SheetName = sheet;
		}

		public XmlChart(string path, string file, XmlRels rels) : this()
		{
			AbsolutePath = path;
			File = file;
			this.rels = rels;
		}

		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}

		public string RelativeToFile()
		{
			return "../" + Path.Combine(RelativePath, File).Remove(0, 3).Replace("\\", "/");
		}

		public string ContTypeToFile()
		{
			return Path.Combine(RelativePath, File);
		}

		public void Save()
		{
			var dir = Path.Combine(AbsolutePath, RelativePath);
			dir.CheckDir();

			var file = Path.Combine(dir, File);
			using (var doc = new XmlWriterWrapper(file))
			{
				CreateDoc(doc);
				doc.Save();
			}
		}

		private void SetAxesLikeSettings(Color clr, SimpleFonts.SimpleFontFamily font, TypedAx axe)
		{
			if (clr != null && !clr.IsEmpty)
				axe.spPr.Ln.fill.schemeColor = clr; //Color.LightGray;
			else
			{
				axe.spPr.mainFill.schemeColor = Color.Empty;
				axe.spPr.mainFill.schemeClr = null;
			}

			if (font != null && axe.txPr != null)
			{
				axe.txPr.def.sz = font.Size * 100;
				axe.txPr.def.Font = font.FontName;
				axe.txPr.def.i = font.Italic ? 1 : 0;
				axe.txPr.def.u = font.Underline ? Underline.sng : Underline.none;
				axe.txPr.def.b = font.Bold ? 1 : 0;
				//axe.txPr.def.fill.schemeColor = clr;
			}
		}

		private SpPr DefaultMajor()
		{
			return new SpPr("c", null,
				new Ln("a", 9525, "flat", "sng", "ctr", new SolidFill("a", 15000, 85000, "tx1"), false));
		}

		private SpPr DefaultMinor()
		{
			return new SpPr("c", null,
				new Ln("a", 9525, "flat", "sng", "ctr", new SolidFill("a", 5000, 95000, "tx1"), false));
		}

		private void CreateDoc(XmlWriterWrapper doc)
		{
			var lay = new Layout();
			var spPr = new SpPr();
			var txPr = new TxPr();

			XmlTools.DefaultDeclaration(doc);

			doc.OpenElement("chartSpace", null, XmlTools.xa["c"], "c")
				.AddAtribute("a", XmlTools.xa["a"], "xmlns")
				.AddAtribute("r", XmlTools.xa["r"], "xmlns")
				.AddAtribute("c16r2", XmlTools.xa["c16r2"], "xmlns");

			doc.OpenPrefElement("chart", "c");

			var titleSet = grapth.TitleSettings;
			var titleSpPr = SpPr.DefaultSpPr;
			var titleTxPr = new TxPr(
					"c",
					new BodyPr("a", "ellipsis", "horz", "square", "ctr", 1),
					 LstStyle.Default,
					new DefRPr("defRPr", "a", titleSet.Size * 100, titleSet.Bold ? 1 : 0, titleSet.Italic ? 1 : 0, titleSet.Underline ? Underline.sng : Underline.none, "noStrike", 0, 0,
						new SolidFill("a", 65000, 17000, "tx1"), titleSet.FontName));
			var titleText = new Tx(grapth.Title)
			{
				Body = titleTxPr.body,
				pPr = titleTxPr.def,
				rPr = new Rpr(titleTxPr.def),
				Style = titleTxPr.style
			};

			var title = new Title()
			{
				spPr = SpPr.DefaultSpPr,
				txPr = titleTxPr,
				Text = titleText
			};
			title.Save(doc);

			doc.OpenPrefElement("autoTitleDeleted", "c").AddAtribute("val", "0").CloseElement()

			.OpenPrefElement("plotArea", "c");

			lay.Save(doc);

			var axId = 100;
			var x = new TypedAx()
			{
				axId = axId++,
				type = TypedAx.TypeAx.valAx,
				axPos = AxePos.b,
				minorGridlines = grapth.XAxisHasMinorGridLines ? DefaultMinor() : null,
				majorGridlines = grapth.HasXMajorTickMark ? DefaultMajor() : null,
				spPr = new SpPr(
				"c",
				null,
				new Ln("a", 1, "flat", "sng", "ctr", new SolidFill("a", 15000, 85000, "tx1"),
				true)),
				Tag = "x",
				numFmt = String.IsNullOrEmpty(grapth.XNumberFormat) ? new NumFmt("General") : new NumFmt(grapth.XNumberFormat)
			};

			//x.txPr = null;
			//x.spPr = new SpPr("c", null, new Ln());			
			SetAxesLikeSettings(grapth.XAxisLinesColor, grapth.XLabelsSettings, x);

			if (!string.IsNullOrEmpty(grapth.TitleX))
			{
				x.Title = new Title(grapth.TitleX, grapth.TitleXSettings)
				{
					spPr = null,
					txPr = null
				};
			}

			if (!double.IsNaN(grapth.MinX))
			{
				x.scaling.min = Convert.ToSingle(grapth.MinX);
			}

			if (!double.IsNaN(grapth.MaxX))
			{
				x.scaling.max = Convert.ToSingle(grapth.MaxX);
			}

			if (grapth.XAxisHasMinorGridLines)
			{
				x.minorGridlines.Ln.fill.schemeColor = grapth.XMinorGridLinesColor;
				//x.minorGridlines.ln.w = 9000;
			}
			else
			{
				x.minorGridlines = null;
			}

			if (grapth.HasXMajorTickMark)
			{
				x.majorGridlines.Ln.fill.schemeColor = grapth.XMajorGridLinesColor;
				if (grapth.XMajorUnits > 0)
				{
					x.majorUnit = grapth.XMajorUnits;
				}
			}
			else
			{
				x.majorGridlines = null;
			}

			var y = new TypedAx()
			{
				axId = axId++,
				type = TypedAx.TypeAx.valAx,
				crossAx = x.axId,
				spPr = new SpPr(
				"c",
				null,
				new Ln("a", 1, "flat", "sng", "ctr", new SolidFill("a", 15000, 85000, "tx1"),
				true)),
				axPos = AxePos.l,
				minorGridlines = grapth.YAxisHasMinorGridLines ? DefaultMinor() : null,
				majorGridlines = grapth.HasYMajorTickMark ? DefaultMajor() : null,
				Tag = "y",
				numFmt = String.IsNullOrEmpty(grapth.YNumberFormat) ? new NumFmt("General") : new NumFmt(grapth.YNumberFormat)
			};

			SetAxesLikeSettings(grapth.YAxisLinesColor, grapth.YLabelsSettings, y);

			if (!string.IsNullOrEmpty(grapth.TitleY))
			{
				y.Title = new Title(grapth.TitleY, grapth.TitleYSettings)
				{
					spPr = null,
					txPr = null,

				};
			}

			if (!double.IsNaN(grapth.MinY))
			{
				y.scaling.min = Convert.ToSingle(grapth.MinY);
			}

			if (!double.IsNaN(grapth.MaxY))
			{
				y.scaling.max = Convert.ToSingle(grapth.MaxY);
			}

			if (grapth.YAxisHasMinorGridLines)
			{
				y.minorGridlines.Ln.fill.schemeColor = grapth.YMinorGridLinesColor;
				//y.minorGridlines.ln.w = 9000;
			}
			else
			{
				y.minorGridlines = null;
			}

			if (grapth.HasYMajorTickMark)
			{
				y.majorGridlines.Ln.fill.schemeColor = grapth.YMajorGridLinesColor;
				if (grapth.YMajorUnits > 0)
				{
					y.majorUnit = grapth.YMajorUnits;
				}
			}
			else
			{
				y.majorGridlines = null;
			}

			if (grapth.IsAxisYRevers)
			{
				y.scaling.orientation = Orientation.maxMin;
				y.axPos = AxePos.t;
			}

			x.crossAx = y.axId;

			var axes = new List<TypedAx>() { x, y };

			var id = 0;
			var order = 0;
			var dataByType = new Dictionary<ChartTypeEnum, List<Ser>>();
			foreach (var chart in grapth.Items)
			{
				var ser = new Ser(chart, SheetName, this);

				if (!dataByType.ContainsKey(chart.ChartType))
				{
					dataByType.Add(chart.ChartType, new List<Ser>() { ser });
				}
				else
				{
					dataByType[chart.ChartType].Add(ser);
				}
			}

			foreach (var el in dataByType)
			{
				foreach (var ser in el.Value)
				{
					ser.idx = id++;
					ser.order = order++;
				}

				WriteGrapth(doc, el.Key, el.Value, axes);
			}

			foreach (var el in axes)
			{
				el.Save(doc);
			}

			spPr.Save(doc);

			doc.CloseElement();

			if (grapth.ShowLegend)
			{
				titleSet = grapth.LegendSettings;
				var legend = new Legend()
				{
					txPr = new TxPr("c", new BodyPr("a", "ellipsis", "horz", "square", "ctr", 1, 1),
					LstStyle.Default,
					new DefRPr("defRPr", "a", titleSet.Size * 100, titleSet.Bold ? 1 : 0, titleSet.Italic ? 1 : 0, titleSet.Underline ? Underline.sng : Underline.none, "noStrike", 0, -1)
					{
						fill = new SolidFill("c", 65000, 35000, "tx1")
					})
				};

				legend.Save(doc);
			}

			doc.OpenPrefElement("plotVisOnly", "c").AddAtribute("val", "1").CloseElement()
				.OpenPrefElement("dispBlanksAs", "c").AddLastAtr("gap").CloseElement()
				.OpenPrefElement("showDLblsOverMax", "c").AddLastAtr("0").CloseElement();

			doc.CloseElement();

		}

		private void WriteGrapth(XmlWriterWrapper doc, ChartTypeEnum type, List<Ser> series, List<TypedAx> axes)
		{
			XmlPropBase obj = null;

			switch (type)
			{
				case ChartTypeEnum.XYGraphic:
				case ChartTypeEnum.Graphic:
				case ChartTypeEnum.Points:
					{
						obj = new XYChart()
						{
							Axes = axes,
							Data = series
						};
						break;
					}
				case ChartTypeEnum.Histogram:
					{
						obj = new BarChart()
						{
							Data = series,
							Axes = axes
						};
						break;
					}
			}

			obj.Save(doc);
		}

		internal void TryLoad()
		{
			var file = AbsoluteToFile();
			var dir = Directory.GetDirectoryRoot(file);

			if (!System.IO.File.Exists(file))
			{
				return;
			}

			var doc = XDocument.Load(file);
			var root = doc.Root;
			var ns = root.GetDefaultNamespace();
			var c = root.GetNamespaceOfPrefix("c");

			var chartMain = root.Element(c + "chart");
			var title = chartMain.Element(c + "title");
			var area = chartMain.Element(c + "plotArea");
			var legend = chartMain.Element(c + "legend");

			grapth = new ReportChart();

			Title xTitle = null;
			if (title != null)
			{
				xTitle = new Title(title);
				var xTlPr = xTitle?.txPr?.def;

				grapth.Title = xTitle?.Text?.Value;

				if (xTlPr != null)
					grapth.TitleSettings = new SimpleFonts.SimpleFontFamily(xTlPr.Font, xTlPr.sz / 100)
					{
						Bold = xTlPr.b == 1 ? true : false,
						Italic = xTlPr.i == 1 ? true : false,
						Underline = xTlPr.u == Underline.sng ? true : false
					};
			}

			Legend xLegend = null;
			if (legend != null)
			{
				xLegend = new Legend(legend);
				var legFnt = xLegend?.txPr?.def;

				if (legFnt != null)
				{
					grapth.ShowLegend = true;
					grapth.LegendSettings = new SimpleFonts.SimpleFontFamily(legFnt.Font, legFnt.sz / 100)
					{
						Bold = legFnt.b == 1 ? true : false,
						Italic = legFnt.i == 1 ? true : false,
						Underline = legFnt.u == Underline.sng ? true : false
					};
				}
			}

			var xProp = area.GetSpPr();
			SpPr prop = null;
			if (xProp.HasInfo())
				prop = new SpPr(xProp);

			var xAxes = area.Elements(c + "catAx").ToList();
			xAxes.AddRange(area.Elements(c + "valAx"));

			var IsY = false;
			var IsX = false;
			var axes = new List<TypedAx>();
			foreach (var xAxe in xAxes)
			{
				var axe = new TypedAx(xAxe);

				//y axe
				if (!IsY && axe.axPos == AxePos.l || axe.axPos == AxePos.r)
				{
					IsY = true;
					grapth.YAxisLinesColor = axe.spPr?.mainFill?.schemeColor ?? Color.Empty;
					grapth.YLabelsSettings = ParseSimpleFont(axe.txPr?.def);
					grapth.TitleYSettings = ParseSimpleFont(axe?.Title?.Text?.pPr ?? axe?.Title?.txPr?.def);
					grapth.TitleY = axe.Title?.Text?.Value;
					grapth.MinY = axe.scaling.min;
					grapth.MaxY = axe.scaling.max;
					if (axe.minorGridlines != null)
					{
						grapth.YMinorGridLinesColor = axe.minorGridlines.Ln.fill.schemeColor;
						grapth.YAxisHasMinorGridLines = true;
					}

					if (axe.majorGridlines != null)
					{
						grapth.HasYMajorTickMark = true;
						grapth.YMajorGridLinesColor = axe.majorGridlines.Ln.fill.schemeColor;
						grapth.YMajorUnits = axe.majorUnit;
					}
					grapth.IsAxisYRevers = axe.scaling.orientation == Orientation.maxMin ? true : false;
				}
				//x axe
				else if (!IsX)
				{
					IsX = true;
					grapth.XAxisLinesColor = axe.spPr?.mainFill?.schemeColor ?? Color.Empty;
					grapth.XLabelsSettings = ParseSimpleFont(axe.txPr?.def);
					grapth.TitleXSettings = ParseSimpleFont(axe?.Title?.Text?.pPr ?? axe?.Title?.txPr?.def);
					grapth.TitleX = axe.Title?.Text?.Value;
					grapth.MinX = axe.scaling.min;
					grapth.MaxX = axe.scaling.max;

					if (axe.minorGridlines != null)
					{
						grapth.XMinorGridLinesColor = axe.minorGridlines.Ln.fill.schemeColor;
						grapth.XAxisHasMinorGridLines = true;
					}

					if (axe.majorGridlines != null)
					{
						grapth.HasXMajorTickMark = true;
						grapth.XMajorGridLinesColor = axe.majorGridlines?.Ln?.fill?.schemeColor ?? Color.Empty;
						grapth.XMajorUnits = axe.majorUnit;
					}
				}
			}

			var xCharts = area.Elements().Where((v) => v.Name.LocalName.Contains("Chart"));

			var charts = new List<IXmlChart>();
			foreach (var xChrt in xCharts)
			{
				var cur = ReadGrapth(xChrt);

				foreach (var ser in cur.Data)
				{
					var item = new ReportChartItem
					{
						ChartType = ParseType(cur, ser),
						Color = ser?.LineProperty?.mainFill?.schemeColor ?? Color.Empty,
						LineWidth = ser?.LineProperty?.Ln?.w / 1000 ?? 0,
						Title = ser?.title?.Values?.Replace("{", "")?.Replace("}", "")
					};

					var xVals = ser.ranges.FirstOrDefault((v) => v.IsX);
					if (xVals != null && String.IsNullOrEmpty(item.RangeX))
					{
						item.IsInLineValueX = xVals.IsInLine;
						item.RangeX = xVals.Values;
					}

					var yVals = ser.ranges.FirstOrDefault((v) => v.IsY);
					if (yVals == null && ser.ranges.Count > 0)
					{
						yVals = ser.ranges.First((v) => v != xVals);
					}
					if (yVals != null && String.IsNullOrEmpty(item.RangeY))
					{
						item.IsInLineValueY = yVals.IsInLine;
						item.RangeY = yVals.Values;
					}

					grapth.Items.Add(item);
				}

				charts.Add(cur);
			}


		}

		private ChartTypeEnum ParseType(IXmlChart cur, Ser ser)
		{
			if (cur is XYChart)
			{
				var tmp = (XYChart)cur;
				//var first = tmp.Data.First();

				if (tmp.scatterStyle == ScatterStyle.lineMarker && ser.Marker != Marker.DefaultMarker && ser.LineProperty == SpPr.DefaultSpPr)
				{
					return ChartTypeEnum.Points;
				}

				if (tmp.scatterStyle == ScatterStyle.smoothMarker && ser.ranges.All((v) => v.IsY))
				{
					return ChartTypeEnum.Graphic;
				}

				return ChartTypeEnum.XYGraphic;
			}

			if (cur is BarChart)
				return ChartTypeEnum.Histogram;

			return ChartTypeEnum.XYGraphic;
		}

		private SimpleFontFamily ParseSimpleFont(DefRPr def)
		{
			if (def == null)
				return null;

			return new SimpleFontFamily(def.Font, def.sz / 100)
			{
				Bold = def.b == 1 ? true : false,
				Italic = def.i == 1 ? true : false,
				Underline = def.u == Underline.sng ? true : false
			};
		}

		private IXmlChart ReadGrapth(XElement xChrt)
		{
			var name = xChrt.Name.LocalName;
			if (BarChart.IsBarChar(name))
			{
				return new BarChart(xChrt);
			}

			if (XYChart.IsXYChart(name))
			{
				return new XYChart(xChrt);
			}

			return null;
		}
	}
}
