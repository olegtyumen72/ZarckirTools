﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.Tools;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// Текстовый обьект со значением. Т.е. Конкретный обьект и его параметры
	/// </summary>
	internal class Tx
	{
		public BodyPr Body { get; set; } = BodyPr.Default;
		public LstStyle Style { get; set; } = LstStyle.Default;
		public DefRPr pPr { get; set; } = null;
		public Rpr rPr { get; set; } = null;
		public string Value { get; protected set; } = null;

		public Tx(string value)
		{
			Value = value;
		}

		public Tx(XElement xTxt)
		{
			TryLoad(xTxt);
		}

		private void TryLoad(XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");
			var a = root.GetNamespaceOfPrefix("a");
			var rich = root.Element(c + "rich");
			if (rich == null && rich.IsFullEmpty())
				return;

			var xBody = rich.Element(a + "bodyPr");
			if (xBody != null && !xBody.IsFullEmpty())
				Body = new BodyPr(xBody);

			var xStyle = rich.Element(a + "lstStyle");
			if (xStyle != null && !xStyle.IsFullEmpty())
				Style = new LstStyle(xStyle);

			var p = rich.GetP();
			if (p == null || p.IsFullEmpty())
				return;

			var xpPr = p.FirstPPR().Element(a + "defRPr");
			if(xpPr != null && !xpPr.IsFullEmpty())
			{
				pPr = new DefRPr("defRPr",xpPr);
			}	

			var r = p.Element(a + "r");
			var xrPr = r.FirstRPR();
			if (xrPr != null && !xrPr.IsFullEmpty())
			{
				if (pPr != null && xrPr.HasAtr("lang"))
					rPr = new Rpr(pPr);
				else
					rPr = new Rpr("rPr", xrPr);
			}

			var t = r.Element(a + "t");
			if (t != null)
				Value = t.Value;
		}

		internal void Save(XmlWriterWrapper doc)
		{
			if (String.IsNullOrEmpty(Value)) return;
			if (pPr == null && rPr == null) throw new Exception("Miss properies for element");

			doc.OpenPrefElement("tx", "c");
			{
				doc.OpenPrefElement("rich", "c");
				{
					Body.Save(doc);
					Style.Save(doc);

					doc.OpenPrefElement("p", "a");
					{
						doc.OpenPrefElement("pPr", "a");
						{
							var mainProp = pPr ?? rPr;
							mainProp.name = "defRPr";
							mainProp.Save(doc);
						}
						doc.CloseElement();

						doc.OpenPrefElement("r", "a");
						{
							var mainProp = rPr ?? pPr;
							mainProp.name = "rPr";							
							mainProp.Save(doc);

							doc.OpenPrefElement("t", "a", Value).CloseElement();
						}
						doc.CloseElement();
					}
					doc.CloseElement();
				}
				doc.CloseElement();
			}
			doc.CloseElement();


		}
	}
}
