﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// Формат чисел для кого-нибудь обьекта
	/// </summary>
	internal class NumFmt : XmlPropBase
	{
		public static NumFmt Default { get { return new NumFmt("General"); } }

		string formatCode { get; set; }
		int sourceLinked { get; set; }
		string name { get; set; } = "numFmt";

		public NumFmt(string format, int linked = 0)
		{
			formatCode = format;
			sourceLinked = linked;
		}

		public NumFmt(XElement xNumFmt) : base(xNumFmt)
		{
		}

		public override void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement(name, "c").AddAtribute(nameof(formatCode), formatCode).AddAtribute(nameof(sourceLinked), sourceLinked)
				.CloseElement();
		}

		public override void TryLoad(XElement root)
		{
			var atrs = XDocumentTools.ParseAtrs(root);

			formatCode = ReadAtrValue(atrs, nameof(formatCode));
			sourceLinked = ReadAtrValue(atrs, nameof(sourceLinked)).ToInt(0);
		}
	}
}
