﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpoiWrapper.Tools
{
	/// <summary>
	/// Данные для подтаблиц, которые в совокупности определяют 1-у большую таблицу
	/// Используется для создания обычных таблиц или одной таблицы, разбитой на несколько частей
	/// Каждый Key - данные подтаблицы
	/// Каждый Value - часть основной таблицы, которая будет присутствовать у всех подтаблиц (может быть пустой или нулевой)
	/// </summary>
	public class TablePart
	{		
		public KeyValuePair<Range, Range> Header;
		public Dictionary<Range, Range> Body;
	}
}
