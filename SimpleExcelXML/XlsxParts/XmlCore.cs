﻿using SimpleExcelXML.Interfaces;
using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts
{
	/// <summary>
	/// Файл настройки - его функции неизвестны, просто нужен
	/// </summary>
	class XmlCore : IXmlVisibleFile
	{

		string Owner;
		string ModifBy;
		DateTime DateCreate;
		DateTime DateModif;

		public string RelativePath { get; } = "docProps\\";
		public string File { get; } = "core.xml";
		public string AbsolutePath { get; set; }
		public string Id { get; set; } //= "rId2";

		public XmlCore(string creator, string modBy, DateTime created, DateTime modified, string path)//, int id)
		{
			Owner = creator;
			DateCreate = created;
			DateModif = modified;
			ModifBy = modBy;
			AbsolutePath = path;
			//Id = $"rId{id}";
		}

		public void Save()
		{
			var dir = Path.Combine(AbsolutePath, RelativePath);
			dir.CheckDir();

			var file = Path.Combine(dir, File);
			//file.CheckTxtFile();

			using (var doc = new XmlWriterWrapper(file))
			{
				CreateDoc(doc);
				doc.Save();
			}
		}

		private void CreateDoc(XmlWriterWrapper doc)
		{
			XmlTools.DefaultDeclaration(doc);

			doc.OpenElement("coreProperties", null, XmlTools.xa["cpCore"], "cp")
				.AddAtribute("dc", XmlTools.xa["dc"], "xmlns")
				.AddAtribute("dcterms", XmlTools.xa["dcterms"], "xmlns")
				.AddAtribute("dcmitype", XmlTools.xa["dcmitype"], "xmlns")
				.AddAtribute("xsi", XmlTools.xa["xsi"], "xmlns")

				.OpenElement("creator", Owner, XmlTools.xa["dc"]).CloseElement()
				.OpenElement("lastModifiedBy", ModifBy, XmlTools.xa["cpCore"]).CloseElement()

				.OpenElement("created", null, XmlTools.xa["dcterms"])
				.AddAtribute("type", "dcterms:W3CDTF", "xsi", XmlTools.xa["xsi"])
				.UpdateValue(DateCreate.ToUniversalTime().ToString("o"))
				.CloseElement()

				.OpenElement("modified", null, XmlTools.xa["dcterms"])
				.AddAtribute("type", "dcterms:W3CDTF", "xsi", XmlTools.xa["xsi"])
				.UpdateValue(DateModif.ToUniversalTime().ToString("o"))
				.CloseElement()

				.CloseElement();
		}

		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}

		public string RelativeToFile()
		{
			return Path.Combine(RelativePath, File);
		}
	}
}
