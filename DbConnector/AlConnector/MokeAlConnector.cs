﻿using AssociativeList;
using DbConnector.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbConnector.AlConnector
{
	public class MokeAlConnector : IDbAlConnector
	{
		public SqlVers SqlVersion
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public bool CngConDb(string name)
		{
			throw new NotImplementedException();
		}

		public bool CreateDb(string name)
		{
			throw new NotImplementedException();
		}

		public bool CreateTable(string table, List<string> col, List<string> etc)
		{
			throw new NotImplementedException();
		}

		public bool DeleteQuery(string table, string where)
		{
			throw new NotImplementedException();
		}

		public bool DeleteTable(string table)
		{
			throw new NotImplementedException();
		}

		public void Dispose()
		{
			throw new NotImplementedException();
		}

		public bool DropDb(string name)
		{
			throw new NotImplementedException();
		}

		public AList GetDictionary(string table)
		{
			throw new NotImplementedException();
		}

		public V GetKeyFromDb<V>(string tbl, string col, string matchCol, string matchVal)
		{
			throw new NotImplementedException();
		}

		public V GetKeyFromDbW<V>(string tbl, string col, string where)
		{
			throw new NotImplementedException();
		}

		public V GetKeyFromDic<V>(string table, string col, string targer)
		{
			throw new NotImplementedException();
		}

		public V GetLast<V>(string table, string col)
		{
			throw new NotImplementedException();
		}

		public bool InsertQuery(string table, string col, string values)
		{
			throw new NotImplementedException();
		}

		public bool IsEmpty(AList data, string col = "")
		{
			throw new NotImplementedException();
		}

		public AList Query(string query)
		{
			throw new NotImplementedException();
		}

		public ISqlPartData<AList> SelectPartial(string table, string col, int offset, int count, string colForOrder = "", string where = "", bool isLazy = true)
		{
			throw new NotImplementedException();
		}

		public AList SelectQuery(string table, string col, string where = "", string etc = "")
		{
			throw new NotImplementedException();
		}

		public bool TruncateTable(string table)
		{
			throw new NotImplementedException();
		}

		public bool UpdateWithDuplicate(string table, string col, string value)
		{
			throw new NotImplementedException();
		}

		AList IDbConnector<AList>.GetDictionary(string table)
		{
			throw new NotImplementedException();
		}

		AList IDbConnector<AList>.Query(string query)
		{
			throw new NotImplementedException();
		}

		ISqlPartData<AList> IDbConnector<AList>.SelectPartial(string table, string col, int offset, int count, string colForOrder, string where, bool isLazy)
		{
			throw new NotImplementedException();
		}

		AList IDbConnector<AList>.SelectQuery(string table, string col, string where, string etc)
		{
			throw new NotImplementedException();
		}
	}
}
