﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Parts
{

	public interface IReportSheet: IList<IReportLine>, ICoreReportSheet
	{

		//IReportSheet Clone();
		//IReportLine this[int row, string colName] { get; }

		IReportLine GetLine(int row);
		
	}
}
