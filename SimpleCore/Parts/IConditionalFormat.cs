﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Parts
{
	public enum ConditionType { CellVal, ColorScale, Expression, Top10, IconSet, Text }
	public enum ConditionOperator { Equal, NotEual, Between, Greater, GreaterEqual, Less, LessEqual, NotBetwenn, }
	public enum TextOperators { BeginsWith, Contains, DoesNotContain, EndsWith }

	public interface IConditionalFormat: ISimpleClone<IConditionalFormat>
	{
		Color Background { get; set; }
		Color Font { get; set; }
		ConditionType CondType { get; set; }
		string Condition { get; set; }
		ConditionOperator Operator { get; set; }
		TextOperators TxtOperat { get; set; }
	}
}
