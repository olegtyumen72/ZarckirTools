﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToWordSplitter.Backend.Parts.Tools
{
	/// <summary>
	/// Задаёт высоту строчки по индексу
	/// </summary>
	public class FormateRow : RowBase
	{
		public int RowHeight = -1;
	}
}
