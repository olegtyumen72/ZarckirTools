﻿using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Charts
{
	internal class Scaling: XmlPropBase
	{

		public static Scaling Default { get { return new Scaling(Orientation.minMax, -1, -1); } }

		public Orientation orientation { get; set; } = Orientation.minMax;
		public double max { get; set; } = -1;
		public double min { get; set; } = -1;



		public Scaling(Orientation orient = Orientation.minMax, int max = -1, int min = -1)
		{
			orientation = orient;
			this.max = max;
			this.min = min;
		}

		public Scaling(XElement xRoot) : base(xRoot)
		{
		}

		public override void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("scaling", "c")
				.OpenPrefElement(nameof(orientation), "c").AddAtribute("val", orientation.ToString()).CloseElement();

			if (max != -1)
				doc.OpenPrefElement("max", "c").AddAtribute("val", max.ToString()).CloseElement();

			if (min != -1)
				doc.OpenPrefElement("min", "c").AddAtribute("val", min.ToString()).CloseElement();

			doc.CloseElement();
		}

		public override void TryLoad(XElement root)
		{
			var atrs = XDocumentTools.ParseAtrs(root);

			orientation = ParseOrientation(ReadAtrValue(atrs, "orientation@val"));
			max = ReadAtrValue(atrs, "max@val")?.ToDbl() ?? -1;
			min = ReadAtrValue(atrs, "min@val")?.ToDbl() ?? -1;

		}	

		private Orientation ParseOrientation(string v)
		{
			var res = Orientation.minMax;
			if(Enum.TryParse(v, out res))
			{
				return res;
			}

			return Orientation.minMax;
		}

		protected override bool IsWritable(double val)
		{
			return val != -1;
		}
	}
}
