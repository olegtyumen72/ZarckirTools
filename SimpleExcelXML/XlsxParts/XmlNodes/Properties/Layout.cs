﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// Функционал класса не ивестен, однако нужен для корректной работы
	/// </summary>
	internal class Layout: XmlPropBase
	{
		public Layout() : base()
		{

		}

		public Layout(XElement xLayout) : base(xLayout)
		{
		}

		public static Layout DefaultLayout { get { return new Layout(); } }

		
		public override void TryLoad(XElement root)
		{
			
		}

		public override void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("layout", "c").CloseElement();
		}
	}
}
