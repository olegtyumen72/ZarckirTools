﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleCore.Templates;
using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.XlsxParts.XmlNodes.Sheets;

namespace SimpleExcelXML.Tools
{
	internal class ShortFormatItem : ReportFormatItem
	{

		//public override ReportFormatItemTmpl<CellRangeTmpl, ConditionalFormatTmpl> Clone()
		//{
		//	var res = new ReportFormatItem(Range, ConditionalFormats)
		//	{
		//		Col = this.Col,
		//		Row = this.Row,
		//		NFormat = this.NFormat,
		//		FontFormat = this.FontFormat == null ? null : this.FontFormat.Clone(),
		//		BordersAll = this.BordersAll,
		//		HorizontalAligmentCenter = this.HorizontalAligmentCenter,
		//		VerticalAligmentCenter = this.VerticalAligmentCenter,
		//		//AutoFit = this.AutoFit,
		//		HideColumns = this.HideColumns,
		//		HideRows = this.HideRows,
		//		//Range = this.Range,
		//		ColumnWidth = this.ColumnWidth,
		//		RowOrientation = this.RowOrientation,
		//		ColumnsOrientation = this.ColumnsOrientation,
		//		Wrap = this.Wrap,
		//		MergeArea = this.MergeArea,
		//		MergeColumns = this.MergeColumns,
		//		AllRows = this.AllRows,
		//		AllCols = this.AllCols,
		//		Background = this.Background,
		//		//ConditionalFormats = this.ConditionalFormats.Select((v) => v).ToList(),
		//		RowHeight = this.RowHeight,
		//		IsSingle = this.IsSingle,
		//		//AutoFitCols = this.AutoFitCols,
		//		//MergeRows = this.MergeRows,
		//		NumFmtId = this.NumFmtId
		//	};

		//	return res;
		//}
	}

	internal class FormatCollection : Dictionary<ShortFormatItem, List<CellRangeTmpl>>
	{
		public List<CellRangeTmpl> this[int key]
		{
			get
			{
				return this.FirstOrDefault((v) => v.Key.StyleId == key).Value;
			}
		}

		public bool ContainsKey(int key)
		{
			return this.Any((v) => v.Key.StyleId == key);
		}
	}

	/// <summary>
	/// Класс-коллекция для загрузки стилей из Reportsheet
	/// </summary>
	internal class StylesRangeCollection : IEnumerable<KeyValuePair<ShortFormatItem, List<CellRangeTmpl>>>
	{

		public ShortFormatItem this[int key]
		{
			get
			{
				return styles.FirstOrDefault((v) => v.Key.StyleId == key).Key;
			}
		}


		public FormatCollection styles { get; private set; }
		public IEnumerable<ShortFormatItem> Keys
		{
			get
			{
				return styles.Keys;
			}
		}

		public StylesRangeCollection()
		{
			styles = new FormatCollection();
		}

		public void AddColRange(ShortFormatItem styleF, int minCol, int maxCol)
		{
			Add(styleF);

			bool isExpanded = false;
			for (int j = 0; j < styles[styleF].Count; j++)
			{
				var range = styles[styleF][j];

				var near = range.NearColumnCell(minCol);
				if (near != int.MinValue && range.IsColumn)
				{
					range.ExpandColumn(minCol, maxCol);
					isExpanded = true;
					break;
				}

				near = range.NearColumnCell(maxCol);
				if (near != int.MinValue && range.IsColumn)
				{
					range.ExpandColumn(minCol, maxCol);
					isExpanded = true;
					break;
				}
			}

			if (!isExpanded)
				styles[styleF].Add(new CellRangeTmpl(minCol, -1, maxCol, -1));
		}

		public void AddCellByColumn(ShortFormatItem styleF, int col, int row)
		{
			Add(styleF);

			bool isExpanded = false;
			for (int i = 0; i < styles[styleF].Count; i++)
			{
				var range = styles[styleF][i];

				var keyCol = range.NearColumnCell(col);
				if (keyCol != int.MinValue && range.IsColumn && range.ContainRow(row))
				{
					range.ExpandColumn(col);
					isExpanded = true;
					break;
				}
			}

			if (!isExpanded)
				styles[styleF].Add(new CellRangeTmpl(col, row, col, row));
		}

		public bool ContainsStyleWidth(ShortFormatItem key)
		{
			return styles.Any((v) => v.Key.StyleId == key.StyleId && v.Key.ColumnWidth == key.ColumnWidth);
		}

		public bool ContainsKey(ShortFormatItem key)
		{
			return styles.ContainsKey(key);
		}

		public bool ContainsKey(int keyId)
		{
			return styles.ContainsKey(keyId);
		}

		public void Add(ShortFormatItem item, List<CellRangeTmpl> list = null)
		{
			if (!ContainsKey(item))
				styles.Add(item, list ?? new List<CellRangeTmpl>());
		}

		internal ShortFormatItem GetByStyleAndWidth(int styleId, double columnWidth)
		{
			return styles.First((v) => v.Key.StyleId == styleId && v.Key.ColumnWidth == columnWidth).Key;
		}

		internal void IndexToCells()
		{
			foreach (var style in styles)
			{
				foreach (var rng in style.Value)
				{
					if (rng.cell1_Col >= 0)
						rng.cell1_Col += 1;
					if (rng.cell1_Row >= 0)
						rng.cell1_Row += 1;
					if (rng.cell2_Col >= 0)
						rng.cell2_Col += 1;
					if (rng.cell2_Row >= 0)
						rng.cell2_Row += 1;
				}
			}
		}

		#region Old
		//internal void MergeRanges()
		//{
		//	foreach (var stlRngs in styles)
		//	{
		//		var ranges = stlRngs.Value;

		//		for (int i = 0; i < ranges.Count; i++)
		//		{
		//			var cur = ranges[i];

		//			for (int j = 0; j < ranges.Count; j++)
		//			{
		//				var next = ranges[j];

		//				if (cur == next)
		//					continue;

		//				if (cur.IsColumn)
		//				{
		//					if (next.IsColumn)
		//					{
		//						if (cur.cell1_Row == next.cell1_Row)
		//						{
		//							var key = cur.NearColumnCell(next);
		//							if (key == int.MinValue)
		//								continue;

		//							cur.ExpandColumn(next.cell1_Col, next.cell2_Col);
		//							ranges.Remove(next);
		//							continue;
		//						}
		//						else if (cur.NearRow(next.cell1_Row))
		//						{
		//							if (cur.cell1_Col != next.cell1_Col || cur.ColumnCount() != next.ColumnCount())
		//								continue;

		//							//var key = cur.NearColumnCell(next);
		//							cur.ExpandRow(next.cell1_Row, next.cell2_Row);
		//							ranges.Remove(next);
		//							continue;
		//						}
		//					}
		//					else if (next.IsRow)
		//					{
		//						continue;
		//					}
		//					else if (next.IsRect)
		//					{
		//						if (!next.NearRow(cur.cell1_Row))
		//							continue;
		//						if (cur.cell1_Col != next.cell1_Col || cur.ColumnCount() != next.ColumnCount())
		//							continue;

		//						var keyRow = next.NearRowCell(cur.cell1_Row);
		//						next.ExpandRow(cur.cell1_Row, cur.cell2_Row);

		//						ranges.Remove(next);
		//						cur = next;
		//						if (ranges.Count > i)
		//							ranges[i] = next;
		//						else
		//							ranges.Add(next);
		//					}
		//					else
		//					{
		//						throw new Exception("Miss range type");
		//					}
		//				}
		//				else if (cur.IsRow)
		//				{
		//					if (next.IsColumn)
		//					{
		//						continue;
		//					}
		//					else if (next.IsRow)
		//					{
		//						if (cur.cell1_Col == next.cell1_Col)
		//						{
		//							var key = cur.NearRowCell(next.cell1_Row);
		//							if (key == int.MinValue)
		//								continue;

		//							cur.ExpandRow(next.cell1_Row, next.cell2_Row);
		//							ranges.Remove(next);
		//							continue;
		//						}
		//						else if (cur.NearColumn(next.cell1_Col))
		//						{
		//							if (cur.cell1_Row != next.cell1_Row || cur.RowCount() != next.RowCount())
		//								continue;

		//							//var key = cur.NearColumnCell(next);
		//							cur.ExpandColumn(next.cell1_Col, next.cell2_Col);
		//							ranges.Remove(next);
		//							continue;
		//						}
		//					}
		//					else if (next.IsRect)
		//					{
		//						if (!next.NearColumn(cur.cell1_Col))
		//							continue;
		//						if (cur.cell1_Row != next.cell1_Row || cur.ColumnCount() != next.ColumnCount())
		//							continue;

		//						//var keyRow = next.NearRowCell(cur.cell1_Row);
		//						next.ExpandColumn(cur.cell1_Col, cur.cell2_Col);

		//						ranges.Remove(next);
		//						cur = next;
		//						if (ranges.Count > i)
		//							ranges[i] = next;
		//						else
		//							ranges.Add(next);
		//					}
		//					else
		//					{
		//						throw new Exception("Miss range type");
		//					}
		//				}
		//				else if (cur.IsRect)
		//				{
		//					if (next.IsRow)
		//					{
		//						if (!cur.NearColumn(next.cell1_Col))
		//							continue;
		//						if (cur.cell1_Row != next.cell1_Row || cur.ColumnCount() != next.ColumnCount())
		//							continue;

		//						//var keyRow = next.NearRowCell(cur.cell1_Row);
		//						cur.ExpandColumn(next.cell1_Col, next.cell2_Col);

		//						ranges.Remove(next);
		//						//cur = next;
		//						//if (ranges.Count > i)
		//						//	ranges[i] = next;
		//						//else
		//						//	ranges.Add(next);
		//					}
		//					else if (next.IsColumn)
		//					{
		//						if (!cur.NearRow(next.cell1_Row))
		//							continue;
		//						if (cur.cell1_Col != next.cell1_Col || cur.ColumnCount() != next.ColumnCount())
		//							continue;

		//						//var keyRow = cur.NearRowCell(next.cell1_Row);
		//						cur.ExpandRow(next.cell1_Row, next.cell2_Row);

		//						ranges.Remove(next);
		//						//cur = next;
		//						//if (ranges.Count > i)
		//						//	ranges[i] = next;
		//						//else
		//						//	ranges.Add(next);
		//					}
		//					else if (next.IsRect)
		//					{
		//						var tmpKey = cur.NearColumnCell(next);
		//						if (tmpKey != int.MinValue)
		//						{
		//							if (cur.cell1_Row != next.cell1_Row || cur.ColumnCount() != next.ColumnCount())
		//								continue;

		//							cur.ExpandColumn(next.cell1_Col, next.cell2_Col);

		//							ranges.Remove(next);
		//						}

		//						tmpKey = cur.NearRowCell(next);
		//						if (tmpKey != int.MinValue)
		//						{
		//							if (cur.cell1_Col != next.cell1_Col || cur.ColumnCount() != next.ColumnCount())
		//								continue;

		//							cur.ExpandRow(next.cell1_Row, next.cell2_Row);

		//							ranges.Remove(next);
		//						}

		//					}
		//					else
		//					{
		//						throw new Exception("Miss range type");
		//					}
		//				}
		//			}
		//		}


		//	}
		//}
		#endregion

		internal void MergeRanges()
		{
			foreach (var stlRngs in styles)
			{
				var ranges = stlRngs.Value;

				for (int i = 0; i < ranges.Count; i++)
				{
					var cur = ranges[i];

					for (int j = 0; j < ranges.Count; j++)
					{
						var next = ranges[j];

						if (next == cur)
							continue;

						if (ExpandByColum(cur, next) || ExpandByRow(cur, next))
						{
							ranges.Remove(next);
							j--;
						}
					}
				}
			}
		}

		internal bool ExpandByColum(CellRangeTmpl src, CellRangeTmpl dest)
		{
			#region Old
			//if (dest.IsRow)
			//{
			//	if (!src.NearColumn(dest.cell1_Col))
			//		return false;
			//	if (src.cell1_Row != dest.cell1_Row || src.CellCount() != dest.CellCount())
			//		return false;

			//	src.ExpandColumn(dest.cell1_Col, dest.cell2_Col);
			//}
			//else if (dest.IsColumn)
			//{
			//	if (!src.NearRow(dest.cell1_Row))
			//		return false; 
			//	if (src.cell1_Col != dest.cell1_Col || src.CellCount() != dest.CellCount())
			//		return false; 

			//	src.ExpandRow(dest.cell1_Row, dest.cell2_Row);
			//} 
			#endregion
			var res = false;
			if (src.cell1_Row == dest.cell1_Row && src.NearColumnCell(dest) != int.MinValue)
			{
				if (src.RowCount() == dest.RowCount())
				{
					src.ExpandColumn(dest.cell1_Col, dest.cell2_Col);
					res = true;
				}
				else if (TryEqualByRow(src, dest))
				{
					src.ExpandColumn(dest.cell1_Col, dest.cell2_Col);
					res = true;
				}
			}

			return res;
		}

		internal bool ExpandByRow(CellRangeTmpl src, CellRangeTmpl dest)
		{
			var res = false;
			if (src.cell1_Col == dest.cell1_Col && src.NearRowCell(dest) != int.MinValue)
			{
				if (src.ColumnCount() == dest.ColumnCount())
				{
					src.ExpandRow(dest.cell1_Row, dest.cell2_Row);
					res = true;
				}
				else if (TryEqualByColumn(src, dest))
				{
					src.ExpandRow(dest.cell1_Row, dest.cell2_Row);
					res = true;
				}
			}

			return res;
		}

		public bool TryEqualByColumn(CellRangeTmpl src, CellRangeTmpl dest)
		{
			CellRangeTmpl core = null, min = null;
			if (src.ColumnCount() > dest.ColumnCount())
			{
				core = src;
				min = dest;
			}
			else
			{
				core = dest;
				min = src;
			}

			var res = true;
			var allRanges = styles.SelectMany((v) => v.Value);
			if (core.cell1_Col < min.cell1_Col && res)
			{
				res = CheckForEmpty(core.cell1_Col + 1, min.cell1_Col, allRanges,
					(range) => range.ContainRow(min.cell1_Row) || range.ContainRow(min.cell2_Row),
					(range, col) => range.ContainColumn(col));
				if (res)
					min.cell1_Col = core.cell1_Col;
			}

			if (core.cell2_Col > min.cell2_Col && res)
			{
				res = CheckForEmpty(min.cell2_Col + 1, core.cell2_Col, allRanges,
					(range) => range.ContainRow(min.cell1_Row) || range.ContainRow(min.cell2_Row),
					(range, col) => range.ContainColumn(col));
				if (res)
					min.cell2_Col = core.cell2_Col;
			}

			return res;
		}

		public bool TryEqualByRow(CellRangeTmpl src, CellRangeTmpl dest)
		{
			CellRangeTmpl core = null, min = null;
			if (src.RowCount() > dest.RowCount())
			{
				core = src;
				min = dest;
			}
			else
			{
				core = dest;
				min = src;
			}

			var res = true;
			var allRanges = styles.SelectMany((v) => v.Value);
			if (core.cell1_Row < min.cell1_Row && res)
			{
				res = CheckForEmpty(core.cell1_Row + 1, min.cell1_Row, allRanges,
					(range) => range.ContainColumn(min.cell1_Col) || range.ContainColumn(min.cell2_Col),
					(range, row) => range.ContainRow(row));
				if (res)
					min.cell1_Row = core.cell1_Row;
			}

			if (core.cell2_Row > min.cell2_Row && res)
			{
				res = CheckForEmpty(min.cell2_Row + 1, core.cell2_Row, allRanges,
					(range) => range.ContainColumn(min.cell1_Col) || range.ContainColumn(min.cell2_Col),
					(range, row) => range.ContainRow(row));
				if (res)
					min.cell2_Row = core.cell2_Row;
			}

			return res;
		}

		private bool CheckForEmpty(int from, int to, IEnumerable<CellRangeTmpl> allRanges, Func<CellRangeTmpl, bool> IsLayInRowRange, Func<CellRangeTmpl, int, bool> IsAnotherRangeContains)
		{
			bool res = true;
			for (int i = from; i <= to; i++)
			{
				foreach (var range in allRanges)
				{
					if (IsLayInRowRange(range) && IsAnotherRangeContains.Invoke(range, i))
					{
						res = false;
						break;
					}
				}

				if (!res)
					break;
			}

			return res;
		}

		//bool CheckEmptyCols(int from, int to, IEnumerable<CellRangeTmpl> allRanges)
		//{
		//	bool res = true;
		//	for (int col = from; col < to; col++)
		//	{
		//		foreach (var range in allRanges)
		//		{
		//			if (range.ContainColumn(col))
		//			{
		//				res = false;
		//				break;
		//			}
		//		}

		//		if (!res)
		//			break;
		//	}

		//	return res;
		//}

		public IEnumerator<KeyValuePair<ShortFormatItem, List<CellRangeTmpl>>> GetEnumerator()
		{
			return styles.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return styles.GetEnumerator();
		}
	}
}
