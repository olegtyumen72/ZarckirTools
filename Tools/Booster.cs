﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools
{
	public static class Booster
	{
		public delegate bool TryParseHandler<T>(string obj, out T res);

		public static T UserFriendlyDefaults<T>()
		{
			var type = typeof(T);
			switch (type.Name.ToLower())
			{
				case "int":
				case "int16":
				case "int32":
				case "double":
				case "float":
				case "single":
					{
						return (T)Convert.ChangeType(-1, type, CultureInfo.InvariantCulture);
					}
				default:
					{
						return default(T);
					}
			}
		}

		public static List<T> ToList<T>(this IList list)
		{
			var res = new List<T>();

			foreach (var el in list)
			{
				res.Add((T)Convert.ChangeType(el, typeof(T)));
			}

			return res;
		}

		public static T TryParse<T>(this string value, TryParseHandler<T> parser)
		{
			if (String.IsNullOrEmpty(value))
				return UserFriendlyDefaults<T>();

			T result;
			if (parser(value, out result))
				return result;

			return UserFriendlyDefaults<T>();
		}

		public static T ILast<T>(this IList src)
		{
			return (T)src[src.Count - 1];
		}

		public static object ILast(this IList src)
		{
			return src[src.Count - 1];
		}

		public static object IFirstOrDefault(this IList src)
		{
			if (src.Count > 0)
			{
				return src[0];
			}
			else
			{
				return null;
			}
		}

		public static object IFirstOrDefault(this IList src, Func<object, bool> act)
		{
			foreach (var el in src)
			{
				if (act(el))
					return el;
			}

			return null;
		}

		public static int ToInt(this string str)
		{
			var res = -1;
			if (int.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out res))
			{
				return res;
			}
			else
			{
				return int.MinValue;
			}
		}

		public static bool ToBool(this string str)
		{
			var res = false;
			if (bool.TryParse(str, out res))
			{
				return res;
			}
			else
			{
				return false;
			}
		}

		public static double ToDouble(this string str)
		{
			var res = -1D;
			if (double.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out res))
			{
				return res;
			}
			else
			{
				return double.MinValue;
			}
		}

		public static float ToSingle(this double src)
		{
			return Convert.ToSingle(src);
		}

		public static float ToSingle(this string src)
		{
			var res = -1f;
			if (float.TryParse(src, NumberStyles.Any, CultureInfo.InvariantCulture, out res))
			{
				return res;
			}
			else
			{
				return float.MinValue;
			}
		}

		public static decimal ToDecimal(this string str)
		{
			decimal res = -1;
			if (decimal.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out res))
			{
				return res;
			}
			else
			{
				return decimal.MinValue;
			}
		}

		public static List<T> Clone<T>(this List<T> src)
		{
			var res = new List<T>();
			foreach (var el in src)
			{
				res.Add(el);
			}

			return res;

		}

		public static List<string> ToListStr(this IList list)
		{
			return list.Cast<string>().ToList();
		}

		public static List<DateTime?> ToListDateTime(this IList list)
		{
			var res = new List<DateTime?>();
			foreach (var el in list)
			{
				if (el == null)
					res.Add(null);
				else if (el.GetType().Name.ToLower() == "string")
					res.Add((DateTime?)DateTime.Parse(el.ToString()));
				else
					res.Add((DateTime?)el);
			}
			return res;
		}

		public static List<int?> ToListInt(this IList list)
		{
			var res = new List<int?>();
			foreach (var el in list)
			{
				if (el == null)
					res.Add(null);
				else if (el.GetType().Name.ToLower() == "int")
					res.Add((int?)el);
				else
					res.Add(int.Parse(el.ToString()));
			}
			return res;
		}

		public static List<double?> ToListDbl(this IList list)
		{
			var res = new List<double?>();
			foreach (var el in list)
			{
				res.Add((double?)el);
			}
			return res;
		}

		private static string[] badString = new string[] { " ", "\"", "»", "«", "_x000D_", "_x000D", "/" };
		private static char[] badChar = new char[] { ((char)65533) };

		public static string DestWSpace(this string str)
		{
			if (str == null || str.Length <= 0) return str;


			foreach (var bChar in badString)
			{
				str.Replace(str, "");
			}

			var res = "";
			for (int i = 0; i < str.Length; i++)
			{
				var ch = str[i];
				bool isBad = false;
				foreach (var chr in badChar)
				{
					if (ch == chr)
					{
						isBad = true;
						break;
					}
				}

				if (!isBad)
					res += ch;
			}

			return res.Trim();
		}

		public static string DelLastChar(this string str, bool close = false)
		{
			if (str.Length <= 0) return str;
			return str.Substring(0, str.Length - 1) + (close ? ")" : "");
		}

		public static double ToDouble(this object obj)
		{
			if (obj == null) throw new Exception("Can convert null to double");
			try
			{
				return Convert.ToDouble(obj, CultureInfo.InvariantCulture);
			}
			catch
			{
				var str = obj.ToString();
				double res = -1;
				if (double.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out res))
				{
					return res;
				}
				else
				{
					throw new Exception("Miss format for convert double");
				}
			}
		}

		public static bool IsNan(this double dbl)
		{
			return Double.IsNaN(dbl);
		}

		public static DateTime ToDateTime(this object obj)
		{
			DateTime res = DateTime.Now;
			if (obj is string)
			{
				res = obj.ToString().TryParse<DateTime>(DateTime.TryParse);
			}
			else
			{
				res = (DateTime)Convert.ChangeType(obj, typeof(DateTime));
			}

			return res;
		}

		public static int ToInt(this object obj)
		{
			return Convert.ToInt32(obj, CultureInfo.InvariantCulture);
		}

		public static int Cvartal(this DateTime dt)
		{
			return (dt.Month / 3) + 1;
		}

		public static byte[] ReadAllBytes(this BinaryReader reader)
		{
			const int bufferSize = 4096;
			using (var ms = new MemoryStream())
			{
				byte[] buffer = new byte[bufferSize];
				int count;
				while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
					ms.Write(buffer, 0, count);
				return ms.ToArray();
			}

		}

		public static List<string> RemoveEmpty(this List<string> list)
		{
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i] == null || list[i].Length <= 0)
				{
					list.RemoveAt(i);
					i--;
				}
			}

			return list;
		}

		public static int KeyIndex<T, V>(this Dictionary<T, V> src, Func<T, bool> act)
		{
			for (int i = 0; i < src.Keys.Count; i++)
			{
				var key = src.Keys.ElementAt(i);
				if (act(key))
				{
					return i;
				}
			}
			return -1;
		}

		public static ObservableCollection<T> ToObservable<T>(this IList src)
		{
			var res = new ObservableCollection<T>();
			foreach (var el in src)
			{
				res.Add((T)el);
			}
			return res;
		}

		public static List<T> ToTypedList<T>(this IEnumerable<object> src)
		{
			var res = new List<T>();
			foreach (var el in src)
			{
				res.Add((T)Convert.ChangeType(el, typeof(T)));
			}

			return res;
		}

		public static void TryAdd<T, V>(this Dictionary<T, V> src, T key, V val)
		{
			if (!src.ContainsKey(key))
				src.Add(key, val);
			//else
			//	src[key] = val;
		}

		public static void TryAdd<T, V>(this Dictionary<T, V> src, T key)
			where V : new()
		{
			if (!src.ContainsKey(key))
				src.Add(key, new V());
		}

		//public static double ToRound(this double src, int dem)
		//{
		//	return Math.Round(src, dem);
		//}

	}
}
