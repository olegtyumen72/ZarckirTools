﻿using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Charts;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Styles
{
	internal class XfsNode
	{
		public int Id { get; set; }
		public bool isDefault { get; set; } = false;
		public bool isGlobal { get; set; } = true;
		public bool IsEmpty { get; private set; }

		public FontNode Font { get; set; }
		public BorderNode Border { get; set; }
		public FillNode Fill { get; set; }
		public NumFmtNode NumFmt { get; set; }
		public XfsNode Xfs { get; set; }

		public bool IsWrap { get; set; }
		public bool IsProtection { get; set; }
		public bool IsFont { get; set; }
		public bool IsBorder { get; set; }
		public bool IsFill { get; set; }
		public bool IsNumFmt { get; set; }
		public bool IsAligment { get; set; }

		public int Rotation { get; set; }
		public Aligment Horizont { get; set; } = Aligment.none;
		public Aligment Vertical { get; set; } = Aligment.none;
		public int ReadingOrder { get; set; }

		public override string ToString()
		{
			return $"ID:{Id} FontID:{Font.Id} Border:{Border.Id} Fill:{Fill.Id} NumFmt:{NumFmt.NumFmtId}";
		}

		public XfsNode(int id, bool isEmpty = false)
		{
			Id = id;
			IsEmpty = isEmpty;
			ReadingOrder = -1;
		}

		public XfsNode(int id, FontNode font, BorderNode bord, FillNode fill, NumFmtNode numfmt) : this(id, false)
		{
			if (font != null)
			{
				Font = font;
				if (!font.Font.IsEmpty || !font.ExcelColor.IsEmpty)
				{
					IsFont = true;
				}
			}

			if (bord != null)
			{
				Border = bord;
				if (!bord.IsEmpty)
					IsBorder = true;
			}

			if (fill != null)
			{
				Fill = fill;
				if (!fill.IsEmpty)
					IsFill = true;
			}

			if (numfmt != null)
			{
				NumFmt = numfmt;
				if (!numfmt.IsEmpty)
					IsNumFmt = true;
			}
		}

		public XfsNode(FontNode font, BorderNode border, FillNode fill, NumFmtNode numFmt, ReportFormatItem format, int id)
			: this(id, font, border, fill, numFmt)
		{
			Horizont = format.HorizontalAligmentCenter ? Aligment.center : Aligment.none;
			Vertical = format.VerticalAligmentCenter ? Aligment.center : Aligment.none;
			Rotation = format.ColumnsOrientation;
			IsWrap = format.Wrap;
		}

		public XfsNode(FontNode font, BorderNode border, FillNode fill, NumFmtNode numFmt, Aligment hor, Aligment vert, int id)
			: this(id, font, border, fill, numFmt)
		{
			Horizont = hor;
			Vertical = vert;
		}

		public XfsNode(FontNode font, BorderNode border, FillNode fill, NumFmtNode numFmt, string horAlig, string vertAlig, int rotato, bool wrap, int id)
			: this(id, font, border, fill, numFmt)
		{
			Rotation = rotato;
			IsWrap = wrap;
			Horizont = ParseAligment(horAlig);
			Vertical = ParseAligment(vertAlig);
		}

		private Aligment ParseAligment(string alig)
		{
			var res = Aligment.none;
			if (String.IsNullOrEmpty(alig))
				return res;

			if (Enum.TryParse(alig, out res))
				return res;

			return Aligment.none;
		}

		public void Save(XmlWriterWrapper doc)
		{
			doc.OpenElement("xf").AddAtribute("numFmtId", NumFmt?.NumFmtId ?? 0)
				.AddAtribute("fontId", Font.Id.ToString())
					.AddAtribute("fillId", Fill.Id.ToString())
					.AddAtribute("borderId", Border.Id.ToString());

			doc.AddAtribute("xfId", 0);

			//bool isAligment = Rotation != 0 || IsWrap || Horizont != Aligment.none || Vertical != Aligment.none;

			doc.
				AddAtribute("applyFont", IsFont.BoolToInt()).
				AddAtribute("applyFill", IsFill.BoolToInt()).
				AddAtribute("applyBorder", IsBorder.BoolToInt()).
				AddAtribute("applyNumberFormat", IsNumFmt.BoolToInt()).
				AddAtribute("applyAlignment", IsAligment.BoolToInt()).
				AddAtribute("applyProtection", IsProtection.BoolToInt());

			if (IsAligment)
			{
				doc.OpenElement("alignment");
				if (Horizont != Aligment.none)
				{
					doc.AddAtribute("horizontal", Horizont.ToString());
				}
				if (Vertical != Aligment.none)
				{
					doc.AddAtribute("vertical", Vertical.ToString());
				}
				if (Rotation != 0)
				{
					doc.AddAtribute("textRotation", Rotation.ToString());
				}
				if (IsWrap)
				{
					doc.AddAtribute("wrapText", "1");
				}

				doc.CloseElement();
			}

			doc.CloseElement();
		}

		internal bool IsSrc(ReportFormatItem p, FontNode f, ReportSheet rs = null)
		{
			var font = f.Id == this.Font.Id;
			var border = Border.IsSrc(p);
			var fille = Fill.isSrc(p, rs);
			var rotato = p.ColumnsOrientation == Rotation;
			var wrap = p.Wrap == IsWrap;
			var horAlig = p.HorizontalAligmentCenter == (Horizont == Aligment.center);
			var vertAlig = p.VerticalAligmentCenter == (Vertical == Aligment.center);
			var numFmt = p.NumFmtId == (NumFmt?.NumFmtId ?? 0);

			return !isDefault && font && border && fille && rotato && wrap && horAlig && vertAlig && numFmt;
		}

		internal XfsNode UpdateNew(XfsNode elem, int id)
		{
			var border = Border.Update(elem.Border);
			var fill = Fill.Update(elem.Fill);
			var numFmt = this.NumFmt.Update(elem.NumFmt);
			var rotato = elem.Rotation;
			var wrap = elem.IsWrap || IsWrap;
			var font = Font.Update(elem.Font);
			var hor = elem.Horizont != Aligment.none ? elem.Horizont : Horizont;
			var vert = elem.Vertical != Aligment.none ? elem.Vertical : Vertical;
			var isProtect = elem.IsProtection || IsProtection;
			var isFont = elem.IsFont || IsFont;
			var isBorder = elem.IsBorder || IsBorder;
			var isFill = elem.IsFill || IsFill;
			var isNumFmt = elem.IsNumFmt || IsNumFmt;
			var isAligm = elem.IsAligment || IsAligment;

			return new XfsNode(font, border, fill, numFmt, hor, vert, id)
			{
				Rotation = rotato,
				IsWrap = wrap,
				IsProtection = isProtect,
				IsFont = isFont,
				IsBorder = isBorder,
				IsFill = isFill,
				IsNumFmt = isNumFmt,
				IsAligment = isAligm
			};
		}

		internal void Update(XfsNode updater, bool changeFont = true)
		{
			if (isDefault) return;

			if (Border.Id != updater.Border.Id)
			{
				Border = Border.Update(updater.Border);
			}

			if (Fill.Id != updater.Fill.Id)
			{
				Fill = Fill.Update(updater.Fill);
			}

			if (updater.Horizont != Aligment.none && Horizont != updater.Horizont)
			{
				Horizont = updater.Horizont;
			}

			if (updater.Vertical != Aligment.none && Vertical != updater.Vertical)
			{
				Vertical = updater.Vertical;
			}

			Rotation = updater.Rotation;
			IsWrap = updater.IsWrap || IsWrap;
			IsProtection = updater.IsProtection || IsProtection;
			IsFont = updater.IsFont || IsFont;
			IsBorder = updater.IsBorder || IsBorder;
			IsFill = updater.IsFill || IsFill;
			IsNumFmt = updater.IsNumFmt || IsNumFmt;
			IsAligment = updater.IsAligment || IsAligment;

			if (changeFont)
			{
				Font = Font.Update(updater.Font);
			}

			if (updater.NumFmt.NumFmtId != NumFmt.NumFmtId)
			{
				NumFmt = NumFmt.Update(updater.NumFmt);
			}

		}

		internal void MergeStyle(XfsNode style)
		{

			if (ReadingOrder < 0 && style.ReadingOrder >= 0)
			{
				ReadingOrder = style.ReadingOrder;
			}

			if (!IsAligment && style.IsAligment)
			{
				IsAligment = style.IsAligment;
				Horizont = style.Horizont;
				Vertical = style.Vertical;
				Rotation = style.Rotation;
			}

			if (!IsWrap && style.IsWrap)
			{
				IsWrap = style.IsWrap;
			}

			if (!IsProtection && style.IsProtection)
			{
				IsProtection = style.IsProtection;
			}

			if (!IsFont && style.IsFont)
			{
				IsFont = style.IsFont;
				Font = style.Font;
			}

			if (!IsBorder && style.IsBorder)
			{
				IsBorder = style.IsBorder;
				Border = style.Border;
			}

			if (!IsFill && style.IsFill)
			{
				IsFill = style.IsFill;
				Fill = style.Fill;
			}

			if (!IsNumFmt && style.IsNumFmt)
			{
				IsNumFmt = style.IsNumFmt;
				NumFmt = style.NumFmt;
			}
		}
	}
}
