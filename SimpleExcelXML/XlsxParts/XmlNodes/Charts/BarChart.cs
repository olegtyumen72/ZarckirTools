﻿using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Charts
{
	internal class BarChart: XmlPropBase, IXmlChart
	{
		public static string Word { get; } = "barChart";

		public bool varyColors { get; set; } = false;
		public List<Ser> Data { get; set; } = new List<Ser>();
		public DLbls dLbls { get; set; } = DLbls.Default;
		public List<TypedAx> Axes { get; set; } = new List<TypedAx>();
		public string Prefix { get; set; } = "c";


		public string barDir { get; set; } = "col";
		public string grouping { get; set; } = "clustered";						
		public int gapWidth { get; set; } = 200;
		public int overlap { get; set; } = 0;
		public string KeyWord { get { return Word; } }

		public BarChart() : base()
		{

		}

		public BarChart(XElement xBar) : base(xBar)
		{
		}

		public override void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("barChart", Prefix)
			   .OpenPrefElement(nameof(barDir)).AddAtribute("val", barDir).CloseElement()
			   .OpenPrefElement(nameof(grouping)).AddLastAtr(grouping).CloseElement()
			   .OpenPrefElement(nameof(varyColors)).AddLastAtr((varyColors ? 1 : 0)).CloseElement();

			foreach(var data in Data)
			{
				data.Save(doc);
			}

			dLbls.Save(doc);

			doc.OpenPrefElement(nameof(gapWidth), Prefix).AddAtribute("val", gapWidth.ToString()).CloseElement()
				.OpenPrefElement(nameof(overlap), Prefix).AddLastAtr(overlap.ToString()).CloseElement();

			foreach(var axe in Axes)
			{

				if(axe.Tag.ToString() == "x")
				{
					axe.type = TypedAx.TypeAx.catAx;
					//axe.majorUnit = -1;
					//axe.minorUnit = -1;
					//axe.scaling = Scaling.Default;
				}

				doc.OpenPrefElement("axId", "c").AddLastAtr(axe.axId.ToString()).CloseElement();
			}

			doc.CloseElement();			
		}

		public override void TryLoad(XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");
			
			barDir = root.Element(c + nameof(barDir))?.Attribute("val")?.Value;
			grouping = root.Element(c + nameof(grouping))?.Attribute("val")?.Value;
			varyColors = ParseBool(root.Element(c + nameof(varyColors))?.Attribute("val")?.Value);

			var sers = root.Elements().Where((v) => v.Name.LocalName.Contains("ser"));
			foreach (var ser in sers)
				Data.Add(new Ser(ser));

			var dlb = root.Element(c + "dLbls");
			if (dlb.HasInfo())
				dLbls = new DLbls(dlb);

			gapWidth = root.Element(c + nameof(gapWidth))?.Attribute("val")?.Value?.ToInt() ?? -1;
			overlap = root.Element(c + nameof(overlap))?.Attribute("val")?.Value?.ToInt() ?? -1;

			var axIds = root.Elements(c + "axId");
			foreach(var el in axIds)
			{
				Axes.Add(new TypedAx() { axId = el.Attribute("val").Value.ToInt() });
			}
		}

		private bool ParseBool(string str)
		{
			if (String.IsNullOrEmpty(str))
				return false;

			var i = str.ToInt();
			if (i == 1) return true;
			else return false;
		}

		internal static bool IsBarChar(string name)
		{
			var res = false;
			var lower = name.Trim().ToLower();
			if (lower.Contains("barchart"))
				res = true;

			return res;
		}
	}
}
