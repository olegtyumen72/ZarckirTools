﻿using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Templates
{
	public class ConditionalFormatTmpl : IConditionalFormat, ISimpleConvert<IConditionalFormat>
	{

		public ConditionalFormatTmpl()
		{

		}

		public ConditionalFormatTmpl(IConditionalFormat format)
		{
			Background = format.Background;
			Font = format.Font;
			CondType = format.CondType;
			Operator = format.Operator;
			TxtOperat = format.TxtOperat;
		}

		public Color Background { get; set; } = Color.Empty;
		public Color Font { get; set; } = Color.Empty;
		public ConditionType CondType { get; set; } = ConditionType.CellVal;
		public string Condition { get; set; } = "";
		public ConditionOperator Operator { get; set; } = ConditionOperator.Equal;
		public TextOperators TxtOperat { get; set; } = TextOperators.Contains;

		public IConditionalFormat Clone()
		{
			return (IConditionalFormat)MemberwiseClone();
		}

		public IConditionalFormat Convert()
		{
			return (IConditionalFormat)this;
		}
	}
}
