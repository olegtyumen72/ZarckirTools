﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbConnector.Core
{
	public interface ISqlPartData<T>
	{
		int Count { get; }
		int Offset { get; }
		bool IsLazy { get; }
		List<T> UsedData { get; }

		T Next();
		T Prev();
		T Current();
		void SetPosition(int posInRange);
		void ResetPosition();
		T this[int cnt, int off] { get; }
		T FirstOrDefaultt(Func<T, bool> func);
		T FirstRowOrDefaultt(Func<T, T> func, Func<T, bool> check);
	}
}
