﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SimpleCore.Tools
{
	public static class RectangleTools
	{

		//static bool SegmentContains(int a, int b, int ptr) {

		//	if (a <= ptr && b >= ptr) return true;
		//	else return false;
		//}


		public static bool SegmentContains<T>(T a, T b, T ptr)
			where T : IComparable
		{
			var aPtr = a.CompareTo(ptr);
			var bPtr = b.CompareTo(ptr);

			if (aPtr <= 0 && bPtr >= 0)
			{
				return true;
			}

			return false;
		}

		public static bool SegmentContain(double a, double b, double ptr)
		{
			return a <= ptr && b >= ptr;
		}

		public static bool RectanglesOnRow(Rectangle src, Rectangle dest)
		{
			if (src.Top == dest.Top)
				return true;

			if (src.Bottom == dest.Bottom)
				return true;

			if (src.Top <= dest.Top && src.Bottom >= dest.Top)
				return true;

			if (src.Top <= dest.Bottom && src.Bottom >= dest.Bottom)
				return true;

			return false;
		}

		public static bool RecnaglesOnCol(Rectangle src, Rectangle dest)
		{
			if (src.Left == dest.Left)
				return true;

			if (src.Right == dest.Right)
				return true;

			if (src.Left <= dest.Left && src.Right >= dest.Left)
				return true;

			if (src.Left <= dest.Right && src.Right >= dest.Right)
				return true;

			return false;
		}
		
		public static bool PtrInRecnatgle(Rectangle src, Point ptr)
		{
			return PtrInRecnatgle(src, ptr.X, ptr.Y);
		}

		public static bool PtrInRecnatgle(Rectangle src, double x, double y)
		{
			return src.Left <= x && src.Right >= x && src.Top <= y && src.Bottom >= y;
		}

		public static bool ContainsOnX(Rectangle src, Rectangle dest)
		{
			return src.Left <= dest.Left && src.Right >= dest.Right;
		}

		public static bool ContainsOnY(Rectangle src, Rectangle dest)
		{
			return src.Top <= dest.Top && src.Bottom >= dest.Bottom;
		}

		public static bool IsCrosses(Rectangle src, Rectangle dest)
		{
			if (
				PtrInRecnatgle(src, dest.Left, dest.Top) ||
				PtrInRecnatgle(src, dest.Left, dest.Bottom) || 
				PtrInRecnatgle(src, dest.Right, dest.Top) || 
				PtrInRecnatgle(src, dest.Right, dest.Bottom)
				)
				return true;

			return false;
		}
	}
}
