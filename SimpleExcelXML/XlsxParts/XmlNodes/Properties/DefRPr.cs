﻿using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Charts;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// Очередные настройки отображения для текста, полные. Не хранит конкретного значения
	/// </summary>
	internal class DefRPr: XmlNameBase
	{
		/// <summary>
		/// Размер шрифта: обычный * 10
		/// </summary>
		public int sz { get;  set; } = -1;
		/// <summary>
		/// Жирность
		/// </summary>
		public int b { get;  set; } = -1;
		/// <summary>
		/// Пропись
		/// </summary>
		public int i { get;  set; } = -1;
		/// <summary>
		/// Подчёркивание
		/// </summary>
		public Underline u { get;  set; } = Underline.miss;
		public string strike { get; set; } = null;
		public int spc { get; set; } = -1;
		public int baseline { get; set; } = -1;
		public Fonts Font { get; set; } = Fonts.Arial;

		public SolidFill fill { get; set; }
		protected string prefix;
		public override string name { get; set; }

		public static DefRPr DefaultRpr { get { return new DefRPr("defRPr"); } }		

		public DefRPr(string name = "defRPr", string pref = "a", int sz = 0, int b = 0, int i = 0, Underline u = Underline.none, string strike = "noStrike", int baseline = 0, int spc = 0, SolidFill fill = null, Fonts f = Fonts.Arial)
		{
			this.sz = sz;
			if (this.sz < 0)
				this.sz = 0;
			this.b = b;
			this.i = i;
			this.u = u;
			this.strike = strike;
			this.baseline = baseline;
			this.fill = fill;
			this.prefix = pref;
			this.spc = spc;
			Font = f;
			this.name = name;
		}

		public DefRPr(string name, XElement xDefRPr) : base(name, xDefRPr)
		{
		}

		protected override void WriteAtrValue(XmlWriterWrapper doc, string val, string atr)
		{
			if (IsWritable(val))
			{
				doc.AddAtribute(atr, val);
			}
		}

		protected override void WriteAtrValue(XmlWriterWrapper doc, int val, string atr)
		{
			if (IsWritable(val))
			{
				doc.AddAtribute(atr, val.ToString());
			}
		}

		protected override string ReadAtrValue(Dictionary<string, string> atrs, string atr)
		{
			if (atrs.ContainsKey(atr))
				return atrs[atr];

			return null;
		}

		protected override bool IsWritable(string val)
		{
			return !String.IsNullOrEmpty(val);
		}

		protected override bool IsWritable(int val)
		{
			return !(val == -1);
		}

		public override void TryLoad(XElement root)
		{						
			var a = root.GetNamespaceOfPrefix("a");
			//var main = root.LazyElement(name);
			var atrs = XDocumentTools.ParseAtrs(root);

			sz = ReadAtrValue(atrs, $"{nameof(sz)}")?.ToInt() ?? -1;
			b = ReadAtrValue(atrs, $"{nameof(b)}")?.ToInt() ?? -1;
			i = ReadAtrValue(atrs, $"{nameof(i)}")?.ToInt() ?? -1;
			u = ParseUnderline(ReadAtrValue(atrs, $"{nameof(u)}"));
			strike = ReadAtrValue(atrs, $"{nameof(strike)}");
			spc = ReadAtrValue(atrs, $"{nameof(spc)}")?.ToInt() ?? -1;
			baseline = ReadAtrValue(atrs, $"{nameof(baseline)}")?.ToInt() ?? -1;

			var xLatin = root.Element(a + "latin");
			if (xLatin != null && !xLatin.IsFullEmpty())
				Font = ParseFont(xLatin.Attribute("typeface").Value);

			var xFill = root.GetSolidFill();
			if (xFill != null && !xFill.IsFullEmpty())
				fill = new SolidFill(xFill);
		}

		private Underline ParseUnderline(string under)
		{
			var res = Underline.miss;
			if (Enum.TryParse(under, out res))
				return res;

			return Underline.miss;
		}

		private Fonts ParseFont(string font)
		{
			font = font.Replace(" ", "_");

			var res = Fonts.Times_New_Roman;
			if(Enum.TryParse(font, out res))
			{
				return res;
			}

			return Fonts.Times_New_Roman;
		}

		//public DefRPr(DefRPr other)
		//{
		//	this.sz = other.sz;
		//	this.b = other.b;
		//	this.i = other.i;
		//	this.u = other.u;
		//	this.Font = other.Font;
		//	this.name = other.name;
		//	this.prefix = other.prefix;
		//}

		public override void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement(name, prefix);
			if (sz != -1)
				doc.AddAtribute(nameof(sz), sz.ToString());
			if (b != -1)
				doc.AddAtribute(nameof(b), b.ToString());
			if (i != -1)
				doc.AddAtribute(nameof(i), i.ToString());

			if (u != Underline.miss)
				doc.AddAtribute(nameof(u), u.ToString());
			if (!String.IsNullOrEmpty(strike))
				doc.AddAtribute(nameof(strike), strike);

			if (spc != -1)
				doc.AddAtribute(nameof(spc), spc.ToString());
			if (baseline != -1)
				doc.AddAtribute(nameof(baseline), baseline.ToString());

			if (fill != null && fill != SolidFill.DefultSolid)
				fill.Save(doc);
			//else
			//	doc.OpenPrefElement("noFill", "a").CloseElement();

			//if (Font != Fonts.Ca)
			{
				doc.OpenPrefElement("latin", "a").AddAtribute("typeface", Font.ToString().Replace("_", " ")).CloseElement();
			}

			doc.CloseElement();
		}
	}
}
