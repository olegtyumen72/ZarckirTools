﻿using SimpleCore.Parts;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Objs
{
	public class ReportChartObj : IReportChart<IReportChartItem>
	{
		public SimpleFontFamily TitleSettings { get; set; } = new SimpleFontFamily();

		/// <summary>
		/// Заголовок графика.
		/// </summary>
		public string Title { get; set; } = "";

		public SimpleFontFamily TitleXSettings { get; set; } = new SimpleFontFamily();

		/// <summary>
		/// Подпись оси X
		/// </summary>
		public string TitleX { get; set; } = "";

		public SimpleFontFamily TitleYSettings { get; set; } = new SimpleFontFamily();

		/// <summary>
		/// Подпись оси Y
		/// </summary>
		public string TitleY { get; set; } = "";

		/// <summary>
		/// Подпись маркеров по оси X. Если их на 1 больше чем строк данных, то надписи вставляются по краям диапазонов.
		/// </summary>
		public List<string> AxesXTitles { get; set; } = new List<string>();

		/// <summary>
		/// Элементы данного графика.
		/// </summary>
		public List<IReportChartItem> Items { get; set; } = new List<IReportChartItem>();

		/// <summary>
		/// Куда выводить график.
		/// </summary>
		public Rectangle GraphRect { get; set; } = new Rectangle(0, 0, 100, 100);

		/// <summary>
		/// Минимальное значение по оси X.
		/// </summary>
		public double MinX { get; set; } = double.NaN;

		/// <summary>
		/// Максимальные значение по оси X.
		/// </summary>
		public double MaxX { get; set; } = double.NaN;

		/// <summary>
		/// Максимальные значение по оси Y.
		/// </summary>
		public double MinY { get; set; } = double.NaN;

		/// <summary>
		/// Максимальные значение по оси Y.
		/// </summary>
		public double MaxY { get; set; } = double.NaN;

		public bool IsAxisYRevers { get; set; } = false;

		public bool YAxisHasMinorGridLines { get; set; } = false;

		public bool XAxisHasMinorGridLines { get; set; } = false;

		public SimpleFontFamily LegendSettings { get; set; } = new SimpleFontFamily();

		public bool ShowLegend { get; set; } = true;

		public Bitmap Pic { get; set; } = null;

		public Color YMajorGridLinesColor { get; set; } = Color.Empty;
		public Color YMinorGridLinesColor { get; set; } = Color.Empty;

		public Color XMajorGridLinesColor { get; set; } = Color.Empty;
		public Color XMinorGridLinesColor { get; set; } = Color.Empty;

		public Color YAxisLinesColor { get; set; } = Color.Empty;
		public Color XAxisLinesColor { get; set; } = Color.Empty;

		public SimpleFontFamily YLabelsSettings { get; set; } = new SimpleFontFamily();
		public SimpleFontFamily XLabelsSettings { get; set; } = new SimpleFontFamily();

		public bool HasXMajorTickMark { get; set; } = true;
		public bool HasYMajorTickMark { get; set; } = true;

		public double XMajorUnits { get; set; } = -1;
		public double YMajorUnits { get; set; } = -1;

		/// <summary>
		/// Не работает: #,##; #.##;
		/// Работает: #0.00 ; #0,00
		/// </summary>
		public string XNumberFormat { get; set; } = "";
		/// <summary>
		/// Не работает: #,##; #.##;
		/// Работает: #0.00 ; #0,00
		/// </summary>
		public string YNumberFormat { get; set; } = "";
	}
}
