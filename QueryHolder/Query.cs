﻿using AssociativeList;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlHelpers
{
	public class Query : AList
	{
		protected enum QueryKs { table, _cols, none }

		public Query()
		{
			//CheckComp(ac);
		}

		public string Table
		{
			get { return this["table"][0].ToString(); }
		}

		public string GetCols(QueryAc ac)
		{
			if (ContainsKey(ac.ToString() + QueryKs._cols.ToString()))
				return this[ac.ToString() + QueryKs._cols.ToString()][0].ToString();
			else
				return "";
		}

		public IList GetVals(QueryAc ac)
		{
			return this[ac.ToString()];
		}

		void CheckComp(QueryAc ac, QueryKs ks = QueryKs.none, bool initFirst = false)
		{
			var cols = ks == QueryKs.none ? "" : ks.ToString();
			if (!ContainsKey(ac.ToString() + cols))
			{
				Add(ac.ToString() + cols, new List<string>());
			}
			if (initFirst)
			{
				this[ac.ToString() + cols].Add("");
			}
		}

		void CheckTbl()
		{
			if (!ContainsKey(QueryKs.table.ToString()))
			{
				Add(QueryKs.table.ToString(), new List<string>() { "" });
			}
		}

		public void AddCols(QueryAc ac, string cols)
		{
			CheckComp(ac, QueryKs._cols, true);
			this[ac.ToString() + QueryKs._cols.ToString()][0] = cols;
		}

		public void AddVals(QueryAc ac, string[] vals)
		{
			CheckComp(ac);
			foreach (var vl in vals)
			{
				this[ac.ToString()].Add(vl);
			}
		}

		public void AddVal(QueryAc ac, string vl = "")
		{
			CheckComp(ac);
			if (vl.Length > 0)
				this[ac.ToString()].Add(vl);
		}

		public void UpdateTable(string tbl)
		{
			CheckTbl();
			this[QueryKs.table.ToString()][0] = tbl;
		}

		public bool Has(QueryAc act)
		{
			return ContainsKey(act.ToString());
		}

	}
}
