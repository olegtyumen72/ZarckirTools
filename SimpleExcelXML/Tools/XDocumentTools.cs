﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.Tools
{
	/// <summary>
	/// Расширенные возможности для Linq XML
	/// </summary>
	internal static class XDocumentTools
	{
		public static bool HasAtr(this XElement el, string atrName)
		{
			return el.Attribute(atrName) != null;
		}

		public static string ValueOrNull(this XElement el, params XName[] parms)
		{
			foreach (var par in parms)
			{
				el = el.Element(par);

				if (el == null)
					return null;
			}

			return el.Value;
		}

		public static string ValueOrNull(this XElement el, params string[] parms)
		{
			foreach (var par in parms)
			{
				el = el.Element(par);

				if (el == null)
					return null;
			}

			return el.Value;
		}

		public static Dictionary<string, List<object>> ParseList(XElement el, string path = "")
		{
			var res = new Dictionary<string, List<object>>();

			//if (!el.HasAttributes)
			//	return res;

			foreach (var atr in el.Attributes())
			{
				res.Add($"{path}{(String.IsNullOrEmpty(path) ? "" : "@")}{atr.Name.LocalName}", new List<object>() { atr.Value });
			}			

			if (el.HasElements)
			{				
				foreach (var child in el.Elements())
				{
					var tmpPath = String.IsNullOrEmpty(path) ? child.Name.LocalName : $"{path}@{child.Name.LocalName}";
					if (!res.ContainsKey(el.Name.LocalName))
						res.Add(el.Name.LocalName, new List<object>());

					res[el.Name.LocalName].Add(ParseList(child, tmpPath));

					//foreach (var childAtrs in ParseAtrs(child, tmpPath))
					//{
					//	res.Add($"{childAtrs.Key}@{cntr++}", childAtrs.Value);						
					//}
				}
			}
			else
			{
				res.Add($"{el.Name.LocalName}@val", new List<object>() { el.Value });
			}

			return res;
		}

		public static Dictionary<string, string> ParseAtrs(XElement el, string path = "")
		{
			var res = new Dictionary<string, string>();

			//if (!el.HasAttributes)
			//	return res;

			foreach (var atr in el.Attributes())
			{
				res.Add($"{path}{(String.IsNullOrEmpty(path) ? "" : "@")}{atr.Name.LocalName}", atr.Value);
			}

			if (el.HasElements)
				foreach (var child in el.Elements())
				{
					var tmpPath = String.IsNullOrEmpty(path) ? child.Name.LocalName : $"{path}@{child.Name.LocalName}";
					foreach (var childAtrs in ParseAtrs(child, tmpPath))
					{
						res.Add(childAtrs.Key, childAtrs.Value);
					}
				}

			return res;
		}

		public static bool IsFullEmpty(this XElement el)
		{
			return (!el.HasAttributes && !el.HasElements && el.Value == null);
		}

		public static bool IsFull(this XElement el)
		{
			return el.HasElements && el.HasAttributes;
		}

		public static XElement GetSolidFill(this XElement root)
		{
			var a = root.GetNamespaceOfPrefix("a");
			return root.Element(a + "solidFill");
		}

		public static XElement GetBody(this XElement root)
		{
			var a = root.GetNamespaceOfPrefix("a");
			return root.Element(a + "bodyPr");
		}

		public static XElement GetStyle(this XElement root)
		{
			var a = root.GetNamespaceOfPrefix("a");
			return root.Element(a + "lstStyle");
		}

		public static XElement GetP(this XElement root)
		{
			var a = root.GetNamespaceOfPrefix("a");
			return root.Element(a + "p");
		}

		public static XElement FirstPPR(this XElement root)
		{
			return root.Elements().FirstOrDefault((v) => v.Name.LocalName.Contains("pPr"));
		}

		public static XElement FirstRPR(this XElement root)
		{
			return root.Elements().FirstOrDefault((v) => v.Name.LocalName.Contains("rPr"));
		}

		public static XElement FirstDefRPr(this XElement root)
		{
			return root.Elements().FirstOrDefault((v) => v.Name.LocalName.Contains("defRPr"));
		}

		public static XElement GetSpPr(this XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");
			return root.Element(c + "spPr");
		}

		public static XElement GetMarker(this XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");
			return root.Element(c + "marker");
		}

		public static XElement GetTxPr(this XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");
			return root.Element(c + "txPr");
		}

		public static XElement GetTitle(this XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");
			return root.Element(c + "title");
		}

		public static bool HasInfo(this XElement root)
		{
			return root != null && !root.IsFullEmpty();
		}

		public static XElement LazyElement(this XElement root, string name)
		{
			return root.Elements().FirstOrDefault((v) => v.Name.LocalName == name);
		}



	}
}
