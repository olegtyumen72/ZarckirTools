﻿using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Interfaces;
using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts
{
	internal class XmlChartSheet : IXmlVisibleSheet
	{
		public string Id { get; set; }
		public string RelativePath { get; } = "xl\\chartsheets\\";
		public string File { get; }
		public string AbsolutePath { get; set; }
		/// <summary>
		/// Набор графиков
		/// </summary>
		private List<XmlDrawing> grapths;
		private ReportSheet rs;

		public XmlChartSheet(ReportSheet rs, string pathToDir)
		{
			File = $"sheet{rs.Id}.xml";
			AbsolutePath = pathToDir;
			grapths = new List<XmlDrawing>();
			this.rs = rs;
			Id = rs.rId;
		}

		public XmlChartSheet(ReportSheet rs, string file, string pathToDir) : this(rs, pathToDir)
		{
			File = file;
		}

		public void Save()
		{
			var dir = Path.Combine(AbsolutePath, RelativePath);
			dir.CheckDir();
			var file = AbsoluteToFile();

			using (var doc = new XmlWriterWrapper(file))
			{
				CreateDoc(doc);
				doc.Save();
			}
		}
		
		public void TryLoad(XmlRels sheetRels)
		{
			var file = AbsoluteToFile();
			var dir = Directory.GetDirectoryRoot(file);

			if (!System.IO.File.Exists(file))
			{
				return;
			}

			var doc = XDocument.Load(file);
			var root = doc.Root;
			var ns = root.GetDefaultNamespace();

			var drawings = root.Elements(ns + "drawing");
			var r = root.GetNamespaceOfPrefix("r");

			foreach (var draw in drawings)
			{
				if (!draw.HasAttributes)
					continue;

				var rId = draw.Attribute(r + "id").Value;
				var drawFile = sheetRels.GetFile<XmlDrawing>(rId);

				rs.LocalGrapth.AddRange(drawFile.GetGrapth());
			}
		}

		private void CreateDoc(XmlWriterWrapper doc)
		{
			doc.OpenElement("chartsheet", "", XmlTools.xa["spredMain"])
				.AddAtribute("r", XmlTools.xa["offRelat"], "xmlns");

			doc.OpenElement("sheetViews").
				OpenElement("sheetView").AddAtribute("workbookViewId", 0).AddAtribute("zoomScale", 100).CloseElement().
				CloseElement();

			foreach (var grapth in grapths)
			{
				doc.OpenElement("drawing").AddAtribute("id", grapth.Id, "r").CloseElement();
			}

			doc.CloseElement();
		}

		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}

		public string RelativeToFile()
		{
			return Path.Combine(RelativePath, File);
		}

		public void AddGrapth(XmlDrawing draw)
		{
			grapths.Add(draw);
		}
	}
}
