﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Parts
{
	public interface ICellRange: ISimpleClone<ICellRange>
	{
		int cell1_Col { get; set; }
		int cell1_Row { get; set; }
		int cell2_Col { get; set; }
		int cell2_Row { get; set; }
		/// <summary>
		/// если хотя бы один из параметров -1, то считаем пустым
		/// </summary>
		/// <returns></returns>
		bool isEmpty();

		int RowCount();

		int ColumnCount();

		bool Contains(ICellRange other);
	}
}
