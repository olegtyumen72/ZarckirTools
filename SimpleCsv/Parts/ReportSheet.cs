﻿using SimpleCore.Objs;
using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCsv.Parts
{
	public class ReportSheet : List<ReportLine>, ISimpleConvert<IReportSheet>
	{
		public object Tag { get; set; }
		public string Name { get; set; }
		

		public ReportSheet(string name)
		{
			Name = name;
		}

		public ReportSheet(string name, IEnumerable<IReportLine> lines) : this(name)
		{
			foreach (var line in lines)
			{
				Add(new ReportLine(line));
			}
		}

		public ReportSheet(IReportSheet rs) : this(rs.Name)
		{
			foreach (var el in rs)
			{
				Add((ReportLine)el);
			}
		}

		public IReportLine GetLine(int row)
		{
			return this[row];
		}

		public IReportSheet Convert()
		{
			var res = new ReportSheetObj(Name,Color.Empty, Color.Empty);

			foreach (var el in this)
			{
				res.Add(el);
			}

			return res;
		}
	}
}
