﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Parts
{
	public interface ITranslater
	{

		/// <summary>
		/// Текущий язык (локаль) ru/en/fr/kz.
		/// </summary>
		string Lang { get; set; }

		/// <summary>
		/// Загрузить язык из папки плагина
		/// </summary>
		/// <param name="filename">Путь к файлу переводов.</param>
		/// <param name="lang">Код языка.</param>
		bool LoadLanguage(string file, string lang);

		/// <summary>
		/// Перевести текст на текущий язык.
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		string Translate(string text);

		/// <summary>
		/// Загрузить язык из папки плагина
		/// </summary>
		/// <param name="file">путь к файлу</param>
		/// <param name="lang">язык, который надо загрузить</param>
		/// <param name="is_key">true - искать атрибут isKey, использовать это значений в качестве ключа</param>
		/// <returns></returns>
		bool LoadLanguage(string file, string lang, bool is_key);

		/// <summary>
		/// перевод текста на загруженный язык по ключу
		/// перед переводом программа собирает аргументы в {} и заменяет их на индекс аргумента
		/// после перевода текста, аргументы восстанавливаются через string.Format
		/// пример:       text = "report sheet = {Name1} for project = {Name2}"
		/// приводится к формату "report sheet = {0} for project = {1}"
		/// </summary>
		/// <param name="text">текст для перевода с аргументами</param>
		/// <returns>переведенный текст</returns>
		string TranslateWithUpdatingText(string text);


	}
}
