﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools.Interface
{
	public interface ILoadable
	{
		void LoadFrom(IDataReader dr);
	}
}
