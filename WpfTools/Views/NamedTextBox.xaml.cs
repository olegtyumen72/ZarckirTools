﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTools.Views
{
	/// <summary>
	/// Interaction logic for NamedTextBox.xaml
	/// </summary>
	public partial class NamedTextBox : UserControl
	{
		public static DependencyProperty TitleDp = DependencyProperty.Register(nameof(Title), typeof(string), typeof(NamedTextBox), new FrameworkPropertyMetadata("Title", new PropertyChangedCallback(OnTitleChange)));

		public static DependencyProperty TextDp = DependencyProperty.Register(nameof(Text), typeof(string), typeof(NamedTextBox), new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnTextChange)));		


		public string Title
		{
			get => (string)GetValue(TitleDp);
			set => SetValue(TitleDp, value);
		}
		public string Text
		{
			get => (string)GetValue(TextDp);
			set => SetValue(TextDp, value);
		}
		public TextWrapping Wrap
		{
			get;
			set;
		} = TextWrapping.NoWrap;
		public bool AcceptReturns
		{
			get;
			set;
		} = false;
		public bool Editable
		{
			get; set;
		} = true;


		public NamedTextBox()
		{
			InitializeComponent();
		}

		static void OnTitleChange(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			(sender as NamedTextBox).Title = (string)e.NewValue;
		}

		private static void OnTextChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			(d as NamedTextBox).Text = (string)e.NewValue;
		}
	}
}
