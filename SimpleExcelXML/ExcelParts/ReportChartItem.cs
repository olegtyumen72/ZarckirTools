﻿using SimpleCore.Parts;
using SimpleCore.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.ExcelParts
{	

	public class ReportChartItem : ReportChartItemTmpl, ISimpleClone<ReportChartItem>
	{		
		public ReportChartItem() : base()
		{			
		}

		public new ReportChartItem Clone()
		{
			return (ReportChartItem)base.Clone();
		}

		public ReportChartItem(IReportChartItem v) : base(v)
		{

		}
	}
}
