﻿using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Tools;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Sheets
{
	internal class Cols
	{
		private const int MaxCol = 16384;

		private List<Col> cols { get; set; }
		private XmlStyle Style { get; set; }
		private List<ReportFormatItem> forFormat { get; set; }
		private ReportSheet rs { get; set; }
		private static int widthScreen { get; }
		private static int heightScreen { get; }
		private static Graphics g { get; }
		private int defStyleId
		{
			get
			{
				var tmp = Style.DefultStyle(rs.Name);
				if (tmp == -1)
				{
					throw new Exception("Cant add col, bacuse styles not init");
				}

				return tmp;
			}
		}
		//private int MaxRsCol { get; set; }

		static Cols()
		{
			if (g == null)
			{
				widthScreen = Screen.PrimaryScreen.Bounds.Width;
				heightScreen = Screen.PrimaryScreen.Bounds.Bottom;

				g = Graphics.FromImage(new Bitmap(widthScreen, heightScreen));
				g.PageUnit = GraphicsUnit.Display;
			}
		}

		public Cols(XmlStyle defStyle, List<ReportFormatItem> formats, ReportSheet rs, List<Col> cols = null)
		{
			Style = defStyle;
			forFormat = formats;
			this.rs = rs;
			this.cols = cols ?? new List<Col>();
		}

		private void InitCols()
		{
			foreach (var format in forFormat)
			{
				AddCol(format);
			}
		}

		/// <summary>
		/// Расчёт ширины колонки для определённой колонки 
		/// </summary>
		/// <param name="excelColIndex">индекс в эксель (начинается с 1)</param>
		/// <param name="font"></param>
		/// <returns></returns>
		public double AutoFitCol(int excelColIndex, SimpleFontFamily font)
		{

			if (font == null)
				return 0;

			g.Clear(Color.White);

			SimpleFontFamily temp = new SimpleFontFamily(Fonts.Calibri, 11); // 11
			//var mainFont = new Font(font.Name, font.Size);
			var mainFont = new Font(temp.Name, temp.Size);

			var max = 4d;
			var index = excelColIndex - 1;

			foreach (var line in rs)
			{
				var simpleWidth = 4d;
				if (line.Count > index && line[index] != null)
				{
					var str = line[index].ToString();
					if (!String.IsNullOrEmpty(str))
					{
// 						rs.CellFormat[]
						
						var maxDigit = 0d;
						for (int chrI = 0; chrI < str.Length; chrI++)
						{
							var digitW = g.MeasureString(str[chrI].ToString(), mainFont).Width;
							maxDigit = maxDigit < digitW ? digitW : maxDigit;
						}

						RectangleF rect = new RectangleF(0, 0, 1000, 1000);
						StringFormat format = new StringFormat();
						CharacterRange[] ranges = { new CharacterRange(0, str.Length) };
						Region[] reg = new Region[1];

						format.SetMeasurableCharacterRanges(ranges);

						reg = g.MeasureCharacterRanges(str, mainFont, rect, format);
						//float simpleWidth2 = reg[0].GetBounds(g).Size.Width + 5;
						//float simpleWidth3 = reg[0].GetBounds(g).Right + 5;

						//double maxpixel = simpleWidth2;

						//double widthpixel3 = Math.Truncate((str.Length * maxpixel + 5.0) / maxpixel * 256.0) / 256.0;

						//var simpleWidth25 = Math.Truncate((str.Length * maxDigit + 150.0) / maxDigit * 256.0) / 256.0;
						simpleWidth = Math.Truncate((str.Length * maxDigit + 5.0) / maxDigit * 256.0) / 256.0;
						//max = max < maxpixel ? maxpixel : max;
						max = max < simpleWidth ? simpleWidth : max;



						// !!!djerom весь код выше можно удалить.
						max = g.MeasureString(str, mainFont).Width / 7;
					}
				}
			}

			return max;
		}

		/// <summary>
		/// Расчёт ширины колонки для ренджа
		/// </summary>
		/// <param name="minColExcel">индекс в экселе(начинается с 1)</param>
		/// <param name="maxColExcel">индекс в экселе(начинается с 1)</param>
		/// <param name="font"></param>
		/// <returns></returns>
		public double AutoFitRange(int minColExcel, int maxColExcel, SimpleFontFamily font)
		{
			if (minColExcel > maxColExcel)
			{
				return 16;
			}

			var max = 4d;
			for (int i = minColExcel; i <= maxColExcel; i++)
			{
				var maxI = AutoFitCol(i, font);
				max = max < maxI ? maxI : max;
			}

			return max;
		}

		public void AddCol(ReportFormatItem item)
		{
			if (item.AllCols || !item.Range.HasEnd)
			{
				var col = new Col(item, item.StyleId);
				if (col.IsAddable)
					cols.Add(col);
			}
		}

		public void AddCol(Col col)
		{
			cols.Add(col);
		}

		public void Save(XmlWriterWrapper doc)
		{
			if (cols.Count <= 0)
				InitCols();

			if (cols.Count <= 0 && !rs.AutoFit && rs.CellsBg == Color.Empty && rs.CellsFont == Color.Empty)
			{
				return;
			}

			doc.OpenElement("cols");
			{

				//если что-то глобальное в листе, то настраиваем колонки
				if (rs.AutoFit || rs.CellsBg != Color.Empty || rs.CellsFont != Color.Empty)
				{
					var defFont = Style.DefaultFont(rs.Name);
					var rsIndexes = new List<int>();
					foreach (var col in cols)
					{
						if (rs.AutoFit)
						{
							for (int i = col.Min; i < col.Max; i++)
							{
								if (rsIndexes.Contains(i))
								{
									continue;
								}

								var newCol = col.Clone();
								newCol.Min = i;
								newCol.Max = i;
								newCol.Width = AutoFitCol(i, defFont);
								newCol.Save(doc);

								rsIndexes.Add(i);
							}
						}
						else
						{
							col.Save(doc);
						}

						//col.Width = rs.AutoFit ? AutoFitRange(col.Min, col.Max, defFont) : col.Width;
						//col.Save(doc);

						//for (int i = col.Min - 1; i <= col.Max - 1; i++)
						//{
						//	if (rsIndexes.Contains(i))
						//	{
						//		continue;
						//	}

						//	rsIndexes.Add(i);
						//}
					}

					var max = rs.Max((v) => v.Count);
					max = (max + 100) < MaxCol ? max + 100 : max;

					for (int i = 0; i < max; i++)
					{
						if (rsIndexes.Contains(i + 1))
						{
							continue;
						}

						var el = new Col(defStyleId)
						{
							Min = i + 1,
							Max = i + 1
						};

						if (rs.AutoFit)
						{
							el.Width = AutoFitCol(i + 1, defFont);
						}

						el.Save(doc);
					}


					#region с группировкой
					//if (rsIndexes.Count < 0)
					//{
					//	for (int i = 0; i < (rs.Max((v) => v.Count) + 100); i++)
					//	{
					//		var el = new Col(defStyleId)
					//		{
					//			Max = i + 1,
					//			Min = i + 1,
					//			Width = AutoFitCol(i, defFont)
					//		};

					//		el.Save(doc);
					//	};
					//}
					//else
					//{
					//	var max = rs.Max((v) => v.Count);
					//	max = (max + 100) < MaxCol ? max + 100 : max;

					//	int start = -1;
					//	for (int i = 0; i < max; i++)
					//	{
					//		if (rsIndexes.Contains(i)) continue;

					//		var el = new Col(defStyleId)
					//		{
					//			Min = i + 1,
					//			Max = i + 1,
					//			Width = AutoFitCol(i + 1, defFont)
					//		};

					//		el.Save(doc);

					//		#region алгоритм с группировкой
					//		//if (rsIndexes.Contains(i))
					//		//{
					//		//	if (start != -1)
					//		//	{
					//		//		var el = new Col(defStyleId)
					//		//		{
					//		//			Max = i,
					//		//			Min = start + 1,
					//		//			Width = AutoFitRange(start, i, defFont)
					//		//		};

					//		//		el.Save(doc);
					//		//		start = -1;
					//		//	}
					//		//}
					//		//else
					//		//{
					//		//	if (start == -1)
					//		//	{
					//		//		start = i;
					//		//	}
					//		//} 
					//		#endregion
					//	}
					//} 
					#endregion
				}
				//если нет, то просто сохраняем нужные нам колонки в нужном формате
				else
				{
					foreach (var col in cols)
					{
						col.Save(doc);
					}
				}
			}
			doc.CloseElement();
		}

	}
}
