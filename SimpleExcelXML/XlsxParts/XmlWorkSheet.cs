﻿using SimpleCore.Templates;
using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Interfaces;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Sheets;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts
{
	/// <summary>
	/// представляет из себя worksheet.xml файл
	/// </summary>
	internal class XmlWorkSheet : IXmlVisibleSheet
	{
		/// <summary>
		/// Основа 
		/// </summary>
		private ReportSheet rs;
		/// <summary>
		/// Представление SharedString - файла из которого берутся все текстовые данные, которые встречаются хоть раз
		/// </summary>
		private XmlSharedString shared;
		/// <summary>
		/// Набор графиков
		/// </summary>
		private List<XmlDrawing> grapths = new List<XmlDrawing>();
		/// <summary>
		/// Представляет style.xml - файл со стилями для каждой ячейки и графиков
		/// </summary>
		private XmlStyle style;
		/// <summary>
		/// Форматирующие элементы из ReportSheet
		/// </summary>
		private IEnumerable<ReportFormatItem> forFormat;
		/// <summary>
		/// Максимально используемая строчка в Rs
		/// </summary>
		private int maxRow = -1;
		/// <summary>
		/// Максимально используемая колонка в виде "A", "B", ... "X", ... ,
		/// </summary>
		private string maxCol = null;
		/// <summary>
		/// Является ли первым
		/// </summary>
		private bool isSelected = false;
		/// <summary>
		/// Макс количество колонок в экселе
		/// </summary>
		private const int MaxCol = 16384;
		/// <summary>
		/// Макс количество строчек в экселе
		/// </summary>
		private const int MaxRow = 1048575;
		/// <summary>
		/// Коллекция стилей только для колонок. Например: ширина колонки
		/// </summary>
		private Cols cols;

		/// <summary>
		/// Свойство для работы с ReportSheet из вне, без возможности замены
		/// </summary>
		public ReportSheet ReportSheet
		{
			get
			{
				return rs;
			}
		}

		/// <summary>
		/// Инициализация
		/// </summary>
		/// <param name="rsCur">Представление данного класса в данных</param>
		/// <param name="file">имя файла для сохранение</param>
		/// <param name="sr">SharedString</param>
		/// <param name="styleCells">Styles</param>
		/// <param name="path">путь для сохранения</param>
		public XmlWorkSheet(ReportSheet rsCur, string file, XmlSharedString sr, XmlStyle styleCells, string path)
		{
			Id = rsCur.rId;
			File = file;
			shared = sr;
			rs = rsCur;
			style = styleCells;
			forFormat = rsCur.LocalFormats;
			AbsolutePath = path;
			cols = new Cols(styleCells, forFormat.ToList(), rsCur);
		}

		public XmlWorkSheet(ReportSheet rsCur, XmlSharedString sr, XmlStyle styleCells, string path)
		{
			Id = rsCur.rId;
			File = $"sheet{rsCur.Id}.xml";
			shared = sr;
			rs = rsCur;
			style = styleCells;
			forFormat = rsCur.LocalFormats;
			AbsolutePath = path;
			cols = new Cols(styleCells, forFormat.ToList(), rsCur);
		}

		public XmlWorkSheet(ReportSheet rsCur, XmlSharedString sr, XmlStyle styleCells, bool selected, string path) : this(rsCur, sr, styleCells, path)
		{
			isSelected = selected;
		}

		/// <summary>
		/// Служит для получения по индеку колонки 
		/// </summary>
		private static char[] alphabet = new char[]
		{
			//1  2   3   4   5   6   7   8   9   10  11  12  13 14  15  16   17  18 19   20 21   22  23  24  25  26
			'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
		};

		/// <summary>
		/// rId - relative id - нужен для rels
		/// </summary>
		public string Id { get; set; }
		/// <summary>
		/// Relative path до файла
		/// </summary>
		public string RelativePath { get; } = "xl\\worksheets\\";
		/// <summary>
		/// Имя файла в который будем сохранять
		/// </summary>
		public string File { get; }
		/// <summary>
		/// Полный путь до расположения файла
		/// </summary>
		public string AbsolutePath { get; set; }

		/// <summary>
		/// Сохранения структуры в xml
		/// </summary>
		public void Save()
		{
			var dir = Path.Combine(AbsolutePath, RelativePath);
			dir.CheckDir();

			var file = Path.Combine(dir, File);
			//file.CheckTxtFile();

			using (var doc = new XmlWriterWrapper(file))
			{
				CreateDoc(doc);
				doc.Save();
			}
		}

		/// <summary>
		/// Загрузка из Xml
		/// </summary>
		/// <param name="sheetRels"></param>
		internal void TryLoad(XmlRels sheetRels)
		{
			var file = AbsoluteToFile();
			var dir = Directory.GetDirectoryRoot(file);

			if (!System.IO.File.Exists(file))
			{
				return;
			}

			var doc = XDocument.Load(file);
			var root = doc.Root;
			var ns = root.GetDefaultNamespace();

			var cols = root.Element(ns + "cols")?.Elements(ns + "col");
			var data = root.Element(ns + "sheetData")?.Elements(ns + "row");
			var merge = root.Element(ns + "mergeCells")?.Elements(ns + "mergeCell");
			var drawings = root.Elements(ns + "drawing");
			var condFormats = root.Elements(ns + "conditionalFormatting");
			var r = root.GetNamespaceOfPrefix("r");

			StylesRangeCollection styles = null;
			if (cols != null)
			{
				styles = LoadCols(cols);
			}

			if (data != null)
			{
				styles = LoadRows(data, styles);
			}

			styles.MergeRanges();
			styles.IndexToCells();

			foreach (var stlRng in styles)
			{
				var style = stlRng.Key;
				this.style.FillFormatItem(rs, style);

				var rngs = stlRng.Value;
				foreach (var rng in rngs)
				{
					var rStyle = (ReportFormatItem)style.Clone();
					rStyle.StyleId = style.StyleId;
					rStyle.Range = rng;
					rs.LocalFormats.Add(new ReportFormatItem(rStyle.Convert()));
					rs.LocalFormats.Last().NumFmtId = rStyle.NumFmtId;
					rs.LocalFormats.Last().StyleId = style.StyleId;
				}
			}

			foreach (var draw in drawings)
			{
				if (!draw.HasAttributes)
					continue;

				var rId = draw.Attribute(r + "id").Value;
				var drawFile = sheetRels.GetFile<XmlDrawing>(rId);

				rs.LocalGrapth.AddRange(drawFile.GetGrapth());
			}

			//foreach(var cndFrm in condFormats)
			//{
			//	var format = new ReportFormatItem();

			//}
		}

		/// <summary>
		/// Загрузка стилей из строчек (основная дата)
		/// </summary>
		/// <param name="data"></param>
		/// <param name="styles"></param>
		/// <returns></returns>
		private StylesRangeCollection LoadRows(IEnumerable<XElement> data, StylesRangeCollection styles = null)
		{
			if (styles == null)
				styles = new StylesRangeCollection();

			foreach (var row in data)
			{
				if (!row.HasAttributes || !row.HasElements)
					continue;

				var atrs = XDocumentTools.ParseList(row);

				var rowI = atrs["r"].First().ToString().ToInt() - 1;
				AddEmptyLines(rowI);

				foreach (Dictionary<string, List<object>> atrRow in atrs["row"])
				{
					var colI = ExcelTools.GetNumberCol(atrRow["c@r"].First().ToString()) - 1;
					AddEmptyColls(rowI, colI);

					var styleId = 0;
					if (atrRow.ContainsKey("c@s"))
						styleId = atrRow["c@s"].First().ToString().ToInt();

					if (atrRow.ContainsKey("c"))
					{
						var rowData = (atrRow["c"].Last() as Dictionary<string, List<object>>);
						if (rowData.ContainsKey("v@val"))
						{
							var val = rowData["v@val"].First().ToString();
							object realV;
							if (atrRow.ContainsKey("c@t") && atrRow["c@t"].First().ToString() == "s" && val != null)
							{
								realV = shared.GetValue(val.ToInt());
							}
							else
							{
								realV = val;
							}

							rs[rowI][colI] = realV;
						}
					}

					ShortFormatItem styleF = null;
					if (styles.ContainsKey(styleId))
						styleF = styles[styleId];
					else
						styleF = new ShortFormatItem()
						{
							StyleId = styleId
						};

					styles.AddCellByColumn(styleF, colI, rowI);
				}
			}

			return styles;
		}

		/// <summary>
		/// Загрузка стилей из колонок (перед основными данными)
		/// </summary>
		/// <param name="cols"></param>
		/// <param name="styles"></param>
		/// <returns></returns>
		private StylesRangeCollection LoadCols(IEnumerable<XElement> cols, StylesRangeCollection styles = null)
		{
			if (styles == null)
				styles = new StylesRangeCollection();

			foreach (var col in cols)
			{
				if (!col.HasAttributes)
					continue;

				var atrs = XDocumentTools.ParseAtrs(col);
				int min = 0, max = 0, styleId = 0;
				double width = 0;

				if (atrs.ContainsKey("min"))
					min = atrs["min"].ToInt();
				if (atrs.ContainsKey("max"))
					max = atrs["max"].ToInt();
				if (atrs.ContainsKey("style"))
					styleId = atrs["style"].ToInt();
				if (atrs.ContainsKey("width"))
					width = atrs["width"].ToDbl();

				var styleF = new ShortFormatItem()
				{
					StyleId = styleId,
					ColumnWidth = width
				};

				if (!styles.ContainsStyleWidth(styleF))
					styles.Add(styleF, new List<CellRangeTmpl>());
				else
					styleF = styles.GetByStyleAndWidth(styleF.StyleId, styleF.ColumnWidth);

				styles.AddColRange(styleF, min, max);

				//var xmlCol = new Col(styleId, max, min, width);
				//this.cols.AddCol(xmlCol);
				//if(atrs.ContainsKey("customWidth"))
				//tmp = "";
			}

			return styles;
		}

		private void AddEmptyColls(int r, int c)
		{
			if (rs[r].Count > c)
				return;

			for (int i = rs[r].Count; i < c + 1; i++)
				rs[r].Add("");
		}

		private void AddEmptyLines(int rowI)
		{
			if (rs.Count > rowI)
				return;

			for (int i = rs.Count; i < rowI + 1; i++)
				rs.Add(new ReportLine());
		}

		//private double AutoFormCol(ReportSheet rs, int col, SimpleFontFamily font)
		//{
		//	var g = Graphics.FromImage(new Bitmap(200, 200));
		//	var mainFont = new Font(font.Name, font.Size);
		//	var widthZero = (double)g.MeasureString("0", mainFont).Width;
		//	var max = -1d;
		//	foreach (var line in rs)
		//	{
		//		var simpleWidth = (double)g.MeasureString(line[col].ToString(), mainFont).Width;
		//		simpleWidth = simpleWidth / widthZero;

		//		max = simpleWidth > max ? simpleWidth : max;
		//	}

		//	return max;
		//}

		/// <summary>
		/// Логика непосредственного создания .xml файла через враппер xml
		/// </summary>
		/// <param name="doc"></param>
		private void CreateDoc(XmlWriterWrapper doc)
		{
			XmlTools.DefaultDeclaration(doc);

			doc.OpenElement("worksheet", null, XmlTools.xa["spredMain"])
				.AddAtribute("r", XmlTools.xa["offRelat"], "xmlns")
				//.AddAtribute("mc", XmlTools.xa["compab"])
				.AddAtribute("Ignorable", "x14ac", "mc", XmlTools.xa["compab"])
				.AddAtribute("x14ac", XmlTools.xa["x14ac"], "xmlns")

				.OpenElement("sheetViews")

				.OpenElement("sheetView");
			if (isSelected)
			{
				doc.AddAtribute("tabSelected", "1");
			}

			doc.AddAtribute("workbookViewId", "0")
			.CloseElement()

			.CloseElement();


			//bool isColsCreated = false;
			Dictionary<CellRangeTmpl, ReportFormatItem> rowsFormats = new Dictionary<CellRangeTmpl, ReportFormatItem>();
			var forMerge = new List<ReportFormatItem>();
			//var identCols = new List<int>();
			foreach (var format in forFormat)
			{
				if (format.RowHeight > 0 || format.HideRows)
				{
					CellRangeTmpl range = null;
					if (format.AllRows && format.Row >= 1)
					{
						range = new CellRangeTmpl(-1, format.Row, -1, MaxRow);
					}
					else if (format.Range != null)
					{
						range = new CellRangeTmpl(-1, format.Range.cell1_Row, -1, format.Range.cell2_Row);
					}

					rowsFormats.Add(range, format);
				}

				if (format.MergeArea || format.MergeColumns)
				{
					forMerge.Add(format);
				}
			}

			cols.Save(doc);

			doc.OpenElement("sheetData");

			for (int row = 0; row < rs.Count; row++)
			{
				var line = rs[row];
				var excelR = row + 1;
				doc.OpenElement("row").AddAtribute("r", (excelR).ToString());

				foreach (var el in rowsFormats)
				{
					var range = el.Key;
					var format = el.Value;
					if (range.cell1_Row <= row && range.cell2_Row >= row)
					{
						if (format.HideRows)
						{
							doc.AddAtribute("ht", 0);
						}
						else if (format.RowHeight > 0)
						{
							doc.AddAtribute("ht", format.RowHeight);
						}

						doc.AddAtribute("customHeight", "1");
						break;
					}
				}

				for (int col = 0; col < line.Count; col++)
				{
					var val = line[col];
					var valStr = "";
					doc.OpenElement("c")
						.AddAtribute("r", $"{NamedColumn(col + 1)}{excelR}");

					var cellStyle = style.StyleForIndex(col, row, rs.Name);
					if (cellStyle != -1)
					{
						doc.AddAtribute("s", cellStyle.ToString());
					}

					if (shared.CanAdd(val))
					{
						doc.AddAtribute("t", "s");
						valStr = shared.AddUnique(val, true).ToString();
					}
					else
					{
						valStr = val.ToString().Replace(',', '.').Trim();

						if (!String.IsNullOrEmpty(valStr))
						{
							var tmp = 0d;
							if (double.TryParse(valStr, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out tmp))
							{
								if (double.IsNaN(tmp) || double.IsInfinity(tmp) || double.MinValue == tmp || double.MaxValue == tmp)
								{
									valStr = "";
								}
							}
							else
							{
								throw new Exception("qwe");
							}
						}
						//valStr = val.ToString().ToString("0." + new string('#', 339));
						//valStr = String.Format("{0:0." + new string('0', 230) + "}", val);
						//valStr = valStr.TrimEnd('0').Replace(',', '.');

					}

					if (!String.IsNullOrEmpty(valStr))
					{
						doc.OpenElement("v").UpdateValue(valStr.ToString()).CloseElement()
							.CloseElement();
					}
					else
					{
						doc.CloseElement();
					}
				}

				var rowStylers = rs.LocalFormats.Where((v) => v.Range.ContainRow(excelR)).ToList();

				foreach(var styler in rowStylers)
				{
					if(line.Count < styler.Range.ColumnCount())
					{
						for(int i=line.Count; i <= styler.Range.ColumnCount(); i++)
						{
							doc.OpenElement("c").AddAtribute("r", $"{NamedColumn(i + 1)}{excelR}");
							var cellStyle = style.StyleForIndex(i, row, rs.Name);
							if (cellStyle != -1)
							{
								doc.AddAtribute("s", cellStyle.ToString());
							}
							doc.CloseElement();
						}
					}
				}

				doc.CloseElement();
			}

			if (rs.Count > 0)
			{
				int maxColInt = rs.Max((v) => v.Count);
				var outDataFormat = forFormat.Where(
					(v) =>
					v.Range != null &&
					(v.Range.cell1_Row > rs.Count ||
						v.Range.cell1_Row == rs.Count && v.Range.RowCount() > 0) ||
					(v.Range.cell1_Col > maxColInt ||
						v.Range.cell1_Col == maxColInt && v.Range.ColumnCount() > 0)
					);


				foreach (var format in outDataFormat)
				{
					var range = format.Range;

					var strRow = range.cell1_Row;
					var endRow = range.cell2_Row;
					var strCol = range.cell1_Col;
					var endCol = range.cell2_Col;

					for (int i = strRow; i <= endRow; i++)
					{
						doc.OpenElement("row").AddAtribute("r", (i).ToString());

						if (format.HideRows)
						{
							doc.AddAtribute("ht", 0);
						}
						else if (format.RowHeight > 0)
						{
							doc.AddAtribute("ht", format.RowHeight);
						}

						for (int j = strCol; j <= endCol; j++)
						{
							var namedCol = NamedColumn(j);
							var cellStyle = style.StyleForIndex(j, i, rs.Name);

							doc.OpenElement("c")
							.AddAtribute("r", $"{namedCol}{i}");

							doc.AddAtribute("s", cellStyle.ToString());
							doc.CloseElement();
						}

						doc.CloseElement();
					}
				}

			}
			doc.CloseElement();

			if (forMerge.Count() > 0)
			{
				if (maxRow == -1)
				{
					maxRow = rs.Count;
				}

				if (String.IsNullOrEmpty(maxCol))
				{
					maxCol = NamedColumn(rs.Max((v) => v.Count));
				}

				doc.OpenElement("mergeCells").AddAtribute("count", forMerge.Count().ToString());
				foreach (var el in forMerge)
				{
					doc.OpenElement("mergeCell").AddAtribute("ref", RangeToRef(el, false)).CloseElement();
				}
				doc.CloseElement();

				maxRow = -1;
				maxCol = null;
			}

			foreach (var grapth in grapths)
			{
				doc.OpenElement("drawing").AddAtribute("id", grapth.Id, "r").CloseElement();
			}

			doc.CloseElement();
		}

		/// <summary>
		/// Переводит из Range в ссылочный тип экселя
		/// </summary>
		/// <param name="item"></param>
		/// <param name="isNameRs"></param>
		/// <returns></returns>
		private string RangeToRef(ReportFormatItem item, bool isNameRs = true)
		{
			var range = item.Range;
			var strRow = item.Row;
			var strCol = item.Col;

			var start = "";
			var end = "";

			if (strCol != -1 && strRow != -1)
			{
				var col = NamedColumn(strCol + 1);
				start = $"{col}{strRow}";

				if (item.AllCols)
				{
					end = $"{maxCol}{strRow}";
				}
				else if (item.AllRows)
				{
					end = $"{col}{maxRow}";
				}
				else if (item.MergeArea)
				{
					end = $"{maxCol}{maxRow}";
				}
			}
			else if (strCol != -1)
			{
				var col = NamedColumn(strCol + 1);
				start = $"{col}1";

				if (item.AllCols)
				{
					end = $"{maxCol}1";
				}
				else if (item.AllRows)
				{
					end = $"{col}{maxRow}";
				}
				else if (item.MergeArea)
				{
					end = $"{maxCol}{maxRow}";
				}
			}
			else if (strRow != -1)
			{
				var col = NamedColumn(strCol + 1);
				start = $"A{strRow}";

				if (item.AllCols)
				{
					end = $"{maxCol}{strRow}";
				}
				else if (item.AllRows)
				{
					end = $"A{maxRow}";
				}
				else if (item.MergeArea)
				{
					end = $"{maxCol}{maxRow}";
				}
			}
			else
			{
				start = $"{NamedColumn(range.cell1_Col)}{range.cell1_Row}";
				end = $"{NamedColumn(range.cell2_Col)}{range.cell2_Row}";
			}

			if (String.IsNullOrEmpty(start) || String.IsNullOrEmpty(end))
			{
				throw new Exception("Wrong range for merge");
			}

			var res = $"@!{start}:{end}";
			if (isNameRs)
				res = res.Replace("@", rs.Name);
			else
				res = res.Remove(0, 2);

			return res;
		}

		/// <summary>
		/// Подбирает выражение для названия колонки
		/// Должен приходить реальный индекс от 1 до бессконечности
		/// </summary>
		/// <param name="index">индекс колонкп</param>
		/// <returns>Название колонки</returns>
		private string NamedColumn(int index)
		{
			var res = "";
			var mod = 0;
			var div = index - 1;

			while (true)
			{
				if (div < alphabet.Length)
				{
					break;
				}
				else
				{
					index = div;
				}

				mod = index % alphabet.Length;
				res += alphabet[mod];
				div = (index / alphabet.Length) - 1;
			}

			res += alphabet[div];
			return String.Join("", res.Reverse());
		}

		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}

		public string RelativeToFile()
		{
			return Path.Combine(RelativePath, File);
		}

		public void AddGrapth(XmlDrawing draw)
		{
			grapths.Add(draw);
		}
	}
}

