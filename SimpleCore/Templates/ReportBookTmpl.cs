﻿using SimpleCore.Objs;
using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Templates
{
	public class ReportBookTmpl<T,V,C,E> : List<T>, ISimpleConvert<IReportBook>, ICoreReportBook
		where V:ICellRange
		where C:IConditionalFormat
		where E: IReportFormatItem<V,C>
		where T: ISimpleConvert<IReportSheet>
	{
		public string Path { get; set; }

		public IReportBook Convert()
		{
			return new ReportBookObj(Path, ConvertAll((v) => v.Convert()));
		}

		public ReportBookTmpl(string path)
		{
			Path = path;
		}

		public ReportBookTmpl(IReportBook v)
		{
			Path = v.Path;

			foreach(var el in v)
			{
				Add((T)Activator.CreateInstance(typeof(T), el));
				//Add(new ReportSheetTmpl(el));
			}
		}
	}
}
