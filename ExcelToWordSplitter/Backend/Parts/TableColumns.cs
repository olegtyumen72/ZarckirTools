﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Document.NET;

namespace ExcelToWordSplitter.Backend.Tools
{
	/// <summary>
	/// Кубик-таблицы для построения таблицы алгоритмом по столбцам
	/// </summary>
	public struct TableColumns
	{
		public TableColumns(
			List<TableRange> head,
			List<TableRange> body,
			bool isStatic,
			bool endWithPhageBreak,
			bool isSticky)
		{
            if (head == null)
            {
                throw new ArgumentNullException(nameof(head));
            }
            else
                Head = head;
            if (body == null)
            {
                throw new ArgumentNullException(nameof(body));
            }
            else
                Body = body;
			IsStatic = isStatic;
			EndWithPhageBreak = endWithPhageBreak;
			IsStickyToPrevious = isSticky;
		}

		/// <summary>
		/// Описание хедеров
		/// </summary>
		public List<TableRange> Head { get; set; }
		/// <summary>
		/// Описание боди
		/// </summary>
		public List<TableRange> Body { get; set; }
		/// <summary>
		/// Повторять ли этот кубик для всех остальных
		/// </summary>
		public bool IsStatic { get; set; }
		/// <summary>
		/// Разделить ли этот кубик от следующего страницей
		/// /// !!!Внимание: при true игнорирует параметр IsStickyToPrevious
		/// </summary>
		public bool EndWithPhageBreak { get; set; }
		/// <summary>
		/// Склеить ли кубик с предыдущим
		/// !!!Внимание: при true игнорирует параметр EndWithPhageBreak
		/// </summary>
		public bool IsStickyToPrevious { get; set; }
	}
}
