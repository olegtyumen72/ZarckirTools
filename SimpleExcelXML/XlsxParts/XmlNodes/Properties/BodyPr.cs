﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// Настройки для элемента графика
	/// </summary>
	internal class BodyPr: XmlPropBase
	{
		private int rot;
		private string vertOverflow;
		private string vert;
		private string wrap;
		private string anchor;
		private int anchorCtr;
		private int spcFirstLastPara;
		private string prefix;

		public static BodyPr Default { get { return new BodyPr(); } }

		public BodyPr(string prefix = "a", string vertOver = null, string vert = null, string wrap = null, string anchor = null, int anchorCtr = -1, int spcFirsLastPar = -1, int rotation = -1)
		{
			vertOverflow = vertOver;
			this.vert = vert;
			this.wrap = wrap;
			this.anchor = anchor;
			this.anchorCtr = anchorCtr;
			spcFirstLastPara = spcFirsLastPar;
			this.prefix = prefix;
			rot = rotation;
		}

		public BodyPr(XElement xBody) : base(xBody)
		{
			//TryLoad(xBody);
		}

		public override void TryLoad(XElement xBody)
		{
			var atrs = XDocumentTools.ParseAtrs(xBody);

			spcFirstLastPara = ReadAtrValue(atrs, nameof(spcFirstLastPara))?.ToInt() ?? -1;
			vertOverflow = ReadAtrValue(atrs,  nameof(vertOverflow));
			vert = ReadAtrValue(atrs,  nameof(vert));
			wrap = ReadAtrValue(atrs,  nameof(wrap));
			anchor = ReadAtrValue(atrs,  nameof(anchor));
			anchorCtr = ReadAtrValue(atrs, nameof(anchorCtr))?.ToInt() ?? -1;
			rot = ReadAtrValue(atrs, nameof(rot))?.ToInt() ?? -1;
		}

		//protected override string ReadAtrValue(Dictionary<string, string> atrs, string atr)
		//{
		//	if (atrs.ContainsKey(atr))
		//		return atrs[atr];

		//	return null;
		//}

		//protected override bool IsWritable(string val)
		//{
		//	return !String.IsNullOrEmpty(val);
		//}

		//protected override bool IsWritable(int val)
		//{
		//	return !(val == -1);
		//}

		//protected override void WriteAtrValue(XmlWriterWrapper doc, string val, string atr)
		//{
		//	if (IsWritable(val))
		//	{
		//		doc.AddAtribute(atr, val);
		//	}
		//}

		//protected override void WriteAtrValue(XmlWriterWrapper doc, int val, string atr)
		//{
		//	if (IsWritable(val))
		//	{
		//		doc.AddAtribute(atr, val.ToString());
		//	}
		//}

		public override void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("bodyPr", prefix);

			WriteAtrValue(doc, spcFirstLastPara, nameof(spcFirstLastPara));
			WriteAtrValue(doc, vertOverflow, nameof(vertOverflow));
			WriteAtrValue(doc, vert, nameof(vert));
			WriteAtrValue(doc, wrap, nameof(wrap));
			WriteAtrValue(doc, anchor, nameof(anchor));
			WriteAtrValue(doc, anchorCtr, nameof(anchorCtr));

			doc.CloseElement();

		}
	}
}
