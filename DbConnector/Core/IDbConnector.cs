﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbConnector.Core
{
	public enum SqlVers { MySql, Sqlite, MsSql }

	public interface IDbConnector<T> : IDisposable
	{
		SqlVers SqlVersion { get; }
		bool CngConDb(string name);
		T Query(string query);
		V GetLast<V>(string table, string col);
		V GetKeyFromDic<V>(string table, string col, string targer);
		T SelectQuery(string table, string col, string where = "", string etc = "");
		ISqlPartData<T> SelectPartial(string table, string col, int offset, int count, string colForOrder = "", string where = "", bool isLazy = true);
		bool InsertQuery(string table, string col, string values);
		T GetDictionary(string table);
		bool UpdateWithDuplicate(string table, string col, string value);
		V GetKeyFromDb<V>(string tbl, string col, string matchCol, string matchVal);
		V GetKeyFromDbW<V>(string tbl, string col, string where);
		bool DeleteQuery(string table, string where);
		bool IsEmpty(T data, string col = "");
		bool CreateTable(string table, List<string> col, List<string> etc);
		bool DeleteTable(string table);
		bool TruncateTable(string table);
		bool CreateDb(string name);
		bool DropDb(string name);
	}
}
