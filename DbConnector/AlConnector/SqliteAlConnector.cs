﻿using AssociativeList;
using DbConnector.Core;
using DbConnector.Tools;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace DbConnector.AlConnector
{
	public class SqliteAlConnector : IDbAlConnector
	{

		SQLiteConnection _con;
		private readonly Action<string> _logMsg;
		private readonly Action<AList> _logData;

		public SqlVers SqlVersion
		{
			get
			{
				return SqlVers.Sqlite;
			}
		}

		public SqliteAlConnector(string connstring, Action<string> logMsg = null, Action<AList> logData = null)
		{
			_con = new SQLiteConnection(connstring);
			_logMsg = logMsg;
			_logData = logData;
			_con.Open();
		}

		#region local

		private AList CreateRes(bool value)
		{
			var res = GetBoolRes(value);
			_logData?.Invoke(res);
			return res;
		}

		private AList GetBoolRes(bool v)
		{
			var tmp = new AList
			{
				{ "0", new List<bool>() { v } }
			};
			return tmp;
		}

		public bool IsEmpty(AList data, string col = "")
		{
			if (data == null) return true;
			col = col.Length <= 0 ? data.First().Key : col;
			return
				data == null ||
				data.Values.First() == null ||
				data.Values.First().Count <= 0 ||
				(data.Values.First().Count == 1 && data.Values.First()[0].GetType().Name.ToLower() == "boolean" && data.Keys.First() == "0");
		}

		private bool GetRes(AList associativeList)
		{
			return associativeList.FirstOrDefault().Value.Cast<bool>().FirstOrDefault();
		}

		#endregion

		#region static

		public static SqliteAlConnector InitNewDb(string path)
		{
			try
			{
				if (!File.Exists(path))
					SQLiteConnection.CreateFile(path);
				var res = new SqliteAlConnector($"Data source={path};Version=3;");

				return res;
			}
			catch
			{
				return null;
			}
		}

		#endregion

		public bool CreateTable(string table, List<string> cols, List<string> etcs)
		{
			var query = $"CREATE TABLE IF NOT EXISTS {table} (";
			foreach (var col in cols)
			{
				query += $"{col},";
			}
			foreach (var etc in etcs)
			{
				query += $"{etc},";
			}
			query = query.DelLastChar() + ")";
			return GetRes(Query(query));
		}

		public bool DeleteQuery(string table, string where)
		{
			string query = $"DELETE FROM {table} WHERE {where}";
			return GetRes(Query(query));
		}

		public bool DeleteTable(string table)
		{
			var query = $"DROP TABLE IF EXISTS {table}";
			return GetRes(Query(query));
		}

		public void Dispose()
		{
			_con.Close();
			_con.Dispose();
		}

		public AList GetDictionary(string table)
		{
			var query = $"Select * from {table}";
			return Query(query);
		}

		public V GetKeyFromDb<V>(string tbl, string col, string matchCol, string matchVal)
		{
			return GetKeyFromDbW<V>(tbl, col, matchCol + " = \"" + matchVal + "\"");
		}

		public V GetKeyFromDbW<V>(string table, string col, string where)
		{
			col = DbTools.AddMySqlBracets(col);
			var data = SelectQuery(table, col, where);
			col = DbTools.RemoveMySqlBracets(col);

			if (IsEmpty(data, col))
			{
				return AList.DefaultByColType<V>();
				//switch (typeof(V).Name.ToLower())
				//{
				//	default:
				//		{
				//			return default(V);
				//		}
				//	case "int":
				//	case "int32":
				//	case "int64":
				//	case "double":
				//	case "float":
				//		{
				//			return (V)Convert.ChangeType(-1, typeof(V));
				//		}
				//}
			}
			else
			{
				return (V)Convert.ChangeType(data[col][0], typeof(V));
			}
		}

		public V GetKeyFromDic<V>(string table, string col, string target)
		{
			var tmp = DbTools.RemoveMySqlBracets(col);
			col = DbTools.AddMySqlBracets(col);
			var data = SelectQuery(table, col, "`Name_eng` = \"" + target + "\"");
			data = IsEmpty(data, tmp) ? SelectQuery(table, col, "`Name_rus` = \"" + target + "\"") : data;

			if (IsEmpty(data, tmp))
			{
				return AList.DefaultByColType<V>();
			}
			else
			{
				return (V)Convert.ChangeType(data[tmp][0], typeof(V));
			}
		}

		public V GetLast<V>(string table, string col)
		{
			col = DbTools.AddMySqlBracets(col);
			var res = SelectQuery(table, col, null, $"Order by {col} DESC LIMIT 1");
			return res.First().Value.Cast<V>().FirstOrDefault();
		}

		public bool InsertQuery(string table, string col, string values)
		{
			var query = $"insert into {table}({col}) VALUES {values}";
			return GetRes(Query(query));
		}

		public AList Query(string query)
		{
			query = query.Replace("[dbo].", "").Replace("dbo.", "");
			_logMsg?.Invoke(query);
			using (var cmd = _con.CreateCommand())
			{
				cmd.CommandText = query;
				try
				{
					var reader = cmd.ExecuteReader();
					var table = new AList(reader);
					if (table.Count <= 0)
					{
						return CreateRes(true);
					}
					else
					{
						_logData?.Invoke(table);
						return table;
					}
				}
				catch
				{
					return CreateRes(false);
				}
			}
		}

		public AList SelectQuery(string table, string col, string where = "", string etc = "")
		{
			var query = $"Select {col} from {table}";
			if (where?.Length > 0)
			{
				query += $" WHERE {where}";
			}
			if (etc?.Length > 0)
			{
				query += $" {etc}";
			}

			return Query(query);
		}

		public bool TruncateTable(string table)
		{
			string query = $"DELETE FROM {table}";
			return GetRes(Query(query));
		}

		public bool UpdateWithDuplicate(string table, string col, string value)
		{
			var cols = col.Split(',');
			var colVal = new string[cols.Length];
			for (int i = 0; i < cols.Length; i++)
			{
				colVal[i] = $"{cols[i]}=VALUES({cols[i]})";
			}

			var query = $"insert into {table}({col}) VALUES {value} ON DUPLICATE KEY UPDATE {String.Join(",", colVal)}";

			return GetRes(Query(query));
		}

		public bool CngConDb(string name)
		{
			try
			{
				DropDb(_con.DataSource);
				CreateDb(name);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public ISqlPartData<AList> SelectPartial(string table, string col, int offset, int count, string orderedCols = "", string where = "", bool isLazy = true)
		{
			orderedCols = orderedCols.Length <= 0 ? col : orderedCols;
			return new PartialSqlData<AList>(count, offset,
			(cnt, off) =>
			{
				var order = $"LIMIT {off},{cnt}"; //$"Order by {orderedCols} {off},{cnt}";
				var res = SelectQuery(table, col, where, order);
				return res;
			}
			, (v) => IsEmpty(v), isLazy);
		}

		public bool CreateDb(string name)
		{
			try
			{
				if (!File.Exists(name))
					SQLiteConnection.CreateFile(name);

				_con = new SQLiteConnection($"Data source={name};");
				_con.Open();
				return true;
			}
			catch
			{
				return false;
			}

		}

		public bool DropDb(string name)
		{
			try
			{
				_con.Close();
				if (File.Exists(name))
				{
					File.Delete(name);
					return true;
				}
				return true;
			}
			catch
			{
				return false;
			}
		}

	}
}
