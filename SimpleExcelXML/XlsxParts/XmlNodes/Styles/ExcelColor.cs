﻿using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Styles
{
	internal class ExcelColor
	{
		public static ExcelColor ColorElem()
		{
			return new ExcelColor();
		}
		public static ExcelColor BgElement()
		{
			var res = new ExcelColor()
			{
				Name = "BgColor"
			};

			return res;
		}
		public static ExcelColor FgElement()
		{
			var res = new ExcelColor()
			{
				Name = "fgColor"
			};

			return res;
		}

		public Color Clr { get; set; }
		public string Theme { get; set; }
		public string Tint { get; set; }
		public int Index { get; set; }
		public string Name { get; set; }
		public int Auto { get; set; }
		public bool IsEmpty { get { return Clr.IsEmpty && String.IsNullOrEmpty(Theme) && Auto < 0 && Index < 0; } }

		public ExcelColor()
		{
			Clr = Color.Empty;
			Theme = "";
			Tint = "";
			Index = -1;
			Name = "color";
			Auto = -1;
		}

		public ExcelColor(Color clr) : this()
		{
			Clr = clr;
		}

		public ExcelColor(string them, string tint) : this()
		{
			Theme = them;
			Tint = tint;
		}

		public ExcelColor(int indx) : this()
		{
			Index = indx;
		}

		public void Update(ExcelColor ec)
		{
			if (Theme != ec.Theme)
				Theme = ec.Theme;

			if (Tint != ec.Tint)
				Tint = ec.Tint;

			if (Clr != ec.Clr)
				Clr = ec.Clr;

			if (Index != ec.Index)
				Index = ec.Index;
		}

		internal void Load(XElement element)
		{
			var atrs = XDocumentTools.ParseAtrs(element);
			Name = element.Name.LocalName;

			if (atrs.ContainsKey("indexed"))
			{
				Index = atrs["indexed"].ToInt();
			}

			if (atrs.ContainsKey("theme"))
			{
				Theme = atrs["theme"];
			}
			if (atrs.ContainsKey("tint"))
			{
				Tint = atrs["tint"];
			}

			if (atrs.ContainsKey("rgb"))
			{
				Clr = atrs["rgb"].ToColor();
			}

			if (atrs.ContainsKey("auto"))
			{
				Auto = atrs["auto"].ToInt();
			}
		}

		public void Save(XmlWriterWrapper doc)
		{
			if (IsEmpty)
				return;

			doc.OpenElement(Name);

			if (!Clr.IsEmpty)
			{
				doc.AddAtribute("rgb", Clr.ToARGB());
			}
			else if (!String.IsNullOrEmpty(Theme))
			{
				doc.AddAtribute("theme", Theme);
				if (!String.IsNullOrEmpty(Tint))
				{
					doc.AddAtribute("tint", Tint);
				}
			}
			else if (Index >= 0)
			{
				doc.AddAtribute("indexed", Index);
			}
			else if (Auto >= 0)
			{
				doc.AddAtribute("auto", Auto);
			}
			doc.CloseElement();
		}



		public ExcelColor Clone()
		{
			return new ExcelColor()
			{
				Auto = this.Auto,
				Clr = this.Clr,
				Index = this.Index,
				Theme = this.Theme,
				Name = this.Name,
				Tint = this.Tint
			};
		}
	}
}
