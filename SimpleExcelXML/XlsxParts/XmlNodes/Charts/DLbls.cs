﻿using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Charts
{
	internal class DLbls: XmlPropBase
	{	
		public static DLbls Default { get { return new DLbls(); } }

		public int showLegendKey { get; set; } = 0;
		public int showVal { get; set; } = 0;
		public int showCatName { get; set; } = 0;
		public int showSerName { get; set; } = 0;
		public int showPercent { get; set; } = 0;
		public int showBubbleSize { get; set; } = 0;
		public string prefix { get; set; } = "c";

		public DLbls() : base()
		{

		}

		public DLbls(XElement xDlbls) : base(xDlbls)
		{
		}

		public override void Save(XmlWriterWrapper doc)
		{
			doc.OpenPrefElement("dLbls", prefix)
				.OpenPrefElement(nameof(showLegendKey)).AddAtribute("val", showLegendKey.ToString()).CloseElement()
				.OpenPrefElement(nameof(showVal)).AddAtribute("val", showVal.ToString()).CloseElement()
				.OpenPrefElement(nameof(showCatName)).AddAtribute("val", showCatName.ToString()).CloseElement()
				.OpenPrefElement(nameof(showSerName)).AddAtribute("val", showSerName.ToString()).CloseElement()
				.OpenPrefElement(nameof(showPercent)).AddAtribute("val", showPercent.ToString()).CloseElement()
				.OpenPrefElement(nameof(showBubbleSize)).AddAtribute("val", showBubbleSize.ToString()).CloseElement()

				.CloseElement();
		}

		public override void TryLoad(XElement root)
		{
			var atrs = XDocumentTools.ParseAtrs(root);

			showLegendKey = ReadAtrValue(atrs, $"{nameof(showLegendKey)}@val").ToInt();
			showVal = ReadAtrValue(atrs, $"{nameof(showVal)}@val").ToInt();
			showCatName = ReadAtrValue(atrs, $"{nameof(showCatName)}@val").ToInt();
			showSerName = ReadAtrValue(atrs, $"{nameof(showSerName)}@val").ToInt();
			showPercent = ReadAtrValue(atrs, $"{nameof(showPercent)}@val").ToInt();
			showBubbleSize = ReadAtrValue(atrs, $"{nameof(showBubbleSize)}@val").ToInt();		
		}
	}
}
