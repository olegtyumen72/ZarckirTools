﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTools.Views
{
    /// <summary>
    /// Interaction logic for FileChooser.xaml
    /// </summary>
    public partial class FileChooser : UserControl
    {
        public FileChooser()
        {
            InitializeComponent();
        }

		public static DependencyProperty FilePathDp;
		public static DependencyProperty TitleDp;
		public static RoutedEvent ChooseClickRe;
		System.Windows.Forms.OpenFileDialog _ofd = new System.Windows.Forms.OpenFileDialog();

		static FileChooser()
		{
			FilePathDp = DependencyProperty.Register(nameof(FilePath), typeof(string), typeof(FileChooser), new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnPathChange)));
			ChooseClickRe = EventManager.RegisterRoutedEvent(nameof(FileChoosen), RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<string>), typeof(FileChooser));
			TitleDp = DependencyProperty.Register(nameof(Title), typeof(string), typeof(FileChooser), new FrameworkPropertyMetadata("Файл", new PropertyChangedCallback(OnTitleChange)));
		}

		public event RoutedPropertyChangedEventHandler<string> FileChoosen
		{
			add => AddHandler(ChooseClickRe, value);
			remove => RemoveHandler(ChooseClickRe, value);
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			if (_ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				var args = new RoutedPropertyChangedEventArgs<string>(FilePath, _ofd.FileName, ChooseClickRe);						
				RaiseEvent(args);
				FilePath = _ofd.FileName;
			}
		}

		public string Title
		{
			get => (string)GetValue(TitleDp);
			set => SetValue(TitleDp, value);
		}

		public string FilePath
		{
			get => (string)GetValue(FilePathDp);
			set => SetValue(FilePathDp, value);
		}

		static void OnTitleChange(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			(sender as FileChooser).Title = (string)e.NewValue;
		}

		static void OnPathChange(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			(sender as FileChooser).FilePath = (string)e.NewValue;
		}

		public void Clear()
		{
			FilePath = "";
		}
	}
}
