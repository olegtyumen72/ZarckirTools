﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.DataTests
{
	[TestClass]
	public class ListVsDicTests
	{
		[TestMethod]
		public void FindTest()
		{
			var logName = $"{nameof(ListVsDicTests)}_{nameof(FindTest)}_log.txt";
			var list = new List<int>();
			var dic = new Dictionary<int, int>();

			for(int i=0; i < 100000; i++)
			{
				list.Add(i);
				dic.Add(i, i);
			}
			list[53212] = 3;
			dic[53212] = 3;

			if (!File.Exists(logName)){
				File.Create(logName);
			}


			var sw = Stopwatch.StartNew();
			var res = list.Find((v) => v == 3);
			sw.Stop();
			File.AppendAllText(logName, Environment.NewLine + $"List find in {sw.Elapsed.TotalSeconds.ToString("0.000###")}" + Environment.NewLine);

			sw.Restart();
			var dicres = dic.ContainsValue(1);
			sw.Stop();
			File.AppendAllText(logName, $"List find in {sw.Elapsed.TotalSeconds.ToString("0.000###")}" + Environment.NewLine);

		}
	}
}
