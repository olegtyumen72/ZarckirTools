﻿using DbConnector.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbConnector.Tools
{
	/// <summary>
	/// Класс для получения части данных из бд
	/// Использует кеширование
	/// </summary>
	/// <typeparam name="T">Тип данных, используемых в ДБ конекторе</typeparam>
	public class PartialSqlData<T> : ISqlPartData<T>, IEnumerable<T>
	{
		public int Count { get; private set; }
		public int Offset { get; private set; }
		public bool IsLazy { get; private set; }
		public int CurPos { get; private set; }
		public int MaxCount { get; private set; }
		///// <summary>
		///// дурацкий костыль: если по дефолту нужен хотя бы 1 пак таблицы, даже пустой, 
		///// то обращаться к этому полю. В нём хранится первое, что придёт из БД.
		///// </summary>
		//public T Template { get; private set; }
		private Dictionary<string, T> _cache;
		public List<T> UsedData
		{
			get
			{
				var res = new List<T>();
				foreach (var el in _cache)
				{
					res.Add(el.Value);
				}

				return res;
			}
		}

		Func<int, int, T> _selectData;
		Func<T, bool> _isEmpty;

		public PartialSqlData(int count, int offset, Func<int, int, T> select, Func<T, bool> empty, bool isLazy)
		{
			_selectData = select;
			_isEmpty = empty;
			Count = count;
			Offset = offset;
			CurPos = 0 - Count;
			_cache = new Dictionary<string, T>();
		}

		public T Next()
		{
			CurPos += Count;
			return CurrentData();
		}

		public T Prev()
		{
			if (CurPos < Count) return default(T);
			CurPos -= Count;
			return CurrentData();
		}

		public T Current()
		{
			return CurrentData();
		}

		private T CurrentData()
		{
			if (!CacheHas(Count, CurPos))
			{
				var data = GetData(Count, CurPos);
				if (_isEmpty(data))
				{
					CacheAdd(Count, CurPos, default(T));
				}
				else
				{
					CacheAdd(Count, CurPos, data);
				}
			}

			return CacheSelector(Count, CurPos);
		}

		public void SetPosition(int posInRange)
		{
			while (posInRange > CurPos)
			{
				CurPos += Count;
			}

			CurPos -= Count;
		}

		public void ResetPosition()
		{
			CurPos = Offset;
		}

		public bool Read()
		{
			return !(CurrentData() == null);
		}

		public T FirstOrDefaultt(Func<T, bool> func)
		{
			foreach (var part in this)
			{
				if (func(part))
				{
					return part;
				}
			}

			return default(T);
		}

		private bool CacheHas(int count, int offset)
		{
			return _cache.ContainsKey($"{count}{offset}");
		}

		private T CacheSelector(int cnt, int off)
		{
			return _cache[$"{cnt}{off}"];
		}

		private T GetData(int cnt, int off)
		{
			return _selectData(cnt, off);
		}

		private void CacheAdd(int cnt, int off, T data)
		{
			_cache.Add($"{cnt}{off}", data);
		}

		public IEnumerator<T> GetEnumerator()
		{
			var posTmp = CurPos;
			CurPos = 0;
			while (Read())
			{
				yield return CurrentData();
				Next();
			}
			CurPos = posTmp;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public T FirstRowOrDefaultt(Func<T, T> func, Func<T, bool> check)
		{
			var reses = new List<T>();

			////parallel
			//Parallel.ForEach(this, (v, state) =>
			//{
			//	if (!state.IsStopped)
			//	{
			//		var res = func(v);
			//		if (res != null && check(res))
			//		{
			//			lock (res)
			//			{
			//				reses.Add(res);
			//			}
			//			state.Stop();
			//		}
			//	}
			//});

			//one thread
			foreach (var part in this)
			{
				var res = func(part);
				if (res != null && check(res))
				{
					reses.Add(res);
					break;
				}
			}

			if (reses.Count <= 0)
				return default(T);
			else
				return reses.First();
		}

		public T this[int cnt, int off]
		{
			get
			{
				if (!CacheHas(cnt, off))
				{
					CacheAdd(cnt, off, GetData(cnt, off));
				}

				return CacheSelector(cnt, off);
			}
		}

		//public int IndexOf(string col, Func<T, int> func)
		//{
		//	var tmp = Current();
		//	var res = func(tmp);
		//	if (res != -1) return tmp[col][res];
		//}

	}
}
