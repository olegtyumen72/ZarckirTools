﻿using SimpleCore.Objs;
using SimpleCore.Parts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCsv.Parts
{
	public enum Separators { Dot, Comma, Tab, Colon, Semicolon }

	public class ReportBook : List<ReportSheet>, ISimpleConvert<IReportBook> //List<IReportSheet>, IReportBook //(IList<IReportSheet>)
	{
		private string _path;
		private List<ReportSheet> _sheets;
		private Separators _sep;
		private string _strSep;
		

		public List<ReportSheet> Sheets
		{
			get
			{
				return _sheets;
			}

			set
			{
				_sheets = value;
			}
		}
		public Separators Separator
		{
			get
			{
				return _sep;
			}

			set
			{
				_sep = value;
				UpdateStrSep();
				UpdateExtentious();
			}
		}
		internal string StrSeparator
		{
			set { _strSep = value; }
			get { return _strSep; }
		}
		internal string Extentious
		{
			get;
			set;
		}

		public ReportBook()
		{
			_sheets = new List<ReportSheet>();
			Separator = Separators.Semicolon;
		}
		public ReportBook(string path) : this()
		{
			Path = path;
		}

		public ReportBook(IReportBook rb) : this(rb.Path)
		{
			foreach (var el in rb)
			{
				Add(new ReportSheet(el.Name, el));
			}
		}

		private void UpdateStrSep()
		{
			switch (Separator)
			{
				case Separators.Colon:
					{
						StrSeparator = ":";
						break;
					}
				case Separators.Comma:
					{
						StrSeparator = ",";
						break;
					}
				case Separators.Dot:
					{
						StrSeparator = ".";
						break;
					}
				case Separators.Semicolon:
					{
						StrSeparator = ";";
						break;
					}
				case Separators.Tab:
					{
						StrSeparator = "\t";
						break;
					}
				default:
					{
						throw new Exception("Unknow separator");
					}
			}
		}
		private void UpdateExtentious()
		{
			switch (Separator)
			{
				case Separators.Colon:
					{
						Extentious = ".csv";
						break;
					}
				case Separators.Comma:
					{
						Extentious = ".csv";
						break;
					}
				case Separators.Dot:
					{
						Extentious = ".dsv";
						break;
					}
				case Separators.Semicolon:
					{
						Extentious = ".ssv";
						break;
					}
				case Separators.Tab:
					{
						Extentious = ".tsv";
						break;
					}
				default:
					{
						throw new Exception("Unknow separator");
					}
			}
		}

		public IReportBook Convert()
		{
			var res = new ReportBookObj(Path);

			foreach(var el in this)
			{
				res.Add(el.Convert());
			}

			return res;
		}


		#region Interface
		
		public string Path
		{
			get
			{
				return _path;

			}

			set
			{
				_path = value;
			}
		}


		#endregion

	}
}
