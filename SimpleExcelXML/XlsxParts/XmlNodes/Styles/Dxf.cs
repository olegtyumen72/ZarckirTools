﻿using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Styles
{
	internal class Dxf
	{		
		public FontNode Font { get; set; }
		public BorderNode Border { get; set; }
		public FillNode Fill { get; set; }
		public NumFmtNode NumFmt { get; set; }

		public Dxf()
		{

		}


		public void Save(XmlWriterWrapper doc)
		{
			doc.OpenElement("dxf");

			Font?.Save(doc);
			Fill?.Save(doc);
			Border?.Save(doc);
			NumFmt?.Save(doc);

			doc.CloseElement();
		}

	}
}
