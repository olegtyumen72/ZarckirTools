﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleCore;
using SimpleCore.Parts;
using SimpleCore.Templates;
using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Interfaces;
using SimpleExcelXML.XlsxParts;

namespace SimpleExcelXML
{
	/// <summary>
	/// Стартовая точка для выгрузки\загрузки экселя
	/// </summary>
	public class SimpleExcel : SimpleTmpl<ReportBook, ReportSheet, ReportLine>, IDisposable
	{
		//private string dir { get; set; }
		/// <summary>
		/// Директория для временных файлов
		/// </summary>
		private string tmpDir { get; set; }
		Action<string> statSetTxt { get; set; }
		Action statAddProg { get; set; }


		//public delegate void ErrorDelegate();
		//private event ErrorDelegate _onError;
		//public event ErrorDelegate OnError
		//{
		//	add { _onError += value; }
		//	remove { _onError -= value; }
		//}

		public SimpleExcel()
		{
			//_onError += () => Dispose();
			System.AppDomain.CurrentDomain.UnhandledException += (v, c) => Dispose();
		}

		public override void Dispose()
		{
			//if (Directory.Exists(dir))
			//{
			//	Directory.Delete(dir, true);
			//}
			if (String.IsNullOrEmpty(tmpDir)) return;

			if (Directory.Exists(tmpDir))
			{
				Directory.Delete(tmpDir, true);
			}
		}


		public override IReportBook Load(string path)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Реализованная загрузка. Запускает метод инициализации данных
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public ReportBook LoadLocal(string path)
		{
			if (
				!File.Exists(path) &&
				!Path.GetExtension(path).ToLower().Contains("xl")
				)
			{
				return null;
			}


			return InitReportBook(path);
		}       

        private ReportBook TryGetMainType(object obj)
		{
			return obj is IReportBook ? new ReportBook((IReportBook)obj) : obj is ReportBook ? (ReportBook)obj : null;
		}

		public override void Save(object obj)
		{
			var rb = TryGetMainType(obj);
			if (rb == null)
			{
				throw new Exception("Miss report book");
			}

			Save(rb);
		}
       
        public override void Save(object obj, string path)
		{
			var rb = TryGetMainType(obj);
			if (rb == null)
			{
				throw new Exception("Miss report book");
			}

			rb.Path = path;
			Save(rb);
		}

		public override void SaveAs(object obj, string path = "")
		{
			SaveFileDialog dlg = new SaveFileDialog()
			{
				AddExtension = true,
				Filter = "Excel 2007-2013|*.xlsx;*.xlsm",
				Title = "Saving"
			};
			var cancel = false;
			while (!cancel)
			{
				if (String.IsNullOrEmpty(path))
				{
					if (dlg.ShowDialog() == DialogResult.OK)
					{
						path = dlg.FileName;
					}
					else
					{
						cancel = true;
					}
				}

				try
				{
					Save(obj, path);
					cancel = true;
				}
				catch (Exception ex)
				{
					path = "";
					string mes = "Error saving data to excel file. Choose another file or press 'Cancel' to cancel saving.\r\nError details: " + ex.Message;
					if (MessageBox.Show(mes, "Save file", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
					{
						cancel = true;
					}
				}
			}
		}

		public void SaveAs(object obj, string path = "", Action<string> statSetTxt = null, Action statAddProg = null)
		{
			this.statSetTxt = statSetTxt;
			this.statAddProg = statAddProg;

			SaveFileDialog dlg = new SaveFileDialog()
			{
				AddExtension = true,
				Filter = "Excel 2007-2013|*.xlsx;*.xlsm",
				Title = "Saving"
			};
			var cancel = false;
			while (!cancel)
			{
				if (String.IsNullOrEmpty(path))
				{
					if (dlg.ShowDialog() == DialogResult.OK)
					{
						path = dlg.FileName;
					}
					else
					{
						cancel = true;
					}
				}

				try
				{
					Save(obj, path);
					cancel = true;
				}
				catch (Exception ex)
				{
					path = "";
					string mes = "Error saving data to excel file. Choose another file or press 'Cancel' to cancel saving.\r\nError details: " + ex.Message;
					if (MessageBox.Show(mes, "Save file", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
					{
						cancel = true;
					}
				}
			}
		}

		public override Task SaveAsync(object obj)
		{
			throw new NotImplementedException();
		}

		public override Task SaveAsync(object obj, string path)
		{
			throw new NotImplementedException();
		}

		public ReportSheet UpdateReportSheetWithTranslater(ReportSheet rs, ITranslater tr = null)
		{
			var new_rs = new ReportSheet(rs.Convert());

			try
			{
				//проверяем нужно ли нам переводить
				if (tr != null)
				{
					new_rs.Name = tr.TranslateWithUpdatingText(new_rs.Name);

					foreach (var cell in rs.LocalCellsTr)
					{
						//номер строки и столбца у первой ячейки должны быть > 0
						//номер строки и столбца начинается с 1, поэтому делаем поправку на это
						if (cell.cell1_Col > 0 && cell.cell2_Col >= cell.cell1_Col && cell.cell1_Row > 0 && cell.cell2_Row >= cell.cell1_Row)
						{
							//номер строки начала и конца диапозона
							int i_start = cell.cell1_Row - 1;
							int i_end = cell.cell2_Row;
							//номер столбца
							int j_start = cell.cell1_Col - 1;
							int j_end = cell.cell2_Col;

							for (int i = i_start; i < i_end; i++)
							{
								//проверим, чтобы мы не вышли за пределы конца диапазона
								if (i > rs.Count - 1)
								{
									break;
								}

								for (int j = j_start; j < j_end; j++)
								{
									//проверим, чтобы мы не вышли за пределы конца диапазона
									if (j > rs[i].Count() - 1)
									{
										break;
									}

									new_rs[i][j] = tr.TranslateWithUpdatingText(new_rs[i][j].ToString());
								}
							}
						}
					}

					//ищем все графики
					foreach (ReportChart chart in new_rs.LocalGrapth)
					{
						//переводи подписи
						chart.Title = tr.TranslateWithUpdatingText(chart.Title);
						chart.TitleX = tr.TranslateWithUpdatingText(chart.TitleX);
						chart.TitleY = tr.TranslateWithUpdatingText(chart.TitleY);

						//если легенда отображается, переводим подписи
						if (chart.ShowLegend)
						{
							foreach (ReportChartItem item in chart.Items)
							{
								item.Title = tr.TranslateWithUpdatingText(item.Title);
							}
						}
					}
				}
			}
			catch { }

			return new_rs;
		}

		bool SaveFileAct(Action act, string errorMsg)
		{
			bool isOk = true;
			while (isOk)
			{
				try
				{
					act();
					break;
				}
				catch (Exception e)
				{
					if (MessageBox.Show(errorMsg, "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Cancel)
					{
						isOk = false;
						break;
					}
				}
			}

			return isOk;
		}

		public override IReportSheet UpdateReportSheetWithTranslater(IReportSheet rs, ITranslater tr = null)
		{
			throw new NotImplementedException();
		}

		//Логики разбора экселя
		#region Excel logic

		/// <summary>
		/// Загрузка экселя
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		private ReportBook InitReportBook(string path)
		{
#if DEBUG
			tmpDir = Path.Combine(Path.GetTempPath(), "LoadDebuge");
#else
			tmpDir = RandomPath();
#endif
			if (Directory.Exists(tmpDir))
				Directory.Delete(tmpDir, true);

			if (!SaveFileAct(
				() => ZipFile.ExtractToDirectory(path, tmpDir),
				$"{Path.GetFileName(path)} file busy another process. Pleace close all excel program and try again"))
			{
				return null;
			}

			var rb = new ReportBook(path);
			var wb = new XmlWorkBook(rb, tmpDir);
			wb.TryLoad();
			var wbRels = new XmlRels(tmpDir, wb.File + ".rels", "xl\\_rels");
			wbRels.TryLoad();

			//проинициализировать структуру и раздать id - локал
			var shared = new XmlSharedString(tmpDir);
			shared.TryLoad();
			var style = new XmlStyle(tmpDir, true);
			style.TryLoad();

			if (wbRels.TypeToPath.ContainsKey(XmlRels.RelType.Worksheet))
			{
				foreach (var el in wbRels.TypeToPath[XmlRels.RelType.Worksheet])
				{
					var dataRs = rb.FirstOrDefault((v) => v.rId == el.RelId);
					var rsFile = Path.GetFileName(el.RelPath);
					var rs = new XmlWorkSheet(dataRs, rsFile, shared, style, tmpDir);

					var sheetRels = new XmlRels(tmpDir, rs.File + ".rels", "xl\\worksheets\\_rels");
					sheetRels.TryLoad();

					if (sheetRels.TypeToPath.ContainsKey(XmlRels.RelType.Drawing))
					{
						foreach (var data in sheetRels.TypeToPath[XmlRels.RelType.Drawing])
						{
							var drawName = Path.GetFileName(data.RelPath);

							var drawRel = new XmlRels(tmpDir, drawName + ".rels", "xl\\drawings\\_rels");
							drawRel.TryLoad();

							var draw = new XmlDrawing(tmpDir, drawName, drawRel);
							draw.Id = data.RelId.ToString();
							draw.TryLoad();
							sheetRels.PathToType.Add(draw, XmlRels.RelType.Drawing);

							foreach (var chData in drawRel.TypeToPath[XmlRels.RelType.Chart])
							{
								var chartName = Path.GetFileName(chData.RelPath);

								var chartRel = new XmlRels(tmpDir, chartName + ".rels", "xl\\charts\\_rels");
								chartRel.TryLoad();

								var chart = new XmlChart(tmpDir, chartName, chartRel);
								chart.Id = chData.RelId.ToString();
								chart.TryLoad();

								drawRel.PathToType.Add(chart, XmlRels.RelType.Chart);
							}

						}
					}

					rs.TryLoad(sheetRels);
					//rb.Add(rs.ReportSheet);
				}
			}

			if (wbRels.TypeToPath.ContainsKey(XmlRels.RelType.Chartsheet))
			{
				foreach (var el in wbRels.TypeToPath[XmlRels.RelType.Chartsheet])
				{
					var dataRs = rb.FirstOrDefault((v) => v.rId == el.RelId);
					var rsFile = Path.GetFileName(el.RelPath);
					var rs = new XmlChartSheet(dataRs, rsFile, tmpDir);

					var sheetRels = new XmlRels(tmpDir, rs.File + ".rels", "xl\\worksheets\\_rels");
					sheetRels.TryLoad();

					if (sheetRels.TypeToPath.ContainsKey(XmlRels.RelType.Drawing))
					{
						foreach (var data in sheetRels.TypeToPath[XmlRels.RelType.Drawing])
						{
							var drawName = Path.GetFileName(data.RelPath);

							var drawRel = new XmlRels(tmpDir, drawName + ".rels", "xl\\drawings\\_rels");
							drawRel.TryLoad();

							var draw = new XmlDrawing(tmpDir, drawName, drawRel);
							draw.Id = data.RelId.ToString();
							draw.TryLoad();
							sheetRels.PathToType.Add(draw, XmlRels.RelType.Drawing);

							foreach (var chData in drawRel.TypeToPath[XmlRels.RelType.Chart])
							{
								var chartName = Path.GetFileName(chData.RelPath);

								var chartRel = new XmlRels(tmpDir, chartName + ".rels", "xl\\charts\\_rels");
								chartRel.TryLoad();

								var chart = new XmlChart(tmpDir, chartName, chartRel);
								chart.Id = chData.RelId.ToString();
								chart.TryLoad();

								drawRel.PathToType.Add(chart, XmlRels.RelType.Chart);
							}

						}
					}

					rs.TryLoad(sheetRels);
				}
			}

			//ReportBook 
			//	ReportSheet
			//		Grapth
			//			vals + Axes + 
			//		ReportLine
			//			val or sharedString



			return rb;
		}


		/// <summary>
		/// Сохранение экселя
		/// </summary>
		/// <param name="rb"></param>
		private void Save(ReportBook rb)
		{
			int idXl = 1;
			int idCXl = 1;
			int idChart = 1;
			int idDrawing = 1;
			int idDrawFile = 1;

			var files = new List<IXmlFile>();
			var rbDir = Path.GetDirectoryName(rb.Path) + "\\";
			tmpDir = RandomPath();

			//сохранить структуру		
			if (!Directory.Exists(tmpDir))
			{
				Directory.CreateDirectory(tmpDir);
			}


			//проинициализировать структуру и раздать id - глобал
			var wb = new XmlWorkBook(rb, tmpDir);
			var core = new XmlCore(Environment.UserName, Environment.UserName, DateTime.Now, DateTime.Now, tmpDir);
			var app = new XmlApp(rb, tmpDir);
			var relsGlob = new XmlRels(new Dictionary<Interfaces.IXmlVisibleFile, XmlRels.RelType>()
			{
				{wb, XmlRels.RelType.Workbook},
				{core, XmlRels.RelType.Core},
				{app, XmlRels.RelType.App},
			}, tmpDir, true);

			files.Add(wb);
			files.Add(core);
			files.Add(app);
			files.Add(relsGlob);

			//проинициализировать структуру и раздать id - локал
			var shared = new XmlSharedString(tmpDir);
			var style = new XmlStyle(tmpDir);
			style.AddRb(rb);
			var rels = new XmlRels(null, tmpDir, false);

			rels.Add(style, XmlRels.RelType.Styles);
			rels.Add(shared, XmlRels.RelType.SharedStrings);

			var ct = new XmlContentType(new Dictionary<string, XmlContentType.Parts>()
			{
				{app.RelativeToFile(), XmlContentType.Parts.App },
				{core.RelativeToFile(), XmlContentType.Parts.Core },
				{shared.RelativeToFile(), XmlContentType.Parts.SharedStr },
				{style.RelativeToFile(), XmlContentType.Parts.Styles },
				//{theme.RelativeToFile(), XmlContentType.Parts.Theme },
				{wb.RelativeToFile(), XmlContentType.Parts.Workbook },

			}, tmpDir);

			files.Add(shared);
			files.Add(style);
			files.Add(rels);
			files.Add(ct);

			var selectedSheet = true;
			//раздать id репортшитам
			foreach (var el in rb)
			{

				if (statSetTxt != null)
				{
					statSetTxt.Invoke(el.Name);
				}

				IXmlVisibleSheet sheet = null;
				//if (el.Count <= 0 && el.LocalGrapth.Count > 0)
				//{
				//	el.Id = idCXl++;
				//	sheet = new XmlChartSheet(el, tmpDir);

				//	rels.Add(sheet, XmlRels.RelType.Chartsheet, el);
				//	ct.Add(XmlContentType.Parts.Chartsheet, sheet.RelativeToFile());
				//}
				//else
				{
					el.Id = idXl++;
					sheet = new XmlWorkSheet(el, shared, style, selectedSheet, tmpDir);
					selectedSheet = false;

					rels.Add(sheet, XmlRels.RelType.Worksheet, el);
					ct.Add(XmlContentType.Parts.Worksheets, sheet.RelativeToFile());
				}								

				if (el.LocalGrapth.Count > 0)
				{
					var chartRels = new XmlRels(tmpDir, $"drawing{idDrawFile++}.xml.rels", "xl\\drawings\\_rels", null);


					var relsSheet = new XmlRels(tmpDir, sheet.File + ".rels", "xl\\worksheets\\_rels");
					var draw = new XmlDrawing(tmpDir, idDrawing++, chartRels);

					relsSheet.Add(draw, XmlRels.RelType.Drawing);
					sheet.AddGrapth(draw);

					ct.Add(XmlContentType.Parts.Drawing, draw.ContTypePath());

					foreach (var grapth in el.LocalGrapth)
					{

						var chart = new XmlChart(tmpDir, grapth, el.Name, idChart++);
						chartRels.Add(chart, XmlRels.RelType.Chart);
						draw.Add(chart, grapth);
						
						ct.Add(XmlContentType.Parts.Chart, chart.ContTypeToFile());

						chart.Save();
						chart = null;
					}

					chartRels.Save();
					chartRels = null;
					draw.Save();
					draw = null;
					relsSheet.Save();
					relsSheet = null;
				}

				sheet.Save();
				sheet = null;

				if (statAddProg != null)
				{
					statAddProg.Invoke();
				}
			}
			
			foreach (var file in files)
			{
				file.Save();
			}

			try
			{
				if (!Directory.Exists(Path.GetDirectoryName(rb.Path)))
				{
					Directory.CreateDirectory(Path.GetDirectoryName(rb.Path));
				}
			}
			catch
			{

			}

			var ext = Path.GetExtension(rb.Path);
			if (String.IsNullOrEmpty(ext))
			{
				rb.Path = Path.Combine(Path.GetDirectoryName(rb.Path), Path.GetFileName(rb.Path)) + ".xlsx";
			}


			if (File.Exists(rb.Path))
			{
				if (!SaveFileAct(
				() => File.Delete(rb.Path),
				$"{Path.GetFileName(rb.Path)} file busy another process. Pleace close all excel program and try again")
					)
				{
					Directory.Delete(tmpDir, true);
					return;
				}
			}

			//заархивировать структуру
			SaveFileAct(
				() => ZipFile.CreateFromDirectory(tmpDir, rb.Path),
				$"{Path.GetFileName(rb.Path)} file busy another process. Pleace close all excel program and try again"
			);
			Directory.Delete(tmpDir, true);
		}

		#endregion

		#region Tools

		private string RandomPath()
		{
			return Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
		}

		private void SplitPath(string path, String dir, String name)
		{
			dir = Path.GetDirectoryName(path);
			name = Path.GetFileNameWithoutExtension(path);
		}

		private void CheckFile(string dir, string name)
		{
			if (!Directory.Exists(dir))
			{
				Directory.CreateDirectory(dir);
			}

			var dirXlsx = Path.Combine(dir, name);
			if (!Directory.Exists(dirXlsx))
			{
				Directory.CreateDirectory(dirXlsx);
			}
		}

		#endregion

		public static ReportBook LoadStatic(string path)
		{
			using (var doc = new SimpleExcel())
			{
				return doc.LoadLocal(path);
			}
		}
		public static void SaveStatic(object obj, string path)
		{
			using (var doc = new SimpleExcel())
			{
				doc.Save(obj, path);
			}
		}
	}
}
