﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools
{
	public static class DbTools
	{
		public static string AddMySqlBracets(string col)
		{
			if (col.Contains("`")) return col;
			return $"`{col}`";
		}

		public static string RemoveMySqlBracets(string col)
		{
			return col.Replace("`", "");
		}

		public static string AddMsSqlBrackets(string col)
		{
			if (!col.Contains("]")) col += "]";
			if (!col.Contains("[")) col = col.Insert(0, "[");
			return col;
		}

		public static string RemoveMsSqlBracets(string col)
		{
			col = col.Replace("[", "");
			col = col.Replace("]", "");
			return col;
		}

		public static void ParseSqliteUnknow(object val)
		{
			var t = val.GetType();
		}
	}

}
