﻿using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Charts
{
	/// <summary>
	/// Енумы на все случаи жизни
	/// </summary>

	public enum LegendPos { b, l, r, t, tr }

	public enum TickPos { b, nextTo, t, none }

	public enum AxePos { b, t, l, r }

	public enum ScatterStyle { lineMarker, smoothMarker }

	public enum VertOverflow { ellipsis, overflow, clip }

	public enum Grouping { standard, clustered }

	/// <summary>
	/// default : horz
	/// </summary>
	public enum Vert { horz, wordArtVert, wordArtVertRtl }

	public enum Wrap { square, none }

	/// <summary>
	/// defult ctr
	/// ctrc = ctr + anchorCtr = 1
	/// </summary>
	public enum Anchor { ctr, t, b, ctrc, tc, bc }

	public enum MarkerSymbol { none, circle, square, diamond, triangle, x, star, dot, dash, plus }

	public enum Orientation { minMax, maxMin }

	public enum TickMark { _in, _out, _cross, _none }

	public enum Underline { none, sng, dbl, miss }

	public enum Aligment { center, left, right, none }
}
