﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.Tools;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Interfaces
{
	/// <summary>
	/// Общий класс с упором на имя
	/// </summary>
	internal abstract class XmlNameBase: XmlPropBase
	{
		public virtual string name { get; set; }

		public XmlNameBase()
		{

		}

		public XmlNameBase(string name, XElement root)
		{
			this.name = name;
			TryLoad(root);
		}
	}

	/// <summary>
	/// Все классы-проперти должны быть от него. Отвечает за работу с атрибутами, структуру для сохранения и выгрузки. Общий класс
	/// </summary>
	internal abstract class XmlPropBase
	{
		public XmlPropBase()
		{

		}

		public XmlPropBase(XElement root)
		{
			TryLoad(root);
		}

		public abstract void TryLoad(XElement root);

		protected virtual string ReadAtrValue(Dictionary<string, string> atrs, string atr)
		{
			if (atrs.ContainsKey(atr))
				return atrs[atr];

			return null;
		}

		protected virtual bool IsWritable(int val)
		{
			return !(val == -1);
		}

		protected virtual bool IsWritable(string val)
		{
			return !String.IsNullOrEmpty(val);
		}

		protected virtual bool IsWritable(double val)
		{
			return !(val == double.NaN);
		}

		protected virtual bool IsWritable(float val)
		{
			return !(val == float.NaN);
		}

		

		protected virtual void WriteAtrValue(XmlWriterWrapper doc, string val, string atr)
		{
			if (IsWritable(val))
				doc.AddAtribute(atr, val);
		}

		protected virtual void WriteAtrValue(XmlWriterWrapper doc, int val, string atr)
		{
			if (IsWritable(val))
				doc.AddAtribute(atr, val);
		}

		protected virtual void WriteAtrValue(XmlWriterWrapper doc, float val, string atr)
		{
			if (IsWritable(val))
				doc.AddAtribute(atr, val);
		}

		protected virtual void WriteAtrValue(XmlWriterWrapper doc, double val, string atr)
		{
			if (IsWritable(val))
				doc.AddAtribute(atr, val);
		}

		protected virtual Dictionary<string, string> ReadAtrs(XElement element)
		{
			return XDocumentTools.ParseAtrs(element);
		}

		public abstract void Save(XmlWriterWrapper doc);
	}
}
