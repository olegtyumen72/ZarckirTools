﻿using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore
{
	public interface ISimpleCore<ReportBook, ReportSheet, ReportLine> : IDisposable		
		where ReportBook: ISimpleConvert<IReportBook>
		where ReportSheet: ISimpleConvert<IReportSheet>
		where ReportLine: ISimpleConvert<IReportLine>
	{

		event Action<int> UpdateMaxState;
		event Action<int> UpdateCurState;
		event Action<double> UpdateProcState;
		event Action WorkEnd;

		SimpleConverter<IReportBook, ReportBook> RbConv { get; }
		SimpleConverter<IReportSheet, ReportSheet> RsConv { get; }
		SimpleConverter<IReportLine, ReportLine> RlConv { get; }

		void Save(object obj);

		void Save(object obj, string path);

		void SaveAs(object obj, string path = "");

		Task SaveAsync(object obj);

		Task SaveAsync(object obj, string path);

		IReportBook Load(string path);

		IReportSheet UpdateReportSheetWithTranslater(IReportSheet rs, ITranslater tr = null);
	}
}
