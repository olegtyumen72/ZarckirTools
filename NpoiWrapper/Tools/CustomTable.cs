﻿using NPOI.XWPF.UserModel;
using NpoiWrapper.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpoiWrapper.Tools
{
	/// <summary>
	/// Phage - таблица растягивается на всю страницу, вне зависимости от количества колонок
	/// ColumnWidth - для всех колонок устанавливается единая ширина
	/// Content - ширина зависит от контента в колонках
	/// </summary>
	public enum AutoFit { Phage, ColumnWidth, Content }

	public static class CustomTableTools
	{
		public static List<CustomTable> CreatePartialTable(this NpoiWord word, int colByPart, int rowByPart, AutoFit fitByPart)
		{


			return null;
		}

	}

	public class CustomTable: IRunableCollection
	{
		internal event Func<double> PhageWidth;
		internal event Func<double> PhageHeight;

		public Func<int, int, string> DataAt;
		public string TableName;
		public string PartsName;
		public Paragraph StyleHeader;
		public Paragraph StyleBody;
		public Paragraph StyleName;
		public AutoFit Fit;
		public List<TablePart> parts;
		public List<ColBase> FormatsCols;
		public ulong ColWidth;
		public bool IsSeparated;
		private bool isColWidthInit
		{
			get
			{
				return ColWidth > 0;
			}
		}

		public CustomTable(string tableName, string partsName, Paragraph nameStyle, Paragraph headerStyle, Paragraph bodyStyle, Func<int, int, string> dataProvider, List<TablePart> tableParts, bool isPhageBreak, AutoFit autoFit = AutoFit.Content, int colWidth = 12)
		{
			TableName = tableName;
			PartsName = partsName;
			StyleHeader = headerStyle;
			StyleBody = bodyStyle;
			DataAt = dataProvider;
			Fit = autoFit;
			parts = tableParts;
			ColWidth = Convert.ToUInt64(colWidth);
			StyleName = nameStyle;
			FormatsCols = new List<ColBase>();
			IsSeparated = isPhageBreak;
		}

		public void Save(NpoiWord doc)
		{
			doc.InsertParagrapth(TableName, false, StyleName);
			
			for (int i = 0; i < parts.Count; i++)
			{
				for (int j = 0; j < parts[i].Body.Count; j++)
				{
					int rows = CountRows(parts[i].Header, parts[i].Body.ElementAt(j)),
						cols = CountColumns(parts[i]);

					if (i != 0 || j != 0)
						doc.InsertParagrapth(PartsName, false, StyleName);

					var tbl = doc._doc.CreateTable(rows, cols);
					FillWithData(tbl, parts[i].Header, parts[i].Body.ElementAt(j));
					FitTable(tbl);

					if (i == parts.Count - 1 && j == parts[i].Body.Count - 1) continue;
					if (IsSeparated)
						doc.InsertPhageBreak();
					else
						doc.InsertNewLine();
				}				
			}			
		}

		private void FitTable(XWPFTable tbl)
		{
			switch (Fit)
			{
				case AutoFit.ColumnWidth:
					{										
						SetGlobalColWidth(tbl);
						break;
					}
				case AutoFit.Content:
				default:
					{
						break;
					}
				case AutoFit.Phage:
					{
						ColWidth = (ulong)(( PhageWidth() / tbl.Rows.First().GetTableCells().Count) * 0.80);
						SetGlobalColWidth(tbl);
						break;
					}
			}
			SetCustomColFormat(tbl);
		}

		private void SetCustomColFormat(XWPFTable tbl)
		{
			foreach (var format in FormatsCols)
			{
				if (format is FormateColumn)
				{
					var tmp = (FormateColumn)format;
					if (tbl.Rows[0].GetTableCells().Count <= tmp.Index1) continue;
					tbl.SetColumnWidth(tmp.Index1, tmp.ColWidth);
					//tbl.Rows.First().GetCell(tmp.Index1).Width = tmp.ColWidth;
					tbl.Rows.First().GetCell(tmp.Index1).SetVerticalAlignment(tmp.VertAligment);
				}
				if (format is MergeColumns)
				{
					var tmp = (MergeColumns)format;
					foreach (var row in tmp.Rows)
					{
						if (tbl.Rows[row].GetTableCells().Count <= tmp.Index2) continue;
						tbl.Rows[row].MergeCells(tmp.Index1, tmp.Index2);

						var pars = tbl.Rows[row].GetCell(tmp.Index1).Paragraphs;
						for (int i = 0; i < pars.Count - 1; i++)
						{
							if (pars[i].Text.Length <= 0) tbl.Rows[row].GetCell(tmp.Index1).RemoveParagraph(i);
						}
					}
				}
			}
		}

		private void SetGlobalColWidth(XWPFTable tbl)
		{
			var cols = tbl.Rows.First().GetTableCells().Count;

			for(int i=0; i < cols; i++)
			{
				tbl.SetColumnWidth(i, ColWidth);
			}
		}

		private void FillWithData(XWPFTable tbl, KeyValuePair<Range, Range> head, KeyValuePair<Range, Range> body)
		{
			int deltaColStart = FindDelta(0, head.Value.Col),
			 //deltaCol = head.Value.ColLength,
			 deltaRowStart = FindDelta(0, head.Value.Row),
			 //deltaRow = head.Value.RowLength,
			 startRow = 0,
			startCol = 0;

			FillRange(tbl, head.Value, deltaColStart, deltaRowStart, startRow, startCol, StyleHeader);
			if (!head.Key.IsEmpty)
			{
				startCol = head.Value.ColLength;
				deltaColStart = FindDelta(0, head.Key.Col);
				deltaRowStart = FindDelta(0, head.Key.Row);
				FillRange(tbl, head.Key, deltaColStart, deltaRowStart, startRow, startCol, StyleHeader);
			}

			startCol = 0;
			startRow += Math.Max(head.Value.RowLength, head.Key.RowLength);
			
			deltaColStart = FindDelta(0, body.Value.Col);
			//deltaCol = FindDelta(body.Value.EndCol, body.Key.Col);
			deltaRowStart = FindDelta(0, body.Value.Row);
			//deltaRow = FindDelta(body.Value.Row, body.Key.EndRow);			
			FillRange(tbl, body.Value, deltaColStart, deltaRowStart, startRow, startCol, StyleBody);
			if (!body.Key.IsEmpty)
			{
				startCol = body.Value.ColLength;
				deltaColStart = FindDelta(0, body.Key.Col);
				deltaRowStart = FindDelta(0, body.Key.Row);
				FillRange(tbl, body.Key, deltaColStart, deltaRowStart, startRow, startCol, StyleBody);
			}

			//startRow += body.Value.RowLength;
		}

		private void FillRange(XWPFTable tbl, Range range, int colDelta, int rowDelta, int startRow, int startCol, Paragraph upd)
		{
			for (int rowI = range.Row; rowI <= range.EndRow; rowI++)
			{
				int tblRow = startRow + (rowI - rowDelta);
				for (int cellJ = range.Col; cellJ <= range.EndCol; cellJ++)
				{
					int tblCell = startCol + (cellJ - colDelta);
					var data = DataAt(rowI, cellJ);
					var cell = tbl.Rows[tblRow].GetCell(tblCell);
					var p = cell.AddParagraph();
					var r = upd.Update(p);
					r.AppendText(data);					
				}
			}
		}

		protected int FindDelta(int Col1, int Col2)
		{
			if (Col1 == Col2)
				return 0;
			else
				return Math.Abs(Col1 - Col2);
		}




		static int CountRows(KeyValuePair<Range, Range> head, KeyValuePair<Range, Range> body)
		{
			int res = 0;
			res += head.Value.IsEmpty ? 0 : head.Value.RowLength;
			res += body.Value.IsEmpty ? 0 : body.Value.RowLength;

			return res;
		}

		static int CountColumns(TablePart tpd)
		{
			int res = 0;
			if (tpd == null) return 0;
			if (!tpd.Header.Key.IsEmpty) res += tpd.Header.Key.ColLength;
			if (!tpd.Header.Value.IsEmpty) res += tpd.Header.Value.ColLength;
			return res;
		}
	}
}
