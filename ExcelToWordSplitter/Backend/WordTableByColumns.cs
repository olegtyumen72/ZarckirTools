﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelToWordSplitter.Backend.Tools;
using SimpleCore.Parts;
using Xceed.Document.NET;
using Xceed.Words.NET;

namespace ExcelToWordSplitter.Backend
{
	/// <summary>
	/// Создаёт разбиение по эксель таблице заполняя и складывая колонки (алгоритм заполнения поколоночно)
	/// </summary>
	public class WordTableByColumns : WordTableBase
	{
		/// <summary>
		/// "Кубики" разбиения эксель таблицы
		/// Каждый элемент - отдельный набор колонок (1 отдельная таблица)
		/// Таблицы можно скрещивать, либо каждую переносить на новую страницу
		/// </summary>
		public List<TableColumns> Parts;
		/// <summary>
		/// Название между таблицам по типу "Продолжение таблицы ..."
		/// </summary>
		public string PartsName;
		/// <summary>
		/// Добавлять ли нумерацию к названиям между таблицами
		/// </summary>
		public bool IsNumeric;
		/// <summary>
		/// Стартовое число для нумерации
		/// </summary>
		public int StartNumber = 1;
		/// <summary>
		/// Обновление стиля параграфа для промежуточных названий таблиц
		/// </summary>
		public Func<Paragraph, Paragraph> FormatTableNamesParahraphs;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parts">Кубики-таблицы</param>
		/// <param name="partsName">Название между таблицам по типу "Продолжение таблицы ..."</param>
		/// <param name="isNumeric">Добавлять ли нумерацию к названиям между таблицами</param>
		/// <param name="doc">Документ куда вставить разбитую таблицу</param>
		/// <param name="tableSettings">Настройки таблицы</param>
		/// <param name="tableName">Название таблицы</param>
		/// <param name="dataSource">Источник данных на основе SimpleCore</param>
		/// <param name="formatTableNamesParahraphs">Обновление стиля для промедуточных названий таблицы</param>
		/// <param name="formatMainCellParagraph">Обновление стиля для каждой ячейки</param>
		/// <param name="startNumber">Стартовое число для нумерации</param>
		public WordTableByColumns(
			List<TableColumns> parts,
			string partsName,
			bool isNumeric,
			DocX doc,
			BaseSettings tableSettings,
			IReportSheet dataSource,
			Func<Paragraph, Paragraph> formatTableNamesParahraphs = null,
			Func<Paragraph, Paragraph> formatMainCellParagraph = null,
			int startNumber = 1
			) : base(tableSettings, dataSource, doc, formatMainCellParagraph)
		{
            if (parts == null)
                throw new ArgumentNullException(nameof(parts));
            else
                Parts = parts;
            if (partsName == null)
                throw new ArgumentNullException(nameof(partsName));
            else
                PartsName = partsName;
			IsNumeric = isNumeric;
			StartNumber = startNumber;
			FormatTableNamesParahraphs = formatTableNamesParahraphs;
			Doc.RemoveProtection();
		}


		Table InitTable(TableColumns part, Table oldTable = null, bool inDoc = true)
		{
			var hasOld = oldTable != null;
			int rows = CountRows(new[] { part.Head, part.Body }),
				cols = CountCols(new[] { part.Head, part.Body });

			if (hasOld) cols += oldTable.ColumnCount;
			if (hasOld) rows = Math.Max(rows, oldTable.RowCount);
			//if (index != 0 && !part.IsStickyToPrevious) Doc.InsertParagraph(PartsName);

			var tbl = Doc.InsertTable(rows, cols);
			tbl.Design = TableSettings.Design;
			tbl.Alignment = TableSettings.Aligmen;

			if (!inDoc)
				tbl.Remove();

			if (hasOld)
			{
				FillTable(oldTable, tbl, 0, 0);
				var curRow = FillWithData(tbl, part.Head, 0, oldTable.ColumnCount);
				FillWithData(tbl, part.Body, curRow, oldTable.ColumnCount);
			}
			else
			{
				var curRow = FillWithData(tbl, part.Head);
				FillWithData(tbl, part.Body, curRow);
			}


			return tbl;
		}


		public override void InsertTable(Paragraph curPar)
		{
			//curPar.InsertText(0,TableName);

			var realParts = new List<TableColumns>();
			var statics = new List<TableColumns>();
			foreach (var part in Parts)
			{
				if (part.IsStatic)
					statics.Add(part);
				else
					realParts.Add(part);
			}


			Table statTbl = null, ltbl = null;
			for (int i = 0; i < statics.Count; i++)
			{
				ltbl = InitTable(statics[i], null, false);

				if (statTbl != null)
				{
					statTbl = MergeTables(statTbl, ltbl);
				}
				else
				{
					statTbl = ltbl;
				}
			}

			Paragraph last = curPar;
			ltbl = null;
			for (int i = 0; i < realParts.Count; i++)
			{
				var part = realParts[i];

				if (i > 0 && !part.IsStickyToPrevious)
				{
					Doc.InsertParagraph();
					var textPar = PartsName + (IsNumeric ? " " + StartNumber++ : "");
                    last = Doc.InsertParagraph(textPar);
                    last = FormatTableNamesParahraphs?.Invoke(last) ?? last;
                }

				if (part.IsStickyToPrevious && ltbl != null)
				{
					ltbl.Remove();
					ltbl = InitTable(part, ltbl);
					//ltbl = tbl = MergeTables(ltbl, tbl);
				}
				else
				{
					ltbl = InitTable(part, statTbl);
				}

				SetTableGridLines(ltbl, Grid);
				FitTable(ltbl);

				if (part.EndWithPhageBreak)
				{
					ltbl.InsertPageBreakAfterSelf();
					ltbl = null;
				}
			}

		}

		protected override int CountRows(params List<TableRange>[] ranges)
		{
			var data = ranges.SelectMany((x) => x);
			var res = 0;
			foreach (var item in data)
			{
				res += item.RowCount();
			}

			return res;
		}

		protected override int CountCols(params List<TableRange>[] ranges)
		{
			var data = ranges.SelectMany((x) => x);
			var res = 0;
			foreach (var item in data)
			{
				res = Math.Max(res, item.ColCount());
			}

			return res;
		}



		protected override int FillWithData(Table tbl, IEnumerable<TableRange> ranges, int startRow = 0, int staticCols = 0)
		{
			int lastRow = startRow;
			foreach (var range in ranges)
			{
				int deltaColStart = FindDelta(staticCols, range.StartCol),
				 deltaRowStart = FindDelta(0, range.StartRow);

				lastRow = FillRange(tbl, range, deltaColStart, deltaRowStart, lastRow);
			}

			return lastRow;
		}

		protected override int FillRange(
			Table tbl,
			TableRange range,
			int colDelta,
			int rowDelta,
			int startRow)
		{
			int lastRow = startRow;
			for (int rowI = range.StartRow - 1; rowI < range.EndRow; rowI++)
			{
				int tblRow = lastRow = startRow + (rowI - rowDelta);

				if (rowI > DataSource.Count)
				{
					lastRow -= 1;
					break;
				}

				for (int cellJ = range.StartCol - 1; cellJ < range.EndCol; cellJ++)
				{
					int tblCell = cellJ - colDelta;
					if (DataSource[rowI].Count > cellJ)
					{
						if (tbl.RowCount < tblRow || tbl.Rows[tblRow].ColumnCount < tblCell)
							continue;

						Cell cell = null;
						try
						{
							cell = tbl.Rows[tblRow].Cells[tblCell];
						}
						catch
						{
							//TODO:  если тут ошибка, то это гг. Что-то не предусмотрел, хз.
							// придётся прям по этапно смотреть всю инициализацию
							continue;
						}

						SetCellGridLines(tbl, range.Grid, tblCell, tblRow);

						var tmp = DataSource[rowI][cellJ]?.ToString() ?? "";
						Paragraph curPar = cell.InsertParagraph();

                        double res;
						if (double.TryParse(tmp, out res) && TableSettings.RoundNumbers != -1)
						{
							var val = Math.Round(res, TableSettings.RoundNumbers).ToString();
							curPar.Append(val);
							UpdateParagrapth(curPar, tblCell + 1, tblRow + 1, val);
						}
						else
						{
							curPar.Append(tmp);
							UpdateParagrapth(curPar, tblCell + 1, tblRow + 1, tmp);
						}

						if (FormatMainCellParagraph != null)
						{
							curPar = FormatMainCellParagraph(curPar);
						}
						RemoveEmptyParagraphs(cell);
					}
				}
			}

			return lastRow + 1;
		}

		protected override Table MergeTables(Table start, Table end)
		{
			var rows = start.RowCount > end.RowCount ? start.RowCount : end.RowCount;
			var cols = start.ColumnCount + end.ColumnCount;

			var newTable = end.InsertTableAfterSelf(rows, cols);

			FillTable(start, newTable, 0, 0);
			FillTable(end, newTable, start.ColumnCount, 0);
			start.Remove(); end.Remove();

			return newTable;
		}
	}
}
