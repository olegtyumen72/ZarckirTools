﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Parts
{
	public interface ICoreReportSheet
	{

		string Name { get; set; }

		object Tag { get; set; }

		/// <summary>
		/// Фон ячеек
		/// </summary>
		Color CellsBg { get; set; }
		/// <summary>
		/// Цвет шрифта ячеек
		/// </summary>
		Color CellsFont { get; set; }

		/// <summary>
		/// Авто-выбор ширины колонки для всех столбцов
		/// </summary>
		bool AutoFit { get; set; }

		List<IReportFormatItem<ICellRange, IConditionalFormat>> Formats { get; set; }

		/// <summary>
		/// объект для хранения словаря переводов по листу
		/// </summary>
		ITranslater Translater { get; set; }
		/// <summary>
		/// список ячеек для перевода
		/// </summary>
		List<ICellRange> cells_tr { get; set; }

		List<IReportChart<IReportChartItem>> Graphs { get; set; }
	}
}
