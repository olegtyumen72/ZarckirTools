﻿using SimpleExcelXML.XlsxParts.XmlNodes;
using SimpleExcelXML.XlsxParts.XmlNodes.Charts;
using SimpleExcelXML.XlsxParts.XmlNodes.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace SimpleExcelXML.Tools
{
	/// <summary>
	/// Почти мёртвый класс, нужен был для первой версии - в идеале избавиться, однако заполняет правильно элементы в xml файле
	/// </summary>
	public static class XmlTools
	{

		public static int IndexOf(this char[] src, char elem)
		{
			for(int i=0; i < src.Length; i++)
			{
				if (elem == src[i])
					return i;
			}

			return -1;
		}

		public static char[] alphabet = new char[]
		{
			//1  2   3   4   5   6   7   8   9   10  11  12  13 14  15  16   17  18 19   20 21   22  23  24  25  26
			'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
		};

		public static List<char> realVals = new List<char>() { '{', ':', '}' };

		public static Dictionary<string, string> xp = new Dictionary<string, string>()
		{
			{"vec", "vector" },
			{"var", "variant"},
			{ "lpstr", "lpst" },
			{"i4", "i4" },
			{"relat", "Relationship" },
			{"ts", "Types" },
			{"def", "Default" },
			{"over", "Override" },
		};

		public static Dictionary<string, string> xa = new Dictionary<string, string>()
		{
			{"s", "size" },
			{"BT", "baseType" },
			{"id", "Id" },
			{"Id", "Id" },
			{"ID", "Id" },
			{"t", "Type"},
			{"trt", "Target" },
			{ "contT", "ContentType" },
			{"ext", "Extension" },
			{"prtN", "PartName" },
			{"extProp", "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" },
			{"vTypes",  "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes" },
			{"extRelProp","http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"  },
			{"relatsProp", "http://schemas.openxmlformats.org/package/2006/relationships" },
			{ "coreProp", "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties" },
			{ "offProp", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" },
			{ "contTs", "http://schemas.openxmlformats.org/package/2006/content-types" },
			{"relatCT", "application/vnd.openxmlformats-package.relationships+xml" },
			{"xmlCT", "application/xml" },
			{"mainCT", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml" },
			{"worksheetCT", "application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml" },
			{"themeCT", "application/vnd.openxmlformats-officedocument.theme+xml" },
			{"stylesCT", "application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml" },
			{"sharedStringsCT", "application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml" },
			{"corePropCT", "application/vnd.openxmlformats-package.core-properties+xml" },
			{"extPropCT", "application/vnd.openxmlformats-officedocument.extended-properties+xml" },
			{"spredMain", "http://schemas.openxmlformats.org/spreadsheetml/2006/main" },
			{"offRelat","http://schemas.openxmlformats.org/officeDocument/2006/relationships" },
			{"compab", "http://schemas.openxmlformats.org/markup-compatibility/2006" },
			{"spredMain1", "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main" },
			{"appN", "appName" },
			{"lastEdi", "lastEdited" },
			{"loweEdi", "lowestEdited" },
			{"xwb", "xWindow" },
			{"ywb", "yWindow" },
			{"n", "name" },
			{"wsId", "sheetId" },
			{"mcIgnor", "x14ac x16r2" },
			{"x14ac", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac" },
			{"x16r2", "http://schemas.microsoft.com/office/spreadsheetml/2015/02/main" },
			{"dc", "http://purl.org/dc/elements/1.1/" },
			{"dcterms", "http://purl.org/dc/terms/" },
			{"dcmitype", "http://purl.org/dc/dcmitype/" },
			{"xsi", "http://www.w3.org/2001/XMLSchema-instance" },
			{"cp", "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"},
			{"cpCore", "http://schemas.openxmlformats.org/package/2006/metadata/core-properties" },
			{"xdr", "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing" },
			{"a", "http://schemas.openxmlformats.org/drawingml/2006/main" },
			{ "c", "http://schemas.openxmlformats.org/drawingml/2006/chart" },
			{"r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships" },
			{"c16r2","http://schemas.microsoft.com/office/drawing/2015/06/chart" }
		};

		internal static void NewRelation(XmlWriterWrapper doc, string id, string target, string type)
		{
			doc.OpenElement(xp["relat"]).AddAtribute("Id", id).AddAtribute("Type", type).AddAtribute("Target", target).CloseElement();
		}

		#region Wrapped

		public static void NewLpstr(XmlWriterWrapper doc, string ns, string val)
		{
			doc.OpenElement("lpstr", val, ns).CloseElement();
		}

		public static void NewVariant(XmlWriterWrapper doc, string ns, string val)
		{
			doc.OpenElement("variant", val, ns);
		}

		public static void NewVector(XmlWriterWrapper doc, string ns, Dictionary<string, string> atrs)
		{
			doc.OpenElement("vector", null, ns);

			foreach (var atr in atrs)
			{
				doc.AddAtribute(atr.Key, atr.Value);
			}
		}

		internal static void NewDefault(XmlWriterWrapper doc, string ext, string contType)
		{
			doc.OpenElement(xp["def"], null).AddAtribute(xa["ext"], ext).AddAtribute(xa["contT"], contType).CloseElement();
		}

		public static void DefaultDeclaration(XmlWriterWrapper doc)
		{
			doc.AddDeclaration("xml", "version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"");
		}

		internal static void NewOverride(XmlWriterWrapper doc, string partName, string contType)
		{
			doc.OpenElement(xp["over"]).AddAtribute(xa["prtN"], partName).AddAtribute(xa["contT"], contType).CloseElement();
		}


		internal static void newTxPr(XmlWriterWrapper doc, BodyPr bodyPr = null,
			int rPrSz = -1, int b = -1, int i = -1, string u = null, string strike = null, int baseLine = -1, SolidFill fill = null)
		{

		}

		#endregion

		#region XmlLinq
		public static XElement NewSheet(string name)
		{
			var res = NewVector(new Dictionary<string, string>()
			{
				{ xa["s"], "1"},
				{xa["BT"], "lpstr" }
			});

			var elem = CreateElement(xp["lpst"], name);
			res.Add(elem);

			return res;
		}

		public static XElement CreateElement(string name, string value = "", Dictionary<XName, string> atrs = null)
		{
			var res = new XElement(name);

			if (atrs != null)
				foreach (var atr in atrs)
				{
					var xAtr = new XAttribute(atr.Key, atr.Value);

					res.Add(xAtr);
				}

			if (value.Length > 0)
				res.Value = value;

			return res;
		}

		public static XElement NewVector(Dictionary<string, string> atrs = null)
		{
			var res = new XElement(xp["var"]);
			if (atrs != null)
			{
				foreach (var atr in atrs)
				{
					res.Add(NewAtr(atr));
				}
			}

			return res;
		}

		public static XAttribute NewAtr(KeyValuePair<string, string> atr)
		{
			return new XAttribute(atr.Key, atr.Value);
		}

		public static XElement NewRelation(string tag, int cnt, string target, string type)
		{
			return NewRelation($"{tag}{cnt}", target, type);
		}

		public static XElement NewRelation(string id, string target, string type)
		{
			return CreateElement(xp["relat"], "", new Dictionary<XName, string>()
			{
				{xa["id"], id },
				{xa["t"], type },
				{xa["trt"], target }
			});
		}

		public static XElement NewDefault(string ext, string contType)
		{
			return CreateElement(xp["def"], "", new Dictionary<XName, string>()
			{
				{ xa["ext"], ext },
				{ xa["contT"], contType }
			});
		}

		public static XElement NewOverride(string partName, string contType)
		{
			return CreateElement(xp["over"], "", new Dictionary<XName, string>()
			{
				{ xa["prtN"], partName },
				{ xa["contT"], contType }
			});
		}

		public static XDeclaration DefaultDeclaration()
		{
			return new XDeclaration("1.0", "UTF-8", "yes");
		}

		#endregion


		public static bool ContainsAll(List<char> cnts, string val)
		{
			for (int i = 0; i < cnts.Count; i++)
			{
				var has = val.Contains(cnts[i]);
				if (!has)
				{
					return false;
				}
			}

			return true;
		}

	}

}
