﻿using NPOI.SS.UserModel;
using NPOI.XWPF.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpoiWrapper.Tools
{
	public abstract class IndexerBase
	{
		public int Index1 = -1;
	}

	public abstract class ColBase : IndexerBase { }

	public abstract class RowBase : IndexerBase { }

	public class FormateColumn : ColBase
	{
		public ulong ColWidth = 0;
		public XWPFTableCell.XWPFVertAlign VertAligment = XWPFTableCell.XWPFVertAlign.BOTTOM;
	}

	public class MergeColumns : ColBase
	{
		public List<int> Rows = new List<int>();
		public int Index2 = -1;
	}

	public class FormateRow : RowBase
	{
		public int RowHeight = -1;
	}
}
