﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Interfaces
{
	/// <summary>
	/// Общий интерфейс для классов-графиков
	/// </summary>
	internal interface IXmlChart
	{

		bool varyColors { get; set; }
		List<Charts.Ser> Data { get; set; }
		Charts.DLbls dLbls { get; set; }
		List<Charts.TypedAx> Axes { get; set; }
		string Prefix { get; set; }
		string KeyWord { get; }


	}
}
