﻿using SimpleCore.Parts;
using SimpleCore.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.ExcelParts
{
	public class ReportBook : ReportBookTmpl<ReportSheet, CellRangeTmpl, ConditionalFormatTmpl, ReportFormatItemTmpl<CellRangeTmpl, ConditionalFormatTmpl>>
	{
		public string Company;
		public string App { get; } = "Microsoft Excel";
		public string Pass { get; private set; }
		public string Version { get; } = "16.0300";
		public string Owner { get; } = Environment.UserName;

		public ReportBook(): base("")
		{

		}

		public ReportBook(string path) : base(path)
		{
		}

		public ReportBook(IReportBook v) : base(v)
		{
		}
	}
}
