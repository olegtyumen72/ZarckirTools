﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpoiWrapper.Tools
{
	public class Range
	{
		public int Col { get; set; }
		public int Row { get; set; }
		public int ColLength { get; set; }
		public int RowLength { get; set; }
		public int EndCol
		{
			get
			{
				return Col + ColLength - 1;
			}
			set
			{
				ColLength = value - Col + 1;
			}
		}
		public int EndRow
		{
			get
			{
				return Row + RowLength - 1;
			}
			set
			{
				RowLength = value - Row + 1;
			}
		}
		public bool IsEmpty
		{
			get
			{
				return Row < 0 || Col < 0 || ColLength < 0 || RowLength < 0;
			}
		}

		public Range(int col, int row)
		{
			Col = col;
			Row = row;
			EndCol = Col + 1;
			EndRow = Row + 1;
		}

		public Range(int col, int row, int colEnd, int rowEnd):this(col, row)
		{
			EndCol = colEnd;
			EndRow = rowEnd;
		}

		public override string ToString()
		{
			return $"({Col};{Row}),({EndCol};{EndRow})";
		}
	}
}
