﻿using SimpleExcelXML.Tools;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// По сути представляет из себя SimpleFontFamily, FontFamily
	/// </summary>
	public class TextProperty
	{
		public int size { get; set; } = 1300;
		public Fonts fonts { get; set; } = Fonts.Arial;
		public string value { get; set; } = null;

		public TextProperty(string val)
		{
			value = val;
		}

		public void Save(XmlWriterWrapper doc)
		{
			if (String.IsNullOrEmpty(value)) throw new Exception("Miss value");

			doc.OpenPrefElement("r", "a")
				.OpenPrefElement("rPr", "a").AddAtribute("sz", size.ToString())
				.OpenPrefElement("latin", "a").AddAtribute("typeface", fonts.ToString()).CloseElement()
				.CloseElement()
				.OpenPrefElement("t", "a", value).CloseElement()
				.CloseElement();
		}



	}
}
