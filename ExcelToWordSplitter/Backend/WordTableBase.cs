﻿using ExcelToWordSplitter.Backend.Parts.Tools;
using ExcelToWordSplitter.Backend.Tools;
using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using Xceed.Document.NET;
using Xceed.Words.NET;

namespace ExcelToWordSplitter.Backend
{
	/// <summary>
	/// Класс прородитель для всех сплитеров таблиц. Описаны основные методы, но их всех можно оверлоднуть при необходимости
	/// </summary>
	public abstract class WordTableBase
	{
		/// <summary>
		/// Делегат для ивента обработки ячейки в таблице
		/// С помощью него можно редактировать любую ячейку как угодно
		/// </summary>
		/// <param name="old"></param>
		/// <param name="col"></param>
		/// <param name="row"></param>
		/// <param name="cell"></param>
		/// <returns></returns>
		public delegate Paragraph CellParagrapthHandler(Paragraph old, int col, int row, object cell);

		/// <summary>
		/// Основные настройки отображения таблицы
		/// </summary>
		public BaseSettings TableSettings { get; set; }
		/// <summary>
		/// Документ куда вставить таблицы
		/// </summary>
		public DocX Doc { get; set; }
		/// <summary>
		/// Источник данных на основе SimpleCore
		/// </summary>
		public IReportSheet DataSource { get; set; }
		/// <summary>
		/// Набор кастомных настроек для строчек\столбцов таблиц
		/// Индексы начинаются с 0
		/// </summary>
		public List<IndexerBase> FormatsCols { get; set; } = new List<IndexerBase>();
		/// <summary>
		/// Настройки для описания линий таблицы в общем случае
		/// </summary>
		public Dictionary<TableBorderType, Border> Grid { get; set; } = new Dictionary<TableBorderType, Border>();
		/// <summary>
		/// Ивент для апдейта параграфа ячейки таблицы
		/// </summary>
		public Func<Paragraph, Paragraph> FormatMainCellParagraph;

		private event CellParagrapthHandler _cellParagrapthUpdate;
		public event CellParagrapthHandler CellParagrapthUpdate
		{
			add { _cellParagrapthUpdate += value; }
			remove { _cellParagrapthUpdate -= value; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="tableSettings">Настройки таблицы</param>
		/// <param name="tableName">Имя таблицы</param>
		/// <param name="dataSource">Источник данных на основе SimpleCore</param>
		/// <param name="doc">Документ куда вставить таблицы</param>
		/// <param name="formatMainCellParagraph">Функция для обновления параграфа у каждой ячейки</param>
		protected WordTableBase(BaseSettings tableSettings, IReportSheet dataSource, DocX doc, Func<Paragraph, Paragraph> formatMainCellParagraph = null)
		{
			TableSettings = tableSettings;
			//TableName = tableName;
			DataSource = dataSource;
			Doc = doc;
			FormatMainCellParagraph = formatMainCellParagraph;
		}

		/// <summary>
		/// Основной метод для вставки таблицы
		/// </summary>
		/// <param name="curPar"></param>
		public abstract void InsertTable(Paragraph curPar);
		/// <summary>
		/// Подсчёт строк. Может быть уникально для каждой таблицы
		/// </summary>
		/// <param name="ranges"></param>
		/// <returns></returns>
		protected abstract int CountRows(params List<TableRange>[] ranges);
		/// <summary>
		/// Подсчёт колонок. Может быть уникально для каждой таблицы
		/// </summary>
		/// <param name="ranges"></param>
		/// <returns></returns>
		protected abstract int CountCols(params List<TableRange>[] ranges);
		/// <summary>
		/// Заполнение таблицы данными из экселя по заданным параметрам
		/// </summary>
		/// <param name="tbl">Таблицы из ворда</param>
		/// <param name="ranges">TableColumns -> Head | Body <--> либо подобная структура</param>
		/// <param name="start">Откуда начинается заполнение</param>
		/// <param name="stat">Откуда начинается статика</param>
		/// <returns></returns>
		protected abstract int FillWithData(Table tbl, IEnumerable<TableRange> ranges, int start = 0, int stat = 0);
		/// <summary>
		/// Заполнение таблицы данными из экселя по заданным параметрам
		/// </summary>
		/// <param name="tbl">Таблица в ворде</param>
		/// <param name="range">конкретный отрезок таблицы</param>
		/// <param name="colDelta">сдвиг относительно 0 ячейки вордовской таблицы к экселевской таблице</param>
		/// <param name="rowDelta">сдвиг относительно 0 ячейки вордовской таблицы к экселевской таблице</param>
		/// <param name="start">с какой позиции заполнять таблицу</param>
		/// <returns></returns>
		protected abstract int FillRange(
			Table tbl,
			TableRange range,
			int colDelta,
			int rowDelta,
			int start);

		/// <summary>
		/// Метод для склейки двух таблиц. Например статики и новой таблицы
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <returns></returns>
		protected abstract Table MergeTables(Table start, Table end);


		/// <summary>
		/// Задаёт ширину для каждой колонки, если SimpleAutoFir стоит на ColWidth
		/// </summary>
		/// <param name="tbl"></param>
		protected void SetGlobalColWidth(Table tbl)
		{
			var ColWidth = TableSettings.ColWidth;
			var VertAligment = TableSettings.VertAligment;

			for (int i = 0; i < tbl.RowCount; i++)
			{
				for (int j = 0; j < tbl.Rows[i].ColumnCount; j++)
				{
					try
					{
						var cell = tbl.Rows[i].Cells[j];
						cell.Width = ColWidth;
						cell.VerticalAlignment = VertAligment;
					}
					catch
					{

					}
				}
			}
		}

		/// <summary>
		/// Поиск сдвига
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		protected int FindDelta(int x, int y)
		{
			return x == y ? 0 : Math.Abs(x - y) - 1;
		}

		/// <summary>
		/// Заполнение стиля таблицы
		/// </summary>
		/// <param name="tbl"></param>
		protected virtual void FitTable(Table tbl)
		{
			switch (TableSettings.Fit)
			{
				case SimpleAutoFit.ColumnWidth:
					{
						tbl.AutoFit = AutoFit.ColumnWidth;
						SetGlobalColWidth(tbl);
						break;
					}
				case SimpleAutoFit.Contents:
				default:
					{
						tbl.AutoFit = AutoFit.Contents;
						break;
					}
				case SimpleAutoFit.Phage:
					{
						tbl.AutoFit = AutoFit.Window;
						TableSettings.ColWidth = (int)((Doc.PageWidth / tbl.ColumnCount) * 0.80);
						SetGlobalColWidth(tbl);
						break;
					}
			}
			SetCustomColFormat(tbl);
		}

		/// <summary>
		/// Устанавливает стиль для линий таблицы
		/// </summary>
		/// <param name="tbl"></param>
		/// <param name="styles"></param>
		protected virtual void SetTableGridLines(Table tbl, Dictionary<TableBorderType, Border> styles)
		{
			if (styles == null)
				return;

			foreach (var style in styles)
			{
				tbl.SetBorder(style.Key, style.Value);
			}
		}

		/// <summary>
		/// Устанавлвает стиль линий для ячейки
		/// </summary>
		/// <param name="tbl"></param>
		/// <param name="styles"></param>
		/// <param name="col"></param>
		/// <param name="row"></param>
		protected virtual void SetCellGridLines(Table tbl, Dictionary<TableCellBorderType, Border> styles, int col, int row)
		{
			if (styles == null)
				return;

			foreach (var style in styles)
			{
				tbl.Rows[row].Cells[col].SetBorder(style.Key, style.Value);
			}
		}

		/// <summary>
		/// Вспомогательный метод. Настраивает формат ячейки
		/// </summary>
		/// <param name="cell"></param>
		/// <param name="vert"></param>
		/// <param name="dir"></param>
		void FormatCell(Cell cell, VerticalAlignment? vert = null, TextDirection? dir = null)
		{
			cell.VerticalAlignment = vert ?? cell.VerticalAlignment;
			cell.TextDirection = dir ?? cell.TextDirection;
		}

		/// <summary>
		/// Устанавливает Уникальные стили для колонок\строк таблицы
		/// </summary>
		/// <param name="tbl"></param>
		protected virtual void SetCustomColFormat(Table tbl)
		{
			foreach (var format in FormatsCols)
			{
				if (format is FormatColumn)
				{
					var tmp = (FormatColumn)format;
					if (tbl.ColumnCount <= tmp.Index1) continue;
					tbl.Rows.First().Cells[tmp.Index1].Width = tmp.ColWidth;
					tbl.Rows.First().Cells[tmp.Index1].VerticalAlignment = tmp.VertAligment;
				}
				else if(format is FormateRow)
				{
					var tmp = (FormateRow)format;
					if (tbl.RowCount <= tmp.Index1) continue;

					tbl.Rows[tmp.Index1].Height = tmp.RowHeight;
					//tbl.Rows[tmp.Index1].MinHeight = tmp.RowHeight;
				}
				else if (format is FormatTextByColumn)
				{
					var tmp = (FormatTextByColumn)format;
					foreach (var row in tmp.Rows)
					{
						if (tbl.RowCount <= row)
							continue;

						if (tbl.Rows[row].ColumnCount <= tmp.Index1)
							continue;

						FormatCell(tbl.Rows[row].Cells[tmp.Index1], tmp.VerticalAlignment, tmp.TextDirection);
					}
				}
				else if (format is FormatTextByRows)
				{
					var tmp = (FormatTextByRows)format;

					foreach (var col in tmp.Cols)
					{
						if (tbl.RowCount <= tmp.Index1)
							continue;

						if (tbl.Rows[tmp.Index1].Cells.Count <= col)
							continue;

						FormatCell(tbl.Rows[tmp.Index1].Cells[col], tmp.VerticalAlignment, tmp.TextDirection);
					}

				}
				else if (format is MergeColumns)
				{
					var tmp = (MergeColumns)format;
					foreach (var row in tmp.Rows)
					{
						if (tbl.Rows[row].ColumnCount <= tmp.Index2) continue;
						tbl.Rows[row].MergeCells(tmp.Index1, tmp.Index2);

						var pars = tbl.Rows[row].Cells[tmp.Index1].Paragraphs;
						RemoveEmptyParagraphs(tbl, pars, row, tmp.Index1);
					}
				}
				else if (format is MergeRows)
				{
					var tmp = (MergeRows)format;

					foreach (var col in tmp.Cols)
					{
						if (tbl.Rows[tmp.Index1].Cells.Count <= col ||
							tbl.Rows[tmp.Index2].Cells.Count <= col) continue;

						tbl.MergeCellsInColumn(col, tmp.Index1, tmp.Index2);

						var pars = tbl.Rows[tmp.Index1].Cells[col].Paragraphs;
						RemoveEmptyParagraphs(tbl, pars, tmp.Index1, col);
						pars = tbl.Rows[tmp.Index2].Cells[col].Paragraphs;
						RemoveEmptyParagraphs(tbl, pars, tmp.Index2, col);
					}
				}
			}
		}

		/// <summary>
		/// Убираем лишние стили для таблиц
		/// </summary>
		/// <param name="tbl"></param>
		/// <param name="pars"></param>
		/// <param name="row"></param>
		/// <param name="col"></param>
		protected void RemoveEmptyParagraphs(Table tbl, IEnumerable<Paragraph> pars, int row, int col)
		{
			for (int i = 0; i < pars.Count() - 1; i++)
			{
				var elem = pars.ElementAt(i);
				if (elem.Text.Length <= 0) tbl.Rows[row].Cells[col].RemoveParagraph(elem);
			}
		}

		protected void RemoveEmptyParagraphs(Cell cell)
		{
			var first = cell.Paragraphs.First();
			cell.Paragraphs.ToList().ForEach((v) =>
			{
				if (v.Text.Length <= 0)
					cell.RemoveParagraph(v);
			});

			if (cell.Paragraphs.Count <= 0)
				cell.InsertParagraph(first);
		}

		/// <summary>
		/// Используется при мердже таблиц ворда для переноса данных
		/// </summary>
		/// <param name="src"></param>
		/// <param name="dest"></param>
		/// <param name="colDelta"></param>
		/// <param name="rowDelta"></param>
		protected virtual void FillTable(Table src, Table dest, int colDelta, int rowDelta)
		{
			for (int row = 0; row < src.RowCount; row++)
			{
				for (int col = 0; col < src.ColumnCount; col++)
				{
					Cell curCell = null;
					//TODO: костыль, пока не знаю с чем связан
					try
					{
						if (src.Rows[row].ColumnCount <= col)
							continue;

						curCell = dest.Rows[row + rowDelta].Cells[col + colDelta];
						curCell.Paragraphs.ToList().ForEach(x =>
						{
							if (x.Text.Length <= 0)
								curCell.RemoveParagraph(x);
						});

						curCell.InsertParagraph(src.Rows[row].Cells[col].Paragraphs.First());
					}
					catch
					{
						//dest.Rows[row].Cells[col + colDelta].RemoveParagraphAt(0);
						dest.Rows[row].Cells[col + colDelta].InsertParagraph("");
					}
				}
			}
		}

		/// <summary>
		/// Метод связанный с ивентом обновления ячейки
		/// Использется для особого формата пользователем
		/// </summary>
		/// <param name="p"></param>
		/// <param name="col"></param>
		/// <param name="row"></param>
		/// <param name="val"></param>
		protected void UpdateParagrapth(Paragraph p, int col, int row, object val)
		{
			_cellParagrapthUpdate?.Invoke(p, col, row, val);
		}
	}
}
