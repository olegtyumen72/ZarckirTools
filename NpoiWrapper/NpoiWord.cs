﻿using NPOI.XWPF.UserModel;
using NpoiWrapper.Interfaces;
using NpoiWrapper.Properties;
using NpoiWrapper.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpoiWrapper
{
	public class NpoiWord
	{
		public int PageWidth = 595;
		public int PageHeight = 841;
		public int ROW_ON_A4 = 50;
		public int FIRST = 30;
		public float PART_PHAGE_HEIGHT = 3F;
		public int NUMBER_OF_TABLE = 1;
		public int NUMBER_OF_PICTURE = 1;
		private string _path;
		internal XWPFDocument _doc;
		public bool IsUnionNUmberForTableAndPics;
		private ObservableCollection<IRunableCollection> _data;
		public ObservableCollection<IRunableCollection> Data
		{
			get
			{
				return _data;
			}
			set
			{
				//foreach (var el in value)
				//{
				//	Connect(el);
				//}

				_data = new ObservableCollection<IRunableCollection>(value);
				//_data.CollectionChanged += _data_CollectionChanged;
			}

		}

		public NpoiWord(string path)
		{
			_path = path;
			_data = new ObservableCollection<IRunableCollection>();

			var mem = new MemoryStream(Resources.temp);
			_doc = new XWPFDocument(mem);
			foreach(var par in _doc.Paragraphs)
			{
				for(int r=0; r < par.Runs.Count; r+=0)
				{
					par.RemoveRun(0);
				}
			}

			IsUnionNUmberForTableAndPics = true;
		}

		public void AddParagraph(Paragraph p)
		{
			_data.Add(p);
		}

		public void AddTable(CustomTable t)
		{
			_data.Add(t);
		}

		internal void InsertParagrapth(string text, bool newLine = false, Paragraph updter = null)
		{
			var p = _doc.CreateParagraph();
			var r = updter.Update(p);
			r.AppendText(text);
			if (newLine)
				r.AddCarriageReturn();
		}

		internal void InsertPhageBreak()
		{
			var p = _doc.CreateParagraph();
			var r = p.CreateRun();
			r.AddBreak(BreakType.PAGE);
		}

		internal void InsertNewLine()
		{
			var p = _doc.CreateParagraph();
			var r = p.CreateRun();
			r.AddCarriageReturn();
		}

		public void Save()
		{
			foreach (var el in _data)
			{
				el.Save(this);
			}

			using (var f = new FileStream(_path, FileMode.Create))
			{
				_doc.Write(f);
			}
		}

		//private void Connect(object el)
		//{
		//	if (el is Paragraph)
		//		(el as Paragraph).GetPicNum += NpoiWord_GetPicNum;
		//}

		private int NpoiWord_GetPicNum()
		{
			return NUMBER_OF_PICTURE++;
		}

		//private void _data_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		//{
		//	if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
		//	{
		//		foreach (var el in e.NewItems)
		//		{
		//			Connect(el);
		//		}
		//	}
		//}

	}
}
