﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfTools.Views
{
	/// <summary>
	/// Interaction logic for WritableMessageBox.xaml
	/// </summary>
	public partial class WritableMessageBox : Window
	{

		public static DependencyProperty MessageDP = DependencyProperty.Register(nameof(Message), typeof(string), typeof(WritableMessageBox), new FrameworkPropertyMetadata("Внимание!", new PropertyChangedCallback(OnPropCange)));
		public static DependencyProperty WriteTextDP = DependencyProperty.Register(nameof(WriteText), typeof(string), typeof(WritableMessageBox), new FrameworkPropertyMetadata("", new PropertyChangedCallback(OnPropCange)));


		public string Message
		{
			get
			{
				return (string)GetValue(MessageDP);
			}
			set
			{
				SetValue(MessageDP, value);
			}
		}

		public string WriteText
		{
			get
			{
				return (string)GetValue(WriteTextDP);
			}
			set
			{
				SetValue(WriteTextDP, value);
			}
		}


		public WritableMessageBox()
		{
			InitializeComponent();
		}

		static void OnPropCange(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var src = (WritableMessageBox)sender;
			switch (e.Property.Name)
			{
				case nameof(WriteText):
					{
						src.WriteText = (string)e.NewValue;
						break;
					}
				case nameof(Message):
					{
						src.Message = (string)e.NewValue;
						break;
					}
				default:
					{
						throw new Exception("Unecaption behaviour");
					}
			}
		}

		private void Ok_BtnClick(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void Cncl_BtnClick(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
		}
	}
}
