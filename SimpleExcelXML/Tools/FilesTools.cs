﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.Tools
{
	static class FilesTools
	{

		internal static void CheckDir(this string pathDir)
		{
			CheckAndCreatDir(pathDir);
		}

		public static void CheckAndCreatDir(string pathDir)
		{
			if (!Directory.Exists(pathDir))
				Directory.CreateDirectory(pathDir);
		}

		public static void CheckTxtFile(this string pathFile)
		{
			CheckAndCreatTxtFile(pathFile);
		}

		public static void CheckAndCreatTxtFile(string pathFile)
		{
			if (!File.Exists(pathFile))
				File.CreateText(pathFile);
		}
	}
}
