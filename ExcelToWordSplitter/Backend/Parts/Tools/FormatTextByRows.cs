﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Document.NET;

namespace ExcelToWordSplitter.Backend.Parts.Tools
{
	/// <summary>
	/// Форматирует положение текста по всем колонкам в Cols в строке Index1
	/// </summary>
	public class FormatTextByRows : IndexerBase
	{
		public TextDirection? TextDirection = null;
		public VerticalAlignment? VerticalAlignment = null;
		public List<int> Cols = new List<int>();
	}
}
