﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools
{
	public class SystemTools
	{
		public static IList GenerateDynamicTypeList(Type t)
		{
			switch (t.Name.ToLower())
			{
				default:
					{
						return typeof(List<>).MakeGenericType(t).GetConstructor(new Type[] { }).Invoke(new object[] { }) as IList;
					}
				case "int":
				case "int32":
					{
						return new List<Nullable<int>>();
					}
				case "string":
					{
						return new List<string>();
					}
				case "float":
				case "double":
					{
						return new List<double?>();
					}
			}
		}

		public static T CloneDinamic<T>(object el)
		{
			return (T)el.GetType().GetConstructor(new Type[] { }).Invoke(new object[] { });
		}
	}
}
