﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Parts
{
	public interface IReportBook : ICoreReportBook, IList<IReportSheet>
	{

		//string Path { get; set; }

		//IReportSheet this[string name] { get; }
		IReportSheet this[string name] { get; }

		void AddRange(IEnumerable<IReportSheet> sheets);
	}	
}
