﻿using SimpleCore.Objs;
using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Templates
{
	public class ReportLineTmpl : List<object>, ISimpleConvert<IReportLine>, IReportLine
	{
		public ReportLineTmpl(IEnumerable<object> collection)
		{
			if (collection != null)
				AddRange(collection);
		}

		public ReportLineTmpl()
		{

		}

		public IReportLine Convert()
		{
			return this;
		}
	}
}
