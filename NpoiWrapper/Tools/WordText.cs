﻿using NPOI.XWPF.UserModel;
using NpoiWrapper.Interfaces;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpoiWrapper.Tools
{

	public class WordText: IDataSource
	{
		public List<string> Sentences;
		
		public WordText()
		{
			Sentences = new List<string>();
		}
	}
}
