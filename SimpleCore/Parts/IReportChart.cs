﻿using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Parts
{
	public interface IReportChart<T>
		where T: IReportChartItem
	{

		SimpleFontFamily TitleSettings { get; set; }

		/// <summary>
		/// Заголовок графика.
		/// </summary>
		string Title { get; set; }

		SimpleFontFamily TitleXSettings { get; set; }

		/// <summary>
		/// Подпись оси X
		/// </summary>
		string TitleX { get; set; }

		SimpleFontFamily TitleYSettings { get; set; }

		/// <summary>
		/// Подпись оси Y
		/// </summary>
		string TitleY { get; set; }

		/// <summary>
		/// Подпись маркеров по оси X. Если их на 1 больше чем строк данных, то надписи вставляются по краям диапазонов.
		/// </summary>
		List<string> AxesXTitles { get; set; }

		/// <summary>
		/// Элементы данного графика.
		/// </summary>
		List<T> Items { get; set; }

		/// <summary>
		/// Куда выводить график.
		/// </summary>
		Rectangle GraphRect { get; set; }

		/// <summary>
		/// Минимальное значение по оси X.
		/// </summary>
		double MinX { get; set; }

		/// <summary>
		/// Максимальные значение по оси X.
		/// </summary>
		double MaxX { get; set; }

		/// <summary>
		/// Максимальные значение по оси Y.
		/// </summary>
		double MinY { get; set; }

		/// <summary>
		/// Максимальные значение по оси Y.
		/// </summary>
		double MaxY { get; set; }

		bool IsAxisYRevers { get; set; }

		bool YAxisHasMinorGridLines { get; set; }

		bool XAxisHasMinorGridLines { get; set; }

		SimpleFontFamily LegendSettings { get; set; }

		bool ShowLegend { get; set; }

		Bitmap Pic { get; set; }

		Color YMajorGridLinesColor { get; set; }
		Color YMinorGridLinesColor { get; set; }

		Color XMajorGridLinesColor { get; set; }
		Color XMinorGridLinesColor { get; set; }

		Color YAxisLinesColor { get; set; }
		Color XAxisLinesColor { get; set; }

		SimpleFontFamily YLabelsSettings { get; set; }
		SimpleFontFamily XLabelsSettings { get; set; }

		bool HasXMajorTickMark { get; set; }
		bool HasYMajorTickMark { get; set; }

		double XMajorUnits { get; set; }
		double YMajorUnits { get; set; }

		/// <summary>
		/// Не работает: #,##; #.##;
		/// Работает: #0.00 ; #0,00
		/// </summary>
		string XNumberFormat { get; set; }
		/// <summary>
		/// Не работает: #,##; #.##;
		/// Работает: #0.00 ; #0,00
		/// </summary>
		string YNumberFormat { get; set; }

	}
}
