﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Interfaces;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{

	/// <summary>
	/// Не посредственно класс для
	/// </summary>
	internal class Range : XmlPropBase
	{
		public static bool IsRange(string Name)
		{
			var lowerName = Name.ToLower();
			if (lowerName.Contains("v") || lowerName.Contains("numlit") || lowerName.Contains("strref") || lowerName.Contains("numref"))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private string SheetName = null;
		private string RefRange;
		private List<string> RealValues;
		public bool IsStr { get; set; }
		public bool IsNumList { get { return RealValues != null && RealValues.Count > 0 && !IsStr; } } 

		public Range(string str, string sheet = null, bool isStr = false)
		{
			this.IsStr = isStr;
			if (str == null)
			{
				RefRange = null;
				RealValues = null;
			}
			else if (String.IsNullOrEmpty(str.Trim()))
			{
				RefRange = null;
				RealValues = null;
			}
			else if (str.Contains("{") && str.Contains("}"))
			{
				ParseRealValues(str);
			}
			else if (str.Contains("!") && str.Contains(":"))
			{
				ParseRefValues(str);
				SheetName = sheet;
			}
			else if (str.Contains(":") && XmlTools.alphabet.Any((v) => str.ToUpper().Contains(v)))
			{
				if (sheet == null || String.IsNullOrEmpty(sheet.Trim()))
				{
					throw new Exception("Miss sheet name");
				}

				SheetName = sheet;
				ParseRefValues(str);
			}
			else
			{
				RefRange = null;
				RealValues = new List<string>() { str };
			}
		}

		public Range(XElement xRange) : base(xRange)
		{
		}

		public bool IsEmpty()
		{
			return RefRange == null && RealValues == null;
		}

		private void ParseRefValues(string str)
		{
			RealValues = null;
			RefRange = str;
		}

		private void ParseRealValues(string str)
		{
			RefRange = null;
			RealValues = str.Split(new string[] { "{", "}", ";" }, StringSplitOptions.None).ToList();

			double test = -1;
			foreach (var el in RealValues)
			{
				if (!String.IsNullOrEmpty(el) &&
					!double.TryParse(
						el,
						System.Globalization.NumberStyles.Any,
						System.Globalization.CultureInfo.InvariantCulture,
						out test)
				)
				{
					throw new Exception($"Can't convert user value {el} to number in list of numbers");
				}
			}
		}

		public override void Save(XmlWriterWrapper doc)
		{
			if (RefRange == null && RealValues != null)
			{
				if (RealValues?.Count == 1 && !IsNumList)
				{
					doc.OpenPrefElement("v", "c", RealValues.First()).CloseElement();
				}
				else
				{
					doc.OpenPrefElement("numLit", "c");
					{
						doc.OpenPrefElement("formatCode", "c", "General").CloseElement();

						doc.OpenPrefElement("ptCount", "c").AddAtribute("val", RealValues.Where((v) => !String.IsNullOrEmpty(v)).Count().ToString()).CloseElement();

						var id = 0;
						foreach (var pt in RealValues)
						{
							if (!String.IsNullOrEmpty(pt))
							{
								doc.OpenPrefElement("pt", "c").AddAtribute("idx", id.ToString())

									.OpenPrefElement("v", "c", pt.ToString()).CloseElement()

									.CloseElement();
							}
							else
							{
								if (id == 0)
								{
									id--;
								}
							}
							id++;
						}
					}
					doc.CloseElement();
				}
			}
			else if (IsStr && RealValues == null && RefRange != null)
			{
				if (SheetName != null)
				{
					doc.OpenPrefElement("strRef", "c").OpenPrefElement("f", "c", $"'{SheetName}'!{RefRange}").CloseElement().CloseElement();
				}
				else
				{
					doc.OpenPrefElement("strRef", "c").OpenPrefElement("f", "c", RefRange).CloseElement().CloseElement();
				}
			}
			else if (!IsStr && RealValues == null && RefRange != null)
			{
				if (SheetName != null)
				{
					doc.OpenPrefElement("numRef", "c").OpenPrefElement("f", "c", $"'{SheetName}'!{RefRange}").CloseElement().CloseElement();
				}
				else
				{
					doc.OpenPrefElement("numRef", "c").OpenPrefElement("f", "c", RefRange).CloseElement().CloseElement();
				}
			}
		}

		public override string ToString()
		{
			if (RealValues != null)
			{
				return $"{{{String.Join(";", RealValues)}}}";
			}
			else
			{
				return RefRange;
			}
		}

		public override void TryLoad(XElement root)
		{			
			var name = root.Name.LocalName.Replace("c:", "").ToLower();

			if (name == "numlit")
			{
								
				var c = root.GetNamespaceOfPrefix("c");
				RealValues = new List<string>();

				int oldId = -1, newId = -1;
				var pts = root.Elements(c + "pt");
				foreach (var pt in pts)
				{
					var id = pt.Attribute("idx").Value;
					var val = pt.Element(c + "v").Value;

					if (oldId == -1)
						oldId = id.ToInt();
					newId = id.ToInt();

					if(newId - oldId > 1)
					{
						for (int i = oldId + 1; i < newId; i++)
						{
							RealValues.Add("");
						}
						//RealValues.Add(val);
						//oldId = newId;
					}					

					RealValues.Add(val);
					oldId = newId;
				}

			}
			else if (name == "strref")
			{
				
				ParseRef("strRef", root);
			}
			else if (name == "numref")
			{
				
				ParseRef("numRef", root);
			}
			else if (name == "v")
			{
				
				RealValues = new List<string>
				{
					root.Value
				};
			}
			else
			{
				throw new Exception("Miss range format");
			}
		}

		private void ParseRef(string name, XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");
			var refVal = root.Element(c + "f")?.Value;

			if (refVal == null)
			{
				return;
			}
			else if (refVal.Contains("!"))
			{
				var parts = refVal.Split('!');
				SheetName = parts[0].Replace("'", "");
				RefRange = parts[1];
			}
			else
			{
				RefRange = refVal;
			}
		}
	}

	/// <summary>
	/// Содержит данные для графика разного типа: от обычных значений до текстовых меток. Обёртка
	/// </summary>
	internal class TypedRange : XmlPropBase
	{
		public static bool IsTypedRange(string name)
		{
			var tmp = name.Replace("c:", "").ToLower();

			if (tmp.Contains("val") || tmp.Contains("cat") || tmp.Contains("tx"))
			{
				return true;
			}

			return false;
		}

		public static bool IsTitle(string name)
		{
			var tmp = name.Replace("c:", "").ToLower();

			if (tmp.Contains("tx"))
			{
				return true;
			}

			return false;
		}

		public enum Type
		{
			xVal, yVal, val, tx,
			cat
		}

		public Type type;
		private Range range;

		public bool IsY
		{
			get
			{
				return type == Type.yVal || type == Type.val;
			}
		}
		public bool IsX
		{
			get
			{
				return type == Type.xVal || type == Type.cat;
			}
		}
		public bool IsInLine
		{
			get
			{
				return range.IsNumList;
			}
		}
		public string Values
		{
			get
			{
				return range.ToString();
			}
		}

		public TypedRange(Type t, string rangeX, bool isStr = false, string sheetName = "")
		{
			type = t;
			range = new Range(rangeX, sheetName, isStr);
		}

		public TypedRange(XElement xTypeRange) : base(xTypeRange)
		{
		}

		public override void Save(XmlWriterWrapper doc)
		{
			if (range.IsEmpty())
			{
				return;
			}

			CreateNode(doc);

			range?.Save(doc);

			doc.CloseElement();
		}

		private void CreateNode(XmlWriterWrapper doc)
		{
			switch (type)
			{
				case Type.tx:
					{
						doc.OpenPrefElement("tx", "c");
						break;
					}
				case Type.val:
					{
						doc.OpenPrefElement("val", "c");
						break;
					}
				case Type.yVal:
					{
						doc.OpenPrefElement("yVal", "c");
						break;
					}
				case Type.xVal:
					{
						doc.OpenPrefElement("xVal", "c");
						break;
					}
				case Type.cat:
					{
						doc.OpenPrefElement("cat", "c");
						break;
					}
			}
		}		

		public bool IsEmpty()
		{
			return range.IsEmpty();
		}

		public override void TryLoad(XElement root)
		{
			type = ParseType(root.Name.LocalName);

			var xRange = root.Elements().FirstOrDefault((v) => Range.IsRange(v.Name.LocalName));
			if (xRange.HasInfo())
			{
				range = new Range(xRange);
			}
		}	

		private Type ParseType(string str)
		{
			var tmp = str.Replace("c:", "");
			var res = Type.val;

			if (Enum.TryParse(str, out res))
				return res;

			return Type.val;
		}
	}
}
