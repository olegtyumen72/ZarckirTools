﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpoiWrapper.Tools
{
	public class ImageSrc
	{

		public Bitmap Pic;
		public string Id;
		public string Text;
		public bool IsNumeric;
		public int Num;

		public MemoryStream MemStream
		{
			get
			{
				var mem = new MemoryStream();
				Pic.Save(mem, ImageFormat.Jpeg);
				mem.Seek(0, SeekOrigin.Begin);
				return mem;
			}
		}

		public ImageSrc(Bitmap src, string name, string txt = "", bool hasNumber = false, int number = -1)
		{
			Pic = src;
			Text = txt;
			IsNumeric = hasNumber;
			Id = name;
			Num = number;
		}

	}
}
