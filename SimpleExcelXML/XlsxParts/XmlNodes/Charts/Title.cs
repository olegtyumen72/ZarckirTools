﻿using SimpleExcelXML.Tools;
using SimpleExcelXML.XlsxParts.XmlNodes.Properties;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Charts
{
	internal class Title
	{

		internal Layout layout { get; set; } = Layout.DefaultLayout;
		bool overlay;
		public Tx Text { get; set; } = null;
		public SpPr spPr { get; set; } = null;
		public TxPr txPr { get; set; } = null;
		public string Prefix { get; set; } = "c";

		public Title(string title = "", string pref = "c", bool overlay = false)
		{
			Prefix = pref;
			this.overlay = overlay;

			if(!String.IsNullOrEmpty(title))
			{
				txPr = TxPr.DefaultTxPr;
				var rpr = new Rpr(txPr.def);
				Text = new Tx(title)
				{
					Body = txPr?.body,
					pPr = txPr?.def,
					Style = LstStyle.Default,
					rPr = rpr
				};
			}			
		}

		public Title(string title, SimpleFontFamily font):this(title)
		{
			if (Text.pPr != null)
			{
				Text.pPr.b = font.Bold ? 1 : 0;
				Text.pPr.i = font.Italic ? 1 : 0;
				Text.pPr.u = font.Underline ? Underline.sng : Underline.miss;
				Text.pPr.sz = font.Size * 100;
				Text.pPr.Font = font.FontName;
				Text.pPr.baseline = -1;
				Text.pPr.spc = -1;
				Text.pPr.strike = null;
			}

			if (Text.rPr != null)
			{
				Text.rPr.b = font.Bold ? 1 : 0;
				Text.rPr.i = font.Italic ? 1 : 0;
				Text.rPr.u = font.Underline ? Underline.sng : Underline.miss;
				Text.rPr.sz = font.Size * 100;
				Text.rPr.Font = font.FontName;
				Text.rPr.baseline = -1;
				Text.rPr.spc = -1;
				Text.rPr.strike = null;
			}
		}

		public Title(XElement title)
		{
			TryLoad(title);
		}

		public void TryLoad(XElement root)
		{
			var c = root.GetNamespaceOfPrefix("c");
			var lay = root.Element(c + "layout");
			if(lay != null && (lay.HasAttributes || lay.HasElements))
			{

			}

			var overlay = root.Element(c + "overlay");
			if(overlay != null && (overlay.HasElements || overlay.HasAttributes))
			{

			}

			var xSpPr = root.Element(c + "spPr");
			if(xSpPr != null && !xSpPr.IsFullEmpty())
			{
				spPr = new SpPr(xSpPr);
			}

			var xTxt = root.Element(c + "tx");
			if (xTxt != null && !xTxt.IsFullEmpty())
				Text = new Tx(xTxt);

			var xTxPr = root.Element(c + "txPr");
			if (xTxPr != null && !xTxPr.IsFullEmpty())
				txPr = new TxPr(xTxPr);		
		}

		public void Save(XmlWriterWrapper doc)
		{

			doc.OpenPrefElement("title", Prefix);

			Text?.Save(doc);

			layout.Save(doc);

			doc.OpenPrefElement(nameof(overlay), "c").AddAtribute("val", (overlay ? 1 : 0).ToString()).CloseElement();

			spPr?.Save(doc);

			txPr?.Save(doc);

			doc.CloseElement();
		}

	}
}
