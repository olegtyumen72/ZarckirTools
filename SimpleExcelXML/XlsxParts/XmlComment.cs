﻿using SimpleExcelXML.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.XlsxParts
{
	internal class XmlComment : IXmlVisibleFile
	{
		public string Id { get; set; }
		public string RelativePath { get; }
		public string File { get; }
		public string AbsolutePath { get; set; }

		public XmlComment(int id, string pathToDir)
		{
			Id = id.ToString();
			RelativePath = "xl\\";
			File = $"comments{Id}.xml";
			AbsolutePath = pathToDir;
		}



		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}

		public string RelativeToFile()
		{
			return Path.Combine(RelativePath, File);
		}

		public void Save()
		{
			throw new NotImplementedException();
		}
	}
}
