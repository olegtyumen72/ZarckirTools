﻿using SimpleCore;
using SimpleCore.Objs;
using SimpleCore.Parts;
using SimpleCsv.Parts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleCsv
{
	public class SimpleCsv
	{
		public static event Action<int> UpdateMaxState;
		public static event Action<int> UpdateCurState;
		public static event Action<double> UpdateProcState;
		public static event Action WorkEnd;


		static SaveFileDialog _dlg = new SaveFileDialog()
		{
			AddExtension = true,
			Filter = "Separated file|*.csv;*.tsv;*.dsv;*.ssv|All files|*.*",
			Title = "Saving"
		};

		static void EqualLines(ReportSheet sheet)
		{
			var maxCnt = ((List<ReportLine>)sheet).Max((v) => v.Count);

			foreach (var el in sheet)
			{
				while (el.Count < maxCnt)
				{
					el.Add("");
				}
			}
		}

		public static void Save(ReportBook rb)
		{
			var dir = Path.GetDirectoryName(rb.Path);

			int cur = 0;
			int max = rb.Sheets.Sum((v) => v.Count);
			UpdateMaxState?.Invoke(max);

			//if (rb.Separator == null) rb.Separator = Separators.Semicolon;
			foreach (var sheet in rb)
			{

				using (var str = new StreamWriter(
					File.Open(
						Path.Combine(dir, sheet.Name + rb.Extentious),
						FileMode.OpenOrCreate
						)
					))
				{
					for (int i = 0; sheet.Count > i; i++)
					{
						var line = sheet[i];
						var strLine = String.Join(rb.StrSeparator, line);
						str.WriteLine(strLine);
					}

					UpdateCurState?.Invoke(++cur);
					UpdateProcState?.Invoke((cur / max) * 100);
				}

				EqualLines(sheet);

			}

			WorkEnd?.Invoke();
		}

		public static void Save(ReportBook rb, string path)
		{
			rb.Path = path;
			Save(rb);
		}

		public static string SaveAs(ReportBook rb, string path = "")
		{
			bool ok = false;

			while (!ok)
			{
				if (path == "")
				{
					if (_dlg.ShowDialog() == DialogResult.OK)
					{
						path = _dlg.FileName;
					}
					else
						return "";
				}

				try
				{
					Save(rb, path);
					ok = true;
				}
				catch (Exception ex)
				{
					path = "";
					string mes = "Error saving data to excel file. Choose another file or press 'Cancel' to cancel saving.\r\nError details: " + ex.Message;
					if (MessageBox.Show(mes, "Save file", MessageBoxButtons.OKCancel) == DialogResult.Cancel) ok = true;
				}
			}

			return path;
		}

		public static Task SaveAsync(ReportBook rb)
		{
			throw new NotImplementedException();
		}

		public static Task SaveAsync(ReportBook rb, string path)
		{
			rb.Path = path;
			return SaveAsync(rb);
		}

		internal static ReportBook Load(string path, Separators sep)
		{
			var rb = new ReportBook(Path.GetDirectoryName(path))
			{
				Separator = sep
			};
			rb.Add(new ReportSheet(Path.GetFileNameWithoutExtension(path)));
			var rs = rb[0];

			var allText = File.ReadAllText(path);
			var allLines = allText.Split(new string[] { "\r\n" }, StringSplitOptions.None);

			foreach (var line in allLines)
			{
				var tmp = line.Split(new string[] { rb.StrSeparator }, StringSplitOptions.None);
				rs.Add(new ReportLine(tmp));
			}
			//using (var str = new StreamReader(File.OpenRead(path)))
			//{
			//	while (!str.EndOfStream)
			//	{
			//		string tmp = "";
			//		do
			//		{
			//			tmp += str.ReadLine();
			//		}
			//		while (tmp[tmp.Length - 1].ToString() != rb.StrSeparator);
			//		var splited = tmp.Split(new string[] { rb.StrSeparator }, StringSplitOptions.None);
			//		var line = new ReportLine(splited);
			//		rs.Add(line);
			//	}
			//}

			EqualLines(rs);

			return rb;
		}
	}

	public class SimpleCsvObj : ISimpleCore<ReportBook, ReportSheet, ReportLine>
	{
		public event Action<int> UpdateMaxState;
		public event Action<int> UpdateCurState;
		public event Action<double> UpdateProcState;
		public event Action WorkEnd;
		public Separators Separator { get; set; }
		public ReportBook Book { get; set; }
		public SimpleConverter<IReportBook, ReportBook> RbConv { get; }
		public SimpleConverter<IReportSheet, ReportSheet> RsConv { get; }
		public SimpleConverter<IReportLine, ReportLine> RlConv { get; }

		public void Dispose()
		{

		}

		public SimpleCsvObj()
		{
			RsConv = new SimpleConverter<IReportSheet, ReportSheet>((v) => new ReportSheet(v), (v) => v.Convert());
			RlConv = new SimpleConverter<IReportLine, ReportLine>((v) => (ReportLine)v, (v) => v);
			RbConv = new SimpleConverter<IReportBook, ReportBook>((v) => new ReportBook(v), (v) => v.Convert());
		}

		public SimpleCsvObj(Separators sep) : this()
		{
			Separator = sep;

		}

		public IReportBook Load(string path)
		{
			Book = SimpleCsv.Load(path, Separator);
			return RbConv.Conv(Book);
		}

		ReportBook TryGetMainType(object obj)
		{
			return obj is ReportBook ? (ReportBook)obj : obj is IReportBook ? new ReportBook((IReportBook)obj) : null;
		}

		void CheckBook(ReportBook rObj, Action<ReportBook> act)
		{
			if (rObj == null) throw new AggregateException("Miss type");

			act?.Invoke(rObj);
		}

		public void Save(object obj)
		{
			var rObj = TryGetMainType(obj);
			CheckBook(rObj, (v) => SimpleCsv.Save(v));
		}

		public void Save(object obj, string path)
		{
			var rObj = TryGetMainType(obj);
			CheckBook(rObj, (v) => SimpleCsv.Save(v, path));

		}

		public void SaveAs(object obj, string path = "")
		{
			var rObj = TryGetMainType(obj);
			CheckBook(rObj, (v) => SimpleCsv.Save(v, path));
		}

		public Task SaveAsync(object obj)
		{
			var rObj = TryGetMainType(obj);
			Task res = null;

			CheckBook(rObj, (v) => res = SimpleCsv.SaveAsync(v));

			return res;
		}

		public Task SaveAsync(object obj, string path)
		{
			var rObj = TryGetMainType(obj);
			Task res = null;

			CheckBook(rObj, (v) => res = SimpleCsv.SaveAsync(v, path));

			return res;
		}

		public IReportSheet UpdateReportSheetWithTranslater(IReportSheet rs, ITranslater tr = null)
		{
			throw new NotImplementedException();
		}
	}

	public static class SimpleCsvBoost
	{
		public static Separators ToSep(this string src)
		{
			Separators res = Separators.Colon;
			if (Enum.TryParse(src, out res))
			{
				return res;
			}
			else
			{
				switch (src)
				{
					case ":":
						{
							return Separators.Colon;
						}
					case ";":
						{
							return Separators.Semicolon;
						}
					case ",":
						{
							return Separators.Comma;
						}
					case ".":
						{
							return Separators.Dot;
						}
					case "Tab":
					case "\t":
						{
							return Separators.Tab;
						}
					default:
						{
							throw new Exception("Miss separator");
						}
				}
			}
		}
	}
}
