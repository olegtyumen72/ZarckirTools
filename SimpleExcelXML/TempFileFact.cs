﻿using SimpleCore.Templates;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SimpleExcelXML.XlsxParts
{
	class TempFileFact
	{
		Dictionary<string, string> xp = new Dictionary<string, string>()
		{
			{"vec", "vt:vector" },
			{"var", "vt:variant"},
			{ "lpstr", "vt:lpst" },
			{"i4", "vt:i4" },
			{"relat", "Relationship" },
			{"ts", "Types" },
			{"def", "Default" },
			{"over", "Override" },
		};

		Dictionary<string, string> xa = new Dictionary<string, string>()
		{
			{"s", "size" },
			{"BT", "baseType" },
			{"id", "Id" },
			{"Id", "Id" },
			{"ID", "Id" },
			{"t", "Type"},
			{"trt", "Target" },
			{ "contT", "ContentType" },
			{"ext", "Extension" },
			{"prtN", "PartName" },
			{"extProp", "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" },
			{"vTypes",  "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes" },
			{"extRelProp","http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"  },
			{"relatsProp", "http://schemas.openxmlformats.org/package/2006/relationships" },
			{ "coreProp", "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties" },
			{ "offProp", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" },
			{ "contTs", "http://schemas.openxmlformats.org/package/2006/content-types" },
			{"relatCT", "application/vnd.openxmlformats-package.relationships+xml" },
			{"xmlCT", "application/xml" },
			{"mainCT", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml" },
			{"worksheetCT", "application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml" },
			{"themeCT", "application/vnd.openxmlformats-officedocument.theme+xml" },
			{"stylesCT", "application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml" },
			{"sharedStringsCT", "application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml" },
			{"corePropCT", "application/vnd.openxmlformats-package.core-properties+xml" },
			{"extPropCT", "application/vnd.openxmlformats-officedocument.extended-properties+xml" }
		};

		const string docProps = "\\docProps";
		const string rels = "\\_rels";
		const string conTps = "\\";
		const string app = "app.xml";
		const string core = "core.xml";
		const string relF = ".rels";
		const string conTpsF = "[Content_Types].xml";
		XDeclaration declaration { get; } =
		XDocument appX;
		XDocument coreX;
		XDocument relsX;
		XDocument contTs;

		public void CreateDocProps(string dirXlsx, ReportBookTmpl rb)
		{
			var root = Path.Combine(dirXlsx, docProps);
			var appP = Path.Combine(root, app);
			var coreP = Path.Combine(root, core);

			if (!Directory.Exists(root))
				Directory.CreateDirectory(root);


			appX = CreateApp(rb);
			coreX = CreateCore();

			if (File.Exists(appP))
				File.Delete(appP);
			if (File.Exists(coreP))
				File.Delete(coreP);

			appX.Save(appP);
			coreX.Save(coreP);
		}

		public void CreateRels(string dirXlsx)
		{
			var relsDir = Path.Combine(dirXlsx, rels);
			var relsF = Path.Combine(relsDir, relF);
			if (!Directory.Exists(relsDir))
				Directory.CreateDirectory(relsDir);

			if (File.Exists(relsF))
				File.Delete(relsF);
			File.CreateText(relsF);

			relsX = CreateRels();
		}

		public void CreateContentTypes(string dirXlsx)
		{
			var dirCt = Path.Combine(dirXlsx, conTps);
			if (!Directory.Exists(dirCt))
				Directory.CreateDirectory(dirCt);

			var ctF = Path.Combine(dirCt, conTpsF);
			if (File.Exists(ctF))
				File.Delete(ctF);
			File.CreateText(ctF);

			contTs = CreateCTs();
		}

		
		XDocument CreateRels()
		{
			var tag = "rId";
			var cnt = 1;

			var rels = new XDocument()
			{
				Declaration = declaration
			};
			var relat = CreateElement("Relationships", "", new Dictionary<string, string>()
			{
				{"xmlns", xa["relatsProp"] }
			});
			rels.Add(relat);

			rels.Add(newRelation(tag, cnt++, "xl/workbook.xml", xa["offProp"]));
			rels.Add(newRelation(tag, cnt++, "docProps/core.xml", xa["coreProp"]));
			rels.Add(newRelation(tag, cnt++, "docProps/app.xml", xa["extRelProp"]));

			return rels;
		}

		XDocument CreateApp(ReportBookTmpl rb)
		{
			
		}

		XDocument CreateCore()
		{
			var doc = new XDocument()
			{
				Declaration = declaration
			};

			var cpProp = CreateElement("cp:coreProperties", "", new Dictionary<string, string>()
			{
				{ "xmlns:cp", "http://schemas.openxmlformats.org/package/2006/metadata/core-properties" },
				{ "xmlns:dc", "http://purl.org/dc/elements/1.1/" },
				{ "xmlns:dcterms", "http://purl.org/dc/terms/" },
				{ "xmlns:dcmitype", "http://purl.org/dc/dcmitype/" },
				{ "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance" }
			});
			doc.Add(cpProp);

			var element = CreateElement("dc:creator");
			cpProp.Add(element);

			element = CreateElement("cp:lastModifiedBy");
			cpProp.Add(element);

			element = CreateElement("dcterms:created", DateTime.Now.ToLongDateString(), new Dictionary<string, string>()
			{
				{"xsi:type", "dcterms:W3CDTF" }
			});
			cpProp.Add(element);

			element = CreateElement("dcterms:modified", "$", new Dictionary<string, string>()
			{
				{"xsi:type",  "dcterms:W3CDTF" }
			});
			cpProp.Add(element);

			return doc;
		}

		

	}
}
