﻿using SimpleCore.Parts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Objs
{
	public class ReportSheetObj : List<IReportLine>, IReportSheet
	{
		public string Name { get; set; }
		public object Tag { get; set; }
		public List<IReportFormatItem<ICellRange, IConditionalFormat>> Formats { get; set; }
		public ITranslater Translater { get; set; }
		public List<ICellRange> cells_tr { get; set; }
		public List<IReportChart<IReportChartItem>> Graphs { get; set; }
		public Color CellsBg { get; set; } = Color.Empty;
		public Color CellsFont { get; set; } = Color.Empty;
		/// <summary>
		/// Авто-выбор ширины колонки для всех столбцов
		/// </summary>
		public bool AutoFit { get; set; } = false;

		public IReportLine GetLine(int row)
		{
			return this[row];
		}

		public ReportSheetObj(string name, Color cBg,Color CFont, object tag = null, IEnumerable<IReportLine> collect = null, IEnumerable<IReportFormatItem<ICellRange, IConditionalFormat>> formats = null, IEnumerable<ICellRange> for_translate = null, ITranslater translater = null, bool auto = false)
		{
			Name = name;
			Tag = tag;
			if (collect != null)
				AddRange(collect);
			Formats = new List<IReportFormatItem<ICellRange, IConditionalFormat>>();
			if (formats != null)
				Formats.AddRange(formats);
			cells_tr = new List<ICellRange>();
			if (for_translate != null)
				cells_tr.AddRange(for_translate);
			
			Translater = translater;
		}
	}
}
