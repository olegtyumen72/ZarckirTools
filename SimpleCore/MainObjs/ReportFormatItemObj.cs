﻿using SimpleCore.Parts;
using SimpleFonts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCore.Objs
{
	public class ReportFormatItemObj : IReportFormatItem<ICellRange, IConditionalFormat>
	{
		public ReportFormatItemObj(ICellRange range, IEnumerable<IConditionalFormat> formats = null)
		{
			Range = range;
			ConditionalFormats = new List<IConditionalFormat>();
			if (formats != null)
				ConditionalFormats.AddRange(formats);
		}

		public int Col { get; set; }
		public int Row { get; set; }
		public string NFormat { get; set; }
		public SimpleFontFamily FontFormat { get; set; }
		public bool BordersAll { get; set; }
		public bool HorizontalAligmentCenter { get; set; }
		public bool VerticalAligmentCenter { get; set; }
		public bool HideColumns { get; set; }
		public bool HideRows { get; set; }
		public ICellRange Range { get; set; }
		public bool IsSingle { get; set; }
		public double ColumnWidth { get; set; }
		public double RowHeight { get; set; }
		public bool MergeArea { get; set; }
		public bool MergeColumns { get; set; }
		public int ColumnsOrientation { get; set; }
		public int RowOrientation { get; set; }
		public bool Wrap { get; set; }
		public List<IConditionalFormat> ConditionalFormats { get; set; }
		public bool AllCols { get; set; }
		public bool AllRows { get; set; }
		public Color Background { get; set; }		

		public IReportFormatItem<ICellRange, IConditionalFormat> Convert()
		{
			return this;
		}
	}
}
