﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Interfaces;
using SimpleExcelXML.Tools;

namespace SimpleExcelXML.XlsxParts
{

	/// <summary>
	/// Файл отвечает за то, в какой области будет отображён график + где будет область + размер
	/// </summary>
	internal class XmlDrawing : IXmlVisibleFile
	{
		public string Id { get; set; }
		public string RelativePath { get; } = "xl\\drawings";
		public string File { get; }
		public string AbsolutePath { get; set; }
		public int FileId { get; set; }
		/// <summary>
		/// Нужно для выгрузки - связка chart.xml -> Reportchart
		/// </summary>
		public Dictionary<IXmlVisibleFile, ReportChart> Charts { get; set; } = new Dictionary<IXmlVisibleFile, ReportChart>();

		/// <summary>
		/// Рельса контролирующая файл
		/// </summary>
		private XmlRels rels;
		//public XmlRels Draw { get; set; }		
		
		/// <summary>
		/// Области для графиков
		/// </summary>
		public Dictionary<string,Rectangle> ChartAreas { get; private set; }

		private int grpthIds = 1;

		public XmlDrawing(string path, int fileId, XmlRels localRels)
		{
			//Id = $"rId{chart.rId}";
			AbsolutePath = path;
			File = $"drawing{fileId}.xml";
			//Chart = chart;
			rels = localRels;
		}

		public XmlDrawing(string path, string file, XmlRels localRels)
		{
			AbsolutePath = path;
			File = file;
			rels = localRels;
		}

		public string AbsoluteToFile()
		{
			return Path.Combine(AbsolutePath, RelativePath, File);
		}

		public string RelativeToFile()
		{
			return "../" + Path.Combine(RelativePath, File);
		}

		public string ContTypePath()
		{
			return Path.Combine(RelativePath, File);
		}

		public void Save()
		{
			var dir = Path.Combine(AbsolutePath, RelativePath);
			dir.CheckDir();

			var file = Path.Combine(dir, File);
			using (var doc = new XmlWriterWrapper(file))
			{
				CreateDoc(doc);
				doc.Save();
			}
		}

		public void Add(IXmlVisibleFile chartFile, ReportChart chart)
		{
			Charts.Add(chartFile, chart);
		}

		private void CreateDoc(XmlWriterWrapper doc)
		{
			XmlTools.DefaultDeclaration(doc);

			doc.OpenElement("wsDr", null, XmlTools.xa["xdr"], "xdr").
				AddXmlnsAtr("a", XmlTools.xa["a"]);
			//AddAtribute("a", null, "a", XmlTools.xa["a"])

			foreach (var elem in Charts)
			{
				var Chart = elem.Value;

				doc.OpenElement("twoCellAnchor", null, XmlTools.xa["xdr"], "xdr")

				.OpenElement("from", null, XmlTools.xa["xdr"], "xdr");

				//var area = Chart.GraphRect;
				//area.X /= 100;
				//area.Y /= 100;
				//area.Width /= 100;
				//area.Height /= 100;

				doc.OpenElement("col", Chart.GraphRect.X.ToString(), XmlTools.xa["xdr"], "xdr").CloseElement()
				.OpenElement("colOff", "0", XmlTools.xa["xdr"], "xdr").CloseElement()
				.OpenElement("row", Chart.GraphRect.Y.ToString(), XmlTools.xa["xdr"], "xdr").CloseElement()
				.OpenElement("rowOff", "0", XmlTools.xa["xdr"], "xdr").CloseElement()

				.CloseElement()
				.OpenElement("to", null, XmlTools.xa["xdr"], "xdr")



				.OpenElement("col", (Chart.GraphRect.Right).ToString(), XmlTools.xa["xdr"], "xdr").CloseElement()
				.OpenElement("colOff", "0", XmlTools.xa["xdr"], "xdr").CloseElement()
				.OpenElement("row", (Chart.GraphRect.Bottom).ToString(), XmlTools.xa["xdr"], "xdr").CloseElement()
				.OpenElement("rowOff", "0", XmlTools.xa["xdr"], "xdr").CloseElement()

				.CloseElement()
				.OpenElement("graphicFrame", null, XmlTools.xa["xdr"], "xdr")

				.OpenElement("nvGraphicFramePr", null, XmlTools.xa["xdr"], "xdr");

				doc.OpenElement("cNvPr", null, XmlTools.xa["xdr"], "xdr")
						.AddAtribute("id", $"{grpthIds}")
						.AddAtribute("name", $"{grpthIds++}")
						.CloseElement();

				doc.OpenElement("cNvGraphicFramePr", null, XmlTools.xa["xdr"], "xdr").CloseElement()
				.CloseElement()

				.OpenElement("xfrm", null, XmlTools.xa["xdr"], "xdr")

				.OpenElement("off", null, XmlTools.xa["a"], "a")
				.AddAtribute("x", "0").AddAtribute("y", "0")
				.CloseElement()

				.OpenElement("ext", null, XmlTools.xa["a"], "a")
				.AddAtribute("cx", "0").AddAtribute("cy", "0")
				.CloseElement()

				.CloseElement()

				.OpenElement("graphic", null, XmlTools.xa["a"], "a")

				.OpenElement("graphicData", null, XmlTools.xa["a"], "a")
				.AddAtribute("uri", XmlTools.xa["c"])

				.OpenElement("chart", null, XmlTools.xa["c"], "c")
				.AddAtribute("id", rels.GetId(elem.Key), "r", XmlTools.xa["r"])
				.CloseElement();

				doc.CloseElement();
				doc.CloseElement();
				doc.CloseElement();

				doc.OpenPrefElement("clientData", "xdr").CloseElement();

				doc.CloseElement();
			}
		}

		internal void TryLoad()
		{
			ChartAreas = new Dictionary<string, Rectangle>();

			var file = AbsoluteToFile();
			var dir = Directory.GetDirectoryRoot(file);

			if (!System.IO.File.Exists(file))
			{
				return;
			}

			var doc = XDocument.Load(file);
			var root = doc.Root;
			var xdr = root.GetNamespaceOfPrefix("xdr");
			var a = root.GetNamespaceOfPrefix("a");			

			var areaElems = root.Elements(xdr + "twoCellAnchor");

			foreach (var areaElem in areaElems)
			{
				var areaFrom = areaElem.Element(xdr + "from");
				var areaTo = areaElem.Element(xdr + "to");
				var chartInfo = areaElem.Element(xdr + "graphicFrame")?.Element(a + "graphic")?.Element(a + "graphicData")?.Elements()?.First();
				
				Dictionary<string, string> atrs = null; //XDocumentTools.ParseAtrs
				if(chartInfo != null)
				{
					atrs = XDocumentTools.ParseAtrs(chartInfo);

					var x = areaFrom.ValueOrNull(xdr + "col").ToInt(-1);
					var y = areaFrom.ValueOrNull(xdr + "row").ToInt(-1);
					var width = areaTo.ValueOrNull(xdr + "col").ToInt(-1) - x;
					var height = areaTo.ValueOrNull(xdr + "row").ToInt(-1) - y;

					ChartAreas.Add(atrs["id"], new Rectangle(
						x,
						y,
						width,
						height
						));
				}
			}
		}

		/// <summary>
		/// Получение набора графиков связанных с drawing через рельсу
		/// </summary>
		/// <returns></returns>
		internal List<ReportChart> GetGrapth()
		{
			var res = new List<ReportChart>();
			var charts = rels.GetFiles<XmlChart>(XmlRels.RelType.Chart);
			foreach(var chart in charts)
			{
				var rc = chart.grapth;
				rc.GraphRect = ChartAreas[chart.Id];
				res.Add(rc);
			}

			return res;
		}
	}
}
