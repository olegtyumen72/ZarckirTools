﻿using SimpleExcelXML.ExcelParts;
using SimpleExcelXML.Tools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Styles
{
	internal class FillNode
	{
		public int Id { get; set; }
		public ExcelColor Fg { get; set; }
		public ExcelColor Bg { get; set; }
		public string patternType { get; set; }
		public bool isDefault { get; set; }
		public bool IsEmpty
		{
			get
			{
				return Fg.IsEmpty && Bg.IsEmpty && String.IsNullOrEmpty(patternType);
			}
		}

		public void Save(XmlWriterWrapper doc)
		{
			doc.OpenElement("fill").OpenElement("patternFill");

			if (Fg.Clr.IsEmpty && Bg.Clr.IsEmpty)
			{
				if (!String.IsNullOrEmpty(patternType))
				{
					doc.AddAtribute("patternType", patternType);
				}
				else
				{
					doc.AddAtribute("patternType", "gray125");
				}
			}
			else
			{
				doc.AddAtribute("patternType", "solid");

				if (!Fg.Clr.IsEmpty)
				{
					doc.OpenElement("fgColor").AddAtribute("rgb", Fg.Clr.ToARGB()).CloseElement();
				}

				if (!Bg.Clr.IsEmpty)
				{
					doc.OpenElement("bgColor").AddAtribute("rgb", Bg.Clr.ToARGB()).CloseElement();
				}
				else
				{
					doc.OpenElement("bgColor").AddAtribute("rgb", Color.Black.ToARGB()).CloseElement();
				}
			}

			doc.CloseElement()
			.CloseElement();
		}


		public FillNode(ReportFormatItem p, int id, ReportSheet rs = null) : this(id)
		{
			//this.Id = id;

			if (p.Background != Color.Empty)
			{
				Fg.Clr = p.Background;
			}
			else if (rs != null && rs.CellsBg != Color.Empty)
			{
				Fg.Clr = rs.CellsBg;
			}
		}

		public FillNode(int id, string patternT, Color fg, Color bg) : this(id)
		{
			Fg.Clr = fg;
			Bg.Clr = bg;
			patternType = patternT;
		}

		public FillNode(int id, int indFg, int indBg) : this(id)
		{
			Fg.Index = indFg;
			Bg.Index = indBg;
		}

		public FillNode(int id)
		{
			Id = id;
			isDefault = false;
			Fg = ExcelColor.FgElement();
			Bg = ExcelColor.BgElement();
			patternType = null;
		}

		public bool isSrc(ReportFormatItem p, ReportSheet rs = null)
		{
			if (p.Background != Color.Empty)
			{
				return p.Background == Fg.Clr;
			}
			else if (rs != null && rs.CellsBg != Color.Empty)
			{
				return Fg.Clr == rs.CellsBg;
			}

			return p.Background == Fg.Clr;
		}

		internal FillNode Update(FillNode fill)
		{
			if (fill.Fg.Clr != Color.Empty && Fg.Clr == Color.Empty)
			{
				return fill;
			}
			else if (isDefault)
			{
				return fill;
			}
			else if (Fg.Clr != Color.Empty && fill.Fg.Clr == Color.Empty)
			{
				return this;
			}
			else
			{
				return fill;
			}
		}
	}
}
