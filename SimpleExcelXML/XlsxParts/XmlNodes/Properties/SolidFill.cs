﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SimpleExcelXML.Tools;

namespace SimpleExcelXML.XlsxParts.XmlNodes.Properties
{
	/// <summary>
	/// Настройки закраски
	/// </summary>
	public class SolidFill
	{
		private string prefix = "a";
		private int lumMod = 65000;
		private int lumOff = 35000;
		public string schemeClr { get; set; } = "tx1";
		public Color schemeColor { get; set; } = Color.Empty;

		public static SolidFill DefultSolid
		{
			get
			{
				return new SolidFill("a", -1, -1, null);
			}
		}


		public SolidFill(string pref, int mod, int off, string schemeClr = null)
		{
			prefix = pref;
			lumMod = mod;
			lumOff = off;
			this.schemeClr = schemeClr;
			//this.schemeColor = main;
		}

		public SolidFill(Color clr)
		{
			schemeColor = clr;
		}

		public SolidFill(XElement xSolidFill)
		{
			TryLoad(xSolidFill);
		}

		public void TryLoad(XElement root)
		{
			var a = root.GetNamespaceOfPrefix("a");

			var colr = root.Element(a + "srgbClr");
			if(colr != null && !colr.IsFullEmpty())
			{
				var colrStr = colr.Attribute("val").Value;
				this.schemeColor = colrStr.ToColor();
			}

			var deflt = root.Element(a + "schemeClr");
			if(deflt != null && !deflt.IsFullEmpty())
			{
				schemeClr = deflt.Attribute("val").Value;

				var mod = deflt.Element(a + "lumMod");
				if (mod != null && !mod.IsFullEmpty())
					lumMod = mod.Attribute("val").Value.ToInt();

				var off = deflt.Element(a + "lumOff");
				if (off != null && !off.IsFullEmpty())
					lumOff = mod.Attribute("val").Value.ToInt();
			}			
		}

		public void Save(XmlWriterWrapper doc)
		{
			if (schemeClr != null || schemeColor != Color.Empty)
			{
				doc.OpenPrefElement("solidFill", prefix);

				if (schemeColor != Color.Empty)
				{
					doc.OpenPrefElement("srgbClr", "a").AddAtribute("val", schemeColor.ToRGB()).CloseElement();
				}
				else
				{
					doc.OpenPrefElement("schemeClr").AddAtribute("val", schemeClr);

					doc.OpenPrefElement("lumMod");

					if (lumMod == -1)
					{
						doc.AddLastAtr("0");
					}
					else
					{
						doc.AddLastAtr(lumMod.ToString());
					}

					doc.CloseElement()

						.OpenPrefElement("lumOff");

					if (lumOff == -1)
					{
						doc.AddLastAtr("0");
					}
					else
					{
						doc.AddLastAtr(lumOff.ToString());
					}

					doc.CloseElement();
					doc.CloseElement();
				}

				doc.CloseElement();
			}
			else
			{
				doc.OpenPrefElement("noFill", "a").CloseElement();
			}
		}

	}
}
