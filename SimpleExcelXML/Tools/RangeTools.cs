﻿using SimpleCore.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.Tools
{
	public static class RangeTools
	{

		public static bool IsContains(this CellRangeTmpl src, CellRangeTmpl dest)
		{
			return src.cell1_Col <= dest.cell1_Col &&
				src.cell2_Col >= dest.cell2_Col &&
				src.cell1_Row <= dest.cell1_Row &&
				src.cell2_Row >= dest.cell2_Row;
		}		

		public static bool CrossesPoints(int a, int b, int ptr)
		{
			if(a > b)
			{
				return ptr >= a && ptr <= b;
			}
			else
			{
				return ptr <= b && ptr >= a;
			}
		}

	}
}
