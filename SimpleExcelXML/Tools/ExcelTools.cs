﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcelXML.Tools
{
	public static class ExcelTools
	{

		public static string GetExcelCol(int index)
		{
			var res = "";
			var mod = 0;
			var div = index;

			while (true)
			{
				if (div < XmlTools.alphabet.Length)
					break;
				else
					index = div;

				mod = index % XmlTools.alphabet.Length;
				res += XmlTools.alphabet[mod - 1];
				div = index / XmlTools.alphabet.Length;
			}

			res += XmlTools.alphabet[div];
			return String.Join("", res.Reverse());
		}

		public static int GetNumberCol(string index)
		{
			var res = 0;
			for (int i = 0; i < index.Length; i++)
			{
				var cElem = index[i];
				var elem = cElem.ToString();
				int tmp = 0;
				if (!int.TryParse(elem, out tmp))
				{
					tmp = XmlTools.alphabet.IndexOf(cElem) + 1;
				}
				else
				{
					return res;
				}

				for (int j = 0; j < i; j++)
				{
					tmp *= 10;
				}

				res += tmp;
			}

			return res;
		}

	}
}
